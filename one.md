# One

One (1, also a *unit*) is the first positive whole [number](number.md), signifying the existence of a single unique object we're counting.

Some facts about this number include:

- It is an [odd](odd.md) number.
- It is a positive number, whole number (integer), [real number](real_number.md), [rational number](rational_number.md) and [complex number](complex_number.md).
- It is by convention NOT a [prime number](prime_number.md), though it is only divisible by 1 and itself.
- It is a multiplicative identity, i.e. *1 * x = x* for any number *x*. Also *x / 1 = x*, *x^1 = x*, *1^x = 1* for any number *x*.
- In programming there is a very common type of [bug](bug.md) called *[off by one](off_by_one.md)* in which a boundary is either incorrectly included or excluded.
- 1 is often a convenient upper bound of many intervals, e.g. when [normalizing](normlization.md) numbers or dealing with [probabilities](probability.md).
- In computing the number and digit 1, as opposed to [0](zero.md), usually means the *true* or *on* value.
- In programming operations with 1 (similarly to [0](zero.md)) are very common and may sometimes be handled as special cases to help efficiency, for example adding or subtracting one is called incrementing and decrementing and many [assembly](assembly.md) languages have special instructions for this.