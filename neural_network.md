# Neural Network

{ Not my field, learning on the go, watch out for errors! ~drummyfish }

In [artificial intelligence](ai.md) a neural network (also *neural net* or just NN) is a system simulating natural biological neural network, i.e. a biological system found in [living organisms](life.md), most importantly in our [brain](brain.md). Neural networks are just another kind of [technology](tech.md) inspired by nature's ingenuity -- they try to mimic and simulate the naturally evolved structure of systems such as brain in hopes of making computers learn and "think" like living beings do, and in recent years they started achieving just that, with great success. Neural network are related to the term [deep learning](deep_learning.md) which basically stands for training multi-layered neural networks.

Even though neural networks absolutely aren't the only possible model used in [machine learning](machine_learning.md) (see e.g. [Markov chains](markov_chain.md), [k-NN](k_nn.md), [support vector machines](svn.md), ...), they seem to be the most promising one -- nowadays neural networks are experiencing a boom and practically all AI research revolves around them; they already made their way from research to practice, not only do the play games such as [chess](chess.md) on superhuman level, they already create extremely complex art and show some kind of understanding of pictures, video, audio and text on a human level (see [chatGPT](chat_gpt.md), [stockfish](stockfish.md), stable diffusion etcetc.), and even surpass humans at specialized tasks. Most importantly of course people use this for generating [porn](porn.md), see e.g. [deepfakes](deepfake.md). The exceptional results are already being labelled "scary" due to fears of [technological singularity](tech_singularity.md), "taking jobs", possible "unethical uses" etc.

**Currently neural networks seem to be bringing back [analog](analog.md) computing.** As of 2023 most neural networks are still simulated with [digital](digital.md) computers, but due to the fact that such networks are analog and parallel in nature the digital approach is inelegant (we make digital devices out of analog circuits and then try to make them behave like analog devices again) and inefficient (in terms of energy consumption). Therefore analog is making a comeback and researchers are experimenting with analog implementations, most notably electronic (classic electronic circuits) and photonic (optics-based) ones. Keep in mind that digital and analog networks are compatible; you can for example train a network digitally and then, once you've found a satisfying network, implement it as analog so that you can e.g. put it in a cellphone so that it doesn't drain too much energy. Analog networks may of course be embedded in digital devices (we don't need to go full analog).

**[Hardware](hw.md) acceleration of neural networks is being developed.** Similarly to how [GPUs](gpu.md) appeared to accelerate computer [graphics](graphics.md) during the 90s video game boom, similar hardware is appearing for accelerating neural network computations -- these are called **[AI accelerators](ai_accelerator.md)**, notably e.g. Google's [TPU](tpu.md) (tensor processing unit). Currently GPUs are still mostly used for neural networks -- purely software networks are too slow. It is possible that future neural network hardware will be analog-based, as mentioned above.

## Details

At the highest level neural network is just a [black box](black_box.md) with *N* real number inputs and *M* real number outputs. For example we may have input values such as *age*, *height*, *weight*, *blood pressure*, and two output values, one saying the expected years to live and the other one saying the confidence of this prediction. Inside this box there is network of neurons that we can train (adjust with different learning [algorithms](algorithm.md)) so that it transforms the input values into output values in a correct way (i.e. here makes useful predictions).

Note that a traditional feed-forward neural network is just a network similar to e.g. a simple [logic circuit](logic_circuit.md), it is NOT a universal [Turing complete](turing_completeness.md) [model of computation](model_of_computation.md) like [Turing machine](turing_machine.md) or [lambda calculus](lambda_calculus.md) because it cannot for example perform loops or take arbitrarily sized input (the number of input values is fixed for given network). Neural network just takes the input numbers and in a certain fixed time (which theoretically doesn't depend on the input) runs it through the network to obtain the output numbers, i.e. it's best to view it as approximating a mathematical [function](function.md) rather than interpreting an algorithm. Of course, a neural network itself can be (and in practice is) embedded in a more complicated system that can do all the above, but in its simple form it's just a bunch of connections between inputs and outputs.

TODO

## History

TODO

## See Also

- [boolean net](boolean_net.md)