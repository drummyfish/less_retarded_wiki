# Compiler Bomb

Compiler bomb is a type of [software bomb](software_bomb.md) (similar to [fork bombs](fork_bomb.md), [zip bombs](zip_bomb.md), [tar bombs](tar_bomb.md) etc.) exploiting [compilers](compiler.md), specifically it's a short [program](program.md) (written in the compiler's [programming language](programming_language.md)) which when compiled produces an extremely large compiled program (i.e. executable binary, [bytecode](bytecode.md), [transpiled](transpilation.md) code etc.). Effectiveness of such a bomb can be measured as the size of output divided by the size of input. Of course compiler bombs usually have to be targeted at a specific compiler (its weaknesses, optimizations, inner mechanisms, ...), the target platform and so on, however some compiler bombs are quite universal as many compilers employ similar compiling strategies and produce similar outputs. Alternatively a compiler bomb can be defined to do other malicious things, like maximizing the amount of [RAM](ram.md) and time needed for compilation etc.

{ Found here: https://codegolf.stackexchange.com/questions/69189/build-a-compiler-bomb. ~drummyfish }

Compiler bombs in various languages:

- [C](c.md): `main[-1u]={1};`, creates 16 GB executable, works by defining a huge array an initializes its first element so the whole array will be explicitly stored in the executable.
- [Rust](rust.md): every program :D
- [comun](comun.md): TODO :-}
- ...