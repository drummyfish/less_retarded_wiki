# Interaction Net

Interaction nets are a way of performing computation by manipulating graphical diagrams according to a few simple rules. Interaction nets can be seen as one of many possible ways to create the lowest level basis for a [computer](computer.md), so we call them a *[model of computation](model_of_computation.md)*; other models of computation are for example [grammars](grammar.md), [Turing machines](turing_machine.md), [lambda calculus](lambda_calculus.md), [cellular automata](cellular_automaton.md) etc. -- all of these can be used to perform the same computations, though in different ways; interaction nets do this by representing a [program](program.md) with a [graph](graph.md)-like diagram which is then manipulated by some predefined rules and "reshaped" into another graph that represents the result of the performed computation. For this interaction nets can also be see as a kind of graphical [programming language](programming_language.md) (and even a textual one once we learn how to represent the graphical diagrams with text, which we call *interaction calculus*), though it's an extremely low level language (even things such as addition of natural numbers have to be programmed from scratch) that would be extremely impractical for everyday programming. The advantage of interaction nets is besides their simplicity usually said to be mainly their **[parallel nature](parallelism.md)**: as computation really means locally replacing parts of the network with predefined patterns, it is possible to simultaneously process multiple parts of the network at once, greatly speeding up the computation.

**WATCH OUT**: interaction nets are a bit confusing to newcomers because they look a bit like [logic circuits](logic_circuit.md) and so people think the computation happens by passing some data through the network -- THIS IS NOT THE CASE! The computation happens by CHANGING THE NETWORK itself, i.e. there is no data flowing, all that happens is REPLACING parts of the network with patterns defined by the rewriting rules (similarly to how in lambda calculus there are really no function calls but just text replacement). The idea is similar to that of rewriting rules in [grammars](grammar.md). Think of the connections less like of electric wires and more like of strings with knots that you tie and untie to perform the computation.

A general interaction net consists of nodes, or **agents**, that are connected with wires, or **edges**. Alongside the net exist **interaction rules** that say how patterns in the net get replaced with other patterns. A concrete definition of specific agent types and interaction rules is called an **interaction system** (i.e. *interaction net* is the general idea, like for example a *[cellular automaton](cellular_automaton.md)*, while *interaction system* is a specific realization of that idea, like for example *[game of life](game_of_life.md)*).

An **agent** is a type of block that may appear in the net. We may see it as a block with a number of ports (wires); it must have exactly one **principal port** (a kind of special, interacting port) and can have any number (even zero) of additional **auxiliary ports** (kind of "passive" ports) -- usually we just say the agent has *N* ports (where *N* >= 1) and we consider the first one to be the principal port, the other ones are auxiliary. Graphically the principal port is usually distinguished e.g. with an arrow. For example let's define three agents:

```
    ______
   |      |
   | ZERO |==>
   |______|
    ______
   |      |
---| SUCC |==>
   |______|
    ______
<==|      |
   | ADD  |---
   |      |
---|______|
```

Here we defined agents `ZERO`, with one port (only the principal one), `SUCC` (for "successor", one principal and one auxiliary port) and `ADD`, with three ports (one of them is principal).

Now let's define interaction rules with these agents. An interaction rule simply defines a pattern of interconnected agents that get replaced by another pattern; however **interaction rule may only be defined for agents both connected by their principal ports** (so called *active pairs*). Naturally, the rules also have to keep the interface (edges going in and out) of the group the same. Our interaction rules will be following:

```
rule 1:
       ______
      |      |       ______
      | ZERO |==><==|      |
      |______|      | ADD  |--- y    rewrites to            x ------------- y
                    |      |
               x ---|______|

rule 2:

       ______
      |      |       ______                                ______        ______
 x ---| SUCC |==><==|      |         rewrites to     x <==|      |      |      |
      |______|      | ADD  |--- z                         | ADD  |------| SUCC |==> z
                    |      |                              |      |      |______|
               y ---|______|                         y ---|______|
```

Now we can perform a computation of 2 + 1. We first set up the interaction net that represents our program and then we'll be applying the interaction rules as long as we can:

```
          representation of 2
   ______        ______        ______
  |      |      |      |      |      |       ______
  | ZERO |==>---| SUCC |==>---| SUCC |==><==|      |
  |______|      |______|      |______|      | ADD  |--- z
                 ______        ______       |      |
                |      |      |      |==>---|______|
                | ZERO |==>---| SUCC |
                |______|      |______|
                
                 representation of 1
               
apply rule 2:
               
   ______        ______
  |      |      |      |       ______        ______
  | ZERO |==>---| SUCC |==><==|      |      |      |
  |______|      |______|      | ADD  |------| SUCC |==> z
   ______        ______       |      |      |______|
  |      |      |      |==>---|______|
  | ZERO |==>---| SUCC |
  |______|      |______|

apply rule 2: 
                 ______
                |      |       ______        ______        ______
                | ZERO |==><==|      |      |      |      |      |
                |______|      | ADD  |------| SUCC |==>---| SUCC |==> z
   ______        ______       |      |      |______|      |______|
  |      |      |      |==>---|______|
  | ZERO |==>---| SUCC |
  |______|      |______|

apply rule 1:  
   ______        ______        ______        ______
  |      |      |      |      |      |      |      |
  | ZERO |==>---| SUCC |==>---| SUCC |==>---| SUCC |==> z
  |______|      |______|      |______|      |______|
  
           representation of 3 (the result)
           
no more rules to apply
```

One specific very important interaction system is called **"interaction combinators"** -- it is a very simple system, consisting of three predefined agents and three patterns of rewrite rules, which can simulate any other system, i.e. it is [Turing complete](turing_complete.md) (we say the model is capable of *universal computation*); we know this because it's possible to e.g. automatically convert any [lambda calculus](lambda_calculus.md) expression to interaction combinators. The following show the interaction combinator rules (`E`, `D` and `Y` are the three agents, `t` is a general subtree with possible additional inputs `{...}`):

```

erasing rule:                                            _____
        _____        _____                              |     |
  x ---|     |      |     |                        x <==|  E  |
  {...}|  t  |==><==|  E  |         rewrites to         |_____|   
  y ---|_____|      |_____|                        {...} _____
                                                        |     |
                                                   y <==|  E  |
                                                        |_____|
    
duplication rule:
                                                          _____        _____
                                                         |     |------|     |
        _____        _____                          x <==|  D  | {...}|  t  |==> z
  x ---|     |      |     |--- z                         |_____|--. .-|_____|
  {...}|  t  |==><==|  D  |         rewrites to     {...} _____    X   _____
  y ---|_____|      |_____|--- w                         |     |--' '-|     |
                                                    y <==|  D  | {...}|  t  |==> w
                                                         |_____|------|_____|

non-termination rule:

   _____                                  _____
  |     |       _____        _____       |     |                    .---.
  |  E  |==>---|     |      |     |---<==|  E  |    rewrites to    (     )
  |_____|      |  D  |==><==|  Y  |      |_____|                    '---'
           .---|_____|      |_____|---.
          (                            )
           '--------------------------'
```

TODO: text representation, compare text representation of interaction nets with grammars?

## See Also

- [rule 110](rule110.md)