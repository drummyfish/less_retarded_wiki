# Hardware

Hardware (HW), as opposed to [software](sw.md), are the physical parts of a [computer](computer.md), i.e. the circuits, the mouse, keyboard, the printer etc. Anything you can smash when the machine pisses you off.