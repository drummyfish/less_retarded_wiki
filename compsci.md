# Computer Science

Computer science, abbreviated as "compsci", is (surprise-surprise) a [science](science.md) studying [computers](computer.md). The term is pretty wide, a lot of it covers very formal and theoretical areas that neighbor and overlap with [mathematics](marh.md), such as [formal languages](formal_language.md), [cryptography](cryptography.md) and [machine learning](machine_learning.md), but also more practical/applied and "softer" disciplines such as [software_engineering](software_engineering.md), [programming](programming.md) [hardware](hardware.md), computer networks or even [user interface](ui.md) design. This science deals with such things as [algorithms](algorithm.md), [data structures](data_structure.md), [artificial intelligence](ai.md) and [information](information.md) theory. The field has become quite popular and rapidly growing after the coming of the 21st century computer/[Internet](internet.md) revolution and it has also become quite spoiled and abused by its sudden lucrativity.

## Overview

Notable fields of computer science include:

- [artificial intelligence](ai.md)
- [computer graphics](graphics.md)
- [databases](database.md)
- [hardware](hardware.md) design
- [networking](network.md)
- [security](security.md) and [cryptography](cryptography.md)
- [software engineering](software_engineering.md)
- theoretical computer science
- [user interface](ui.md)
- smaller field or subfields such as [operating systems](os.md), [compiler](compiler.md) design, formal verification, speech recognition etc.

Computer science also figures in interdisciplinary endeavors such as [bioinformatics](bioinformatics.md) and [robotics](robotics.md).

In the industry there have arisen fields of [art](art.md) and study that probably shouldn't be included in computer science itself, but are very close to it. These may include e.g. [web design](webdesign.md) (well, let's include it for the sake of completeness), [game](game.md) design, [system administration](sysadmin.md) etc.