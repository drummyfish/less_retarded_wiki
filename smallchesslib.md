# Smallchesslib

Smallchesslib (SCL) is a small [CC0](cc0.md) [LRS](lrs.md)/[suckless](suckless.md) [chess](chess.md) [library](library.md) written in [C](c.md) by [drummyfish](drummyfish.md). It is a single header library written in fewer than 4000 [lines of code](loc.md), with no [dependencies](dependency.md) and comes with a tiny engine called *smolchess*. It implements the basic chess rules as well as those of chess960, convenience functions (even primitive board picture export), handling of basic formats such as FEN and PGN, and a simple alpha-beta minimax [AI](ai.md) -- *smolchess* played on lichess where it got a rating of about 1500 (https://lichess.org/@/smolchessbot). The repository is available at https://codeberg.org/drummyfish/smallchesslib. Smallchesslib/smolchess is extremely simple and has been tested to run even on super weak platform such as [Arduboy](arduboy.md) which only has some 2 KB of RAM! That's pretty incredible, no?

```
     A B C D E F G H
  8  r:: b:: k:: n:r
  7 :: p:p  :: p::
  6  p:b n::  ::  :p
  5 ::# ::  :P  :p
  4   ::#B:: N::  ::
  3 :P  ::  :: N::
  2   :P P:B  :: P:P
  1 :R  ::  :K  :: R

white played b5c4
black to move

ply number: 27
FEN: r1b1k1nr/1pp2p2/pbn4p/4P1p1/2B1N3/P4N2/1PPB2PP/R3K2R b KQ - 1 14
board static evaluation: 0.167969 (43)
board hash: 3262264934
phase: midgame
en passant: 0
50 move rule count: 1
PGN: 1. e4 e5 2. d4 d5 3. dxe5 dxe4 4. Qxd8+ Kxd8 5. f3 exf3 6. Nxf3 Ke8 7. Nc3 Bb4 8. a3 Bc5 9. Ne4 Bb6 10. Bg5 h6 11. Bf4 g5
12. Bb5+ Nc6 13. Bd2 a6 14. Bc4*
```

*"Screenshot" of smolchess in action, playing against itself.*

Technical details: it's no [stockfish](stockfish.md), simplicity is favored over strength. Evaluation function is hand-made with 16 bit integer score (no [float](float.md)!). Board is represented as an [array](array.md) of bytes (in fact it's an array of [ASCII](ascii.md) characters that can conveniently be printed right out). AI uses basic [recursive](recursion.md) minimax and alpha-beta pruning, with quiescence handled by extending search for exchanges and checks. For simplicity there is no move ordering, transposition table, opening book or similar fanciness, so the performance isn't great, but it manages to play some intermediate moves. Xboard is supported.

While there are many high level engines focusing on maximizing playing strength, there are almost no seriously simple ones focusing on other points -- smallchesslib/smolchess tries to change this. It can be used as an educational tool, a basis for other engines, a simple engine runnable on weak devices, a helper for processing standard chess formats etc.
