# Unary

Unary generally refers to having "one of a thing". In different contexts it may specifically signify e.g.:

- **unary numeral system**: A [base](base.md) for writing numbers (just as [binary](binary.md), [decimal](decimal.md), [hexadecimal](hex.md) etc.). This base is kind of an extreme, using only one symbol (0) and has at least two possible versions:
  - The most primitive "caveman" system of recording numbers with a single symbol, recording a number simply by writing "that many symbols", e.g. using the symbol 0, one is written as 0, two as 00, three as 000 etc. Zero itself is represented by an empty string (writing nothing). Though primitive, this system is actually usable.
  - The system following rules of computers, i.e. having a fixed space, i.e. number of places, for storing a number (just as in binary we have may have e.g. 8 bits for storing a number). However since each of those places can only hold one value (the single symbol of the unary system, usually set to be 0), the system **is a [joke](jokes.md)**, because no matter how many places, we can only ever record one number -- zero. The advantage is that we can store zero even with zero places, i.e. we don't even need any memory to store the number.
- **unary function, operator etc.**: [function](function.md), [operator](operator.md) etc. that only has one parameter (e.g. [square root](sqrt.md), ...).
- ...