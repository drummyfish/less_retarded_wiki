# BBS

{ I am too young to remember this shit so I'm just writing what I've read on the web. ~drummyfish }

Bulletin board system (BBS) is, or rather used to be, a kind of [server](server.md) that hosts a community of users who connect to it via [terminal](terminal.md), who exchange messages, files, play [games](game.md) and otherwise interact -- BBSes were mainly popular before the invention of [web](www.md), i.e. from about 1978 to mid 1990s, however some still exist today. BBSes are powered by special BBS [software](software.md) and the people who run them are called sysops.

Back then people connected to BBSes via dial-up [modems](modem.md) and connecting was much more complicated than connecting to a server today: you had to literally dial the number of the BBS and you could only connect if the BBS had a free line (for zoomers: mobile phones were hardly a thing, every home had a land-line, a physical wire for phone). **Early BBSes weren't normally connected through [Internet](internet.md)** but rather through other networks like [UUCP](uucp.md) working through phone lines. I.e. a BBS would have a certain number of modems that defined how many people could connect at once. It was also expensive to make calls into other countries so BBSes were more of a local thing, people would connect to their local BBSes. Furthermore these things ran often on non-[multitasking](multitasking.md) systems like [DOS](dos.md) so allowing multiple users meant the need for having multiple computers. The boomers who used BBSes talk about great adventure and a sense of intimacy, connecting to a BBS meant the sysop would see you connecting, he might start chatting with you etc. Nowadays the few existing BBSes use protocols such as [telnet](telnet.md), nevertheless there are apparently about 20 known dial-up ones in north America. Some BBSes evolved into more modern communities based e.g. on [public access Unix](pubnix.md) systems -- for example [SDF](sdf.md).

A BBS was usually focused on a certain topic such as technology, fantasy [roleplay](rolaplay.md), dating, [warez](warez.md) etc., they would typically greet the users with a custom themed [ANSI art](ansi_art.md) welcome page upon login -- it was pretty cool. BBSes were used to share [plain text](plain_text.md) files of all sorts, be it [shareware](shareware.md) versions of games, [anarchist](anarchism.md) writings, computer manuals, poetry or recipes. It really was a HUGE thing, you can dig up a lot of fun and obscure material by searching for BBS stuff -- http://textfiles.com is one place that gathers tons and tons of plain text files that were shared on these networks; searching and downloading files was just one favorite activity and obsession of BSS users (there is a very funny text "confession" of a chronic BBS downloader called `dljunkie.txt`, look that up, it's funny as hell).

{ There's some documentary on BBS that's upposed to give you an insight into this shit, called literally *BBS: The documentary*. It's about 5 hours long tho. ~drummyfish }

Considerable part of BBS community **frowned upon anonymity** (see e.g. http://textfiles.com/law/ethics.txt), a rule of some BBSes was that you had to use your real life info like name and address to communicate with others, some even advised against using handles. You met real, non-hiding humans back then, not some anonymous furry they/thems faggot who is scared to even tell you what continent he lives on. Of course, no one probably even considered any encrypted connection back then. This show that today's [privacy](privacy.md) hysteria is a [bullshit](bullshit.md), it's sad that today you'll see the exact opposite -- sites that PROHIBIT use of real life credentials. The world is fucked up now.

The first BBS was CBBS (computerized bulletin board system) created by Ward Christensen and Randy Suess in 1978 during a blizzard storm -- it was pretty primitive, e.g. it only allowed one user to be connected at the time. The ideas evolved from those of [time sharing](time_sharing.md) computers such as those running [Unix](unix.md), BBS just tried to make them more "user friendly" and so bring in more public to where there were mostly just professionals before, kind of an ancient [Facebook](facebook.md)-like mini revolution. After publication of their invention, BBSes became quite popular and the number of them grew to many thousands -- later there was even a magazine solely focused on BBSes (*BBS Magazine*). BBSes would later group into larger networks that allowed e.g. interchange of mail. The biggest such network was [FidoNet](fidonet.md) which at its peak hosted about 35000 nodes.

{ Found some list of BBSes at http://www.synchro.net/sbbslist.html. ~drummyfish }

## See Also

- [public access Unix](pubnix.md)
- [Usenet](usenet.md)
- [modem world](modem_world.md)
- [tildeverse](tildeverse.md)
- [multi user dungeon](mud.md)
- [imageboard](imageboard.md)
- [textboard](textboard.md)
- [SDF](sdf.md)
- [FidoNet](fidonet.md)