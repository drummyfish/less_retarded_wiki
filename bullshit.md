# Bullshit

Bullshit (BS) is nonsense, arbitrary unnecessary [shit](shit.md) and/or something made up only out of necessity. Typical example are e.g. so called *bullshit [jobs](work.md)* -- jobs that are arbitrarily created just to keep people employed and don't actually serve anything else.

**Simplified example of capitalist bullshit**: under [capitalism](capitalism.md) basically the whole society is based on bullshit of many kinds, small and big, creating many bullshit clusters that are all intertwined and interact in complex ways, creating one huge bullshit. For simplicity let's consider an educational isolated bullshit cluster (that we won't see that often in reality), a hypothetical [car](car.md) factory. No matter how the factory came to be, it now ended up making cars for people so that these people can drive to work at the car factory to make more cars for other people who will work to the car factory to make cars etc. -- a bullshit cycle that exists just for its own sake, just wastes natural resources and lives of people. Of course at one point all the factory employees will own a car and the factory will have no more demand for the cars, which threatens its existence. Here capitalism employs adding more bullshit: let's say they create new bullshit jobs they call something like "[smart](smart.md) car research center" -- this will create new work position, i.e. more people who will need cars to drive to work, but MAINLY the job of these people will be adding [artificial obsolescence](artificial_obsolescence.md) to the cars, which will make them last much shorter time and regularly break so that they will need repairs using parts manufactured at the factory, creating more work that will need to be done and more bullshit jobs in the car repair department. Furthermore the team will make the cars completely dependent on [subscription](subscription.md) software, employing [consumerism](consumerism.md), i.e. the car will no longer be a "buy once" thing but rather something one has to keep feeding constantly (fuel, software subscription, insurance, repairs, cleaning, tire changes, and of course once in a few years just buying a new non-obsolete model), so that workers will still need to drive to work every day, perpetuating their need for being preoccupied with owning and [maintaining](maintenance.md) a car. This is a bullshit cluster society could just get rid of without any cost, on the contrary it would gain many free people who could do actually useful things like curing diseases, eliminating world hunger, creating art for others to enjoy. However if you tell a capitalist any part of this system is bullshit, he will defend it by its necessity in the system as a whole ("How will people get to work without cars?!", "Factories are needed for the economy!", "Economy is needed to drive manufacturing of cars!") -- in reality the bullshit clusterfuck spans the whole world to incredibly deep levels so you just can't make many people see it, especially when they're preoccupied with maintaining their own existence and just get by within it.

Some things that are bullshit include:

- anti[cheat](cheating.md)
- [antiviruses](antivirus.md)
- [army](army.md)
- [bureaucracy](bureaucracy.md)
- [capitalism](capitalism.md)
- [censorship](censorship.md)
- [clothes](clothes.md)
- [consumerism](consumerism.md)
- [countries](country.md)
- [crypto](crypto.md)
- [disclaimers](disclaimer.md)
- [DRM](drm.md)
- [economy](economy.md)
- management
- [fashion](fashion.md)
- "[game design](game_design.md)" (it's just part of programming games)
- [gender studies](gender_studies.md)
- [guns](gun.md)
- [insurance](insurance.md)
- [jobs](work.md)
- [law](law.md)
- [licenses](license.md)
- "life coaching" [lmao](lmao.md)
- [market](market.md)
- [marketing](marketing.md), ads
- [money](money.md)
- [police](police.md)
- [political correctness](political_correctness.md)
- prisons
- [privacy](privacy.md)
- [productivity](productivity_cult.md)
- [property](property.md) (including [copyright](copyright.md) etc.)
- "[right](rights_culture.md) to be forgotten" :D aka right to [delete yourself](suicide.md) aka "I can do whatever I want then everyone forgets it when I press a button", amazing [troll](trolling.md)
- [security](security.md)
- [states](state.md)
- [UML](uml.md)
- [unions](union.md)
- [wars](war.md)
- Anything justified by "economy needing it" is 100% pure bullshit.
- ...

OK then, what's not bullshit? Well, things that matter, for example [food](food.md), health, [education](education.md), [love](love.md), [fun](fun.md), [art](art.md), [technology](technology.md), knowledge about the world, [science](science.md), [morality](morality.md), exploration, ...