# Esoteric Programming Language

So called esoteric programming languages (esolangs) are highly experimental and [fun](fun.md) [programming languages](programming_language.md) that employ bizarre and/or unconventional ideas. Popular languages of this kind include [Brainfuck](brainfuck.md), [Chef](chef.md) or [Omgrofl](omgrofl.md).

There is a great [wiki](wiki.md) for esolangs, the Esolang Wiki (https://esolangs.org). If you want to behold esolangs in all their beauty, see https://esolangs.org/wiki/Hello_world_program_in_esoteric_languages_(nonalphabetic_and_A-M). The Wiki is published under [CC0](cc0.md)!

Many esolangers seem to be [code golfers](golf.md), i.e. people who do various programming challenges while aiming for the shortest code which often requires a wise choice of language... or perhaps making a completely new language just for the job :) Codegolf stack exchange is therefore one place to see many esolangs in action.

Some notable ideas employed by esolangs are:

- Using images instead of text as [source code](source_code.md) (e.g. *Piet*).
- Doing nothing (e.g. *Nothing*).
- Being two or more dimensional (e.g. *Befunge* or *Hexagony*).
- Source code resembling cooking recipes (e.g. *Chef*).
- Trying to be as hard to use as possible.
- Trying to be as hard to compile as possible (e.g. *Befunge*).
- Adding [randomness](randomness.md) to program execution (e.g. *Entropy*), or working with randomness in other ways (e.g. XD has only one command, *XD*, which always translates to random [Brainfuck](brainfuck.md) command).
- Having no [input/output](io.md) (e.g. *Compute*).
- Obligation to beg the compiler to do its job (e.g. *INTERCAL*).
- Using only white characters in source code (e.g. *Whitespace*).
- Using only a single letter in source code (e.g. *Unary*).
- Using git repository structure as source code (e.g. *legit*).
- Source code resembling dramatic plays (e.g. *Shakespeare*, actual [real-life](real_life.md) plays were performed).
- Solely focus on [golfing](golf.md), i.e. writing the shortest possible programs (e.g. *GoldScript*)
- Using [unicode](unicode.md) characters (e.g. *UniCode*).
- Being infinitely many languages (e.g. *MetaGolfScript*, each one solves a specific program in 0 bytes).
- ...

Esolangs are great because:

- **They are [fun](fun.md)** and have a cool community around them.
- **They are actually useful research in language design and food for thought**, even if most of the ideas aren't useful directly, esolangs really teach us about the borders and definitions of what languages are. And sometimes, by mistake, actual discoveries are made.
- **They are great exercise in [programming](programming.md)** and design. Simple languages that are allowed to not be useful are potentially good for education as they let the programmer fully focus on a specific idea and its implementation.
- **They blend technology with [art](art.md)**, train creativity and thinking "outside the box".
- **They are a breath of fresh air** in the sometimes too serious area of technology. Hobbyist and non-commercial programming communities are always great to have.
- ...

A famous one-man organization related to esolangs is Cat's Eye run by Chris Pressey, currently reachable at https://catseye.tc.

## History

INTERCAL, made in 1972 by Donald Woods and James Lyon, is considered the first esolang in history: its goal was specifically intended to be different from traditional languages and so for example a level of politeness was introduced -- if there weren't enough PLEASE labels in the source code, the compiler wouldn't compile the program.

In 1993 [Brainfuck](brainfuck.md), probably the most famous esolang, was created.

In 2005 esolang wiki was started.

TODO

## Specific Languages

The following is a list of some notable esoteric languages.

- **`!@$%^&*()+`**: Source code looks like gibberish.
- **[Brainfuck](brainfuck.md)**: Extremely simple but hard to program in, arguably the most famous esolang with many forks.
- **[Brainfork](brainfork.md)**: Brainfuck with added [multithreading](multithreading.md).
- **[Befunge](befunge.md)**: Two dimensional language that's extremely hard to compile.
- **[Chef](chef.md)**: Source codes look like cooking recipes.
- **Entropy**: Adds randomness to programs, data in variables decay.
- **[FALSE](false.md)**: Aims for as small compiler as possible, inspired creation of Brainfuck and other esolangs, very [minimalist](minimalism.md).
- **Gravity**: Executing programs involves solving [differential equations](differential_equation.md) related to gravity, which is [uncomputable](computability.md).
- **[INTERCAL](intercal.md)**: Maybe the first esolang, includes such statements as `PLEASE DO` which have to be present in order for the compilation to be successful.
- **Nothing**: Does nothing, guarantees zero bugs.
- **[Compute](compute.md)**: Can compute any existing problem in arbitrarily short time, but has no output so the result cannot be printed.
- **[Omgrofl](omgrofl.md)**: Source code is composed of internet acronyms such as *lol*, *wtf*, *lmao* etc.
- **Pi**: Source code looks like the number [pi](pi.md), errors encode the program.
- **[Piet](piet.md)**: Source codes are images.
- **Text**: Language that always prints its source code (it is not Turing complete). All [ASCII](ascii.md) files are programs in Text.
- **Polynomial**: Programs are [polynomials](polynomial.md) whose zeros determine the commands.
- **[Unary](unary.md)**: Source code uses only 1 character: `0`. Each program is just a sequence of zeros of different length.
- **[Velato](velato.md)**: Source codes are [MIDI](midi.md) files.
- **[Whitespace](whitespace.md)**: Source code uses only white characters (spaces, tabs and newlines) so it looks seemingly empty.
- **XENBLN**: [Golfing](golf.md) language, hello world is just `š`.
- ...

{ There used to be an esolang webring, now only accessible through archive: https://web.archive.org/web/20110728084807/http://hub.webring.org/hub/esolang. You can find nice links there. ~drummyfish }

## See Also

- [WPU](wpu.md) (weird processing unit)
- [conlang](conlang.md)
- [micronation](micronation.md)
- [NaNoGenMo](nanogenmo.md)