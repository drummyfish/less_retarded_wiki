# LRS Dictionary

WORK IN PROGRESS

{ Most of these I just heard/read somewhere, e.g. on [4chan](4chan.md), in [Jargon File](jargon_file.md) or from [RMS](rms.md), some terms I made myself. ~drummyfish }

| mainstream                                 | correct/cooler                                              |
| ------------------------------------------ | ----------------------------------------------------------- |
| Amazon                                     | Scamazon                                                    |
| American                                   | Americunt, Amerifag, Yankee                                 |
| [anime](anime.md)                          | tranime                                                     |
| [Apple](apple.md) user                     | iToddler, iDiot                                             |
| [Asperger](autism.md)                      | assburger                                                   |
| [assertiveness](assertiveness.md)          | assholism                                                   |
| average citizen                            | normie, normalfag, bloatoddler, NPC, ...                    |
| [Bill Gates](bill_gates.md)                | Bill Gayte$                                                 |
| [Blender](blender.md)                      | Blunder                                                     |
| Brave browser                              | Slave browser                                               |
| [censorship](censorship.md)                | censorshit                                                  |
| [CEO](ceo.md)                              | capitalist evil oppressor                                   |
| [cloud](cloud.md) computing                | clown computing                                             |
| [cloudflare](cloudfalre.md)                | cuckflare, clownflare, crimeflare                           |
| code of conduct ([COC](coc.md))            | code of coercion, code of censorship                        |              
| [comun](comun.md)                          | coomun { One friend suggested this :D ~drummyfish }         |
| consume                                    | consoom (see also [coom](coom.md))                          |
| [copyright](copyright.md)                  | copywrong, copyrestriction, copyrape                        |
| [CSS](css.md)                              | cascading stylish [shit](shit.md)                           |
| [C++](cpp.md)                              | crippled C                                                  |
| [Debian](debian.md)                        | Lesbian                                                     |
| [democracy](democracy.md)                  | democrazy                                                   |
| digital garden                             | digital swamp                                               |
| digital rights management ([DRM](drm.md))  | digital restrictions management                             |
| Discord                                    | Discunt                                                     |
| [economy](economy.md)                      | money religion                                              |
| [encryption](encryption.md)                | bloatcryption                                               |
| [entrepreneur](entrepreneur.md)            | murderer                                                    |
| [Facebook](facebook.md) user               | zucker, used                                                |
| fair trade                                 | fair rape                                                   |
| [feminism](feminism.md)                    | feminazism, femifascism                                     |
| [Firefox](firefox.md)                      | Furryfox                                                    |
| [gaming](game.md)                          | gayming                                                     |
| [geek](geek.md)                            | retard                                                      |
| [global warming](global_warming.md)        | global heating                                              |
| [Google](google.md)                        | Goolag                                                      |
| Gmail                                      | Gfail                                                       |
| [GNU](gnu.md)                              | GNUts, GNU's not usable, Gigantic and Nasty but Unavoidable |
| influencer                                 | manipulator                                                 |
| [Intel](intel.md)                          | [Incel](incel.md)                                           |
| [Internet Explorer](internet_explorer.md)  | Internet Exploder, Internet Exploiter                       |
| [Internet of things](iot.md)               | Internet of stinks/stings                                   |
| [iPad](ipda.md)                            | iBad                                                        |
| [iPhone](iphone.md)                        | spyPhone                                                    |
| [JavaScript](js.md)                        | HitlerScript, JavaShit                                      |
| job                                        | slavery                                                     |
| "left"                                     | [pseudoleft](pseudoleft.md), SJW                            |
| [LGBT](lgbt.md)                            | FGTS, TTTT                                                  |
| [liberal](liberal.md)                      | libtard                                                     |                            
| "[Linux](linux.md)"                        | [GNU](gnu.md), lunix, loonix                                |
| [logic gate](logic_gate.md)                | logic gayte                                                 |
| Macintosh                                  | Macintoy, Macintrash, Maggotbox                             |
| [Microsoft](microsoft.md)                  | Microshit                                                   |
| [microtransaction](microtransaction.md)    | microtheft                                                  |
| [moderation](moderation.md)                | [censorship](censorship.md)                                 |
| [modern](modern.md)                        | malicious, shitty                                           |
| [.NET](dot_net.md)                         | .NEET                                                       |
| [network](network.md)                      | notwork                                                     |
| neurodivergent                             | retarded, neuroretarded                                     |
| neurotypical                               | typical retard                                              |
| [NFS](nfs.md)                              | nightmare file system                                       |
| [Nintendo](nintendo.md)                    | Nintendont                                                  |
| NPOV                                       | normie/NPC point of view                                    |
| [NSA](nsa.md)                              | national spying agency                                      |
| null hypothesis significance test          | null ritual                                                 |
| [NVidia](nvidia.md)                        | NoVidya                                                     |
| object oriented programming ([OOP](oop.md))| object obsessed programming                                 |
| object oriented                            | objectfuscated                                              |
| [objective C](objective_c.md)              | objectionable C                                             |
| [openbsd](openbsd.md)                      | openbased                                                   |
| peer-reviewed                              | peer-censored                                               |
| "person"                                   | man                                                         |
| [playstation](playstation.md)              | gaystation                                                  |
| plug and play                              | plug and pray                                               |
| proprietary service                        | disservice                                                  |
| school                                     | indoctrination center                                       |
| "science"                                  | [soyence](soyence.md)                                       |
| software as a service ([SAAS](saas.md))    | service as a software substitute (SAASS)                    |
| [Steve Jobs](steve_jobs.md)                | Steve Jewbs                                                 |
| subscription                               | [microrape](microrape.md)                                   |
| [suckless](suckless.md)                    | suckass                                                     |
| [systemd](systemd.md)                      | shitstemd, soystemd                                         |
| [TikTok](tiktok.md)                        | ShitTok                                                     |
| [Twitter](twitter.md)                      | titter, twatter                                             |
| United States of America                   | United Shitholes of America, burgerland                     |
| user (of a proprietary system)             | used, lusr                                                  |
| using [JavaScript](js.md)                  | JavaScrippled                                               |
| voice assistant                            | personal spy agent                                          |
| [wayland](wayland.md)                      | whyland                                                     |
| [webassembly](webassembly.md)              | weebassembly                                                |
| website                                    | webshite                                                    |
| Wikipedian                                 | wikipedo                                                    |
| [Windows](windows.md)                      | Winshit, Windoze, Winbloat, Backdoors? :D                   |
| [woman](woman.md)                          | femoid                                                      |
| [work](work.md)                            | slavery                                                     |
| [world wide web](www.md)                   | world wide wait                                             |
| [YouTube](youtube.md)                      | JewTube                                                     |

Words you should use often include: abomination, crazy, cretin, [faggot](faggot.md), fat, femoid, idiot, imbecile, insane, landwhale, moron, [nigger](nigger.md), [retard](retard.md), subhuman, ugly.

## See Also

- [acronyms](acronym.md)
- [newspeak](newspeak.md)
