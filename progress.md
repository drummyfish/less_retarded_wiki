# Progress

The true definition of progress is "advancement towards more [good](good.md)", though in the [mainstream](mainstream.md) the term has been twisted to stand for falsehoods such as "[more complicated technology](capitalist_technology.md)", "[bigger economy](capitalism.md)" and so on. [Idiots](retard.md) rarely think, they can't ask a series of two questions in a row such as "what will this lead to and is the result what we want?", they can [only understand](shortcut_thinking.md) extremely simple equalities such as "moar buttons in a program = more gooder", hence the language degeneration.

It's crucial to realize that by definition the only true progress that matters is just that which gets us closer to our [ideal society](less_retarded_society.md), i.e. progress is only that which makes the life of every individual better as a whole and all other kinds of "progress", such as [technological](tech.md), [scientific](science.md), artistic, political and so on only exist SOLELY to serve the main progress of well being of individuals. A thousand year leap in technological development is worth absolutely nothing if it doesn't serve the main goal, it's useless if we can send a car to space, harvest energy of a whole star or find the absolute meaning of life if that doesn't serve the main goal -- in fact such "progress" is [nowadays](21st_century.md) mostly made so that it works AGAINST the main goal, i.e. [corporations](corporation.md) develop more complicated technology to exploit people more and make them more miserable -- that is not true progress, it is its exact opposite.

Here is a comparison of what TRUE progress is and what it isn't:

| what                                                | before                          | after                                          | is it progress?  |
| --------------------------------------------------- | ------------------------------- | ---------------------------------------------- | ---------------- |
| You see food, can you just eat it?                  | yes                             | no, you have to pay for it                     | no, the opposite |
| Can you draw the same picture as someone else?      | yes                             | no, because of [copy"right"](copyright.md)     | no, the opposite |
| Can you walk around naked?                          | yes                             | no                                             | no, the opposite |
| Can you pee wherever you want?                      | yes                             | no                                             | no, the opposite |
| Do you have to remember ten different passwords?    | no                              | yes                                            | no, the opposite |
| Can you drink river water?                          | yes                             | no, likely has toxic chemicals                 | no, the opposite |
| Do you have to check your bank account often?       | no, there are no banks          | yes                                            | no, the opposite |
| You have to do something you hate most of the day?  | yes, e.g. work on the field     | yes, e.g. work on a computer                   | no               |
| What do you do?                                     | manufacture (things)            | manufacture (machines that manufecture things) | no               |
| Are you forced to work?                             | yes, with a whip                | yes, with [law](law.md)/tech/consumerism       | no               |
| Can your society do something spectacular?          | yes, build a pyramid            | yes, fly a toy helicopter on Mars              | no               |
| You are sick, will you get treatment?               | only if you're special (noble)  | only if you're special (rich)                  | no               |
| Will you get education?                             | only if you're special (noble)  | only if you're special (rich)                  | no               |
| What do you kill each other with?                   | sticks                          | machine guns                                   | no               |
| Can you do something you enjoy most of the day?     | no                              | yes                                            | yes              |
| Can you talk to someone on the other side of Earth? | no                              | yes, thanks to [Internet](internet.md)         | yes              |
| Can you have sex with anyone you want?              | no                              | yes                                            | yes              |
| How long will you likely live?                      | not beyond 50                   | probably above 65                              | yes              |
| Can you communicate complex ideas to others?        | no (apes)                       | yes, thanks to [language](human_language.md)   | yes              |
| What do you kill each other with?                   | sticks                          | nothing                                        | yes              |
