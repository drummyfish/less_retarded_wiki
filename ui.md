# User Interface

User interface, or UI, is an interface between human and computer. Such interface interacts with some of the human senses, i.e. it can be visual (text, images), auditory (sound), touch (haptic) etc.

Remember the following inequality:

*non-interactive [CLI](cli.md) > interactive [CLI](cli.md) > [TUI](tui.md) > [GUI](gui.md)*

Some faggots make living just by designing interfaces without even knowing programming lmao. They call it "user experience" or [UX](ux.md). We call it a [bullshit](bullshit.md).