# C++

C++ (also crippled C) is an [object-obsessed](oop.md) [joke](jokes.md) language based on [C](c.md) to which it adds only [capitalist](capitalist_software.md) features and [bloat](bloat.md), most notably [object obsession](oop.md). Most good programmers such as [Richard Stallman](rms.md) and [Linus Torvalds](linus_torvalds.md) agree that C++ is hilariously messy and also tragic in that it actually succeeded to become mainstream. The language creator [Bjarne Stroustrup](stroustrup.md) himself infamously admitted the language sucks but laughs at its critics because it became successful anyway -- indeed, in a retarded society only [shit](shit.md) can succeed. As someone once said, "C++ is not an increment, it is excrement". C++ specification has **over 2000 pages** :D

C++ source code files have the extensions `.cpp` or `.cc` (for "crippled C").

## Examples

Here is our standardized **[divisor tree](divisor_tree.md)** program in C++:

```
#include <iostream> // include standard I/O library
using namespace std;

// recursive function, prints divisor tree of x
void printDivisorTree(unsigned int x)
{
  int a = -1, b = -1;
 
  for (unsigned int i = 2; i <= x / 2; ++i) // find two closest divisors
    if (x % i == 0)
    {
      a = i;
      b = x / i;

      if (b <= a)
        break;
    }

  cout << '(';
  
  if (a > 1)
  {
    printDivisorTree(a);
    cout << ' ' << x << ' ';
    printDivisorTree(b);
  }
  else
    cout << x;
    
  cout << ')';
}

int main()
{
  while (1) // main loop, read numbers from the user
  {
    unsigned int number;
    cout << "enter a number: " << flush;
    cin >> number;
    
    if (!cin.fail() && number < 1000)
    {
      printDivisorTree(number);
      cout << endl;
    }
    else
      break;
  }

  return 0;
}
```