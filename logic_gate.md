# Logic Gate

Logic gate is a basic element of [logic circuits](logic_circuit.md), a simple device that implements a [Boolean function](bool.md), i.e. it takes a number of [binary](binary.md) (1 or 0) input values and transforms them into an output binary value. Logic gates are kind of "small boxes" that eat 1s and 0s and spit out other 1s and 0s. Strictly speaking a logic gate must implement a mathematical function, so e.g. [flip-flops](flip_flop.md) don't fall under logic gates because they have an internal state/[memory](memory.md).

Logic gates are to logic circuits kind of what [resistors](resistor.md), [transistors](transistor.md) etc. are for electronic circuits. They implement basic functions that in the realm of boolean logic are equivalents of addition, multiplication etc.

Behavior of logic gates is, just as with logic circuits, commonly expressed with so called [truth tables](truth_table.md), i.e. a tables that show the gate's output for any possible combination of inputs. But it can also be written as some kind of equation etc.

There are 2 possible logic gates with one input and one output:

- **[identity](identity.md) (buffer)**: Output equals the input. This doesn't have any function from the logic perspective but can e.g. be used as a placeholder or to introduce intentional delay in the physical circuit etc.
- **[NOT](not.md)**: Negates the input (0 to 1, 1 to 0).

There are 16 possible logic gates with two inputs and one output (logic table of 4 rows can have 2^4 possible output values), however only some of them are commonly used and have their own names. These are:

- **[OR](or.md)**: Gives 1 if at least one input is 1, otherwise 0.
- **[AND](and.md)**: Gives 1 if both inputs are 1, otherwise 0.
- **[XOR](xor.md) (exclusive OR)**: Gives 1 if inputs differ, otherwise 0.
- **[NOR](nor.md)**: Negation of OR.
- **[NAND](nand.md)**: Negation of AND.
- **[XNOR](xnor.md)**: Negative XOR (equality).

The truth table of these gates is as follows:

| x | y | x OR y | x AND y | x XOR y | x NOR y | x NAND y | x XNOR y |
|---|---|--------|---------|---------|---------|----------|----------|
| 0 | 0 |   0    |   0     |   0     |   1     |    1     |    1     |
| 0 | 1 |   1    |   0     |   1     |   0     |    1     |    0     |
| 1 | 0 |   1    |   0     |   1     |   0     |    1     |    0     |
| 1 | 1 |   1    |   1     |   0     |   0     |    0     |    1     |

```
    ___             ___              _____            _____
 ---\  ''-.      ---\  ''-.      ---|     '.      ---|     '.      
     )     )---      )     )O--     |       )---     |       )O--
 ---/__..-'      ---/__..-'      ---|_____.'      ---|_____.'
     OR              NOR             AND              NAND
    ___             ___             .                .
 --\\  ''-.      --\\  ''-.         |'.              |'.
    ))     )---     ))     )O--  ---|  >---       ---|  >O--
 --//__..-'      --//__..-'         |.'              |.'
     XOR             XNOR           ' BUFFER         ' NOT

 alternatively:
     ____          ____          ____          ____
 ---|=> 1|     ---| &  |     ---|= 1 |        | 1  |
    |    |---     |    |---     |    |---  ---|    |o--
 ---|____|     ---|____|     ---|____|        |____|
     OR            AND           XOR           NOT
    
 or even:
    ___        ___        ___        ___
 --|OR |--  --|AND|--  --|XOR|--  --|NOT|--
 --|___|    --|___|    --|___|      |___|  
```

*symbols often used for logic gates*

Functions NAND and NOR are so called [functionally complete](functional_completeness.md) which means we can implement any other gate with only one of these gates. For example NOT(x) = NAND(x,x), AND(x,y) = NAND(NAND(x,y),NAND(x,y)), OR(x,y) = NAND(NAND(x,x),NAND(y,y)) etc. Similarly NOT(x) = NOR(x,x), OR(x,y) = NOR(NOR(x,y),NOR(x,y)) etc.

## See Also

- [logic circuit](logic_circuit.md)
- [quantum gate](quantum_gate.md)

