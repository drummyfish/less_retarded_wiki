# Vim

{ This is WIP, I use Vim but am not such guru really so there may appear some errors, I know this topic is pretty religious so don't eat me. ~drummyfish }

Vim (Vi Improved) is a legendary [free as in freedom](free_software.md), fairly (though not hardcore) [minimalist](minimalism.md) and [suckless](suckless.md) [terminal](terminal.md)-only (no [GUI](gui.md)) [text editor](text_editor.md) for skilled programmers and hackers, and one of the best editors you can choose for text editing and [programming](programming.md). It is a successor of a much simpler editor [vi](vi.md) that was made in 1976 and which has become a standard text editor installed on every [Unix](unix.md) system. Vim added features like tabs, [syntax highlight](syntax_highlight.md), [scriptability](script.md), screen splitting, [unicode](unicode.md) support, sessions and [plugins](plugin.md) and as such has become not just a simple text editor but  an editor that can comfortably be used for [programming](programming.md) instead of any bloated [IDE](ide.md). Observing a skilled Vim user edit text is really like watching a magician or a literal movie hacker -- the editing is extremely fast, without any use of mouse, it transcends mere text editing and for some becomes something akin a way of life.

Vim is generally known to be **"difficult to learn"** -- it is not because it is inherently difficult but rather for being very different from other editors -- it has no [GUI](gui.md) (even though it's still a screen-oriented [interactive](interactive.md) [TUI](tui.md)), it is keyboard-only and is operated via text commands rather than with a [mouse](mouse.md), it's also preferable to not even use arrow keys but rather [hjkl](hjkl.md) keys. There is even a [meme](meme.md) that says Vim is so difficult that just exiting it is a non-trivial task. People not acquainted with Vim aren't able to do it and if they accidentally open Vim they have to either Google how to close it or force kill the terminal [xD](xd.md) Of course it's not so difficult to do, it's a little bit different than in other software -- you have to press escape, then type `:q` and press enter (although depending on the situation this may not work, e.g. if you have multiple documents open and want to exit without saving you have to type `:wqa` etc.). The (sad) fact is that most [coding monkeys](coding.md) and "professional programmers" [nowadays](kids_these_days.md) choose some ugly [bloated](bloat.md) [IDE](ide.md) as their most important tool rather than investing two days into learning Vim, probably the best editor.

**Why use Vim?** Well, simply because it is (relatively) [suckless](suckless.md), universal and extremely good for editing any text and for any kind of [programming](programming.md), for many it [settles](settled.md) the search for an editor -- once you learn it you'll find it is flexible, powerful, comfortable, modifiable, lightweight... it has everything you need. Anyone who has ever invested the time to learn Vim will almost certainly tell you it was one of the best decisions he made and that guy probably only uses Vim for everything now. Many people even get used to it so much they download mods that e.g. add Vim commands and shortcuts to programs like web browsers. A great advantage is that **vi is installed on every Unix** as it is a standard utility, so if you know Vim, you can just comfortably use any Unix-like system just from the [command line](terminal.md): when you [ssh](ssh.md) into a server you can simply edit files without setting up any remote GUI or whatever. Therefore Vim is automatically a must learn skill for any sysadmin. A huge number of people also use Vim for **"[productivity](productivity_cult.md)"** -- even though we don't fancy the productivity cult and the bottleneck of programming speed usually isn't the speed of typing, it is true that **Vim makes you edit text extremely fast** (you don't move your hands between mouse and keyboard, you don't even need to touch the arrow keys, the commands and shortcuts make editing very efficient). Some nubs think you "need" a huge IDE to make big programs, that's just plain wrong, you can do anything in Vim that you can do in any other IDE, it's as good for editing tiny files as for managing a huge codebase.

Vim's biggest rival is [Emacs](emacs.md), a similar editor which is however more complex and [bloated](bloat.md) (it is [joked](jokes.md) that Emacs is really an [operating system](os.md)) -- Vim is more [suckless](suckless.md), yet not less powerful, and so it is naturally the choice of the suckless community and also [ours](lrs.md). Vim and Emacs are a subject of a **[holy war](holy_war.md)** for the the best editor yet developed; the Emacs side calls itself the *[Church of Emacs](church_of_emacs.md)*, led by [Richard Stallman](rms.md) (who created Emacs) while the Vi supporters are called members of the *[Cult of Vi](cult_of_vi.md)* (vi vi vi = 666).

It has to be noted that **Vim as a program is still kind of [bloated](bloat.md)**, large part of the [suckless](suckless.md) community acknowledges this ([cat-v](cat_v.md) lists Vim as [harmful](harmful.md), recommends [Acme](acme.md), [Sam](sam.md) or [ed](ed.md) instead). Nonetheless the important thing is that **Vim is a good [de facto standard](de_facto_standard.md)** -- the Vim's interface and philosophy is what matters the most, there are alternatives you can comfortably switch to. The situation is similar to for example "Unix as a concept", i.e. its interface, [philosophy](unix_philosophy.md) and culture, which together create a certain standardization that allows for different implementations that can be switched without much trouble. In the suckless community Vim has a similar status to [C](c.md), [Linux](linux.md) or [X11](x11.md) -- it is not ideal, by the strict standards it is a little bit bloated, however it is one of the best existing solutions and makes up for its shortcomings by being a stable, well established de-facto standard.

## How To

These are some Vim basics for getting started. There are two important editing modes in Vim:

- **insert mode**: For writing text, you just type text as normal. Pressing *ESC* enters command mode. Here **CTRL + `n`** can be used for text completion.
- **command mode**: For executing commands. Pressing *i* enters insert mode.

Some important commands in command mode are:

- **arrow keys**: Can be used for moving cursor, even though many prefer to use the *hjkl* keys. With *SHIFT* held down the cursor moves horizontally by words and by screens vertically.
- **`h`,`j`,`k`,`l`**: Cursor movement.
- **`$`**: Move cursor to end of the line.
- **`0`**: Move cursor to start of the line.
- **CTRL + `w` + `w`**: Move between windows (created with `:split` or `:vspit`).
- **CTRL + PAGEUP**, **CTRL + PAGEDOWN**: Move between tabs.
- **u**: Undo.
- **CTRL + SHIFT + `r`**: Redo.
- **`:`**: Allows entering longer commands. *TAB* can be used for completion and up/down keys for listing command history. Some of the commands are:
  - **`q`**: Quit, or close the current extra window/tab. Use **`qa`** for closing all windows/tabs and quit. **`q!`** (or **`qa!`**) quits without saving changes, **`wq`** quits with saving changes, **`wqa`** quits all saving all changes etc.
  - **`w`**: Save changes (can be followed by filename).
  - **`noh`**: Cancel highlighted text (e.g. after search).
  - **`!`**: Run command in terminal, you can e.g. compile your program this way. For running Vim commands before the terminal commands use **`vimcommands |! terminalcommands`**, e.g. `:wa |! make && ./program`.
  - **`tabedit filename`**: Opens given file in a new tab (tabs are closed with `:q`).
  - **`tabmove number`**: Move current tab to given position (`+` and `-` can be used for relative movement of tabs).
  - **`vsplit`**: Creates a new window by splitting the current one vertically.
  - **`split`**: Creates a new window by splitting the current on horizontally.
  - **`>`**: Indent to the right, well combined with text selection (`v`, `V`).
  - **`.`**: Repeat previous command.
  - **`%s/find/replace/g`**: Search and replace [regex](regex.md), [sed](sed.md) style.
  - **`help command`**: Show help about given command.
  - **`set variable value`**: Set a variable, used e.g. in configs.
- **`/pattern`**: Search for a [regex](regex.md) patter entered after `/`. Found matches will be highlighted (`:noh` cancels the highlight), you can move to the next one with `n` and to the previous one with `N`.
- **NUMBER `G`**: Go to line with given number (0 means last line).
- **`v`**, **`V`**: Select/highlight text (by characters and by lines).
- **`d`**: Delete, this is followed by a command saying what to delete, e.g. `dw` deletes to the end of the word, **`dd`** deletes the whole line. WARNING: delete actually copies the deleted text into clipboard (it behaves like *cut*)!
- **`o`**: Insert new line.
- **`p`**: Paste.
- **`y`**: Copy (yank), followed by a command saying what to copy (e.g. `yw` copies a word), **`yy`** copies the whole line.
  
Vim can be configured with a file named **`.vimrc`** in home directory. In it there is a set of commands that will automatically be run on start. Example of a simple config file follows:

```
set number " set line numbering
set et     " expand tabs
set sw=2
set hlsearch
set nowrap          " no line wrap
set colorcolumn=80  " highlight 80th column
set list
set listchars=tab:>.
set backspace=indent,eol,start
syntax on
```

## Alternatives

*See also a nice big list at http://texteditors.org/cgi-bin/wiki.pl?ViFamily.*

Of course there are alternatives to Vim that are based on different paradigms, such as [Emacs](emacs.md) (or possibly more "minimal" clones of it such as [Zile](zile.md)), its biggest rival, or plan9 editors such as [acme](acme.md) (or maybe even [ed](ed.md)). In this regard any [text editor](text_editor.md) is a potential alternative. Nevertheless people looking for Vim alternatives are usually looking for other vi-like editors. These are for example:

- **[vi](vi.md)**: While you probably won't use the original ancient vi program but rather something like [nvi](nvi.md), vi is a [POSIX](posix.md) standard for a text editor that's much simpler and universal than Vim. It lacks many features one may be used to from Vim such as tabs, [autocompletion](autocomplete.md), [syntax highligh](syntax_highlight.md) or multiple [undos](undo.mf). But limiting yourself to only using the features specified by the standard makes you more likely to be able to operate any vi-like text editor you encounter. (List of features added by Vim to vi can be found in `runtime/doc/vi_diff.txt` in Vim source tree.)
- **[neovim](neovim.md)**: Tries to be the "[modernized](modern.md)" ([refactored](refactoring.md)) fork of Vim, it removes some code, adds a new plugin system but also [bloat](bloat.md) like [CMake](cmake.md). One of its self-stated goals is to be more "community driven". It is also written in C99 (while Vim is in C89, more portable). { At least I think. ~drummyfish }
- **[vis](vis.md)**: Inspired by Vim and [Sam](sam.md), written in C99, seems to have only a few dependencies. Has no tabs. { At least I didn't find them. ~drummyfish }
- **[nvi](nvi.md)** (new vi): Vi implementation originally made for [BSD](bsd.md), much simpler than Vim (see vi above).
- **[elvis](elvis.md)**: Another vi implementation, pretty simple (see vi above).
- **[BusyBox vi](busybox.md)**: Very minimal vi implementation with very few features (see vi above), missing even window splits etc.
- **[gvim](gvim.md)**: Various versions of Vim with [GUI](gui.md) frontends made with libraries such as [GTK3](gtk3.md) or [Xaw](xaw.md). These run in a graphical window and have a menu with items you find in mainstream editors (*save*, *open*, *find* etc.). Of course you can still use this in the same way as the terminal version.