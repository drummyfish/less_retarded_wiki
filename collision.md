# Collision

Collision, sometimes also *conflict*, happens when two or more things want to occupy the same spot. This situation usually needs to be addressed somehow; then we talk about **collision resolution**. In [programming](programming.md) there are different types of collisions, for example:

- **[hash](hash.md) collision**: When two items produce the same hash, they will map to the same index in a [hash table](hash_table.md). Typical solution is to have a [list](list.md) at each table index so that multiple items can fit there.
- **collision of bodies in a [physics engine](physics_engine.md)**: See [collision_detection](collision_detection.md). These collision are resolved by separating the bodies and updating their velocities so that they "bounce off" as in [real life](irl.md).
- **request collision**: General situation in which multiple clients request access to something that can be used only by one client at a time, e.g. a communication [bus](bus.md). Resolution is usually done by some kind of [arbiter](arbiter.md) who decides, by whatever algorithm, who to grant the access to.
- **name collision**: When e.g. the same [identifier](identifier.md) is used in two separate [libraries](library.md) that are included at the same time, the [compiler](compiler.md) doesn't know which one is intended. This is addressed by [namespaces](namespace.md).
- ...