# Lambda Calculus

Lambda calculus is an extremely simple, low-level [mathematical](math.md) system that can perform computation with mathematical [functions](function.md), and can in fact be used to describe and perform any conceivable computation. Lambda calculus provides a theoretical basis for [functional programming languages](functional.md) and is a **[model of computation](model_of_computation.md)** similar to e.g. a [Turing machine](turing_machine.md) or [interaction nets](interaction_net.md) -- lambda calculus has actually exactly the same computational power as a Turing machine, which is the greatest possible computational power, and so these systems are alternatives to one another. Lambda calculus can also be viewed as a primitive [programming language](programming_language.md), however its extreme simplicity (for example the lack of even such basic concepts as [numbers](number.md)) doesn't allow its pure form to be used for practical programming, it is more of a mathematical tool for studying computers theoretically, constructing proofs etc. The system is a result of searching for most minimal systems capable of computation, just like the most essential physics equations come from searching for the most basic rules of our [Universe](universe.md). Nevertheless anything that can be programmed in any classic programming language can in theory be also programmed in lambda calculus.

While Turing machines use memory cells as the medium to carry out computation -- which closely imitates the "number crouching" of real life computers -- lambda calculus instead performs computation solely by simplifying an expression made of pure mathematical functions -- that means there are no [global variables](variable.md) or [side effects](side_effect.md) (the role of memory is essentially replaced by the expression itself, the lambda expression is both the program and its memory at the same time). It has to be stressed that the functions in question are mathematical functions, also called **pure functions**, NOT functions we know from programming (which can do all kinds of nasty stuff). A pure function cannot have any side effects such as changing global state and its result also cannot depend on any global state or randomness, the only thing a pure function can do is return a value, and this value has to always be the same if the arguments to the function are same. In addition to this the pure mathematical functions are yet much simpler than those we encounter in high school, there are no algebraic operators or numbers, just symbols.

## How It Works

(For simplicity we'll use pure ASCII text. Let the letters L, A and B signify the Greek letters lambda, alpha and beta.)

Lambda calculus is extremely simple in its definition, but it may not be so simple to learn to understand it. Most students don't get it the first time, so don't worry :)

In lambda calculus function have no names, they are what we'd call anonymous functions or lambdas in programming (now you know why they're called lambdas).

Computations in lambda calculus don't work with numbers but with sequences of symbols, i.e. the computation can be imagined as manipulating text strings with operations that can intuitively just be seen as "search/replace". If you know some programming language already, the notation of lambda calculus will seem familiar to functions you already know from programming (there are functions, their bodies, arguments, variables, ...), but BEWARE, this will also confuse you; functions in lambda calculus work a little different (much simpler) than those in traditional programming languages; e.g. you shouldn't imagine that variables and function arguments represent numbers -- they are really just "text symbols", all we're doing with lambda calculus is really manipulating text with very simple rules. Things like numbers, their addition etc. don't exist at the basic level of lambda calculus, they have to be implemented (see later). This is on purpose (feature, not a bug), lambda calculus is really trying to explore how simple we can make a system to still keep it as powerful as a Turing machine.

In lambda calculus an expression, also a **lambda term** or "program" if you will, consists only of three types of [syntactical](syntax.md) constructs:

1. *x*: **variables**, represent unknown values (of course we can use also other letters than just *x*).
2. *(Lx.T)*: **abstraction**, where *T* is a lambda term, signifies a function definition (*x* is a variable that's the function's parameter, *T* is its body).
3. *(S T)*: **application** of *S* to *T*, where *S* and *T* are lambda terms, signifies a function call/invocation (*S* is the function, *T* is the argument).

For example *(La.(Lb.x)) x* is a lambda term while *xLx..y* is not.

Brackets can be left out if there's no ambiguity. Furthermore we need to distinguish between two types of variables:

- **bound**: A variable whose name is the same as some parameter of a function this variable is in. E.g. in *(Lx.(Ly.xyz))* variables *x* and *y* are bound.
- **free**: Variable that's not bound.

Every lambda term can be broken down into the above defined three constructs. The actual computation is performed by simplifying the term with special rules until we get the result (similarly to how we simplify expression with special rules in [algebra](algebra.md)). This simplification is called a **reduction**, and there are only two rules for performing it:

1. **A-conversion**: Renames (substitutes) a bound variable inside a function, e.g. we can apply A-conversion to *Lx.xa* and convert it to *Ly.ya*. This is done in specific cases when we need to prevent a substitution from making a free variable into a bound one. 
2. **B-reduction**: Takes a body of a function and replaces a parameter inside this body with provided argument, i.e. this is used to reduce *applications*. For example *(Lx.xy) a* is an application (we apply *(Lx.xy)* to *a* ). When we apply B-reduction, we take the function body (*xy*) and replace the bound variable (*x*) with the argument (*a*), so we get *ay* as the result of the whole B-reduction here.

A function in lambda calculus can only take one argument. The result of the function, its "return value", is a "string" it leaves behind after it's been processed with the reduction rules. This means a function can also return a function (and a function can be an argument to another function), which allows us to implement functions of multiple variables with so called *[currying](currying.md)*.

For example if we want to make a function of two arguments, we instead create a function of one argument that will return another function of one argument. E.g. a function we'd traditionally write as *f(x,y,z) = xyz* can in lambda calculus be written as *(Lx.(Ly.(Lz.xyz)))*, or, without brackets, *Lx.Ly.Lz.xyz* which will sometimes be written as *Lxyz.xyz* (this is just a [syntactic sugar](syntactic_sugar.md)).

**This is all we need to implement any possible program**. For example we can encode numbers with so called Church numerals: 0 is *Lf.Lx.x*, 1 is *Lf.Lx.fx*, 2 is *Lf.Lx.f(fx)*, 3 is *Lf.Lx.f(f(fx))* etc. Then we can implement functions such as an increment: *Ln.Lf.Lx.f((nf)x)*, etc.

Let's take a complete **example**. We'll use the above shown increment function to increment the number 0 so that we get a result 1:

```
(Ln.Lf.Lx.f((nf)x) (Lf.Lx.x)     application
(Ln.Lf.Lx.f((nf)x) (Lf0.Lx0.x0)  A-conversion (rename variables)
(Lf.Lx.f(((Lf0.Lx0.x0)f)x)       B-reduction (substitution)
(Lf.Lx.f((Lx0.x0)x)              B-reduction
(Lf.Lx.fx)                       B-reduction
```

We see we've gotten the representation of number 1.

TODO: C code

## See Also

- [sigma calculus](sigma_calculus.md) (for [OOP](oop.md))
- [interaction calculus](interaction_calculus.md)