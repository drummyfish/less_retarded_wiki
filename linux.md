# Linux

*Not to be [confused](often_confused.md) with [GNU](gnu.md)/Linux.*

{ At this point it's best to start using a different kernel if you can. Consider BSD or Hurd maybe. ~drummyfish }

Linux (also Lunix or Loonix, [egoistically](egoism.md) named after its original creator [Linus Torvalds](linus_torvalds.md)) is a partially "[open-source](open_source.md)" [pseudoleftist](pseudoleft.md) [unix-like](unix_like.md) [operating system](operating_system.md) [kernel](kernel.md), probably the most successful "mostly [FOSS](foss.md)" kernel, nowadays already hijacked and milked by [capitalism](capitalism.md) and not worth using anymore. One of its greatest advantages was support of a lot of [hardware](hardware.md) (though probably owing greatly to the sneaky embedding of proprietary blobs in it); it ran besides others on [x86](x86.md), [PowerPC](ppc.md), [Arm](arm.md), had many [drivers](driver.md) and could be compiled to be quite small so as to run well even on very weak computers. **Linux is NOT an operating system**, only its basic part -- for a whole operating system more things need to be added, such as some kind of [user interface](ui.md) and actual user programs (so called [userland](userland.md)), and this is what [Linux distributions](linux_distro.md) do (there hundreds of these) -- Linux distributions, such as [Debian](debian.md), [Arch](arch.md) or [Ubuntu](ubuntu.md) are complete operating systems (but beware, most of them are not fully [FOSS](foss.md)). The mascot of the project is a penguin named [Tux](tux.md) (under some vague non-standard [license](license.md)). Linux is one of the biggest collaborative programming projects, as of now it has more than 15000 contributors. Despite popular misconceptions **Linux is [proprietary](proprietary.md) software** by containing [binary blobs](binary_blob.md) (pieces of proprietary code sneakily inserted into obscure parts of the source code) -- completely free distributions have to use [forks](fork.md) that remove these (see e.g. [Linux-libre](linux_libre.md), [Debian](debian.md)'s Linux fork etc.). Linux is also **greatly [bloated](bloat.md)** (though not anywhere near [Windows](windows.md) and such) and **[tranny software](tranny_software.md)**, abusing technology as a vehicle for promoting [liberal politics](sjw.md). While back in the day Linux was one of the coolest projects, by 2024 **Linux is absolute shit** and basically dead, it has [code of censorship](coc.md), it's been absolutely hijacked by capitalism, developed by the worst corporations and fascist political groups ([feminists](feminism.md), [LGBT](lgbt.md), ...), it is greatly overcomplicated for the sake of keeping a [bloat monopoly](bloat_monopoly.md), commercialized, full of [Rust](rust.md) code; there are already even backdoor popping in (see the 2024 XZ scandal), basically it's almost unusable now. The spirit, significance, journey and eventual fate of Linux are similar to e.g. [Wikipedia](wikipedia.md) -- initially a project of freedom later on couldn't resist the immense capitalist pressure and eventually started selling its popularity to evil entities, becoming the opposite of its past self.

[Fun](fun.md) note: there is a site that counts certain words in the Linux source code, https://www.vidarholen.net/contents/wordcount. For the lulz in 2019 some word counts were: "fuck": 16, "shit": 33, "idiot": 17, "retard": 4, "hack": 1571, "todo": 6166, "fixme": 4256.

Linux is written in the [C](c.md) language, specifically the old  C89 standard (which design-wise is very good), as of 2022 (there seem to be plans to switch to a newer version).

Linux is typically combined with a lot of **[GNU](gnu.md)** software and the [GNU](gnu.md) project (whose goal is to create a [free](free_software.md) operating system) uses Linux (actually a [fork](fork.md) of it, called [Linux-libre](linux_libre.md)) as its official kernel, so in the wild we usually encounter the term **[GNU/Linux](gnu_linux.md)** or GNU+Linux to mean a whole operating system (basically a distro), though the system should really be called just GNU. Despite this most people still call these systems just "Linux", which is completely wrong and shows their misunderstanding of technology -- GNU is the whole operating system, it existed long before Linux, Linux joined GNU later to be integrated into it. Terms like "Linux kernel" also don't make sense, Linux IS a kernel, there is no need to add the word "kernel", it's like "John human" -- no need to add the word "human" here.

Linux is sometimes called "[free as in freedom](free_software.md)", however that's a blatant lie, it is at best a partially "[open-source](open_source.md)" or "[FOSS](foss.md)" project. **Linux is in many ways bad**, especially lately. Some reasons for this are:

- It actually includes **[proprietary](proprietary.md) software** in the form of [binary blobs](blob.md) ([drivers](drivers.md)). The [Linux-libre](linux_libre.md) project tries to fix this.
- It is **[tranny software](tranny_software.md)** and has a fascist [code of conduct](coc.md) (`linux/Documentation/process/code-of-conduct.rst`). Recently it started to even **incorporate [Rust](rust.md)**, getting shitty also by the technological side. In near future Rust will become a hard dependency of Linux, that will be its final blow.
- Its development practices are [sus](sus.md), it is **involved with many [corporations](corporation.md)** (through the [linux foundation](linux_foundation.md)) including [Microsoft](microsoft.md) (one of the greatest enemies of free software) who is trying to take control over it ([EEE](eee.md)), [Google](google.md), [Intel](intel.md), [IBM](ibm.md) and others. Such forces will inevitably shape it towards corporate interests.
- It is **[bloat](bloat.md)** and [bloat monopoly](bloat_monopoly.md) and in some ways **[capitalist software](capitalist_software.md)**. It currently has **more than 10 million [lines of code](loc.md)**. Just try to [fork](fork.md) Linux on your own, maintain it and add/modify actual features.
- It uses a **restrictive [copyleft](copyleft.md)** [GPL](gpl.md) license as opposed to a permissive one.
- It is a monolithic kernel which goes against the [KISS](kiss.md) philosophy. { Or does it? At this scale probably yes tho. ~drummyfish }

Nevertheless, despite its mistakes and inevitable shitty [future](future.md) (it's just going to become "Windows 2.0" in a few years), nowadays (2023) GNU/Linux still offers a relatively comfy, powerful [Unix](unix.md)/[POSIX](posix.md) environment which means it can be drop-in replaced with another unix-like system without this causing you much trouble, so using GNU/Linux is at this point considered OK (until Microsoft completely seizes it at which point we migrate probably to [BSD](bsd.md), [GNU Hurd](hurd.md), [HyperbolaBSD](hyperbolabsd.md) or something). It can be made fairly [minimal](minimalism.md) (see e.g. [KISS Linux](kiss_linux.md) and [Puppy Linux](puppy.md)) and [LRS](lrs.md)/[suckless](suckless.md) friendly. It is in no way perfect but can serve as an acceptable temporary boat on the sail towards freedom, until it inevitably sinks by the weight of [capitalism](capitalism.md).

Linux is so called monolithic kernel (oppose to [microkernel](microkernel.md)) and as such tries to do many things at once, becoming quite [bloat](bloat.md)ed. However it "[just works](just_works.md)" and has a great [hardware](hardware.md) support so it wins many users over alternatives such as [BSD](bsd.md).

Some alternatives to Linux (and Linux-libre) and GNU/Linux are:

- [GNU Hurd](hurd.md), an unfinished (but somewhat usable) kernel developed by [GNU](gnu.md) itself.
- [BSD](bsd.md) operating systems such as [FreeBSD](freebsd.md), [NetBSD](netbsd.md) and [OpenBSD](openbsd.md) (OpenBSD probably being closest to [LRS](lrs.md))
- [bare metal](bare_metal.md) UwU
- [HyperbolaBSD](hyperbolabsd.md)
- [Minix](minix.md)? Keep checking out smaller projects like [sortix](sortix.md), e.g. on osdevwiki.
- non-Unix systems like [FreeDOS](freedos.md), [Haiku](haiku.md) (tho possibly not 100% libre?) etc.?
- [DuskOS](duskos.md) maybe?
- TODO: MOAR

## Switching To GNU/Linux

2024 UPDATE: Don't switch to it, switch to something else now.

One of the basic mistakes of [noobs](noob.md) who just switched from [Windows](windows.md) to GNU/Linux is that they try to continue to do things the *Windows way*. They try to force-run Windows programs on GNU/Linux, they look for program installers on the web, they install [antiviruses](antivirus.md), they try to find a [GUI](gui.md) program for a thing that is solved with 2 lines of [shell](shell.md) script (and fail to find one), they keep [distro hoppoing](distro_hopping.md) instead of customizing their system etc. Many give up and then go around saying "brrruh, Loooonix sux" -- yes, it kind of does, but for other reasons. You're just using it wrong. Despite its corruption, it's still a [Unix](unix.md) system, you do things elegantly and simply, however these ways are naturally completely different from how ugly systems like Windows do them -- and how they nurture normal people to do them. If you want to convert an image from *png* to *jpg*, you don't need to download and crack a graphical program that takes 100 GB and installs ads on your system, you do it via a simple [command line tool](cli.md) -- don't be afraid of the [terminal](terminal.md), learn some basic commands, ask experiences people how they do it (not how to achieve the way you want to do it). Everyone single individual who learned it later thanked himself for doing it, so don't be stupid.

TODO: more

## History

{ Some history of Linux can be read in the biography of Linus Torvalds called *Just For Fun*. ~drummyfish }

Linux was created by [Linus Torvalds](linus_torvalds.md). He started the project in 1991 as a university student. He read a book about operating system design and [Unix](unix.md) and became fascinated with it. Then when he bought a new no-name PC (4 MB RAM, 33 MHz CPU), he installed [Minix](minix.md) on it, a then-[proprietary](proprietary.md) [Unix-like](unix.md) operating system. He was frustrated about some features of Minix and started to write his own software such as terminal emulator, disk driver and [shell](shell.md), and he made it all [POSIX](posix.md) compliant. These slowly started to evolve into an OS kernel.

Linus originally wanted to name the project *Freax*, thinking *Linux* would sound too self-centered (it would). However the admin of an FTP server that hosted the files renamed it to *Linux*, and the name stuck (and it still sounds [self-centered](egoism.md)).

On 25 August 1991 { One year plus one day after I was born :D ~drummyfish } he made the famous public announcement of Linux on [Usenet](usenet.md) in which he claimed it was just a hobby project and that it "wouldn't be big and professional as [GNU](gnu.md)". In November 1991 Linux became [self-hosted](self_hosting.md) with the version 0.10 -- by the time a number of people were already using it and working on it (among them for example [Alan Cox](alan_cox.md) who would become probably the second most famous contributor after Linus himself). Back then Linus created so called Boot-Root images of Linux, something that might be seen as a precursor to [distros](distro.md). Similar "mini distros" were slowly popping up (e.g. SLS, Yggdrasil etc.).

In 1992, with version 0.12, Linux became [free software](free_software.md) with the adoption of the [GPL](gpl.md) license. By this it became the main kernel for the free operating system called [GNU](gnu.md) (started by [Richard Stallman](rms.md)) -- the system would henceforth be called GNU/Linux. Though being separate projects, GNU and Linux were (and still are) very close: GNU uses Linux as its kernel and Linux uses GNU tools (such as the [gcc](gcc.md) compiler) and software (e.g. user programs) for its distros. But indeed there is also a great amount of dissonance between the two projects (GNU is focused on ethics, tolerates absolutely no proprietary parts, uses the term "free software", while Linux accepted capitalism, it is friendly to business, tolerates "justified" proprietary parts, sometimes prefers to use the term "open source"; GNU even has to actively remove proprietary blobs from Linux to be able to use it).

In 1993 two important distributions appeared: Slackware and [Debian](debian.md). Debian would go on to become especially important: its aim was to create and maintain a completely free, non-commercial, simple to use operating system for the people (however it would not be endorsed by [GNU](gnu.md) for some disagreements about e.g. even giving an option to install proprietary software). Debian developed its own [package](package.md) system and being completely free, it would become a basis for other very popular forks such as [Ubuntu](ubuntu.md), [Mint](mint.md), [Devuan](devuan.md) and many others -- these would in turn also adopt the package system, widening and de facto standardizing its use. For this reason Debian-based distros are extremely common nowadays.

On 14 March 1994 Linux 1.0 -- a fully functional version -- was released.

TODO: moar

## See Also

- [Hurd](hurd.md)
- [GNU](gnu.md)
- [BSD](bsd.md)
- [HyperbolaBSD](hyperbolabsd.md)
- [Linux-libre](linux_libre.md)
- [Linux for niggers](linux_for_niggers.md)