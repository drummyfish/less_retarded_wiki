# Crime Against Economy

Crime against [economy](economy.md) refers to any [bullshit](bullshit.md) "crime" invented by [capitalism](capitalism.md) that is deemed to "hurt economy", the new God of society. In the current dystopian society where money has replaced God, worshiping economy is the new religion; to satisfy economy human and animal lives are sacrificed just as such sacrifices used to be made to please the gods of ancient times.

You know a system is bad when it makes it illegal to destroy or remove itself, it's like a politician who once elected makes it illegal for himself to be replaced -- by this he is absolutely clearly and undeniably declaring he aims only for his own benefit, not for the benefit of the people. If a system makes it illegal to destroy it, it firstly prevents people from putting in place a better system and secondly allows itself to do literally what it wants as nothing can any longer happen to it -- it can for example just start killing everyone for its own entertainment. Any system making it illegal to destroy it has to be destroyed out of principle, if only for this sole reason.

Examples of crimes against economy include:

- Fixing purposefully broken technology, e.g. removing [DRM](drm.md).
- Shielding oneself from marketing torture and refusing to [consume](consumerism.md), e.g. by using [adblocks](adblock.md).
- Burning money.
- Doing literally anything that could destabilize economy such as simply giving away too many things for free or for very low prices.
- Sharing useful information with other people which is called a "theft" of [intellectual property](intellectual_property.md), "[piracy](piracy.md)" etc.
- Taking basic natural resources from monstrously rich corporations who declare to own natural resources and deny access to them. E.g. printing money or physically taking goods from corporations without paying is declared a crime.
- Destroying [ads](ads.md) and so sparing other of suffering.
- Encouraging others to commit crimes against economy.
- Revealing certain truths about rich people, corporations, their products or states, so called [whistleblowing](whistleblowing.md), [antivaxxing](antivax.md) etc.
- ...