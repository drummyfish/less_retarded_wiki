# Less Retarded Software

Less [retarded](retard.md) software (LRS) is a specific kind of [software](software.md) aiming to be a truly good technology maximally benefiting and respecting its users, following the philosophy of extreme [minimalism](minimalism.md) ([Unix philosophy](unix_philosophy.md), [suckless](suckless.md), [KISS](kiss.md), ...), [anarcho pacifism](anpac.md), [communism](communism.md) and [freedom](free_software.md). The term was invented by [drummyfish](drummyfish.md).

By extension LRS can also stand for *[less retarded society](less_retarded_society.md)*, a kind of ideal society which we aim to achieve with our technology.

LRS is a set of ideas and kind of a mindset, a philosophy, though it tries to not become a traditional movement or even something akin a centrally organized group; by [anarchist](anarchism.md) principles it sees following people and groups of people as harmful, it always advocates to only follow ideas and to associate loosely. Therefore it tries to only be a concept that will remain pure, such as for example that of [free software](free_software.md), but NOT an organization, such as for example the [FSF](fsf.md), which will always become corrupt.

As a symbol of LRS we sometimes use heart ([love](love.md)), the peace symbol (pacifism, nonviolence) and A in circle ([anarchism](anarchism.md)), but these only serve as a universal identifier of the philosophy, not as a flag or anything similar -- as flags are a sign of [fascism](fascism.md) -- for this the official LRS flag is defined to be a completely transparent square which has side length of one billion light years times the [busy beaver](busy_beaver.md) function of the current 64 bit [Unix time](unix_time.md) -- this is so that the flag cannot practically be manufactured and even scaled down versions will hardly serve the purpose of a flag (only the ideal version of the flag is acceptable, i.e. that which is completely transparent and invisible). The official international LRS day is every day in the year and it always takes precedence over any other cause whose day it is supposed to be (as long as it is aligned with LRS the other cause may be acknowledged too, but only in second or lower place).

{ TODO: official currency? }

But please don't be fooled -- despite occasional [joking](jokes.md) being part of LRS, it is in indeed a **[serious effort](serious_business.md)**. It sincerely seeks increasing good in the world, just not by traditional means (which LRS usually identifies as harmful) -- that's why it may seem confusing, weird or satirical. LRS is not a company, it's not a non profit, it's not a political party, people aren't paid for it, there are no bosses, therefore correctness and seriousness gives way to sincerity and humanity. Sometimes a joke has blurry borders, sometimes the author himself can't tell if what he said is a joke or not, sometimes something sounds like a joke simply because it truthfully reflects a ridiculous state of society, sometimes something said as a joke turns out to actually be a good idea.

{ In case the previous paragraph still seemed like joke itself: yes, it IS a serious effort. ~drummyfish }

## Definition

The definition here is not strict but rather [fuzzy](fuzzy.md), it is in a form of ideas, style and common practices that together help us subjectively identify software as less retarded.

[Software](software.md) is less retarded if it adheres, to a high degree (not necessarily fully), to the following principles:

- Being made with a **[truly selfless](selflessness.md)** goal of maximally helping all living beings who may use the software without any intent of taking advantage of them in any way.
- Trying to follow the **[Unix philosophy](unix_philosophy.md)** (do one thing well, use text interfaces, ...).
- Trying to follow the **[suckless](suckless.md) philosophy** (configs as source files, distributing in source form, mods as patches, ...).
- Being **[minimalist](minimalism.md)** ([single compilation unit](single_compilation_unit.md), [header-only](header_only.md) libraries, no build systems, no [OOP](oop.md) languages, simple [version numbering](version_numbering.md), ...), [countercomplex](countercomplex.md), [KISS](kiss.md), [appropriate technology](appropriate_tech.md). Any project has to be **solo manageable** if that's at all possible.
- Minimizing **[freedom distance](freedom_distance.md)**, i.e. trying to offer freedom to as many people as possible.
- Being **[free software](free_software.md)** legally but ALSO practically (well commented, not [bloated](bloat.md) and [obscured](obscurity.md) etc., so as to truly and practically enable the freedoms to study, modify etc.). This may also include attributes such as [decentralization](decentralization.md).
- Being **[free culture](free_culture.md)**, i.e. LRS programs are free as a whole, including art assets, data etc.
- **Minimizing [dependencies](dependency.md)**, even those such as standard library or relying on OS concepts such as files or threads, even indirect ones such as build systems and even non-software ones (e.g. avoiding [floating point](float.md), GPU, 64bit etc.).
- Very **[portable](portability.md)**, hardware non-discriminating, i.e. being written in a portable language, minimizing resource usage (RAM, CPU, ...) and so on.
- Being written in a **good, [suckless](suckless.md) programming language** -- which languages are acceptable is debatable, but some of them most likely include [C](c.md) (C89 or C99), [comun](comun.md), [Forth](forth.md), [Lisp](lisp.md) (mainly [Scheme](scheme.md)), maybe even [Brainfuck](brainfuck.md), [False](false.md), [Lua](lua.md), [Smalltalk](smalltalk.md), [Pascal](pascal.md) etc. On the other hand bloated languages like [Python](python.md), [JavaScript](js.md) or [Rust](rust.md) are absolutely unacceptable.
- **[Future-proof](future_proof.md)**, **[self-contained](self_contained.md)** (just compile and run, no unnecessary config files, daemons, database services, ...), [finished](finished.md) as much as possible, not controlled by anyone (should follow from other points). This may even include attributes such as physical durability and design that maximizes the devices life.
- **[Hacking](hacking.md) friendly**, repairable and inviting to improvements and customization, highly adhering to [hacker culture](hacking.md).
- Built on top of other LRS or LRS-friendly technology such as the [C99](c.md) language, comun, Unix, our own libraries etc.
- Simple permissive licensing (being suckless legally) with great preference of **[public domain](public_domain.md)**, e.g. with [CC0](cc0.md) + patent [waivers](waiver.md).
- Elegant by its simple, well thought-through solutions. (This is to be contrasted with modern rapid development.)
- **No [bullshit](bullshit.md)** such as [codes of conduct](coc.md), furry mascots, tricky licensing conditions, [ads](marketing.md) etc.

## Further Philosophy

Here are a few bullet points giving further ideas about what LRS is about, also serving as advice for creating such technology:

- [Do one thing well](unix_philosophy.md).
- [Keep it simple](kiss.md), no [bullshit](bullshit.md). Less is more, worse is better, small is beautiful. Don't overengineer.
- Users are programmers. This means users can fiddle with their programs; instead of going to request a feature from the developer, the user can many times implement it himself thanks to simple design of the program, EVEN if the user is not an actual programmer (anyone can ctrl+F keywords and rewrite values in source code).
- Bug reports are [patches](patch.md).
- Customization is [forking](fork.md). A software tool is customized by applying personally selected patches and making personal changes to the source code (configuration is also part of source code) -- this creates a personal fork of the tool.
- Forking is good.
- Users compile their programs. Compilation is trivial and fast.
- Programs are distributed in source form.
- Be [selfless](selflessness.md), program's goal is only to help its user.
- Programs are efficient and take long time to make, they aren't consumerist products, they can't be made on schedule but they should aim to be [finished](finished.md).
- No one owns programs, no one owns [data](data.md), no one owns [art](art.md), no one owns [information](information.md) and ideas. Everything is [free](free_software.md), legally AND [in any other ways](de_facto.md).
- Use universal interfaces (text), be compatible
- No capitalist style [usercentrism](usercentrism.md): a user is NOT above programmer or any other living being (as it is in [capitalism](capitalism.md)). This means that if e.g. a feature can make user's life 1% better but will enslave additional 10 programmers with perpetual [maintenance](maintenance.md), it should NOT be added.
- Code is reusable.
- [Hacking](hacking.md) is good. Allow hacking, allow breaking and raping of your program in ways you didn't intend, do not artificially prevent anything.
- Be [portable](portability.md), respect weaker platforms and platforms of other types.
- Programs are technology (NOT brands, franchises, weapons, political grounds, social networks, work opportunities, property, platforms, ...).
- [Work](work.md) is [shit](shit.md), laziness is [good](good.md).
- [Secrets](secret.md) are bad, [encryption](encryption.md) is stupid.
- [Low level](low_level.md) is good, use only minimum necessary [abstraction](abstraction.md).
- ...

## Why

LRS exists for a number of reasons, one of the main ones is that we simply need better technology -- not better as in "having more features" but better in terms of design, purpose and ethics. Technology has to make us more free, not enslave us. Technology has to be a tool that serves us, not a device for our abuse. We believe [mainstream](capitalist_software.md) technology poses a serious, even existential threat to our civilization. We don't think we can prevent [collapse](collapse.md) or a dystopian scenario on our own, or even if these can be prevented at all, but we can help nudge the technology in a better direction, we can inspire others and perhaps make the future a little brighter, even if it's destined to be dark. Even if future seems hopeless, what better can we do than try our best to make it not so?

There are other reason for LRS as well, for example it can be very satisfying and can bring back joy of programming that's been lost in the modern toxic environment of the [capitalist](capitalism.md) mainstream. [Minimalist](minimalism.md) programming is pleasant on its own, and in many things we do we can really achieve something great because not many people are exploring this way of technology. For example there are nowadays very few programs or nice artworks that are completely [public domain](public_domain.md), which is pretty sad, but it's also an opportunity: you can be the first human to create a completely public domain software of certain kind. Software of all kind has already been written, but you can be the first one who creates a truly good version of such software so that it can e.g. be run on embedded devices. If you create something good that's public domain, you may even make some capitalist go out of business or at least lose a lot of money if he's been offering the same thing for money. You free people. That's a pretty nice feeling and makes you actually live a good life.

{ Here and there I get a nice email from someone who likes something I've created, someone who just needed a simple thing and found that I've made it, that alone is worth the effort I think. ~drummyfish. }

## Specific Software

*see also [LRS projects needed](needed.md)*

The "official" LRS programs and libraries have so far been solely developed by [drummyfish](drummyfish.md), the "founder" of LRS. These include:

- **[Anarch](anarch.md)**: Game similar to [Doom](doom.md).
- **[comun](comun.md)**: LRS [programming language](programming_language.md).
- **[raycastlib](raycastlib.md)**: Advanced 2D [raycasting](raycasting.md) rendering library.
- **[SAF](saf.md)**: Tiny library for small portable [games](game.md).
- **[small3dlib](small3dlib.md)**: Simple software rasterizer for 3D rendering.
- **[smallchesslib](smallchesslib.md)**: Simple [chess](chess.md) library and engine ([AI](ai.md)).
- **[microtd](utd.md)**: Simple [tower defense](tower_defense.md) game written with [SAF](saf.md).
- **[tinyphysicsengine](tinyphysicsengine.md)**: Very simple 3D [physics engine](physics_engine.md).
- smaller projects like [dumbchat](dumbchat.md) and [shitpress](shitpress.md)

Apart from this software a lot of other software developed by other people and groups can be considered LRS, at least to a high degree (there is usually some minor inferiority e.g. in licensing). Especially [suckless](suckless.md) software mostly fits the LRS criteria. The following programs and libraries can be considered LRS at least to some degree:

- **[brainfuck](brainfuck.md)**: Extremely simple [programming language](programming_language.md).
- **[dwm](dwm.md)**: Official [suckless](suckless.md) [window manager](wm.md).
- **[Collapse OS](collapseos.md)** and **[Dusk OS](duskos.md)**: Extremely minimalist [operating systems](operating_system.md).
- **[LIL](lil.md)**: Tiny embeddable [scripting](script.md) programming language.
- **[Lisp](lisp.md)** (mainly [Scheme](scheme.md)): Programming language with a pretty elegant design.
- **[st](st.md)**: Official [suckless](suckless.md) [terminal emulator](terminal.md).
- **[badwolf](badwolf.md)**: Very small yet very usable [web browser](browser.md).
- **[netsurf](netsurf.md)**: Nice minimalist web browser.
- **[Forth](forth.md)**: Minimalist programming language, one of the best examples of good design.
- **[surf](surf.md)**: Official [suckless](suckless.md) [web browser](browser.md).
- **[tcc](tcc.md)**: Small [C](c.md) [compiler](compiler.md) (alternative to [gcc](gcc.md)).
- **[musl](musl.md)**: Tiny [C](c.md) standard library (alternative to [glibc](glibc.md)).
- **[FALSE](false.md)**: Extremely small programming language.
- **[vim](vim.md)** (kind of): [TUI](tui.md) text/[programming](programming.md) [editor](editor.md). Vim is actually relatively big but there are smaller builds, flavors and alternatives.
- **[Simon Tatham's portable puzzle collection](stppc.md)**: Very portable collection of puzzle [games](game.md).
- ...

Other potentially LRS software to check out may include [TinyGL](tinygl.md), [bootleg3d](bootleg3d.md), [scc](scc.md), [ed](ed.md), [chibicc](chibicc.md), [IBNIZ](ibniz.md), [lynx](lynx.md), [links](links.md), [tcl](tcl.md), [uClibc](uclibc.md), [miniz](miniz.md), [Lua](lua.md), [nuklear](nuklear.md), [dmenu](dmenu.md), [sbase](sbase.md), [sic](sic.md), [tabbed](tabbed.md), [svkbd](svkbd.md), [busybox](busybox.md), [darcs](darcs.md), [raylib](raylib.md), [IRC](irc.md), [PortableGL](portablegl.md), [3dmr](3dmr.md), [openbsd](openbsd.md), [mtpaint](mtpaint.md) and others.

Another idea: **search for very old versions of "[modern](modern.md)" FOSS software**, from the times before things like [CMake](cmake.md), [Python](python.md) and [QT](qt.md) got popular (or even existed) -- many such projects got bloated with time, but their earlier versions may have been more aligned with LRS. You can get the old source code, it's present either in the git, on the project's website, on Internet Archive etc., compiling it should probably be much easier than compiling the "modern" version. This won't help with things like web browsers (as it won't understand the new formats and protocols), but will be fine text editors, drawing programs, 3D editors, games etc. You can also [fork](fork.md) the old version and make it a little better, customize it or publicly turn it into a new program, helping the whole world.

It is also possible to talk about LRS data formats, [protocols](protocol.md), standards, designs and concepts as such etc. These might include:

- **[ASCII](ascii.md)**: Text encoding.
- **[fixed point](fixed_point.md)**: Fractional number format, as opposed to [floating point](float.md).
- **[RGB332](rgb332.md)**, **[RGB565](rgb565.md)**: Simple [RGB](rgb.md) formats/palettes.
- **[bytebeat](bytebeat.md)**: Simple and powerful [procedural](procedural.md) music technique.
- **[farbfeld](farbfeld.md)**: [Suckless](suckless.md) image format.
- **[flatfile](flatfile.md)**: Using files instead of [database](database.md).
- **[rock carved binary data](rock_carved_binary_data.md)**: Way of recording binary data for ages by manually carving them into rock, plastic or similar durable material.
- **[gopher](gopher.md)**: Simple alternative to the [Web](www.md).
- **[json](json.md)**: Simple [data](data.md) text format.
- **[lambda calculus](lambda_calculus.md)**: Minimal [functional](functional.md) language.
- **[markdown](markdown.md)**: Very simple document format.
- **[ppm](ppm.md)**: Simple image format.
- **[qoi](qio.md)**: Lossless [compression](compression.md) image format in < 1000 LOC, practically as good as [png](png.md).
- **[reverse polish notation](rpn.md)** as opposed to traditional expression notation with brackets, operator precedence and other [bloat](bloat.md).
- **[set theory](set_theory.md)**: Basis of all [mathematics](math.md).
- **[textboards](textboard.md)**, **[imageboards](imageboard.md)** and pure [HTML](html.md) personal websites as opposed to [forums](forum.md) (no registration, no users, simple interface) or even [social networks](social_network.md)
- **[Turing machine](turing_machine.md)**: Minimal definition of a [computer](computer.md).
- **[txt2tags](txt2tags.md)**: Very simple document format.
- ...

Other technology than software may also be aligned with LRS principles, e.g.:

- simple and cheap [bicycle](bicycle.md) without changing gears, as opposed to e.g. a [car](car.md)
- [sundial](sundial.md), [hourglass](hourglass.md), ...
- old technology such as toys, alert and cars (e.g. the 1980s toy "See n' Say") used to play back prerecorded sounds without using any electronics or requiring batteries, using only a plastic disc that span on needle (in the same way vinyl records work)
- [knives](knife.md) are pretty less retarded
- [rocks](rock.md)
- [tangram](tangram.md), [chess](chess.md), [go](go.md), [backgammon](backgammon.md), ...
- simple flute or a home-made drum kit as musical instruments (as opposed to e.g. grand piano)
- street [football](football.md) as a cheap, simple and accessible sport (unlike for example ice hockey)
- [less retarded hardware](less_retarded_hardware.md)
- ...

## Politics/Culture And Society

*See also [less retarded society](less_retarded_society.md) and [FAQ](faq.md).*

LRS is connected to a pretty specific political beliefs, but it's not a requirement to share those beliefs to create LRS or be part of the community centered around LRS technology. We just think that it doesn't make logical sense to support LRS and not the politics that justifies it and from which it is derived, but it's up to you to verify this.

With that said, the politics behind LRS is an [idealist](idealism.md) [anarcho pacifist](anpac.md) [communism](communism.md), but NOT [pseudoleftism](pseudoleftism.md) (i.e. we do not support political correctness, [COC](coc.md)s, [cancel culture](cancel_culture.md), Marxism-Leninism etc.). In our views, goals and means we are similar e.g. to the [Venus project](venus_project.md), even though we may not agree completely on all points. We are not officially associated with any other project or community. **We [love](love.md) all [living beings](life.md)** (not just people), even those who cause us pain or hate us, we believe love is the only way towards a good society -- in this we follow similar philosophy of nonviolence that was preached by [Jesus](jesus.md) but without necessarily being religious, we simply think it is the only correct way of a mature society to behave nonviolently and lovingly towards everyone. We do NOT have any leaders or [heroes](hero_culture.md); people are imperfect and giving some more power, louder voices or greater influence creates hierarchy and goes against anarchism, therefore we only follow ideas. We aim for true social (not necessarily physical) equality of everyone, our technology helps everyone equally. **We reject [competition](competition.md) as a basis of society** and anti-equality means such as violence, [fights](fight_culture.md), bullying ([cancelling](cancel_culture.md) etc.), [censorship](censorship.md) ([political correctness](political_correctness.md) etc.), [governments](government.md) and [capitalism](capitalism.md). We support things such as [universal basic income](ubi.md) (as long as there exist [money](money.md) which we are however ultimately against), [veganism](veganism.md) and [slow movement](slow_movement.md). We highly prefer peaceful [evolution](evolution.md) to [revolution](revolution.md) as revolutions tend to be violent and have to be [fought](fight_culture.md) -- we do not intend to push any ideas by force but rather to convince enough people to a voluntary change.

## See Also

- [less retarded society](less_retarded_society.md)
- [suckless](suckless.md)
- [Venus project](venus_project.md)
- [reactionary software](reactionary_software.md)
- [KISS](kiss.md)
- [Buddhism](buddhism.md)
- [bitreich](bitreich.md)