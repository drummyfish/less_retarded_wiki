# English

*"there'dn't've"* --English

English is a natural [human language](human_language.md) spoken mainly in the [USA](usa.md), UK and Australia as well as in dozens of other countries and in all parts of the world (with about 1.5 billion speakers). It is the default language of the world nowadays. Except for the awkward relationship between written English and its pronunciation it is a pretty simple and [suckless](suckless.md) language (even though not as suckless as [Esperanto](esperanto.md)), even a braindead man can learn it { Knowing Czech and learning Spanish, which is considered one of the easier languages, I can say English is orders of magnitude simpler. ~drummyfish }. It is the lingua franca of the tech world (virtually every [programming language](programming_language.md) is based on English for example) and many other worldwide communities as well as the [Internet](internet.md). Thanks to its simplicity (lack of declension, fixed word order, relatively simple grammatical rules etc.) it is pretty suitable for computer analysis and as a basis for [programming languages](programming_language.md).

If you haven't noticed, this wiki is written in English.

## Retarded Mistakes You Make In English

TODO

- **"The reason is because ..."**: This is just awful, if you have any brain at all your ears just bleed. Correctly you say "The reason is (that) ...". Why? Here is a small helper: "Why are you not working?", "Because I'm lazy.", "What is the sound?", "The sound is noise.", "You are not working. What is the reason?", "The reason is my laziness" OR "The reason is (that) I'm lazy.".
- **"Just because X doesn't mean Y"**: this is completely grammatically wrong, it is either "Just the fact that X doesn't mean Y." OR "Just because X not(Y)." You just have to hear it, but here is an attempt at showing you why this it's wrong: "I don't have to cry just because I'm sad.", we can reverse the sentences to "Just because I'm sad I don't have to cry" OR "Just the fact that I'm sad doesn't mean I have to cry".
- **"how it looks like"**: It's either fucking "what it looks like" OR "how it looks", NOT both at once.
- **Using apostrophe for plural**, especially with [acronym](acronym.md)s, for example "VPN's" (WRONG) instead of "VPNs" (correct). With short words, e.g. "a", 99.9999999999% will use apostrophe for plural ("a's" instead of as), demonstrating their stupidity.
- **Spelling**: beginner stuff like "its" vs "it's", "they're" vs "their", "effects" vs "affects" etc.
- **Countable vs uncountable** bitch, I double dare you to ever say shit like "less mistakes".
- ...