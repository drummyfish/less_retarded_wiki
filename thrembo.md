# Thrembo

Thrembo (also hidden or forbidden number) is allegedly a "fictional" whole number lying between numbers 6 and 7, it's a subject of [jokes](jokes.md) and [conspiracy theories](conspiracy.md) with most people seeing it as a [meme](meme.md) and others promoting its existence, it is represented by a [Unicode](unicode.md) symbol U+03EB. Thrembo originated as a schizo post on [4chan](4chan.md) in 2021 by someone implying there's some kind of conspiracy to hide the existence of a number between 6 and 7, who was of course in turn advised to take his meds, however the [meme](meme.md) has already been started. Thrembo now even has its own [subreddit](reddit.md) (though it's extremely retarded, don't go there).

How can there be an integer between 6 and 7? Well, that's what thrembologists research. Sure, normally there is no space on the number line to fit a number between 6 and 7 so that its distance is 1 to both its neighbors, however this only holds due to simplifications we naively assume because of our limited [IQ](iq.md); one may for example imagine a curved, non Euclidean number line (:'D) on which this is possible, just like we can draw a triangle with three right angles on a surface of a sphere. In history we've seen naysayer proven wrong in mathematics, for example those who claimed there is no solution to the equation *x^2 = -1*; some chad just came and threw at us a new number called [i](i.md) (he sneakily made it just a letter so that he doesn't actually have to say how much it ACTUALLY equals), he just said "THIS NUMBR IS HENCEFORTH THE SOLUTION BECAUSE I SAY SO" and everyone was just forced to admit defeat because no one actually had a bigger authority than this guy. That's how real mathematics is done kids. As we see e.g. with [political correctness](political_correctness.md), with enough propaganda anything can be force-made a fact, so if the number gets enough likes on twitter, it will just BE.

Some pressing questions about thrembo remaining to be researched are following. Is thrembo even of odd? Is it a [prime](prime.md)? If such number can exist between 6 and 7, can similar numbers exist between other "mainstream" numbers?

## See Also

- [noncomputable](computability.md) numbers
- [schizophrenic number](schizo_number.md)
- [illegal number](illegal_number.md)
- [asankhyeya](asankhyeya.md)
- [42](42.md)
- [pi](pi.md)