# Libre

Libre is an alternative term for [free](free_software.md) (as in freedom). It is used to prevent confusion of *free* with *[gratis](gratis.md)*.