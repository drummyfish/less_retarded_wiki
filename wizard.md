# Wizard

Wizard is a male [virgin](virgin.md) who is at least 30 years old ([female](female.md) virgins of such age haven't been seen yet). The word is sometimes also used for a man who's just very good with [computers](computer.md). These two sets mostly overlap so it rarely needs to be distinguished which meaning we intend.

There is an [imageboard](imageboard.md) for wizards called [wizardchan](wizchan.md). It is alright but also kind of sucks, for example you can't share your [art](art.md) with others because of stupid anti-[doxxing](dox.md) rules that don't even allow to dox yourself.

## See Also

- [incel](incel.md)
- [volcel](volcel.md)