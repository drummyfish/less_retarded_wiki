# Dinosaur

In the [hacker](hacker.md) jargon *dinosaur* is a type of a big, very old, mostly non-interactive ([batch](batch.md)), possibly partly mechanical computer, usually an IBM [mainframe](mainframe.md) from 1940s and 1950s (so called Stone Age). They resided in *dinosaur pens* (mainframe rooms).

{ This is how I understood it from the [Jargon File](jargon_file.md). ~drummyfish }