# Game

Most generally game is a form of play which is restricted by certain rules, the goal of which is typically [fun](fun.md), providing challenge and/or [competition](competition.md) (and sometimes more, e.g. [education](education.md), training etc.). A game may have various combinations of mathematical/mental elements (e.g. competitive mental calculations, mathematically defined rules, ...), physical elements (based in [real life](irl.md) physics, e.g. [football](football.md), [marble racing](marble_racing.md), ...) and even other types of elements (e.g. social, psychological, ...); nowadays very popular games are [computer](computer.md) games, a type of video games (also gaymes, video gaems or vidya, e.g. [Anarch](anarch.md), [minesweeper](minesweeper.md), [Doom](doom.md), ...), which are played with the help of a computer. Other types of games are e.g. board games (such as [chess](chess.md)), various sports ([football](football.md), athletics and so on), card games (such as [poker](poker.md)), parlour games, puzzles and so on. An entity (human, computer, animal, ...) playing a game is called a player and his ability to play it well is called [skill](skill.md); however some games may involve pure [randomness](randomness.md) and chance which may limit or even eliminate the need of skill (e.g. [rock paper scissors](rock_paper_scissors.md)). *Game* is also a [mathematical](math.md) term in [game theory](game_theory.md) which studies games and competition rigorously.

A fun take at the very concept of a game is [Nomic](nomic.md), a game in which changing the game rules is part of the game. It leads to all kinds of mindfucks.

**What does a good game look like?** It is [simple](simple.md), [LRS](lrs.md) and [beautiful](beauty.md), with only a few rules, but has great depth and provides endless hours of [fun](fun.md) and challenge -- so called [easy to learn, hard to master](easy_to_learn_hard_to_master.md). From mathematical point of view the game's simple rules open up a complex world that's deep enough to keep exploring forever -- for this reason it's best if solving the game is for example [NP hard](np_hard.md) and players therefore have to look for [heuristics](heuristic.md) for playing it efficiently. A good game is [free](free_culture.md), owned by no one, belonging to the people, and lives its own life by relying on **self imposed goals** rather than "content consumption" in form of constant [updates](update_culture.md) and centralized control by some kind of "owner" (as is the case with capitalist games) -- i.e. despite having a goal, the game doesn't try to hard force the player to do something, but rather opens up a nice environment (in which the main goal is but one of many fun things to do) for player's own creativity (once the player beats the game, he may e.g. try to beat it [as fast as possible](speedrun.md), play it with some deliberate limitation, try to play it as bad as possible, combine it with other games etc.). One such nice game is possibly [racetrack](racetrack.md). For competition it's probably best if the game is symmetric, i.e. all players have (at least as much as possible) equal conditions (same weapons, same goals, ...) -- this ensures that the game always stays balanced, even when new tricks are being discovered as these can be utilized by everyone.

### Types Of Games

It's quite hard to exactly define what a game is, it is a [fuzzy](fuzzy.md) concept, and it is also hard to categorize games. Let us now define a simple classification of games by their basic nature, which will hopefully be suitable for us here:

- **[mathematical](math.md) games**: Games taking place in an abstract mathematical space, with exactly defined rules. Though mathematical games may of course be represented in real life (e.g. by physical chess pieces made of wood or a physical Rubik's cube), such a representation is only a helper for the player and doesn't rule the game out of this category. Mathematicians try to *solve* these games in various ways, e.g. by trying to construct an [algorithm](algorithm.md) for perfect play or proving that with perfect play one of the players can always secure a win.
  - **[computer](computer.md) games**: Mathematical games that practically REQUIRE a computer (and usually have been design as such) to be played due to the computations involved being very numerous and/or complex -- for example [Doom](doom.md).
  - **non-computer mathematical games**: Mathematical games that do not require a computer (though of course their computer implementations may exist) as the calculations involved can be practically performed without it -- for example [chess](chess.md).
- **[real life](irl.md) games**: Games taking place in real life, i.e. usually making use of real world physics or other laws (e.g. social ones) -- for example [football](football.md) or [marble racing](marble_racing.md).
- **hybrid games**: Various combinations of mathematical and real life games, e.g. [chess boxing](chess_boxing.md).

Furthermore many different ways of division and classifications are widely used -- for example computer games may be divided as any other software ([free](free_software.md) vs [proprietary](proprietary.md), [suckless](suckless.md) vs [bloat](bloat.md), ...), but also by many other aspect such as their genre, interface, platform etc. The following are common divisions we find usually among computer games, but often applicable to other typed of games also:

- by genre:
  - [minigames](minigame.md)
  - [shooters](shooter.md)
  - [role playing](rpg.md)
  - [tower defenses](tower_defense.md)
  - [racing](racing.md)
  - [platformers](platformer.md)
  - [strategy](strategy.md)
  - [adventures](adventure.md)
  - [sport](sport.md)
  - [sandbox](sandbox.md)
  - ...
- by game design:
  - [easy to learn, hard to master](easy_to_learn_hard_to_master.md)
  - [hard to learn, easy to master](hard_to_learn_easy_to_master.md)
  - [easy to learn, easy to master](easy_to_learn_easy_to_master.md)
  - [hard to learn, hard to master](hard_to_learn_hard_to_master.md)
  - [symmetric](symmetry.md) vs asymmetric gameplay
  - ...
- by number of players:
  - [zero player](zero_player.md)
  - [single player](single_player.md)
  - [multiplayer](multiplayer.md)
  - [massively multiplayer](mmo.md)
- by [information](information.md):
  - [complete information](complete_information.md)
  - [incomplete information](incomplete_information.md)
- by interface/graphics/world representation:
  - [2D](2d.md)
  - [3D](3d.md)
  - ["pseudo3D"/primitive3D](pseudo3d.md)
  - [command line/text](cli.md)
  - audio
  - ...
- by importance of skill:
  - purely skill based
  - involving chance
  - purely chance based
- by time management:
  - [realtime](realtime.md)
  - [turn based](turn_based.md)
- by platform
  - [real life](irl.md)
  - [computer](computer.md) ([console](console.md) vs [PC](pc.md), ...)
  - [pen and paper](pen_and_paper.md)
- by budget/scale/financing:
  - hobbyist/amateur
  - [indie](indie.md)
  - [AAA](aaa.md)
- by [business model](business_model.md):
  - [freeware](freeware.md)
  - [shareware](shareware.md)
  - [free to play](free_to_play.md)
  - [subscription](subscription.md), ["software as a service"](saas.md)
  - buy once
  - [pay to win](pay_to_win.md)
  - [pay what you want](pay_what_you_want.md)/donation
  - [adware](adware.md)
  - [spyware](spyware.md)
  - [rapeware](rapeware.md)
  - ...
- ...

## Computer Games

[Computer](computer.md) game is most commonly understood to be [software](software.md) whose main purpose is to be played and, nowadays in most cases interactively, entertain the [user](user.md); in a wider sense it may perhaps be anything we might call a game that happens to run on a computer (e.g. game theory games that serve research rather than entertainment etc.). Let us implicitly assume the former now. Sadly most such computer games are [proprietary](proprietary.md) and [toxic](toxic.md), as anything that's a subject of lucrative [business](business.md) under [capitalism](capitalism.md).

Among [suckless](kiss.md) software proponents there is a disagreement about whether games are legit software or just a [meme](meme.md) and harmful kind of entertainment. The proponents of the latter argue something along the lines that technology is there only to get real work done, that games are for losers, that they hurt MUH [PRODUCTIVITY](productivity_cult.md), are an unhealthy addiction, wasted time and effort etc. Those in support of games as legitimate software see them as a valid form of relaxation, a form of [art](art.md) that's pleasant both to make and enjoy as a finished piece, and also a way to advancing technology along the way (note we are NOT talking about consumerist games here; any consumerist art is bad). Developing games has historically led to improvements of other kinds of software, especially e.g. [3D rendering](3d_rendering.md), physics simulation and virtual reality. If games are done well, in a non-[capitalist](capitalism.md) way, then **we, [LRS](lrs.md), fully accept and support games as legitimate software**; of course as long as their purpose is to help all people, i.e. while we don't reject games as such, we reject most games the industry produces nowadays. We further argue that **in games it is acceptable to do what in real life is unethical** (even to characters controlled by other live players) and that this is in fact one of their greatest potential: to allow satisfying natural needs that were crucial in the jungle but became obsolete and harmful in advanced society, such as those for [competition](competition.md), violence, [fascism](fascism.md), [egoistic](egoism.md) behavior and others -- provided the player can tell the difference between a game and real life of course. As such, games help us build a [better society](less_retarded_society.md) in which people can satisfy even harmful needs without doing actual harm; in a game it is acceptable to torture people, roleplay as a [capitalist](capitalism.md) or even verbally bully other players in chat (who joined the server willingly knowing this is just a simulation, a roleplay), even though these things would be unacceptable to do in real life.

Despite arguments about the usefulness of games, most people agree on one thing: that the mainstream AAA games produced by big [corporations](corporation.md) (and nowadays basically just all commercial games, even the small ones, especially e.g. mobile games) are [harmful](harmful.md), [bloated](bloat.md), [toxic](toxic.md), badly made and designed to be highly malicious, consumerist products whose sole purpose is to rape the user. They are one of the worst cases of [capitalist software](capitalist_software.md) ([rapeware](rapeware.md)). Such games are never going to be considered anywhere near good from our perspective (and even the mainstream is turning towards classifying modern games as [shit](shit.md)), not even if they do some good.

PC games are mostly made for and played on [MS Windows](windows.md) which is still the "gaming OS", even though in recent years we've seen a boom of "[GNU](gnu.md)/[Linux](linux.md) gaming", possibly thanks to Windows getting shittier and shittier every year. While smallbrains see this as good, in fact it only leads to more windowization of GNU/Linux, i.e. games will just move to GNU/Linux, make it the new place of business and destroy it just as surely (indeed for example [Valve](valve.md) is already raping it, by 2023 "Linux" is already almost unusable as it became more mainstream and popular). Many normies nowadays are practicing "mobile" or console gayming which may be even worse, but really choosing between PC, consoles and phones is like choosing which kind of torture is best to endure before death. Sadly most games, even when played on [GNU](gnu.md)/Linux, are still [proprietary](proprietary.md), [capitalist](capitalist_software.md) and [bloated](bloat.md) as hell. So yeah, the world of mainstream and even mainstream indie games is one big swamp that's altogether best to be avoided.

{ If you are really so broken that you HAVE TO play proprietary games to live a meaningful life, the least harmful way for everyone is to [SOMEHOW GET YOUR HANDS ON](piracy.md) old DOS games, or maybe games for some old consoles like [gameboy](gameboy.md), [playstation](playstation.md) 1 etc., or at worst some pre 2005 Windowzee gaymes, and play them in [dosbox](dosbox.md)/[wine](wine.md) or engine recreations like [OpenMW](openmw.md) etc. Yeah it's dirty, proprietary, non-free shit, but at least you don't need a supercomputer, you won't be tortured by ads, robbed by microthefts or bullied into consuming Internet. It's best if you just use this method to slowly rid yourself of your gayming addiction to be finally free. Also make sure to absolutely NEVER pay for a proprietary game -- NO, not even an indie one. Give the money to the homeless. ~drummyfish }

We might call this the **great tragedy of games**: the industry has become similar to the industry of **drug abuse**. Games feel great and can become very addictive, especially to people not aware of the dangers (children, retards, ...). Today not playing latest games makes you left out socially, out of the loop, a weirdo. Therefore contrary to the original purpose of a game -- that of making life better and bringing joy -- an individual "on games" from the capitalist industry will crave to constantly consume more and more "experiences" that get progressively more expensive to satisfy. This situation is purposefully engineered by the big game producers who exploit psychological and sociological phenomena to enslave *gamers* and make them addicted. Games become more and more predatory and abusive and of course, there are no moral limits for corporations of how far they can go: games with [microthefts](microtransaction.md) and lootboxes, for example, are similar to gambling, and are often targeted at very young children and people prone to gambling addictions. The game industry cooperates with the hardware and software industry to together produce a consumerist hell in which one is required to constantly [update](update_culture.md) his hardware and software and to keep spending money just to stay in. The gaming addiction is so strong that even the [FOSS](foss.md) people somehow create a **mental exception** for games and somehow do not mind e.g. [proprietary](proprietary.dm) games even though they otherwise reject proprietary software. Even most of the developers of free software games can't mentally separate themselves from the concepts set in place by capitalist games, they try to subconsciously mimic the toxic attributes of such games (bloat, unreasonably realistic graphics and hardware demands, content consumerism, [cheating](cheating.md) "protection", language filters and safe spaces, ...).

Therefore it is crucial to stress that **games are [technology](tech.md) like any other**, they can be exploiting and abusive, and so indeed all the high standards we hold for other technology we must also hold for games. Too many people judge games solely by their externals, i.e. gameplay, looks and general fun they have playing them. For us at [LRS](lrs.md) gameplay is but one attribute, and not even the one of greatest importance; factors such as [software freedom](free_software.md), [cultural freedom](free_culture.md), [sucklessness](suckless.md), good internal design and being [future proof](future_proof.md) are even more important.

A small number of games nowadays come with a [free](free_software.md) engine, which is either official (often retroactively freed by its developer in case of older games) or developed by volunteers. Example of the former are the engines of ID games ([Doom](doom.md), [Quake](quake.md)), example of the latter can be [OpenMW](openmw.md) (a free engine for TES: Morrowind) or [Mangos](mangos.md) (a free server for [World of Warcraft](wow.md)). Console [emulators](emulator.md) (such as of Playstation or Gameboy) can also be considered a free engine for playing proprietary games.

Yet a smaller number of games are completely free (in the sense of [Debian](debian.md)'s free software definition), including both the engine and game assets. These games are called **free games** or **libre games** and many of them are clones of famous proprietary games. Examples of these probably (one can rarely ever be sure about legal status) include [SuperTuxKart](stk.md), [Minetest](minetest.md), [Xonotic](xonotic.md), [FLARE](flare.md) or [Anarch](anarch.md). There exists a wiki for libre games at https://libregamewiki.org and a developer forum at https://forum.freegamedev.net/. Libre games can also be found in Debian software repositories. However WATCH OUT, all mentioned repositories may be unreliable!

{ NOTE: Do not blindly trust libregamewiki and freegamedev forum, non-free games ocassionaly DO appear there by accident, negligence or even by intention. I've actually found that most of the big games like SuperTuxKart have some licensing issues (they removed one proprietary mascot from STK after my report). Ryzom has been removed after I brought up the fact that the whole server content is proprietary and secret. So if you're a purist, focus on the simpler games and confirm their freeness yourself. Anyway, LGW is a good place to start looking for libre games. It is much easier to be sure about freedom of suckless/LRS games, e.g. Anarch is legally safe practically with 100% certainty. ~drummyfish }

Some games are pretty based as they don't even require [GUI](gui.md) and are only played in the text [shell](shell.md) (either using [TUI](tui.md) or purely textual I/O) -- these are called TTY games or command line games. This kind of games may be particularly interesting to [minimalists](minimalism.md), hobbyists and developers with low ([zero](zero.md)) budget, little spare time and/or no artistic skills. Roguelike games are especially popular here; there sometimes even exist GUI frontends which is pretty neat -- this demonstrates how the [Unix philosophy](unix_philosophy.md) can be applied to games.

Another kind of cool games are computer implementations of non-computer games, for example [chess](chess.md), [backgammon](backgammon.md), [go](go.md) or various card games. Such games are very often well tested and fine-tuned gameplay-wise, popular with active communities and therefore [fun](fun.md), yet simple to program with many existing free implementations and good AIs (e.g. GNU chess, GNU go or [Stockfish](stockfish.md)). What's more, they are also many times completely [public domain](public_domain.md)!

{ There is a great lost world of nice old-style games that used to be made for old dumb phones with [Java](java.md) (J2ME) -- between about 2000 and 2010 there were tons and tons of quality Java mobile games that had e.g. entire magazines dedicated solely to them. These games are mostly lost and impossible to find, even videos of them, but if you can somehow get your hands on some of those old magazines, you're in for a great nostalgia trip. Check out e.g. *Stolen in 60 seconds*, *Alien Shooter 3D*, *Gangstar* ([GTA](gta.md) clone), *Playman World Soccer*, *Paid to Kill*, *Tibia Online*, *Ancient Empires*, *Legacy* (dungeon crawler), *Townsmen*, *Juiced 3D*, *Midtown Madness* and myriad of others. ~drummyfish }

### Games As LRS

Computer games can be [suckless](suckless.md) and just as any other software should try to adhere to the [Unix philosophy](unix_philosophy.md). A [LRS](lrs.md) game should follow all the principles that apply to any other kind of such software, for example being completely [public domain](public_domain.md) or aiming for high [portability](portability.md) and getting [finished](finished.md). This is important to mention because, sadly, many people see games as some kind of exception among software and think that different technological or moral rules apply -- this is wrong.

If you want to make a simple LRS game, there is an official LRS [C](c.md) library for this: [SAF](saf.md).

A LRS game will be similar to any other [suckless](suckless.md) program, one example of a design choice it should take is the following: while mainstream games are built around the idea of having a precompiled engine that runs [scripts](script.md) written in some interpreted language, a **LRS/suckless game wouldn't use run-time scripts** but would rather have such "scripts" written as a part of the whole game's source code (e.g. in a file `scripts.h`), in the same language as the engine (typically [C](c.md)) and they would be compiled into the binary program. This is the same principle by which suckless programs such as [dwm](dwm.md) don't use config files but rather have the configuration be part of the source code (in a file `config.h`). Doing this in a suckless program doesn't really have any disadvantages as such program is extremely easy and fast to recompile, and it brings in many advantages such as only using a single language, reducing complexity by not needing any interpreter, not having to open and read script files from the file system and also being faster.

Compared to mainstream games, a LRS game shouldn't be a consumerist product, it should be a tool to help people entertain themselves and relieve their stress. From the user perspective, the game should be focused on the fun and relaxation aspect rather than impressive visuals (i.e. photorealism etc.), i.e. it will likely utilize simple graphics and audio. Another aspect of an LRS game is that the technological part is just as important as how the game behaves on the outside (unlike mainstream games that have ugly, badly designed internals and mostly focus on rapid development and impressing the consumer with visuals).

The paradigm of LRS gamedev differs from the mainstream gamedev just as the [Unix philosophy](unix_philosophy.md) differs from the [Window philosophy](windows_philosophy.md). While a mainstream game is a monolithic piece of software, designed to allow at best some simple, controlled and limited user modifications, a LRS game is designed with [forking](fork.md), wild [hacking](hacking.md), unpredictable abuse and code reuse in mind. 

Let's take an example. A LRS game of a real-time 3D [RPG](rpg.md) genre may for example consist of several independent modules: the RPG library, the game code, the content and the [frontend](frontend.md). Yes, a mainstream game will consist of similar modules, however those modules will probably only exist for the internal organization of work and better testing, they won't be intended for real reuse or wild hacking. With the LRS RPG game it is implicitly assumed that someone else may take the 3D game and make it into a purely non-real-time [command line](cli.md) game just by replacing the frontend, in which case the rest of the code shouldn't be burdened by anything 3D-rendering related. The paradigm here should be similar to that existing in the world of computer [chess](chess.md) where there exist separate engines, graphical frontends, communication protocols, formats, software for running engine tournaments, analyzing games etc. [Roguelikes](roguelike.md) and the world of [quake](quake.md) engines show some of this modularity, though not in such a degree we would like to see -- LRS game modules may be completely separate projects and different processes communicating via text interfaces through [pipes](pipe.md), just as basic Unix tools do. We have to think about someone possibly taking our singleplayer RPG and make it into an MMORPG. Someone may even take the game and use it as a research tool for [machine learning](machine_learning.md) or as a VFX tool for making movies, and the game should be designed so as to make this as easy as possible -- the user interface should be very simple to be replaced by an [API](api.md) for computers. The game should allow easy creation of [tool assisted speedruns](tas.md), to record demos, to allow [scripting](script.md) (i.e. manipulation by external programs, traditional in-game interpreted scripting may be absent, as mentioned previously), modifying ingame variables, even creating [cheats](cheat.md) etc. And, importantly, **the game content is a module as well**, i.e. the whole RPG world, its lore and storyline is something that can be modified, forked, remixed, and the game creator should bear this in mind (see also [free universe](free_universe.md)).

Of course, LRS games must NOT contain such shit as "[anti-cheating](anti_cheat.md) technology", [DRM](drm.md) etc. For our stance on cheating, see the article [about it](cheat.md).

## Legal Matters

Thankfully gameplay mechanisms cannot (yet) be [copyrighted](copyright.md) (however some can sadly be [patented](patent.md)) so we can mostly happily [clone](clone.md) proprietary games and so free them. However this must be done carefully as there is a possibility of stepping on other mines, for example violating a [*trade dress*](trade_dress.md) (looking too similar visually) or a [trade mark](trade_mark.md) (for example you cannot use the word *tetris* as it's owned by some shitty company) and also said patents (for example the concept of minigames on loading screens used to be patented in the past).

Trademarks have been known to cause problems in the realm of libre games, for example in the case of Nexuiz which had to rename to [Xonotic](xonotic.md) after its original creator trademarked the name and started to make trouble.

**Advice on [cloning](clone.md) games**: copy only the gameplay mechanics, otherwise make it original and very different from the cloned game or else you're threading the fine legal lines. See this as an opportunity to add something new, something that's yours, and potentially to apply and exploit [minimalism](minimalism.md), i.e. if you're going to clone Doom, do not make a game about shooting demons from hell that's called Gnoom -- just take the gameplay and do something new, e.g. why not try to make it a mix of sci-fi and fantasy with procedurally generated levels which will additionally save you a lot of time on level design?

## Nice And Notable Gaymes

Of [proprietary](proprietary.md) video games we should mention especially those that to us have [clonning](clone.md) potential. [Doom](doom.md) (possibly also [Wolfenstein 3d](wolf3d.md)) and other 90s shooters such as [Duke Nukem 3D](duke3d.md), Shadow Warrior and [Blood](blood.md) (the great 90s [boomer shooters](boomer_shooter.md)) were excellent. [Trackmania](trackmania.md) is a very interesting racing game like no other, based on kind of [speedrunning](speedrun.md), [easy to learn, hard to master](easy_to_learn_hard_to_master.md), very entertaining even solo. The Witness was a pretty rare case of a good newer game, set on a strange island with puzzles the player learns purely by observation. [The Elder Scrolls](tes.md) (mainly Morrowind, Obvlidion and Skyrim) are very captivating [RPG](rpg.md) games like no other, with extreme emphasis on [freedom](freedom.md) and lore; [Pokemon](pokemon.md) games on [GBC](gbc.md) and [GBA](gba.md) were similar in this while being actually pretty tiny games on small old handhelds. [GTA](gta.md) games also offered a great open world freedom and fun based on violence, sandbox world and great gangster-themed story. Advance Wars was a great turn based strategy on [GBA](gba.md) (and possibly one of the best games on that console), kind of glorified [chess](chess.md) with amazing pixel art graphics. Warcraft III was possibly the best real time strategy game with awesome aesthetics. Its successor, [World of Warcraft](wow.md), is probably the most notable [MMORPG](mmorpg.md) with the same lovely aesthetics and amazing feel that would be worth bringing over to the free world (even if just in 2D or only [text](mud.md)). [Diablo](diablo.md) (one and two) were a bit similar to WoW but limited to singleplayer and a few man multiplayer; there exists a nice libre Diablo clone called [Flare](flare.md) now. Legend of Grimrock (one and two) is another rare case of actually good new take on an old concept of [dungeon crawlers](dungeon_crawler.md). Half Life games are also notable especially for their atmosphere, storyline and lore. [Postal](postal.md) 2 was an excellent game. [Minecraft](minecraft.md) was another greatly influential game that spawned basically a new genre, though we have now basically a perfect clone called [Minetest](minetest.md) (but we still DO [need](needed.md) a non-bloated clone). [Dwarf Fortress](dwarf_fortress.md) is also worth mentioning as the "most complex simulation ever made" -- it would be nice to have a free clone. TODO: more.

**[Gamebooks](gamebook.md)** -- books that require the reader to participate in the story and make choices executed by jumping to different pages based on given choice -- are worthy of mention as an interesting combination of a [book](book.md) and a game, something similar to computer adventure games -- in gamebooks lies a great potential for creating nice LRS games.

As for the [free (as in freedom)](free_software.md) libre games, let the following be a sum up of some nice games that are somewhat close to [LRS](lrs.md), at least by some considerations.

**Computer games:** [Anarch](anarch.md) and [microTD](utd.md) are examples of games trying to closely follow the [less retarded](lrs.md) principles while still being what we would normally call a computer game. [SAF](saf.md) is a less retarded game library/fantasy console which comes with some less retarded games such as [microTD](utd.md). If you want something closer to the mainstream while caring about freedom, you probably want to check out libre games (but keep in mind they are typically not so LRS and do suck in many ways). Some of the highest quality among them are [Xonotic](xonotic.md), 0 A.D., [openarena](openarena.md), [Freedoom](Freedoom.md), Neverball, SupertuxKart, [Minetest](minetest.md), The Battle for Wesnoth, Blackvoxel, [Lix](lix.md) etcetc. -- these are usually quite [bloated](bloat.md) though.

As for **non-computer games**: these are usually closer to LRS than any computer game. Many old board games are awesome, including [chess](chess.md), [go](go.md), [shogi](shogi.md), [xiangqi](xiangqi.md), [backgammon](backgammon.md), [checkers](cheskers.md) etc. [Gamebooks](game_book.md) can be very LRS -- they can be implemented both as computer games and non-computer physical books, and can further be well combined with creating a [free universe](free_universe.md). Some card games also, TODO: which ones? :) Some games traditionally played on computers, such as [sokoban](sokoban.md), can also be played without a computer. Pen and pencil games that are amazing include [racetrack](racetrack.md), pen and pencil football etc. Nice real life physics games include [football](football.md), [marble racing](marble_racing.md) etc.

## See Also

- [minigame](minigame.md)
- [demake](demake.md)
- [game engine](game_engine.md)
- [brain software](brain_software.md)
- [open console](open_console.md)
- [fantasy console](fantasy_console.md)
- [SAF](saf.md)
- [chess](chess.md)
- [gamebook](gamebook.md)
- [tangram](tangram.md)
- [game of life](game_of_life.md)
- [minesweeper](minesweeper.md)