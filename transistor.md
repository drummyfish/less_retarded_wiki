# Transistor

Transistor is a small [semiconductor](semiconductor.md) element of [electronic](electronics.md) circuits that can be used as an amplifier or a switch, and which is a basic building block of [digital](digital.md) electronic [computers](computer.md), integrated circuits and many other electronic devices. Transistors replaced [vacuum tubes](vacuum_tube.md) and [relays](relay.md) which were used in primitive computers of the 20th century; transistors can be made much smaller, cheaper, more reliable and, unlike relays, operated purely electronically and therefore much faster. Transistor has become the most manufactured device in history.

Transistor generally has three terminals. Its key principle is that of behaving like an electronically operated amplifier or switch: we can make a transistor *open* or *close* (i.e. conduct or not conduct electricity) by applying different voltage or current (and we can also make it something between *open* and *close*). The voltage/current by which we control the transistor can be lower than that which we control, so we can see this as an amplifier: we can control high current with low current, i.e. the high current follows the low current but has higher amplitude. We can also see this as a switch: by applying voltage/current we can make a wire connect (low resistivity) or disconnect (high resistivity) similarly to a physical switch. This switch behavior is important for computers because we can exploit it to implement [binary](binary.md) (on/off) [logic circuits](logic_gate.md).

A basic division of transistors is following:

- **bipolar junction transistor (BJT)**: They have 3 terminals: **emitter**, **base** and **collector**. By applying **current** in the base we control the current between emitter and collector (this current can be greater than the control current). BJTs use both kinds of carriers at once, electrons and holes. BJTs are older than FETs, not much used in integrated circuits now.
  - **PNP**: There is P semiconductor (emitter), N (base) and then P again (collector), creating 2 NP junctions. It opens when there is no current at base.
  - **NPN**: The other way around that PNP (N, then P, then N). It opens when there is current at base.
- **field effect transistor (FET)**: They have 3 terminals: **source**, **gate** and **drain**. By applying **voltage** to gate we control the current between source and drain. They use only one kind of charge carriers (electrons or holes). This is due to a different internal structure from BJTs, e.g. by having gate separated from source and drain with a metal oxide layer in MOSFET. A voltage applied here kind of "pushes" the holes or electrons out of the way to create a channel between source and drain. The transistor can either be in enhancement mode (high voltage opens the transistor) or depletion mode (low voltage opens the transistor). FETs are newer and have some nicer properties, e.g. lower noise or lower power consumption: they **only consume power on state change**.
  - **P channel**: Source and drain are made of P semiconductor put into an N semiconductor.
  - **N channel**: Source and drain are made of N semiconductor put into a P semiconductor. They have a bit different properties from P channel FETs.

Commonly used graphical symbols for transistor are (usually in a circle):
  
```
        E            E           D           D
        |            |           |           |
B___|.-'     B___|.-'     G   |--'    G   |--'
    |'>.         |'<.     -->-|--.    --<-|--.
        |            |           |           |
        C            C           S           S

BJT (NPN)    BJT (PNP)    FET (N)     FET (P)        
```
  
First FET transistors were JFETs (junction-gate FET) but by today were mostly replaced by **MOSFETs** (metal-oxide-semiconductor FET), a transistor using a metal oxide layer for separating the gate terminal which gives it some nice properties over JFET. These transistors are used to implement [logic gates](logic_gate.md) e.g. using the **[CMOS](cmos.md)** fabrication process which uses complementary pairs of P and N channel FETs so that e.g. one is always off which decreases power consumption.
  
