# Multi User Dungeon

{ WIP, researching. ~drummyfish }


**Are there any free MUD codebases written in good languages?** No. You have to write your own, it seems like MUD faggots don't wanna free their code, and if they do they write it in JavaScript++. Many times it's also because the old code (written in nice languages such as C) started to be created a long time ago when licenses weren't yet such a big thing or didn't exist at all. Some source available codebases are e.g. DikuMUD (written in C, marked LGPL on GitHub, however the code may be PROPRIETARY, their site explicitly mentions one of the original authors was unreachable during relicensing) and SMAUG (written in C, marked GPL on GitHub, however the code is most likely PROPRIETARY as it's a long chain of derivatives from some obscure weird licensed source).