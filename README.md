# Less Retarded Wiki

This is online at http://www.tastyfish.cz/lrs/main.html.

Wiki about less retarded software and related topics.

By contributing you agree to release your contribution under CC0 1.0, public domain (https://creativecommons.org/publicdomain/zero/1.0/). Please do **not** add anything copyrighted to this Wiki (such as copy pasted texts from elsewhere, images etc.).

Start reading at the [main page](main.md).
