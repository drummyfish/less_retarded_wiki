# Julia Set

Julia sets (named after mathematician *Gaston Julia*) are [sets](set.md) of 2D points that are very similar to [Mandelbrot set](mandelbrot_set.md) and just as Mandelbrot set they typically (but not always) have a [fractal](fractal.md) shape. While there is only one Mandelbrot set, there are [infinitely](infinity.md) many Julia sets because in the equation defining Julia set (which has the same format as for Mandelbrot set, just with different variables) there is a parameter we can change to get a different set. Specifically for any [complex number](complex_number.md) (which we may see as a point in 2D plane) there is one Julia set.

The **definition** of Julia set will follow (there is actually a more general one, but we'll stick to the narrower, most common one), notice how the equation is similar to that of Mandelbrot set. Initially we pick a constant complex number *c* that will define the whole set; then for each complex number *z* (a point in 2D plane for which we want to see if it belongs to the set or not) we consider the following iteration:

*z[n + 1] = z[n]^2 + c*

Then we see if under infinitely many iterations this series diverges towards infinity or if it stays bounded. If the point didn't in fact diverge, it belongs to the set, otherwise not. Should we be visualizing the set with a [computer](computer.md), we [approximate](approximation.md) this infinite iteration by performing just a big number of iterations.

The following is a picture of one possible Julia set:

```
 ___________________________________________________________________
| Julia Set for -0.34 - 0.63i       :.                              |
|                                ..':. ..                           |
|                                '':.:'''      ..       ..          |
|                                 :':::.. ''  ::.    .. :.'         |
|                                  '::::. :: :::. .   :.'': .       |
|                              ......':::.::.:: ...::.:::.::.. .    |
|                              :::::::::':'.':.::::::::':.::''::..  |
|                   .             '::::'':':::':'':::'  ::''  '     |
|                   ':.       .   .. ..::'::':::.   '   :'          |
|                 . :: :'     ::..::::::: ::: ':::..     '          |
|                   :'::::   '.:::::::::'.::::'  ''                 |
|                    .:::::' ':::::::::. ''::::'.                   |
|                  :. '::::'.::::::::::.  '::':.'                   |
|          . .   '':::. ::: ::::::::'::'    .::::                   |
|         :':.  ... ':::.:':::''  '  '        ''.                   |
|        ..::  .::::::...':.::::::.:                                |
|   :::...' '.::::::::'.: .:.:'::::'':                              |
|    '' :. : .:''':' :::'::':::.   ' '                              |
|         '::'': '' '::: ::'':::::                                  |
|          ::       ':.  '' '':::.:                                 |
|         ' '       '        ::.:.'.'                               |
|                              ::'                                  |
|                              '                                    |
|___________________________________________________________________|
```

# Code

The code below is a simple [C](c.md) program that renders given Julia set into terminal (for demonstrative purposes, it isn't efficient or do any [antialiasing](antialiasing.md)).

```
#include <stdio.h>

#define ROWS 30
#define COLS 70
#define SET_X -0.36 // Julia set parameter
#define SET_Y -0.62 // Julia set parameter
#define FROM_X -1.5
#define FROM_Y 1.0
#define STEP (3.0 / ((double) COLS))

unsigned int julia(double x, double y)
{
  double cx = x, cy = y, tmp;

  for (int i = 0; i < 1000; ++i)
  {
    tmp = cx * cx - cy * cy + SET_X;
    cy = 2 * cx * cy + SET_Y;
    cx = tmp;

    if (cx * cx + cy * cy > 10000000000)
      return 0;
  }

  return 1;
}

int main(void)
{
  double cx, cy = FROM_Y;

  for (int y = 0; y < ROWS; ++y)
  {
    cx = FROM_X;

    for (int x = 0; x < COLS; ++x)
    {
      unsigned int point = 
        julia(cx,cy) + (julia(cx,cy + STEP) * 2);   

      putchar(point == 3 ? ':' : (point == 2 ? '\'' : 
        (point == 1 ? '.' : ' ')));

      cx += STEP;
    }

    putchar('\n');

    cy -= 2 * STEP;
  }

  return 0;
}
```
