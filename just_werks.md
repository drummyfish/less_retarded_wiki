# Just Werks

"Just werks" (for "just [work](work.md)s" if that's somehow not clear) is a common phrase usually used by [noobs](noob.md) to justify using a piece of [technology](tech.md) while completely neglecting any other deeper and/or long term consequences, though the argument has legitimate uses as well. A noob doesn't think about technology further than how it can immediately perform some task for him, to him "just werks" is a mere rationalization providing the comfort needed to not think things through.

"Just werks" can be used legitimately to express that something simply works thanks to lack of [bullshit](bullshit.md) such as [bloat](bloat.md), as in "[PDF](pdf.md)s suck, [plaintext](plaintext.md) just werks". This use of the term is acceptable.

The phrase is frequently used on [4chan](4chan.md)/g, however mostly in the wrong way. It possibly originated there.

The ignorant "just werks" philosophy completely ignores questions such as:

- **Is there anything better in the long run?** A normie will always prefer a shitty software he can immediately use to a software that would take one day to learn and that would make the task many times easier, comfortable, cheaper etc.
- **Is this affecting my [freedom](freedom.md) (and things like "[security](security.md)" etc.)?** A normie doesn't realize that by using proprietary or bloated program will limit the number of people who can maintain, fix and improve his software, and that technology is used to abused him, e.g. by spying on him, making him depend on something unnecessary etc.
- **How is this affecting my computing in a wider sense?** A normie won't even think about such thing as that using some proprietary format will likely immediately close the door to working with it with a FOSS program, or that installing a specific OS will limit what programs he can run.
- **Am I becoming a slave to this technology?** By adopting something [proprietary](proprietary.md) and/or bloated I am slowly becoming dependent on an ecosystem that's completely under the control of some [corporation](corporation.md), an ecosystem that can quickly change for the worse or even disappear completely with me being able to do nothing about it.
- **Am I supporting [evil](evil.md)?** E.g. by paying to a corporation, letting someone collect data in the background, promoting a bad piece of technology etc.
- **Am I hurting better alternatives by not using them?** E.g. by using a proprietary social network gives one more user to it and one fewer to a potentially more ethical free social network.
- **Is there anything just plain better?** A normie will take the first thing that's handed to him and "just werks" without even checking if something better exists that would satisfy him better.
- ...

## See Also

- [everyone does it](everyone_does_it.md)
- [just doing my job](just_doing_my_job.md)
