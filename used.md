# Used

Used is someone using technology that abuses him, most notably [proprietary](proprietary.md) software. For example those using [Windows](windows.md) are not users but useds. This term was popularized by [Richard Stallman](rms.md).