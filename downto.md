# Downto Operator

In [C](c.md) the so called "downto" operator is a [joke](jokes.md) played on nubs. It goes like this: Did you know C has a hidden downto operator `-->`? Try it:

```
#include <stdio.h>

int main(void)
{
  int n = 20;

  while (n --> 10) // n goes down to 10
    printf("%d\n",n);

  return 0;
}
```

Indeed this compiles and works. In fact `-->` is just `--` and `>` operators.