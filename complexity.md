# Complexity

Complexity may stand for several things, for example:

- opposite of [simplicity](kiss.md), general complexity of technology, see e.g. [bloat](bloat.md)
- [computational complexity](computational_complexity.md), mathematical study of computer resource usage
- ...