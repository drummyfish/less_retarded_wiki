# Chasm: The Rift

*Poor man's Quake.*

Chasm: The Rift is a 1997 FPS [game](game.md), developed basically by a few Ukrainian basement nerds to be a cheaper version of [Quake](quake.md), which was of course completely overshadowed by this giant and therefore isn't much known but which is nevertheless quite technically impressive upon closer inspection.

{ TODO: do more research about the engine, attempt to translate the Russian YT video, reach the devs. ~drummyfish }

The game requirements were a [486](486.md) [CPU](cpu.md) (which reached 100 MHz at most), 16 MB [RAM](ram.md) and 75 MB storage space.

**The engine** is possibly the most [interesting](interesting.md) part of the game as it used [software rendering](sw_rendering.md) that combined a ["2.5D"](pseudo3d.md) level rendering and "true 3D" polygonal models for things like level decorations, enemies and weapon view model. Not much is known about the internals as the whole code is proprietary and "closed source", we may only inspect it visually and through [reverse engineering](reverse_engineering.md). To us it is not known if environment rendering uses [BSP](bsp.md) rendering, [portal rendering](portal_rendering.md), [raycasting](raycasting.md), something similar or whether it just utilizes its 3D model renderer for levels too, however there are some 2.5D simplifications going on as levels are defined as 2D (no room-above-room) and looking up/down is faked (even for the environment inserted "true 3D" models, the up/down look is limited to just a small offset probably to mask the "2.5D" nature of the engine). In fact it isn't even possible to have different height levels of floor, all levels just have the same floor height (ceiling height can be set though)! This is masked a bit by using 3D models onto which it is indeed possible to jump. The game's level editor shows levels use a square grid on which however it is possible to place even non-90 degree walls. There is also a [lightmap](lightmap.md) lighting system present allowing [dynamic](dynamic.md) lights -- a pretty advanced feature, though the lightmap only seems to be 2D, just as the level itself. Destructible environment is also faked in some levels by having a 3D model behaving like part of a wall, then disappearing when destroyed.

Apart from the engine the game was also nice for being quite [KISS](kiss.md), taking similar approach to e.g. [Anarch](anarch.md) by using very minimal menu and controls: for example there is no door opening or "use" button, items just activate by proximity, weapon switching is also performed by single button. This is actually quite nice as setting up controls and learning them is many times something that just puts you off.

## See Also

- [Gloom](gloom.md)