# Settled

When a [software](software.md) is so good it makes you stop searching further for any other software in the same category, it makes the debate settled -- you've found the ultimate tool, you're now free, you no longer have to think about the topic or invest any more precious time into trying different alternatives. You've found the [ultimate](ultimate.md), nearly [perfect](perfect.md) tool for the job, the one that makes the others obsolete. Settling the competition of different tools is one of the goal of [good technology](lrs.md) as it frees users as well as programmers.

For example [Vim](vim.md) often settles the search for a text/programming editor for many programmers (yes, for others it's [Emacs](emacs.md)). It is one's own personal "category killer" (a term used by [ESR](esr.md) in *Cathedral and Bazaar*).

Nevertheless some soyboys just like hopping and switching their tools just because, they simply like wasting their time with things like [distrohopping](distrohopping.md) etc. There's no help to these people.

{ software that made the debate settled for me: [vim](vim.md), [dwm](dwm.md), [st](st.md), [badwolf](badwolf.md) ~drummyfish }