# Shit

Shit is something that's awfully bad. [Unicode](unicode.md) for pile of shit is U+1F4A9.

```
   __--""""--__
  /  ,--""--,  \
 |\__"--..--"__/|\
 |   ""----"" : | "._
 |   :        : |    ""|
 |   :        : |      |
 |   :        : |      |
 |   :        : |      |
  \__:       _:/ \_    |
     ""----""      "-..|
```

*Are you looking for this?*

Some **things that are shit** include [systemd](systemd.md), [capitalism](capitalism.md), [feminism](feminism.md), [Windows](windows.md), [Linux](linux.md), [Plan9](plan9.md), [OOP](oop.md), [LGBT](lgbt.md), [security](security.md), [encryption](encryption.md), [military](military.md), [laws](law.md), [liberalism](liberalism.md), [USA](usa.md), [money](money.md), [cryptocurrencies](crypto.md) and many more.

## See Also

- [crap](crap.md)
- [cancer](cancer.md)
- [harmful](harmful.md)
- [bullshit](bullshit.md)
- [toilet](toilet.md)
- [suck ass](suck_ass.md)
- [pinus](pinus.md)
- [benis](benis.md)
