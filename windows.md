# Micro$oft Window$

Microsoft Windows (also Winshit, Windoze, Winbloat etc.) is a series of malicious, [bloated](bloat.md) [proprietary](proprietary.md) "[operating systems](os.md)" with preinstalled [malware](malware.md). AVOID THIS [SHIT](shit.md).

Windows has these advantages:

- none

Windows has these disadvantages (this is just a few things, we can't possibly aspire to recount them all):

- It's slow, huge, ugly and gigantically [bloated](bloat.md).
- It's [proprietary](proprietary.md), you have no control over it, it does whatever it wants.
- It spies on you.
- It has blatant backdoors, Microsoft can absolutely take over your computer whenever they want.
- It's paid and expensive.
- It needs to restart on updates, it won't let you reject updates, it will restart in the middle of your work and ruin it.
- It shows you ads.
- It crashes extremely often.
- It doesn't work.
- You can't customize it.
- It forces you to update to newer and shittier versions.
- It forces you to constantly buy new hardware else it stops working.
- It's made for absolute retards and only retards use it. It is designed by retards for retards.
- It's made by terrorists and murderers whom you support by using it.
- It takes forever to boot.
- It automatically installs stuff you don't want.
- 99% viruses will target you.
- You have to only have one brain cell to use it.
- It doesn't work on old (superior) computers.
- It's unusable for servers. It's also unusable for anything else.
- It shits on [Unix philosophy](unix_philosophy.md), it does everything EXACTLY the opposite way than it should be done. It pushes harmful concepts such as monolithic software, GUI for everything etc.
- It does poorly even in implementing its own philosophy, for example its GUI design is absolute crap cretinous retarded shit that was designed by a monkey, incoherent and unintuitive rushed piece of shit.
- It constantly downloads stuff from the Internet, eating up your bandwidth, stopping to work when Internet goes down.
- It's hostile to anything [free](free_software.md), for example it will nuke all other operating systems when installed.
- It has a shitty EULA that makes Micro$oft able to sue you for absolutely anything, it forbids studying the system, copying it, borrowing it, basically doing anything with it.
- It pushes [DRM](drm.md) and similar shit.
- It gives you [cancer](cancer.md).
- It's an absolutely laughable shitty [capitalist](capitalism.md) product that manipulates you, it shills Micro$oft's inferior software, for example if you want to install a web browser it will just push you to installing Micro$oft's harmful browser etc.
- It's inefficient, eats too much electricity, increases CO2, heat pollution, forces you to buy big harddrives, more expensive Internet connection etc.
- ...

Not that you should use bloated Windows programs but even if you WANT that you can do it with [Wine](wine.md) under GNU/Linux, sometimes the programs even run better under Wine than on winshit itself lol. By this there is zero (or maybe even fewer) reasons to ever use windows, it's literally just for [faggots](faggot.md).

Some people still decide to use it.

**Should we compile our programs for Window$?** [Free software](free_software.md) supporters regularly debate this question, some say we shouldn't make Window$ versions of free programs so as to not support the platform. Nevertheless even such purists as [GNU](gnu.md) make Window$ versions of their programs with the justification that providing Window$ useds with the taste of freedom may convince them to leave the system (though their critics may equally see it as mere populism, i.e. just making their program more popular). It is probably true that making some free tools available on Window$ makes a transition to a free system easier just by making the transition more gradual: the used first learns to use free tools, then switches the underlying system, as opposed to making one giant leap into a completely foreign environment. { This is how it worked for myself anyway. ~drummyfish } **Nevertheless** our [LRS](lrs.md) point of view is yet a bit different -- we oppose any kind of [censorship](censorship.md), artificial scarcity and so on, including actively breaking compatibility (which includes not making something compatible if it is trivial to do), we simply refuse to be overlords strategically dictating whether something should work or not, that would be the [evil](evil.md) way. For this our advice is: if it's easy to make your program work somewhere, make it work there. Never put extra effort into lowering compatibility or accessibility. If you just don't care about some platform or it would present too much trouble for you to make it compatible, it's fine to not do it, but at least make it easy for others to do for you. So yes, you can (and probably should) make a Window$ version of your program, but it is also OK to have a bit of [fun](fun.md) while doing so -- for example [Anarch](anarch.md) on Window$ warns the player that his operating system is [malware](malware.md) :) **How to compile shit for Window$ when you don't have Window$?** There are several ways: for [C](c.md) (or C++ etc.) programs you may comfortably use e.g. MinGW ([mingw](mingw.md)) (basically the GNU compilers + binary tools compiled and packaged for Window$) -- this you can either run natively under GNU/Linux (look for mingw packages) or you may run the Window$ versions of it under [wine](wine.md) or in some [VM](vm.md) such as [qemu](qemu.md) or virtualbox (where you may additionally also test the compiled program); you may also theoretically e.g. make a web browser version of your program (with stuff like emscripten) which will run on all OSes.

## Versions

TODO

All are shit.