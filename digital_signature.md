# Digital Signature

Digital signature is a method of [mathematically](math.md) (with [cryptographical](cryptography.md) [algorithms](algorithm.md)) proving that, with a very high probability, a digital message or document has been produced by a specific sender, i.e. it is something akin traditional signature which provides a proof that something has been written by a specific individual.

It works on the basis of [asymmetric cryptography](asymmetric_cryptography.md): the signature of a message is a pair of a public key and a number (the signature) which can only have been produced by the owner of the private key associated with the public key. This signature is dependent on the message data itself, i.e. if the message is modified, the signature will no longer be valid, preventing anyone who doesn't posses the private key from modifying the message. The signature number can for example be a [hash](hash.md) of the message decoded with the private key -- anyone can check that the signature encoded with the public key gives the document hash, proving that whoever computed the signature number must have possessed the private key.

Signatures can be computed e.g. with the [RSA](rsa.md) algorithm.

The advantage here is that **[anonymity](anonymity.md) can be kept with digital signatures**; no private information such as the signer's real name is required to be revealed, only his public key. Someone may ask why we then even sign documents if we don't know by whom it is signed lol? But of course the answer is obvious: many times we don't need to know the identity of the signer, we just need to know that different messages have all been written by the same man, and this is what a digital signature can ensure. And of course, if we want, a public key can have a real identity assigned if desirable, it's just that it's not required.

## See Also

- [tripcode](tripcode.md)