# Easy To Learn, Hard To Master

"Easy to learn, hard to master" (ETLHTM) is a type of design of a [game](game.md) (and by extension a potential property of any [art](art.md) or [skill](skill.md)) which makes it relatively easy to learn to play while mastering the play (playing in near optimal way) remains very difficult.

Examples of this are games such as [tetris](tetris.md), [minesweeper](minesweeper.md) or [Trackmania](trackmania.md).

[LRS](lrs.md) sees the ETLHTM design as extremely useful and desirable as it allows for creation of [suckless](suckless.md), simple games that offer many hours of [fun](fun.md). With this philosophy we get a great amount of value for relatively little effort.

This is related to a fun coming from **self imposed goals**, another very important and useful concept in games. Self imposed goals in games are goals the player sets for himself, for example completing the game without killing anyone (so called "pacifist" gameplay) or completing it very quickly ([speedrunning](speedrun.md)). Here the game serves only as a platform, a playground at which different games can be played and invented -- inventing games is fun in itself. Again, a game supporting self imposed goals can be relatively simple and offer years of fun, which is extremely cool.

The simplicity of learning a game comes from simple rules while the difficulty of its mastering arises from the complex emergent behavior these simple rules create. Mastering of the game is many times encouraged by [competition](competition.md) among different people but also competition against oneself (trying to beat own score). In many simple games such as [minesweeper](minesweeper.md) there exists a competitive scene (based either on direct matches or some measurement of skill such as [speedrunning](speedrun.md) or achieving high score) that drives people to search for strategies and techniques that optimize the play, and to training skillful execution of such play.

The opposite is [hard to learn, easy to master](hard_to_learn_easy_to_master.md).

## See Also

- [easier done than said](easier_done_than_said.md)
- [speedrun](speedrun.md)