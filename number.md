# Number

WIP kind of

{ There's most likely a lot of BS, math people pls send me corrections, thank u. ~drummyfish }

Numbers (from Latin *numerus* coming from a Greek word meaning "to distribute") are one of the most elementary [mathematical](math.md) objects, building stones serving most often as quantitative values (that is: telling count, size, length, order etc.), in higher math also used in much more [abstract](abstraction.md) ways which have only distant relationship to traditional counting. Examples of numbers are minus [one](one.md) half, [zero](zero.md), [pi](pi.md) or [i](i.md). Numbers constitute the basis and core of mathematics and as such they sit almost at the [lowest level](low_level.md) of it, i.e. most other things such as algebra, [functions](function.md) and [equations](equation.md) are built on top of numbers or require numbers to even be examined. In modern mathematics numbers themselves aren't on the absolute bottom of the foundations though, they are themselves built on top of [sets](set.md), as set theory is most commonly used as a basis of whole mathematics, however for many purposes this is just a formalism that's of practical interest only to some mathematicians -- on the other hand numbers just cannot be avoided anywhere, by a mathematician or just a common folk. The word *number* may be the first that comes to our mind when we say *mathematics*. The area of [number theory](number_theory.md) is particularly focused on examining numbers (though it's examining almost exclusively integer numbers because these seem to have the deepest pattern related e.g. to divisibility).

Let's not [confuse](often_confused.md) numbers with digits or figures (numerals) -- a number is a purely abstract entity while digits serve as symbols for numbers so that we can write them down. One number may be written in many ways, using one of many [numeral systems](numeral_system.md) (Roman numerals, tally marks, Arabic numerals of different [bases](base.md) etc.), for example 4 stands for a number than can also be written as IV, four, 8/2, 16:4, 2^2, 4.00 or 0b100. There are also numbers which cannot exactly be captured within our traditional numeral systems, for some of them we have special symbols -- most famous example is of course [pi](pi.md) whose digits we cannot ever completely write down -- and there are even numbers for which we have no symbols at all, ones that are yet not well researched and are only described by equations to which they are the solution. Sure enough, a number by itself isn't too interesting and probably doesn't even make sense, it's only in context, when it's placed in relationship with other numbers (by ordering them, defining operations and properties based on those operations) that patterns and useful attributes emerge.

Humans first started to use positive natural numbers (it seems as early as 30000 BC), i.e. 1, 2, 3 ..., so as to be able to trade, count enemies, days and so on -- since then they kept expanding the concept of a number with more [abstraction](abstraction.md) as they encountered more complex problems. First extension was to fractions, initially reciprocals of integers (like one half, one third, ...) and then general ones. Around 6th century BC Pythagoras showed that there even exist numbers that cannot be expressed as fractions ([irrational numbers](irrational_number.md), which in the beginning was a controversial discovery), expanding the set of known numbers further. A bit later (around 100 BC) negative numbers started to be used. Adoption of the number [zero](zero.md) also took some time (1st use of true zero seem to be in 4th century BC), with it first just having a limited use as a mere placeholder digit. Since 16th century a highly abstract concept of [complex numbers](complex_number.md) started to appear, which was later (19th century) expanded further to [quaternions](quaternion.md). With more advancement in mathematics -- e.g. with the development of set theory -- more and more concepts of new kinds of numbers appeared and still appear to this day. Nowadays we have greatly abstract numbers, ones existing in many dimensions, capable of counting and measuring infinitely large and infinitely small entities, and it seems we still haven't nearly discovered everything there is to know about numbers.

Basically **anything can be encoded as a number** which makes numbers a universal abstract "medium" -- we can exploit this in both mathematics and programming (which are actually the same thing). Ways of encoding [information](information.md) in numbers may vary, for a mathematician it is natural to see any number as a multiset of its [prime](prime.md) factors (e.g. 12 = 2 * 2 * 3, the three numbers are inherently embedded within number 12) that may carry a message, a programmer will probably rather encode the message in [binary](binary.md) and then interpret the 1s and 0s as a number in direct representation, i.e. he will embed the information in the digits. You can probably come up with many more ways.

But what really is a number? What makes number a number? Where is the border between numbers and other abstract objects? Essentially number is an abstract mathematical object made to model something about [reality](irl.md) (most fundamentally the concept of counting, expressing amount) which only becomes meaninful and useful by its relationship with other similar objects -- other numbers -- that are parts of the same, usually (but not necessarily) infinitely large set. We create systems to give these numbers names because, due to there being infinitely many of them, we can't name every single one individually, and so we have e.g. the [decimal](decimal.md) system in which the name 12345 exactly identifies a specific number, but we must realize these names are ultimately not of mathematical importance -- we may call a number 1, I, 2/2, "one", "uno" or "jedna", it doesn't matter -- what's important are the relationships between numbers that create a STRUCTURE. I.e. a set of infinitely many objects is just that and nothing more; it is the relationships that allow us to operate with numbers and that create the difference between integers, real numbers or the set of colors. These relatinships are expressed by operations (functions, maps, ...) defined with the numbers: for example the comparison operation *is less than* (<) which takes two numbers, *x* and *y*, and always says either *yes* (*x* is smaller than *y*) or *no*, gives numbers order, it creates the number line and allows us to count and measure. Number sets usually have similar operations, typically for example addition and multiplication, and this is how we intuitively judge what numbers are: they are sets of objects that have defined operations similar to those of natural numbers (the original "cavemen numbers"). However some more "advanced" kind of numbers may have lost some of the simple operations -- for example [complex numbers](complex_number.md) are not so straightforward to compare -- and so they may get more and more distant from the original natural numbers. And this is why sometimes the border between what is and what isn't a number may be blurry -- for example it can't objectively be said if infinity is a number or not, simply because number sets that include infinity lose many of the nicely defined operations, the structure of the set changes a lot. So arguing about what is a number ultimately becomes subjective, it's similar to arguing about what is and isn't a planet.

**[Order](order.md)** is an important concept related to numbers, we usually want to be able to compare numbers so apart from other operations such as addition and multiplication we also define the comparison operation. However note that not every order is total, i.e. some numbers may be incomparable (consider e.g. complex numbers).

Here are some [fun](fun.md) facts about numbers:

- Some people associate numbers with [colors](color.md), though what color each number has seems to be completely subjective. See [synesthesia](synesthesia.md).
- There is a funny hypothetical number between 6 and 7 called [thrembo](thrembo.md).
- There exist [illegal numbers](illegal_number.md), owing to the above mentioned fact that any information can be encoded as a number along with the fact that some information is illegal (see e.g. "[intellectual property](intellectual_property.md)").
- ...

```
 quaternions                . imaginary line
            projected       : (imaginary numbers)
 projected   j line     2i ~+~ ~ ~ ~ ~+ 1 + 2i
  k line       :            :         ,
     ...        :_          :         ,             complex numbers
        \___      \_ j      :         ,
            \___    +_   i ~+~ ~ ~ ~ ~+ 1 + i
                +___  \_    :         ,
               k    \___\_  :         ,
                        \_\_:         1         2         3         4
  - - -~|~-~-~-~-~|~-~-~-~-~+~-~-|-~-~|~-~-~|~-~|~-~-~-|-~|~|~-~-~-~|~- - -
       -2        -1        0:   1/2   ,    phi         e    pi           real line    
                = i^2       :  = 0.5  ,    ~=         ~=   ~= 3.14...  (real numbers)
                            :         ,   1.61...    2.71...     
                        -i ~+~ ~ ~ ~ ~+
                            :           1 - i
                            .
```

*Number lines and some notable numbers -- the horizontal line is real line, the vertical is imaginary line that adds another dimension and reveals complex numbers. Further on we can see quaternion lines projected, hinting on the existence of yet higher dimensional numbers (which however cannot properly be displayed using mere two dimensions here).*

The following is a table demonstrating just one way of how you can play around with numbers -- of course, we have generated it with a program, so we also practice [programming](programming.md) a bit ;) Here we just examine whole positive numbers (like number theorists would) up to 50 and take a look at some of their attributes -- we count each one's total number of divisors (excluding 1 and itself, 0 here means the number is [prime](prime.md) except for 1, if the number is highest in the series so far the number is called "highly composite"), unique divisors (excluding itself), minimum divisor (excluding 1 except for 1), maximum divisor (excluding itself except for 1), sum of total and unique divisors (if the number equal sum of unique divisors, it is said to be a "perfect number"), average "dividing spread" (distance of each tested potential divisor's remainder after division from half of this tested potential divisor, kind of "amount of not dividing the number") in percents, maximum dividing spread and normalized range between smallest and biggest divisor expressed in percents (-1 if there are none). You can make quite interesting graphs from similar data and discover cool and interesting patterns.

{ Be warned the following is just me making some quick unoriginal antiresearch, I may mess something up, it's just to show the process of playing around with numbers. ~drummyfish }

| number   | divisors |divisors uniq.|min. div.|max. div.|divisor sum|uniq. div. sum|avg. div. spread (%)|max div. spread (%)|div. range (%)|
| -------- | -------- | ------------ | ------- | ------- | --------- | ------------ | ------------------ | ----------------- | ------------ |
|        1 |        0 |            1 |        1|        1|         0 |            1 |                  0 |                 0 |           -1 |
|        2 |        0 |            1 |        2|        1|         0 |            1 |                  0 |                 0 |           -1 |
|        3 |        0 |            1 |        3|        1|         0 |            1 |                  0 |                 0 |           -1 |
|        4 |        2 |            2 |        2|        2|         4 |            3 |                 33 |               100 |            0 |
|        5 |        0 |            1 |        5|        1|         0 |            1 |                 16 |                50 |           -1 |
|        6 |        2 |            3 |        2|        3|         5 |            6 |                 43 |               100 |           16 |
|        7 |        0 |            1 |        7|        1|         0 |            1 |                 24 |                66 |           -1 |
|        8 |        4 |            3 |        2|        4|        10 |            7 |                 44 |               100 |           25 |
|        9 |        2 |            2 |        3|        3|         6 |            4 |                 36 |               100 |            0 |
|       10 |        2 |            3 |        2|        5|         7 |            8 |                 40 |               100 |           30 |
|       11 |        0 |            1 |       11|        1|         0 |            1 |                 34 |                80 |           -1 |
|       12 |        5 |            5 |        2|        6|        17 |           16 |                 53 |               100 |           33 |
|       13 |        0 |            1 |       13|        1|         0 |            1 |                 35 |                83 |           -1 |
|       14 |        2 |            3 |        2|        7|         9 |           10 |                 43 |               100 |           35 |
|       15 |        2 |            3 |        3|        5|         8 |            9 |                 44 |               100 |           13 |
|       16 |        7 |            4 |        2|        8|        24 |           15 |                 49 |               100 |           37 |
|       17 |        0 |            1 |       17|        1|         0 |            1 |                 38 |                87 |           -1 |
|       18 |        5 |            5 |        2|        9|        23 |           21 |                 47 |               100 |           38 |
|       19 |        0 |            1 |       19|        1|         0 |            1 |                 42 |                88 |           -1 |
|       20 |        5 |            5 |        2|       10|        23 |           22 |                 51 |               100 |           40 |
|       21 |        2 |            3 |        3|        7|        10 |           11 |                 45 |               100 |           19 |
|       22 |        2 |            3 |        2|       11|        13 |           14 |                 43 |               100 |           40 |
|       23 |        0 |            1 |       23|        1|         0 |            1 |                 42 |                90 |           -1 |
|       24 |        8 |            7 |        2|       12|        39 |           36 |                 55 |               100 |           41 |
|       25 |        2 |            2 |        5|        5|        10 |            6 |                 45 |               100 |            0 |
|       26 |        2 |            3 |        2|       13|        15 |           16 |                 45 |               100 |           42 |
|       27 |        4 |            3 |        3|        9|        18 |           13 |                 44 |               100 |           22 |
|       28 |        5 |            5 |        2|       14|        29 |           28 |                 49 |               100 |           42 |
|       29 |        0 |            1 |       29|        1|         0 |            1 |                 45 |                92 |           -1 |
|       30 |        6 |            7 |        2|       15|        41 |           42 |                 52 |               100 |           43 |
|       31 |        0 |            1 |       31|        1|         0 |            1 |                 45 |                93 |           -1 |
|       32 |        9 |            5 |        2|       16|        42 |           31 |                 48 |               100 |           43 |
|       33 |        2 |            3 |        3|       11|        14 |           15 |                 45 |               100 |           24 |
|       34 |        2 |            3 |        2|       17|        19 |           20 |                 47 |               100 |           44 |
|       35 |        2 |            3 |        5|        7|        12 |           13 |                 48 |               100 |            5 |
|       36 |       10 |            8 |        2|       18|        65 |           55 |                 54 |               100 |           44 |
|       37 |        0 |            1 |       37|        1|         0 |            1 |                 45 |                94 |           -1 |
|       38 |        2 |            3 |        2|       19|        21 |           22 |                 45 |               100 |           44 |
|       39 |        2 |            3 |        3|       13|        16 |           17 |                 46 |               100 |           25 |
|       40 |        8 |            7 |        2|       20|        53 |           50 |                 51 |               100 |           45 |
|       41 |        0 |            1 |       41|        1|         0 |            1 |                 47 |                95 |           -1 |
|       42 |        6 |            7 |        2|       21|        53 |           54 |                 51 |               100 |           45 |
|       43 |        0 |            1 |       43|        1|         0 |            1 |                 46 |                95 |           -1 |
|       44 |        5 |            5 |        2|       22|        41 |           40 |                 49 |               100 |           45 |
|       45 |        5 |            5 |        3|       15|        35 |           33 |                 47 |               100 |           26 |
|       46 |        2 |            3 |        2|       23|        25 |           26 |                 47 |               100 |           45 |
|       47 |        0 |            1 |       47|        1|         0 |            1 |                 47 |                95 |           -1 |
|       48 |       12 |            9 |        2|       24|        85 |           76 |                 53 |               100 |           45 |
|       49 |        2 |            2 |        7|        7|        14 |            8 |                 48 |               100 |            0 |
|       50 |        5 |            5 |        2|       25|        47 |           43 |                 49 |               100 |           46 |

Now we may start working with the [data](data.md), let's for example notice we can make a nice [tree](tree.md) of the numbers by assigning each number as its parent its greatest divisor:

```
                                     1
                                     |
 .----.-----------.------------.-----'--.-----.---.--.--.--.--.--.--.--.--.
 |    |           |            |        |     |   |  |  |  |  |  |  |  |  |
 2    3           5            7       11    13  17 19 23 29 31 37 41 43 47  <--- primes
 |    |           |            |        |     |   |  |  |
 |  .-'--.   .----+----. .---.-'-.--. .-'-. .-'-. |  |  |
 |  |    |   |    |    | |   |   |  | |   | |   | |  |  |
 4  6    9   10   15  25 14  21 35 49 22 33 26 39 34 38 46
 |  |    |   |    |    | |   |        |
 |  |  .-'-. |  .-'-.  | |   |        |
 |  |  |   | |  |   |  | |   |        |
 8  12 18 27 20 30 45 50 28  42       44
 |  |  |     |
 16 24 36    40
 |  |
 32 48
```

Here patterns start to show, for example the level one of the tree are all prime numbers. Also in this tree we can nicely find the [greatest common divisor](gcd.md) of two numbers as their closest common ancestor. Also if we go from low numbers to high numbers (1, 2, 3, ...) we see we go kind of in a zig-zag direction around the bottom-right diagonal -- what if we make a program that plots this path? Will we see something [interesting](interesting.md)? We could use this tree to encode numbers in an alternative way too, by indicating path to the number, for example *45 = {2,1,1}*. Would this be good for anything? If we write numbers like this, will some operations maybe become easier to perform? You can just keep diving down rabbit holes like this.

## Numbers In Math

There are different types of numbers, in mathematics we classify them into [sets](set.md) (if we further also consider the operations we can perform with numbers we also sort them into algebras and structures like [groups](group.md), [fields](field.md) or [rings](ring.md)). Though we can talk about finite sets of numbers perfectly well (e.g. [modulo](mod.md) arithmetic, [Boolean algebra](boolean_algebra.md) etc.), we are firstly considering [infinite](infinity.md) sets (curiously some of these infinite sets can still be considered "bigger" than other infinite sets, e.g. by certain logic there is more real numbers than rational numbers, i.e. "fractions"). Some of these sets are subsets of others, some overlap and so forth. Here are some notable number sets (note that a list can potentially not capture all relationships between the sets):

- **all**: Anything conceivable as a number, even by stretch. E.g. [zero](zero.md), minus [infinity](infinity.md) or aleph one.
  - **[unknowable](knowability.md)**: Cannot be known for some reason, e.g. being non-computable or requiring more energy for their computation than will ever be present in our [Universe](universe.md).
    - **[noncomputable](computability.md)**: Cannot be computed, i.e. any such number has no [Turing machine](turing_machine.md) which when passed *N* on input would output *N*th digit of the number in finite time. E.g. Chaitin's constant (probability that a randomly generated program will halt).
  - **[transfinite (infinite) numbers](transfinite_number.md)**: Numbers that are in a sense "infinite", used to compare objects that are infinite in size (e.g. number sets themselves). E.g. omega, beth two or aleph one. 
  - **[surreal numbers](surreal_number.md)**, **\*R: hyperreal numbers**, **superreal numbers**, ...: Various extensions of real numbers, include also infinitesimals and some transfinite numbers.
    - **[infinitesimals](infinitesimal.md)**: Are closer to zero than any real number without actually being zero, i.e. "infinitely small" numbers, play big role in [calculus](calculus.md). E.g. 0.000...1 (with infinitely many 0 digits before the 1).
  - **Qp: [p-adic numbers](p_adic_number.md)**: Alternative way of generalizing rational numbers; p-adics are quite mindblowing as they may have infinitely many digits to the left side (for which they are sometimes called *leftist numbers*), there are numbers that are their own squares without either being 1 or 0, they also contain negative numbers and fractions without having to add extra symbols. There are different kinds of p-adic number sets for different *p*s, e.g. 10-adic, 3-adic and so on (prime number *p*s are chosen for good properties). E.g. (10-adic) ...333.33, ...87187, ...11112 etc.
  - **H: [quaternions](quaternion.md)**: A sum of real number, imaginary number and two other kinds of numbers, forming a number in four dimensional space. E.g. 1 + i + j - k, 50 - 0.6k or 2i + 7j.
    - **C: [complex](complex_number.md)**: A sum of real and imaginary number, forming a number in two dimensional plane. E.g. 3 + 2i, 0.5 - 13i or 100i.
      - **complex integers**: Complex numbers with both real and imaginary component being integer. E.g. 13 - 2i, 44i or 0.
      - **[algebraic](algebraic_number.md)**: Are roots of one variable [polynomials](polynomial.md) with integer coefficients. E.g. 4/3, the [golden ratio](golden_ratio.md) or square root of two.
      - **[transcendental](transcendental_number.md)**: Aren't algebraic. E.g. [pi](pi.md), [sine](sin.md) of [e](e.md) or two to the power of square root of two.
      - **[imaginary](imaginary_number.md)**: Are similar to real numbers but lie in another dimension, on a line perpendicular to the real number line, going through 0 -- they are connected to real numbers by the fact that imaginary unit ([i](i.md)) squared equals minus one. E.g. 0, 3i or -i.
      - **R: [real](real_number.md)**: Measure any continuous one dimensional quantity (such as height or length), the line they form is continuous. E.g. -0.3, [pi](pi.md) or cube root of 10000.
        - **negative**: Smaller than zero. E.g. -1, -123 or -1000.
        - **R0+: non-negative**: Aren't negative. E.g. 0, 1 or 1000.
        - **R+: positive**: Greater than zero. E.g. 1, 456 or 1000.
        - **irrational**: Aren't rational. E.g. [pi](pi.md), minus [e](e.md) or square root of 2.
        - **Q: [rational](rational_number.md)**: "Fractions", countable set, can be written as a fraction of two integers; between any two there is always another one, so they are very densely "packed", though the line they form is not truly continuous. E.g. -2/3, 0.12345 or 2135.
          - **Z: [whole (integers)](integer.md)**: Are [discrete](discrete.md), starting at zero, extending in positive and negative direction, all neighbors are spaced by the same distance of one unit. E.g. -5123, 32 or 0.
            - **even**: Are divisible by 2. E.g. -8, 0 or 1024.
            - **odd**: Aren't even. E.g. 1, -13 or 1023.
            - **N0: [natural](natural_number.md) (with zero)**:   E.g. 0, 16 or 1000.
              - **[Fibonacci](fibonacci.md)**: Are part of a sequence that starts with 0 and 1 and continues with numbers each of which is the sum of previous two. E.g. 0, 3 or 89.
              - **[modulo](mod.md) numbers**: Finite sets of numbers up to some *N* which are allowed to "[overflow](overflow.md)", basic operations like subtraction and multiplication are still well defined. Numbers in computer mostly behave this way. E.g. numbers modulo 5 are 0, 1, 2, 3 and 4.
              - **N: natural (without zero)**: "Caveman numbers", the kind of numbers people started to use first. E.g. 1, 10 or 945.
                - **[prime](prime.md)**: Are only divisible by 1 and themselves, excluding 1. E.g. 2, 7 or 809.
                - **composite**: Aren't primes, excluding 1. For example 4, 22 or 150.
                  - **highly composite**: Composite numbers that have more divisors than any lower number. E.g. 4, 36 or 1260.
                  - **[perfect](perfect_number.md)**: Equal to the sum of its divisors. E.g. 6, 28 or 8128.

One of the most interesting and mysterious number sets are the [prime numbers](prime.md), in fact many number theorists dedicate their whole careers solely to them. Primes are the kind of thing that's defined very simply but give rise to a whole universe of mysteries and whys, there are patterns that seem impossible to describe, conjectures that look impossible to prove and so on. Another similar type of numbers are the [perfect numbers](perfect_number.md).

Of course there are countless other number sets, especially those induced by various number sequences and functions of which there are whole encyclopedias. Another possible division is e.g. to *cardinal* and *ordinal* numbers: ordinal numbers tell the order while cardinals say the size (cardinality) of a set -- when dealing with finite sets the distinction doesn't really have to be made, within natural numbers the order of a number is equal to the size of a set of all numbers up to that number, but with infinite sets this starts to matter -- for example we couldn't tell the size of the set of natural numbers by ordinals as there is no last natural number, but we can assign the set a cardinal number (aleph zero) -- this gives rise to new kind of numbers.

Worthy of mentioning is also [linear algebra](linear_algebra.md) which treats [vectors](vector.md) and [matrices](matrix.md) like elementary algebra treats numbers -- though vectors and matrices aren't usually seen as numbers, they may be seen as an extension of the concept.

**Numbers are [awesome](awesome.md)**, just ask any number theorist (or watch a numberphile video for that matter). Normal people see numbers just as boring soulless quantities but the opposite is true for that who studies them -- study of numbers goes extremely deep, possibly as deep as humans can go and once you get a closer look at something, you discover the art of nature. Each number has its own unique set of properties which give it a kind of "personality", different sets of numbers create species and "teams" of numbers. Numbers are intertwined in intricate ways, there are literally infinitely many patterns that are all related in weird ways -- normies think that mathematicians know basically everything about numbers, but in higher math it's the exact opposite, most things about number sequences are mysterious and mathematicians don't even have any clue about why they're so, many things are probably even [unknowable](knowability.md). Numbers are also self referencing which leads to new and new patterns appearing without end -- for example prime numbers are interesting numbers, but you may start counting them and a number that counts numbers is itself a number, you are getting new numbers just by looking at other numbers. The world of numbers is like a whole universe you can explore just in your head, anywhere you go, it's almost like the best, most free video [game](game.md) of all time, embedded right in this [Universe](universe.md), in [logic](logic.md) itself. Numbers are like animals, some are small, some big, some are hardly visible, trying to hide, some can't be overlooked -- they inhabit various areas and interact with each other, just exploring this can make you quite happy. { Pokemon-like game with numbers when? ~drummyfish }

There is a famous [encyclopedia](encyclopedia.md) of integer sequences at https://oeis.org/, made by number theorists -- it's quite [minimalist](minimalism.md), now also [free licensed](free_culture.md) (used to be [proprietary](proprietary.md), they seem to enjoy license hopping). At the moment it contains more than 370000 sequences; by browsing it you can get a glimpse of how deep the study of numbers goes. These people are also [funny](fun.md), they give numbers entertaining names like *happy numbers* (adding its squared digits eventually gives 1), *polite numbers*, *friendly numbers*, *cake numbers*, *lucky numbers* or *weird numbers*.

**Some numbers cannot be computed**, i.e. there exist [noncomputable](computability.md) numbers. This follows from the existence of noncomputable functions (such as that representing the [halting problem](halting_problem.md)). For example let's say we have a real number *x*, written in [binary](binary.md) as *0. d0 d1 d2 d3 ...*, where *dn* is *n*th digit (1 or 0) after the radix point. We can define the number so that *dn* is 1 if and only if a [Turing machine](turing_machine.md) represented by number *n* halts. Number *x* is noncomputable because to compute the digits to any arbitrary precision would require being able to solve the unsolvable halting problem.

**All [natural numbers](natural_number.md) are [interesting](interesting.md)**: there is a [fun](fun.md) [proof](proof.md) by contradiction of this. Suppose there exists a set of uninteresting numbers which is a subset of natural numbers; then the smallest of these numbers is interesting by being the smallest uninteresting number -- we've arrived at contradiction, therefore a set of uninteresting numbers cannot exist.

TODO: what is the best number? maybe top 10? would 10 be in top 10? what's the first number that's in top itself?

## Numbers In Programming/Computers

While mathematicians work mostly with infinite number sets and all kind of "weird" hypothetical numbers like hyperreals and transcendentals, [programmers](programming.md) still mostly work with "normal", practical numbers and have to limit themselves to finite number sets because, of course, computers have limited memory and can only store limited number of numeric values -- computers typically work with [modulo](mod.md) arithmetic with some high power of two modulo, e.g. 2^32 or 2^64, which is a [good enough](good_enough.md) approximation of an infinite number set. Mathematicians are as precise with numbers as possible as they're interested in structures and patterns that numbers form, programmers just want to use numbers to solve problems, so they mostly use [approximations](approximation.md) where they can -- for example programmers normally approximate [real numbers](real_number.md) with [floating point](float.md) numbers that are really just a subset of rational numbers. This isn't really a problem though, computers can comfortably work with numbers large and precise enough for solving any practical problem -- a slight annoyance is that one has to be careful about such things as [underflows](underflow.md) and [overflows](overflow.md) (i.e. a value wrapping around from lowest to highest value and vice versa), limited and sometimes non-uniform precision resulting in [error](error.md) accumulation, unlinearization of linear systems and so on. Programmers also don't care about strictly respecting some properties that certain number sets must mathematically have, for example integers along with addition are mathematically a [group](group.md), however signed integers in [two's complement](twos_complement.md) aren't a group because the lowest value doesn't have an inverse element (e.g. on 8 bits the lowest value is -128 and highest 127, the lowest value is missing its partner). Programmers also allow "special" values to be parts of their number sets, especially e.g. with the common IEEE [floating point](float.md) types we see values like plus/minus [infinity](infinity.md), [negative zero](negative_zero.md) or [NaN](nan.md) ("not a number") which also break some mathematical properties and creates situations like having a number that says it's not a number, but again this really doesn't play much of a role in practical problems. Numbers in computers are represented in [binary](binary.md) and programmers themselves often prefer to write numbers in binary, hexadecimal or octal representation -- they also often meet powers of two rather than powers of ten or primes or other similar limits (for example the data type limits are typically limited by some power of two). There also comes up the question of specific number encoding, for example direct representation, sign-magnitude, [two's complement](twos_complement.md), [endianness](byte_sex.md) and so on. Famously programmers start counting from 0 (they go as far as using the term "zeroth") while mathematicians rather tend to start at 1. Just as mathematicians have different sets of numbers, programmers have an analogy in numeric [data types](data_type.md) -- a data type defines a set of values and operations that can be performed with them. The following are some of the common data types and representations of numbers in computers:

- **numeric**: Anything considered a number. In very high level languages there may be just one generic "number" type that can store any kind of number, automatically choosing best representation for it etc.
  - **[unsigned](unsigned.md)**: Don't allow negative values -- this is sufficient in many cases, simpler to implement and can offer higher range in the positive direction.
  - **[signed](signed.md)**: Allow also negative values which brings up the issue of what representation to use -- nowadays the most common is [two's complement](twos_complement.md).
  - **fixed size**: Most common, each number takes some fixed size in memory, expressed in [bits](bit.md) or [bytes](byte.md) -- this of course determines the maximum number of values and so for example the minimum and maximum storable number.
    - **8bit**: Can store 256 value (e.g. integers from 0 to 255 or -128 to 127).
    - **16bit**: Can store 65536 values.
    - **32bit**: Can store 4294967296 values.
    - ...
  - **[arbitrary size](arbitrary_size_int.md)**: Can store arbitrarily high/low and/or precise value, take variable amount of memory depending on how much is needed, used only in very specialized cases, may be considerably slower.
  - **[integer](int.md)**: Integer values, most common, usually using direct or [two's complement](twos_complement.md) representation.
  - **fractional**: Have higher precision than integers, allow storing fractions, are often used to [approximate](approximation.md) real numbers.
    - **[fixed point](fixed_point.md)**: Are represented by a number with radix point in fixed place, have uniform precision.
    - **[floating point](float.md)**: Have movable radix point which is more [complicated](bloat.md) but allows for representing both very high and very small values due to non-uniform precision.
  - **[complex](complex_number.md)**: Analogous to mathematical complex numbers.
  - **[quaternion](quaternion.md)**: Analogous to mathematical quaternions.
  - **symbolic**: Used in some specialized mathematical software to perform symbolic computation, i.e. computation done in a human-like way, by manipulating symbols without using concrete values that would have to resort to approximation.
  - ...
  
However some programming languages, such as [Lisp](lisp.md), sometimes treat numbers in very abstract, more mathematical ways (for the price of some performance loss and added [complexity](bloat.md)) such as exactly handling rational numbers with arbitrary precision, distinguishing between exact and inexact numbers etc.

## Notable Numbers

Here is a table of some notable numbers, mostly important in math and programming but also some famous ones from [physics](physics.md) and popular culture (note: the order is rougly from lower numbers to higher ones, however not all of these numbers can be compared easily or at all, so the ordering isn't strictly correct).

| number                              | value               | equal to                                 |                    notes                                |
| ----------------------------------- | ------------------- | ---------------------------------------- | ------------------------------------------------------- |
| minus [infinity](infinity.md)       |                     |                                          | not always considered a number, smallest possible value |
| minus/negative one                  | -1                  | i^2, j^2, k^2                            |                                                         |
| "[negative zero](negative_zero.md)" | "-0"                | zero                                     | non-mathematical, sometimes used in programming         |
| [zero](zero.md)                     | 0                   | negative zero, e^(i * pi) + 1            | "nothing"                                               |
| epsilon                             |                     | 1 / omega                                | infinitesimal, "infinitely small" non-zero              |
|                                     |4.940656... * 10^-324|                                          | smallest number storable in IEEE-754 64 binary float    |
|                                     |1.401298... * 10^-45 |                                          | smallest number storable in IEEE-754 32 binary float    |
|                                     |1.616255... * 10^-35 |                                          | Planck length in meters, smallest "length" in Universe  |
| one eight                           | 0.125               | 2^-3                                     |                                                         |
| one fourth                          | 0.25                | 2^-2                                     |                                                         |
| one third                           | 0.333333...         | ...1313132 (5-adic)                      |                                                         |
| one half                            | 0.5                 | 2^-1                                     |                                                         |
| [one](one.md)                       | 1                   | 2^0, 0!, 0.999...                        | NOT a prime                                             |
| [square root](sqrt.md) of two       | 1.414213...         | 2^(1/2)                                  | irrational, diagonal of unit square, important in geom. |
| supergolden ratio                   | 1.465571...         | solve(x^3 - x^2 - 1 = 0)                 | similar to golden ratio, bit more difficult to compute  |
|phi ([golden ratio](golden_ratio.md))| 1.618033...         | (1 + sqrt(5)) / 2, solve(x^2 - x - 1 = 0)| irrational, visually pleasant ratio, divine proportion  |
| [two](two.md)                       | 2                   | 2^1, 0b000010                            | (only even) prime                                       |
| [silver ratio](silver_ratio.md)     | 2.414213...         | 1 + sqrt(2), solve(x^2 - 2 * x - 1 = 0)  | similar to golden ratio                                 |
| [e](e.md) (Euler's number)          | 2.718281...         |                                          | base of natural [logarithm](log.md)                     |
| [three](three.md)                   | 3                   | 2^2 - 1                                  | prime, max. unsigned number with 2 bits                 |
| [pi](pi.md)                         | 3.141592...         |                                          | circle circumference to its diameter, irrational        |
| [four](four.md)                     | 4                   | 2^2, 0b000100                            |first composite number, min. needed to color planar graph|
| [five](five.md)                     | 5                   |                                          | (twin) prime, number of platonic solids                 |
| [six](six.md)                       | 6                   | 3!, 1 * 2 * 3, 1 + 2 + 3                 | highly composite number, perfect number                 |
| [tau](tau.md)                       | 6.283185...         | 2 * pi                                   | radians in full circle, defined mostly for convenience  |
| [thrembo](thrembo.md)               | ???                 |                                          | the hidden number                                       |
| [seven](seven.md)                   | 7                   | 2^3 - 1                                  |(twin) prime, days in week, max. unsigned n. with 3 bits |
| [eight](eight.md)                   | 8                   | 2^3, 0b001000                            |                                                         |
| [nine](nine.md)                     | 9                   |                                          |                                                         |
| [ten](ten.md)                       | 10                  | 10^1, 1 + 2 + 3 + 4                      | your IQ? :D                                             |
| twelve, dozen                       | 12                  | 2 * 2 * 3                                | highly composite number                                 |
| fifteen                             | 15                  | 2^4 - 1, 0b1111, 0x0f, 1 + 2 + 3 + 4 + 5 | maximum unsigned number storable with 4 bits            |
| [sixteen](sixteen.md)               | 16                  | 2^4, 2^2^2, 0b010000                     |                                                         |
| twenty four                         | 24                  | 2 * 2 * 2 * 3, 4!                        | highly composite number                                 |
| thirty one                          | 31                  | 2^5 - 1                                  |max. unsigned number storable with 5 bits, Mersenne prime|
| [thirty two](thirty_two.md)         | 32                  | 2^5, 0b100000                            |                                                         |
| thirty six                          | 36                  | 2 * 2 * 3 * 3                            | highly composite number                                 |
| thirty seven                        | 37                  |                                          | most commonly chosen 1 to 100 "random" number           |
| [fourty two](42.md)                 | 42                  |                                          | cringe number, answer to some stuff                     |
| fourty eight                        | 48                  | 2^5 + 2^4, 2 * 2 * 2 * 2 * 3             | highly composite number                                 |
| sixty three                         | 63                  | 2^6 - 1                                  | maximum unsigned number storable with 6 bits            |
| [sixty four](sixty_four.md)         | 64                  | 2^6                                      |                                                         |
| [sixty nine](69.md)                 | 69                  |                                          | sexual position                                         |
| ninety six                          | 96                  | 2^5 + 2^6                                | alternative sexual position                             |
| one hundred                         | 100                 | 10^2                                     |                                                         |
| one hundred twenty one              | 121                 | 11^2                                     |                                                         |
| one hundred twenty seven            | 127                 | 2^7 - 1                                  | maximum value of signed byte, Mersenne prime            |
| one hundred twenty eight            | 128                 | 2^7                                      |                                                         |
| one hundred fourty four             | 144                 | 12^2                                     |                                                         |
| one hundred sixty eight             | 168                 | 24 * 7                                   | hours in week                                           |
| two hundred fifty five              | 255                 | 2^8 - 1, 0b11111111, 0xff                | maximum value of unsigned [byte](byte.md)               |
| two hundred fifty six               | 256                 | 2^8, 16^2, ((2^2)^2)^2                   | number of values that can be stored in one byte         |
| three hundred sixty                 | 360                 | 2 * 2 * 2 * 3 * 3 * 5                    | highly composite number, degrees in full circle         |
| four hundred twenty                 | 420                 |                                          | stoner shit (they smoke it at 4:20), divisible by 1 to 7|
| five hundred twelve                 | 512                 | 2^9                                      |                                                         |
| six hundred and sixty six           | 666                 |                                          | number of the beast                                     |
| one thousand                        | 1000                | 10^3                                     |                                                         |
| one thousand twenty four            | 1024                | 2^10                                     |                                                         |
| two thousand fourty eight           | 2048                | 2^11                                     |                                                         |
| four thousand ninety six            | 4096                | 2^12                                     |                                                         |
| ten thousand                        | 10000               | 10^4, 100^2                              |                                                         |
| ... (enough lol)                    | 65535               | 2^16 - 1                                 | maximum unsigned number storable with 16 bits           |
|                                     | 65536               | 2^16, 256^2, 2^(2^(2^2))                 | number of values storable with 16 bits                  |
|                                     | 80085               |                                          | looks like BOOBS                                        |
| hundred thousand                    | 100000              | 10^5                                     |                                                         |
| one [million](million.md)           | 1000000             | 10^6                                     |                                                         |
| one [billion](billion.md)           | 1000000000          | 10^9                                     |                                                         |
|                                     | 3735928559          | 0xdeadbeef                               | one of famous hexadeciaml constants, spells out DEADBEEF|
|                                     | 4294967295          | 2^32 - 1, 0xffffffff                     | maximum unsigned number storable with 32 bits           |
|                                     | 4294967296          | 2^32                                     | number of values storable with 32 bits                  |
| one trillion                        | 1000000000000       | 10^12                                    |                                                         |
|                                     |18446744073709551615 | 2^64 - 1                                 | maximum unsigned number storable with 64 bits           |
|                                     |18446744073709551616 | 2^64                                     | number of values storable with 64 bits                  |
|                                     | 3.402823... * 10^38 |                                          | largest number storable in IEEE-754 32 binary float     |
|                                     | 10^80               |                                          | approx. number of atoms in observable universe          |
| [googol](googol.md)                 | 10^100              |                                          | often used big number                                   |
| [asankhyeya](asankhyeya.md)         | 10^140              |                                          | religious number, often used in [Buddhism](buddhism.md) |
|                                     | 4.65... * 10^185    |                                          | approx. number of Planck volumes in observable universe |
|                                     |1.797693... * 10^308 |                                          | largest number storable in IEEE-754 64 binary float     |
| [googolplex](googolplex.md)         | 10^(10^100)         | 10^googol                                | another large number, number of genders in 21st century |
| [Graham's number](grahams_number.md)|                     | g64                                      | extremely, unimaginably large number, > googolplex      |
| TREE(3)                             | unknown             |                                          | yet even larger number, > Graham's number               |
| [infinity](infinity.md)             |                     |                                          | not always considered a number, largest possible value  |
| [aleph](aleph.md) zero              |                     | beth zero, cardinality(N)                | infinite cardinal number, "size" of the set of nat. num.|
| [i](i.md) (imaginary unit)          |                     | j * k                                    | part of complex numbers and quaternions                 |
| [j](j.md)                           |                     | k * i                                    | one of quaternion units                                 |
| [k](k.md)                           |                     | i * j                                    | one of quaternion units                                 |

## See Also

- [illegal number](illegal_number.md)
- [offensive number](offensive_number.md)
