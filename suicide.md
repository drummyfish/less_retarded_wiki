# Suicide

{ I hate disclaimers but I'm not advising you to commit fucking suicide, OK? I mean it's an option and sometimes it's the best option, but I want you to live if it's at least a little possible -- remember, LRS loves all life and all life is precious. We will all die, no need to rush it. Also if you're feeling like shit you can send me a mail, we can talk. ~drummyfish }

Suicide is when someone voluntarily kills himself. Suicide offers an immediate escape from [capitalism](capitalism.md) and is therefore a kind of last-resort hope; it is one of the last remaining freedoms in this world, even though capitalists can't profit from dead people and so are working hard on preventing people from killing themselves (rather than trying to make them NOT WANT TO kill themselves of course).

TODO: methods, add suicide by internet/free speech :D

For SCIENCE RESEARCHERS: there is a text file describing suicide methods at http://textfiles.com/fun/suicide.txt. One site related to suicide that's being censored because it discussed methods of killing oneself is called **sanctioned suicide** (https://sanctioned-suicide.net).

## See Also

- [ACK](ack.md)
- [ragequit](ragequit.md)