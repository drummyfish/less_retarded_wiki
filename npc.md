# NPC

NPC (non-player character) is a character in a video [game](game.md) that's not controlled by a player but rather by [AI](ai.md). NPC is also used as a term for people in [real life](irl.md) that exhibit low intelligence and just behave in accordance with the system.

## See Also

- [normie](normie.md)
- [muggle](muggle.md)