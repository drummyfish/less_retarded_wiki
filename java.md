# Java

*Unfortunately 3 billion devices run Java.*

Java (not to be confused with [JavaScript](js.md)) is a highly [bloated](bloat.md), inefficient, "[programming language](programming_language.md)" that's sadly kind of popular. It is compiled to [bytecode](bytecode.md) and therefore "[platform independent](platform_independence.md)" (as long as the platform has a lot of resources to waste on running Java [virtual machine](vm.md)). Some of the features of Java include [bloat](bloat.md), slow loading, slow running, [supporting capitalism](capitalist_software.md), forced and unavoidable [object obsession](oop.md) and the necessity to create a billion of files to write even a simple program.

{ I tried programming in Java but it made me so much dumber, it literally closed my eyes in so many ways, never touch it if you can. ~drummyfish }

Avoid this [shit](shit.md).

{ I've met retards who seriously think Java is more portable than [C](c.md) lol. I wanna [suicide](suicide.md) myself. ~drummyfish }