# John Carmack

John Carmack (born 1970 in Kansas, [US](usa.md)) is a brilliant legendary programmer that's contributed mostly to [computer graphics](graphics.md) and stands behind engines of such [games](game.md) as [Doom](doom.md), [Wolfenstein](wolf3D.md) and [Quake](quake.md). He helped pioneer real-time [3D graphics](3d_rendering.md), created many [hacks](hacking.md) and [algorithms](algorithm.md) (e.g. the reverse shadow volume algorithm). He is also a rocket [engineer](engineer.md). He had some trouble with the law as a young and dropped out of university.

```
  ____
 /_____\
 |O-O''@
 | _  |
 \____/

```

*[ASCII art](ascii_art.md) of John Carmack*

He is kind of the ridiculously [stereotypical](stereotype.md) [nerd](nerd.md) with glasses who just by the way he talks gives out the impression of someone with high functioning [autism](autism.md). You can just sense his [IQ](iq.md) is over 9000. However it appears he's the kind of very narrowly focused robot, a calculator capable of superhuman programming and math calculations who knows little to nothing about unrelated areas -- for example he doesn't seem to be able to speak any other language than English or know much about empathy (judging from his conformance to entrepreneurship). { If I'm incorrect, let me know please. I searched if he speaks any languages but haven't found any clues about that being true. ~drummyfish } Some nice [shit](shit.md) about him can be read in the (sadly [proprietary](proprietary.md)) book *Masters of Doom*.

Carmack is a proponent of "[FOSS](foss.md)" and has released his old game engines as such which gave rise to an enormous amount of modifications, forked engines and even new games (e.g. [Freedoom](freedoom.md) and [Xonotic](xonotic.md)). He's probably leaning more towards the dark side of the source: the [open-source](open_source.md). In 2021 Carmack tweeted that he would have rather licensed his old Id engines under a permissive BSD [license](license.md) than the [GPL](gpl.md), which is good.

Tragically Carmack sold his soul to [Facebook](facebook.md) in 2013 to pursue [work](work.md) on [VR](vr.md) (in a Facebook owned company Oculus). Sometimes he is sadly not so based -- in one interview he admitted he was an "amoral little jerk" as a kid, but he probably kept a bit of that amorality still as he in general supports [business](business.md), in the 90s even praised some features of Microsoft [Windows](windows.md) etc., not very cool. But he's an [american](usa.md) after all, so what would you expect.