# Suckless

Suckless, software that [sucks](suck.md) less, is a type of [free](free_software.md) [software](software.md), programming philosophy as well as an organization (http://suckless.org/), that tries to adhere to a high technological [minimalism](minimalism.md), [freedom](freedom.md) and [hackability](hacking.md), and opposes so called [bloat](bloat.md) and unnecessary complexity which has been creeping into most "[modern](modern.md)" software and by which technology has started to become less useful and more burdening. It is related to [Unix philosophy](unix_philosophy.md) and [KISS](kiss.md) but brings some new ideas onto the table. It became somewhat known and highly influenced some newly formed groups, e.g. [Bitreich](bitreich.md) and our own [less retarded software](lrs.md). Suckless seems to share many followers with [cat-v.org](cat_v.md). Suckless has a very minimalist logo that's just a super simple letter *S*.

The community used to be relatively a small underground niche, however after a rise in popularity sometime in 2010s, thanks to tech youtubers such as [Luke Smith](luke_smith.md), [Distro Tube](distro_tube.md) and [Mental Outlaw](mental_outlaw.md), the awareness about the group spread a lot wider, even mainstream programmers now usually know what *suckless* stands for. It has also gained traction on [4chan](4chan.md)'s technology board which again boosted suckless popularity but also inevitably brought some retardism in. While the group core consisting a lot of expert programmers and [hackers](hacker.md) mostly interested in systems like [GNU](gnu.md)/[Linux](linux.md), [BSDs](bsd.md) and [Plan 9](plan9.md), a lot of less skilled "[Linux](linux.md)" users and even complete non-programmers now hang around suckless to various degrees -- especially the [dwm](dwm.md) window manager has seen a great success among "Unix porn" lovers and chronic [ricers](ricing.md). While most of the true suckless followers are hardcore minimalists and apply their principles to everything, many of the noobs around suckless just cherry pick programs they find nice to look at and integrate them in their otherwise bloated systems.

Suckless is pretty cool, it has inspired [LRS](lrs.md), but watch out, as with most of the few promising things nowadays it is half cool and half shitty -- for example most suckless followers seem to be [rightists](left_vs_right.md) and [capitalists](capitalism.md) who are motivated by [harmful](harmful.md) goals such as their own increased [productivity](productivity_cult.md), not by [altruism](altruism.md). Many suckless people are quite pragmatic -- though they believe in hardcore minimalism, they will oftentimes, for practical reasons, rather choose e.g. a well established programming language ([C](c.md)) before the more minimal one (e.g. [Forth](forth.md)). LRS takes the good and tries to fix the issues of suckless, we only take the good ideas of suckless. Also it seems like by now that part of the suckless community degenerated a bit by its increase in popularity into a bit of what it opposed -- a kind of consumerist fashion followers who aren't interested so much in good design of technology but rather constantly ricing their dwm in pursuit of cool looking [pseudominimalist](pseudominimalism.md) system in ways not dissimilar to those of iToddlers.

{ From what it seems to me, the "official" suckless community is largely quiet and closed, leading conversations mostly on mailing lists and focusing almost exclusively on the development of their software without politics, activism and off topics, probably because they consider it bullshit that would only be distracting. There is also suckless subreddit which is similarly mostly focused on the software alone. They let their work speak. Some accuse the community of being Nazis, however I believe this is firstly irrelevant and secondly mostly false accusations of haters, even if we find a few Nazis among them, just as in any community. Most pro-suckless people I've met were actually true socialists (while Nazis are not socialist despite their name). Unlike [tranny software](tranny_software.md), suckless software itself doesn't promote any politics, it is a set of purely functional tools, so the question of the developers' private opinions is unimportant here, we have to separate ideas and people. Suckless ideas are good regardless of whose brains they came from. ~drummyfish }

## Attributes

Notable attributes of suckless software include:

- **Being [free software](free_software.md)** with the preference of **permissive [licenses](license.md)** such as [MIT](mit.md) and [CC0](cc0.md).
- **Extreme [minimalism](minimalism.md) and minimizing [dependencies](dependency.md)**, elimination of any [bullshit](bullshit.md) and **[bloat](bloat.md)**, minimizing [freedom distance](freedom_distance.md). Advocating [Unix philosophy](unix_philosophy.md), [KISS](kiss.md) etc.
- **Configuration of software is part of its source code** (`config.h`) and change of this configuration requires recompiling the software (which is extremely easy and fast with suckless software). This removes the need for dealing with config files which requires special libraries, file systems and extra code.
- Mainly using two [programming languages](programming_language.md): **[C](c.md)** (C89 or C99) for compiled programs and **[POSIX shell](posix_shell.md)** for scripting. Some also use languages such as [go](go.md) or [lisp](lisp.md), but they're in minority.
- **[Forking](fork.md) and [compiling](compiling.md) by default**, software is distributed in source format (no binaries), every user is supposed to create a personal customized fork and compile/customize the software himself.
- Mods (extension/addons) are implemented and distributed as **[patch](patch.md) files**. The idea is to fork the base version of the software and then apply patches to make a unique, completely personalized version of the software.
- **Typical upper limit for [lines of code](loc.md) of about 10k**, mostly just about 1-2k. This makes software easy to understand, modify, fork and maintain.
- **Focus on the technology itself** without mixing it with [politics](politics.md) and other [bullshit](bs.md) such as [COCs](coc.md).
- Not aiming for mainstream popularity, being a bit of an **elitist club**, in the good sense -- suckless is for expert users who understand, handle and create non-mainstream technology without handholding. Trying to be normie friendly would just lead to software and community that looks like the mainstream software and its community. { My view on this is that it's not that suckless WANTS to be an elitist club for its own sake; the issue lies in mainstream technology being hostile towards ethical software -- using ethical software nowadays requires one to be very tech savvy, hence it's not suckless who is discriminating but rather those who create mainstream technology. ~drummyfish }

## History

Suckless in current form has existed since 2006 when the domain suckless.org was registered by a German guy Anselm R. Garbe who is the founder of the community. It has evolved from a community centered around specific software projects, most notably [wmii](wmii.md). Garbe has given interview about suckless in FLOSS Weekly episode 355.

Some time before 2010 suckless developed [stali](stali.md), a statically linked [glibc](glibc.md)-less ["Linux distro"](distro.md) that was based on the idea that [dynamic linking](dynamic_linking.md) is [harmful](harmful.md) and that [static linking](static_linking.md) is mostly advantageous. It also came with suckless software by default. This project was made independent and split from suckless in 2018 by Garbe.

In 2012 a core veteran member of suckless, a Spanish guy nicknamed [Uriel](uriel.md), has killed himself and became a [meme](meme.md).

## Projects

Notable projects developed by the suckless group include:

- [dwm](dwm.md)
- [st](st.md)
- [dmenu](dmenu.md)
- [surf](surf.md)
- [stali](stali.md)
- ...

However there are many more ([IRC](irc.md) clients, file formats, presentation software, ...), check out their website.

## See Also

- [less retarded software](lrs.md)
- [reactionary software](reactionary_software.md)
- [bitreich](bitreich.md)
- [cat-v](cat_v.md)