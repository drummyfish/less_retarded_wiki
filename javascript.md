# JavaScript

*Not to be [confused](often_confused.md) with [Java](java.md).*

JavaScript (JS) is a very popular, highly [shitty](shit.md) [bloated](bloat.md) [scripting](script.md) [programming language](programming_language.md) used mainly on the [web](www.md). The language is basically the centerpoint of [web development](webdev.md), possibly the worst area a programmer can find himself in, so it is responsible for a great number of [suicides](suicide.md), the language is infamously surrounded by a clusterfuck of most toxic [frameworks](framework.md) you can imagine and a curious fact is also that people who program in JavaScript are less intelligent than people who don't program at all. JavaScript is NOT to be confused with an unrelated language called [Java](java.md), which for some time used to be used on the web too but works very differently. JavaScript should also not be confused with [ECMAScript](ecmascript.md), a language standard which JavaScript is based on but to which it adds yet more antifeatures, i.e. JavaScript is a dialect of ECMAScript (other similar ECMAScript-based languages are e.g. ActionScript and JScript). [LRS](lrs.md) stance towards this language is clear: as any other mainstream [modern](modern.md) language **JavaScript is an absolutely unacceptable choice for any serious project**, though it may be used for quick experiments and ugly temporary programs as the language is high level, i.e. extremely easy, it doesn't require any ability to think, it works in every browser (so you get a kind of [multiplatformness](multiplatform.md)) and allows making things such as [GUI](gui.md) and visualizations super quickly and easily. But remember that this kind of "comfort" always comes for a cost too high to pay.

**How bloated is JavaScript?** Very much. A [MINIMALIST](minimalism.md) [C](c.md) implementation called QuickJS has around 80K [lines of code](loc.md) -- compare e.g. to about 25K for [tcc](tcc.md), a similar style implementation of C, and about 5K for [comun](comun.md). A more mainstream implementation of JavaScript, the [v8](v8.md) engine (used e.g. in node.js) has **over 1 million lines of code** of C++. { Checked with *cloc*. V8 also contains web assembly aside from JavaScript, but still you get the idea. ~drummyfish }

Number 1 rule of a good webdesign is: **NEVER use JavaScript**. Website is not a program, website is a document, so it doesn't need any scripts. Privacy freaks hate web JavaScript because it's a huge [security](security.md) vulnerability (websites with JavaScript can spy easily on you -- yes, even if the script is "[free software](free_software.md)" and yes, even if it runs under VM and so on) -- we don't fancy security but JavaScript is still bloat and [capitalist](capitalism.md) shit, it makes a website literally unusable in good browsers (those that don't implement JavaScript) so [we](lrs.md) hate it too. Basically everyone hates it.

In the past JavaScript was only a **[client](client.md) side** scripting language, i.e. it was used in [web browsers](web_browser.md) (the clients) to make computations on the client computer (which suffices for many things but not all) -- as a browser language JavaScript interoperates with [HTML](html.md) and [CSS](css.md), other two languages used on websites (which are however not programming languages). For server side computations [PHP](php.md), a different language, was used, however later on (around 2010) a framework/environment called [node.js](node_js.md) appeared which allowed JavaScript to be used as a more general language and to be used for server side programming as well; as it's more comfortable to write everything in a single language, JavaScript started to replace PHP in many places, though PHP is still used to this day.

[jQuery](jquery.md) is a very popular [library](library.md) that's often used with JavaScript. It's kind of a universal library to do things one often wants to do. We mention it because everyone around JavaScript just supposes you'll be using it.

**Why is it called JavaScript if it has nothing to do with Java?** Simply put the name was chosen because back then Java was the big thing and they wanted to show that JavaScript is kind of similar but complementary, the developers of the languages were associated with each other and they thought it would be good [marketing](marketing.md) to associate the languages through naming, but of course the languages are completely different.

TODO: some more shit

## Examples

Below is our standardized [divisor tree](divisor_tree.md) program written in browser JavaScript:

```
<html> <head> <script>

// recursive function, returns divisor tree of x as a string
function divisorTreeStr(x)
{
  let a = -1, b = -1;

  for (let i = 2; i <= x / 2; i++) // find closest divisors
    if (x % i == 0)
    {
      a = i;
      b = x / i;

      if (b <= a)
        break;
    }

  return "(" + (a > 1 ? divisorTreeStr(a) + " " + x + " " +
    divisorTreeStr(b) : x) + ")";
}

function main()
{
  while (true) // main loop, read numbers from the user
  {
    let x = parseInt(prompt("enter a number:"));

    if (isNaN(x) || x < 0 || x >= 1000)
      break;

    console.log(divisorTreeStr(x));
  }
}

</script> </head> <body onload="main()"> </body> </html>
```

## JavaScript Fun

Here let be recorded funny code in this glorious language.

**This kills the JavaScript**:

```
clear(this);
```

That's right, the above code just make JavaScript commit [suicide](suicide.md) by deleting its whole global thing.

{ Found here: https://codegolf.stackexchange.com/questions/61115/make-your-language-unusable. ~drummyfish }

Here is how to **make your page work only with JavaScript turned off**:

```
<html>
<head>
<script>
function nuke() { document.body.innerHTML = "<p>disable JavaScript to view this page</p>"; }
</script>
</head>

<body onload="nuke()">
  <h1> My awesome page </h1>
  <p> My awesome page text :) </p>
</body>

</html>
```

{ NOTE: Remember that normally breaking compatibility on purpose is probably bad, it shouldn't be done seriously (don't waste effort on breaking something, rather make something nice, also censorship is always bad), but it's a nice [troll](troll.md) :D Don't forget to have fun sometimes. ~drummyfish }

Or this will give an epileptic seizure to everyone who has Javascript on:

```
<html>
<head>
<script>
alert("Turn off Javascript to make it stop.");

var count = 0;

function go()
{
  count += 1;
  document.body.style.backgroundColor = (count % 2) ? "red" : "blue";
  document.body.style.color = (count % 2) ? "green" : "yellow";
  document.body.style["font-size"] = (count % 64).toString() + "px";
  setTimeout(go,10);
}
</script>
</head>

<body onload="go()">
  <h1> My awesome page </h1>
  <p> My awesome page text :) </p>
</body>

</html>
```

TODO: some JS nonsense like below (https://wtfjs.com/)

```
"11" + 1  // "111"
"11" - 1  // 10
```