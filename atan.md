# Arcus Tangent

Arcus tangent, written as *atan* or *tan^-1*, is the inverse [function](function.md) to the [tangent](tan.md) function. For given argument *x* (any real number) it returns a number *y* (from -[pi](pi.md)/2 to pi/2) such that *tan(y) = x*.

**Approximation**: Near 0 *atan(x)* can very rougly be approximated simply by *x*. For a large argument *atan(x)* can be approximated by *pi/2 - 1/x* (as *atan*'s [limit](limit.md) is pi/2). The following formula { created by me ~drummyfish } approximates atan with a [poylnomial](polynomial.md) for non-negative argument with error smaller than 2%:

*atan(x) ~= (x * (2.96088 + 4.9348 * x))/(3.2 + 3.88496 * x + pi * x^2)*

```
            | y
       pi/2 +                  
            |       _..---''''''
            |   _.''
            | .'
-----------.+'-+--+--+--+--+--> x
        _.' |0 1  2  3  4  5
     _-'    |
.--''       |
      -pi/2 +
            |
```

*plot of atan(x)*