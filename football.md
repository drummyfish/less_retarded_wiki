# Football

*Not to be confused with any [American](usa.md) pseudosport.*

Football is one of the most famous [sport](sport.md) [games](game.md) in which two teams face each other and try to score goals by kicking an inflated ball. It is one of the best sports not only because it is genuinely [fun](fun.md) to play and watch but also because of its essentially simple rules, accessibility (not for rich only, all that's really needed is something resembling a ball) and relatively low discrimination -- basically anyone can play it, unlike for example basketball in which height is key; in amateur football even fat people can take part (they are usually assigned the role of a goalkeeper). [Idiots](usa.md) call football *soccer*.

We, [LRS](lrs.md), highly value football, as it's a very [KISS](kiss.md) sport that can be played by anyone anywhere without needing expensive equipment. It is the sport of the people, very popular in poor parts of the world.

Football can be implemented as a video [game](game.md) or inspire a game mode -- this has been done e.g. in [Xonotic](xonotic.md) (the Nexball mode) or [SuperTuxKart](supertuxkart.md). There is a popular mainstream [proprietary](proprietary.md) video game called Rocket League in which cars play football (INB4 zoomers start calling football "Rocket League with people"). There is also a greatly suckless [pen and paper](pen_and_paper.md) version of football called [paper football](paper_football.md).

## Rules

As football is so widely played on all levels and all around the world, there are many versions and rule sets of different games in the *football* family, and it can sometimes be difficult to even say what classifies as football and what's a different sport. There are games like futsal and beach football that may or may not be seen as a different sport. The most official rules of what we'd call football are probably those known as *Laws of the Game* governed by International Football Association Board (IFAB) -- these rules are used e.g. by FIFA, various national competitions etc. Some organizations, e.g. some in the [US](usa.md), use different but usually similar rules. We needn't say these high level rules are pretty complex -- *Laws of the Game* have over 200 pages and talk not just about the mechanics of the game but also things such as allowed advertising, political and religious symbolism, referee behavior etc.

Here is a simple ASCII rendering of the football pitch:

```
  C1_________________________________________C2
   |                    :                    |
   |                    :                    |
   |........            :            ........|
   |       :            :            :       |
   |....   :            :            :   ....|
 __|   :   :            :            :   :   |__
|G :   :   :            :            :   :   : G|
|1 :   :   :            O            :   :   : 2|
|__:   :   :            :            :   :   :__|
   |...:   :            :            :   :...|
   |       :            :            :       |
   |.......:            :            :.......|
   |                    :                    |
   |                    :                    |
   |____________________:____________________|
  C3                                         C4
 
```

In amateur games simpler rules are used -- a sum up of such rules follows:

- There are **two teams** of players facing each other in a match, officially 11 players on each team (10 "normal" players and one goal keeper), but of course this can wildly differ in just for fun games (street rules may also state that there is no fixed goal keeper; goal keeper is the one currently closest to own goal, or there may simply be no goal keeper at all, especially with small goals). Each team usually wears different colors so they're easily distinguished.
- The match is played on a flat **rectangular field** (officially between 100x64 and 110x75 meters, but for fewer player this can of course be much smaller) where there are **two goals** in the center of the shorter sides. Each team has one goal (which may be as simple as two shoes marking the goal borders) into which the opponent team tries to score goals.
- There is **one ball** in the game, players move the ball by kicking it with their feet -- they may use other body parts too **except for their arms**. The exception is goal keeper who may touch the ball also with his arms and hands, but only within the small area near the goal he protects. If no ball is available other things resembling it may be used.
- The game starts with both teams on their half of the field, one is given the possession of the ball (this is usually decided by coin toss, but sometimes e.g. one team is given the choice of goal and other one gets the ball).
- If the ball ends up in one team's goal, the opposite team **scores a point** and the game is restarted.
- If the ball leaves the play field on one of the longer sides, the team who's player didn't touch the ball last gets a **throw-in** -- one player takes the ball in his hands and throws it to the play field from the point at which the ball left the field. If a game is played in an environment which the ball cannot leave (e.g. a small sport hall), this rule is simply ignored.
- If the ball leaves the play field on one of the shorter sides, then two things may happen. If the ball was last touched by the team whose goal is on the side at which the ball left the field, the opposite team gets a **corner kick** (a free kick from the nearest corner of the field). Otherwise the goal keeper of the goal on the side where the ball left the play field gets a kick off from near his goal. 
- **Offside**: this is the infamous rule that [women](woman.md) don't get. This rule basically states that it is illegal for a player to get the ball while he's on the opponent's half and at the time the ball started to be passed to him he was closer to the opponent's goal than the second to last opponent's player (usually the back-most defender). This is so that one player doesn't just camp in front of the opponent's goal and wait for a long pass to score and easy goal. However in fun games this rule may be just ignored.
- There are **punishments** for breaking the rules (e.g. playing with arm, attacking the opponent player or stalling the game) which include a free kick (from the spot at which an offense happened), a penalty kick (one player gets a free kick on the opponent's goal with goal keeper from certain distance), yellow card (warning to a player), red card (given after yellow card, the player has to leave the game) etc.
- The game is played in **two halves**, each one officially 45 minutes long, but in fun games the number of parts and their duration is pretty arbitrary. After the first half there is a short rest pause and the teams switch sides. After the second half whichever team has scored more points wins. If the score is equal, the match may be prolonged as a tie breaker, either just by adding more time or playing the instant death. If no winner is decided here, there may eventually be penalty kicks to decide the winner.
- It's good if there's a referee, but if there is none, players just enforce the rules collectively.

## See Also

- [hockey](hockey.md)
- [handegg](handegg.md)