# Moderation

Moderation is an [euphemism](euphemism.md) for [censorship](censorship.md) encountered mostly in the context of Internet communication platforms (forum discussions, chats etc.). 