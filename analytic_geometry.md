# Analytic Geometry

Analytic geometry is part of [mathematics](math.md) that solves [geometric](geometry.md) problems with [algebra](algebra.md); for example instead of finding an intersection of a [line](line.md) and a [circle](circle.md) with ruler and compass, analytic geometry finds the intersection by solving an equation. In other words, instead of using pen and paper we use numbers. This is very important in computing as computers of course just work with numbers and aren't normally capable of drawing literal pictures and drawing results from them -- that would be laughable (or awesome?). Analytic geometry finds use especially in such fields as [physics simulations](physics_engine.md) ([collision](collision.md) detections) and [computer graphics](graphics.md), in methods such as [raytracing](raytracing.md) where we need to compute intersections of rays with various mathematically defined shapes in order to render 3D images. Of course the methods are used in other fields, for example [rocket science](rocket_science.md) and many other physics areas. Analytic geometry reflects the fact that geometric and algebraic problem are often analogous, i.e. it is also the case that many times problems we encounter in arithmetic can be seen as geometric problems and vice versa (i.e. solving an equation is the same as e.g. finding an intersection of some N-dimensional shapes).

[Fun](fun.md) fact: approaches in the opposite direction also exist, i.e. solving mathematical problems physically rather than by computation. For example [back in the day](back_then.md) when there weren't any computers to compute very difficult [integrals](integral.md) and computing them by hand would be immensely hard, people literally cut physical function plots out of paper and weighted them in order to find the integral. Awesome oldschool [hacking](hacking.md).

Anyway, **how does it work?** Typically we work in a 2D or 3D [Euclidean space](euclidean_space.md) with [Cartesian coordinates](cartesian_coords.md) (but of course we can generalize to more dimensions etc.). Here, geometric shapes can be described with [equations](equation.md) (or [inequalities](inequality.md)); for example a zero-centered circle in 2D with radius *r* has the equation *x^2 + y^2 = r^2*  ([Pythagorean theorem](pythagorean_theorem.md)). This means that the circle is a set of all points *[x,y]* such that when substituted to the equation, the equation holds. Other shapes such as [lines](line.md), [planes](plane.md), [ellipses](ellipse.md), [parabolas](parabola.md) have similar equations. Now if we want to find intersections/unions/etc., we just solve systems of multiple equations/inequalities and find solutions (coordinates) that satisfy all equations/inequalities at once. This allows us to do basically anything we could do with pen and paper such as defining helper shapes and so on. Using these tools we can compute things such as angles, distances, areas, collision points and much more.

Analytic geometry is closely related to [linear algebra](linear_algebra.md).

## Examples

**Nub example**:

Find the intersection of two lines in 2D: one is a horizontal line with *y* position 2, the other is a 45 degree line going through the [0,0] point in the positive *x* and positive *y* direction, like this:

```
  y
  :        _/ line 2
  :      _/
_2:_____/_______ line 1
  :  _/
  :_/
--:----------x
_/:
  :
```

The equation of line 1 is just *y = 2* (it consists of all points *[x,2]* where for *x* we can plug in any number to get a valid point on the line).

The equation of line 2 is *x = y* (all points that have the same *x* and *y* coordinate lie on this line).

We find the intersection by finding such point *[x,y]* that satisfies both equations. We can do this by plugging the first equation, *y = 2*, to the second equation, *x = y*, to get the *x* coordinate of the intersection: *x = 2*. By plugging this *x* coordinate to any of the two line equations we also get the *y* coordinate: 2. I.e. the intersection lies at coordinates *[2,2]*.

**Advanced nub example**:

Let's say we want to find, in 2D, where a line *L* intersects a circle *C*. *L* goes through points *A = [-3,0.5]* and *B = [3,2]*. *C* has center at *[0,0]* and radius *r = 2*.

The equation for the circle *C* is *x^2 + y^2 = 2^2*, i.e. *x^2 + y^2 = 4*. This is derived from [Pythagorean theorem](pythagorean_theorem.md), you can either check that or, if lazy, just trust this. Equations for common shapes can be looked up.

One possible form of an equation of a 2D line is a "slope + offset" equation: *y = k * x + q*, where *k* is the [tangent](tangent.md) (slope) of the line and *q* is an offset. To find the specific equation for our line *L* we need to first find the numbers *k* and *q*. This is done as follows.

The tangent (slope) *k* is *(B.y - A.y) / (B.x - A.x)*. This is the definition of a [tangent](tangent.md), see that if you don't understand this. So for us *k = (2 - 0.5) / (3 - -3) = 0.25*.

The number *q* (offset) is computed by simply substituting some point that lies on the line to the equation and solving for *q*. We can substitute either *A* or *B*, it doesn't matter. Let's go with *A*: *A.y = k * A.x + q*, with specific numbers this is *0.5 = 0.25 * -3 + q* from which we derive that *q = 1.25*.

Now we have computed both *k* and *q*, so we now have equations for both of our shapes:

- circle *C*: *x^2 + y^2 = 4*
- line *L*: *y = 0.25 * x + 1.25*

Feel free to check the equations, substitute a few points and plot them to see they really represent the shapes (e.g. if you substitute a specific *x* shape to the line equation you will get a specific *y* for it).

Now to find the intersections we have to solve the above system of equations, i.e. find such couples (coordinates) *[x,y]* that will satisfy both equations at once. One way to do this is to substitute the line equation into the circle equation. By this we get:

*x^2 + (0.25 * x + 1.25)^2 = 4*

This is a [quadratic equation](quadratic_equation.md), let's get it into the standard format so that we can solve it:

*x^2 + 0.0625 * x^2 + 0.625 * x + 1.5625 = 4*

*1.0625 * x^2 + 0.625 * x - 2.4375 = 0*

Note that this makes perfect sense: a quadratic equation can have either one, two or no solution (in the realm of [real numbers](real_number.md)), just as there can either be one, two or no intersection of a line and a circle.

Solving quadratic equation is simple so we skip the details. Here we get two solutions: *x1 = 1.24881* and *x2 = -1.83704*. These are the x position of our intersections. We can further find also the y coordinates by simply substituting these into the line equation, i.e. we get the final result:

- intersection 1: *[1.24881, 1.5622025]*
- intersection 2: *[-1.83704, 0.79074]*

## See Also

- [linear algebra](linear_algebra.md)