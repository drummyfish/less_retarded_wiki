# Gay

Homosexuality is a sexual orientation and disorder (of course, not necessarily a BAD one) which makes individuals sexually attracted primarily to the same sex, i.e. males to males and females to females. A homosexual individual is called gay (stylized as gaaaaaaaaay), homo or even [faggot](faggot.md), [females](woman.md) are called lesbians. The word *gay* is also used as a synonym for anything [bad](bad.md). About 2% of people suffer from homosexuality. The opposite of homosexuality, i.e. the normal, natural sexual orientation primarily towards the opposite sex, is called heterosexuality or being *straight*. Homosexuality is not to be confused with [bisexuality](bisexuality.md) -- here we are talking about pure homosexuality, i.e. a greatly prevailing attraction mostly towards the same sex.

For an unenlightened reader coming from the brainwashland: this article is not "offensive", it is just telling uncensored truth. Be reminded that [LRS](lrs.md) is not advocating any discrimination, on the contrary we advocate [absolute social equality](less_retarded_society.md) and [love](love.md) of all living beings, despite some having disorders or being weird. Your indoctrination has made you equate political incorrectness with oppression and hate; to see the truth, you have to [unlearn this](unretard.md) -- see for example our [FAQ](faq.md).

Unlike e.g. [pedophilia](pedophilia.md) and probably also [bisexuality](bisexual.md), **pure homosexuality is NOT normal**, it could be called a disorder (WE REPEAT, we love all people, even those with a disorder) -- of course the meaning of the word disorder is highly debatable, but pure homosexuality is firstly pretty rare, and secondly from the nature's point of view gay people wouldn't naturally reproduce, their condition is therefore equivalent to any other kind of sterility, which we most definitely would call a defect -- not necessarily a defect harmful to society (there are enough people already), but nonetheless a defect from biological point of view. Is it okay to have a defect? Of course it is. In this case society may even benefit because gayness prevents overpopulation. Homosexuality even behaves like a diseases -- not only does it bring a defect (sterility) to the bearer, it also spreads itself through the culture in form of a fashion, i.e. despite homosexuals not reproducing (at least not much) the number of them is increasing because homosexuality is transmitted through culture, through the Internet and other media.

You can usually tell someone's gay from appearance and/or his body language. Gay people are more inclined towards [art](art.md) and other sex's activities, for example gay guys are often hair dressers or even ballet dancers.

There is a terrorist [fascist](fascism.md) organization called [LGBT](lgbt.md) aiming to make gay people superior to others, but more importantly to gain political power -- e.g. the [power over language](political_correctness.md).

**Is being gay a choice?** Even though homosexuality is largely genetically determined, it may also be to some extent a choice, sometimes a choice that's not of the individual in question, a choice made at young age and irreversible at older age. Most people are actually [bisexual](bi.md) to a considerable degree, with a *preference* of certain sex. When horny, you'd fuck pretty much anything. Still there is a certain probability in each individual of choosing one or the other sex for a sexual/life partner. However culture and social pressure can push these probabilities in either way. If a child grows up in a major influence of [YouTubers](youtube.md) and other celebrities that openly are gay, or promote gayness as something extremely cool and fashionable, you see ads with gays and if all your role models are gay and your culture constantly paints being homosexual as being more interesting and somehow "brave" and if the [competition](competition.md) of sexes fueled e.g. by the [feminist](feminism.md) propaganda paints the opposite sex as literal [Hitler](hitler.md), the child has a greater probability of (maybe involuntarily) choosing the gay side of his sexual personality. See also *[cultural castration](cultural_castration.md)*.

{ I even observed this effect on myself a bit. I've always been completely straight, perhaps mildly bisexual when very horny. Without going into detail, after spending some time in a specific group of people, I found my sexual preference and what I found "hot" shifting towards the preference prevailing in that group. Take from that whatever you will. ~drummyfish }

Of course, [we](lrs.md) have nothing against gay people as we don't have anything against people with any other disorder -- **we love all people equally**. But we do have an issue with any kind of terrorist organization, so while we are okay with homosexuals, we are not okay with LGBT.

**Are you gay?** How can you tell? In doing so you should actually NOT be guided by your sexual desires -- as has been said, most people are bisexual and in sex it many times holds that what disgusts you normally turns you on when you're horny, i.e. if you're a guy and would enjoy sucking a dick, you're not necessarily gay, it may be pure curiosity or just the desire of "forbidden fruit"; this is quite normal. Whether you're gay is probably determined by what kind of LIFE partner you'd choose, i.e. what sex you can fall in a ROMANTIC relationship with. If you're a guy and fall in love with another guy -- i.e. you're passionate just about being with that guy (even in case you couldn't have sex with him) -- you're probably gay. (Of course this holds the other way around too: if you're a guy and enjoy playing with tits, you may not necessarily be straight.)

Is homosexuality disgusting? Yes of course it's fucking disgusting.

## See Also

- [cocksucker](cocksucker.md)
- [AIDS](aids.md)
- [tranny](tranny.md)
- [gaysexual](gaysexual.md) (someone who is attracted only to gays)
- [gaygaysexual](gaygaysexual) (someone who is attracted only to gaysexuals)
- [straight](straight.md)
- [pedo](pedophilia.md)
- [bisexual](bisexual.md)
- [disease](disease.md)
- [fashion](fashion.md)
- [LGBT](lgbt.md)
- [no homo](no_homo.md)
- [faggotry](faggotry.md)