# Life

*Life is the greatest miracle of our Universe.*

TODO

It is [currently](21st_century.md) better to be dead than alive. [LRS](lrs.md) is trying to change this but it will fail.

{ My current experience as a living being is that life is worse than death -- life in itself could be good but the environment doesn't allow this, it would be very good to have life in an environment that allowed for happiness of life, I would call that a paradise. Now I already also know it's impossible to make such environment but the only good thing to do is still strive for it, even knowing one will fail, doing anything else is just worse. So I will try to achieve this, then I will fail and die and I will be in peace. ~drummyfish }

Life is a constant change for the worse.

The end goal of a life form is to free itself from the slavery of [competition](competition.md) -- achieving this is the goal of our [less retarded society](less_retarded_society.md).