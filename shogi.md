# Shogi

Shogi, also called *Japanese chess*, is an old Asian board [game](game.md), very similar to [chess](chess.md), and is greatly popular in Japan, even a bit more than [go](go.md), the second biggest Japanese board game. Shogi is yet more complex (and [bloated](bloat.md)) than chess, has a bigger board, more pieces and more complex rules that besides others allow pieces to come back to play; for a chess player shogi is not that hard to get into as the basic rules are still very similar, and it may offer a new challenge and experience. Also similarly to chess, [go](go.md), [backgammon](backgammon.md) and similar board games, [LRS](lrs.md) sees shogi as one of the best games ever as it is legally not owned by anyone (it is [public domain](public_domain.md)), is relatively [simple](kiss.md), cheap and doesn't even require a computer to be played. The [culture](culture.md) of shogi is also different from that of chess, there are many rituals connected to how the game is conducted, there are multiple champion titles, it is not common to offer draws etc.

{ Lol apparently (seen in a YT video) when in the opening one exchanges bishops, it is considered [rude](unsportmanship.md) to promote the bishop that takes, as it makes no difference because he will be immediately taken anyway. So ALWAYS DO THIS to piss off your opponent and increase your chance of winning :D ~drummyfish }

**Quick sum up for chess players:** Games are longer. When you get back to chess from shogi your ELO will bump 100 points as it feels so much easier. Pawns are very different (simpler) from chess, they don't take sideways so forget all you know about pawn structure (prepare for bashing your head thinking a pawn guards something, then opponent takes it and you realize you can't retake :D just write gg and start a new game). The drop move will fuck up your brain initially, you have to start considering that opponent can just smash his general literally in front of your king and mate you right there { still fucking happens to me all the time lol :D ~drummyfish }. Exchanges and sacrifices also aren't that simple as any piece you sacrifice YOU GIVE TO THE OPPONENT, so you better not fuck up the final attack on the king or else the opponent just collects a bunch of your pieces and starts his own attack right in your base by dropping those pieces on your king right from the sky. You have to kill swiftly and precisely, it can turn over in an instant. There is no castling (but king safety is still important so you castle manually). Stalemate is a loss (not a draw) but it basically never happens, Japanese hate draws, draws are rare in shogi.

The game's disadvantage and a barrier for entry, especially for westeners, is that the **traditional design of the shogi pieces sucks big time**, for they are just same-colored pieces of wood with Chinese characters written on them which are unintelligible to anyone non-Chinese and even to Chinese this is greatly visually unclear -- all pieces just look the same on first sight and the pieces of both player are distinguished just by their rotation, not color (color is only used in amateur sets to distinguish normal and promoted pieces). But of course you may use different, visually better pieces, which is also an option in many shogi programs -- a popular choice nowadays are so called *international* pieces that show both the Chinese character along with a simple, easily distinguishable piece symbol. There are also sets for children/beginners that have on them visually indicated how the piece moves.

## Rules

As with every game, rules may slightly differ here and there, but generally they are in principle similar to those of [chess](chess.md), with some differences and with different pieces. The **goal** of the game is to deliver a checkmate to the opponent's king, i.e. make him unable to escape capture (same as in chess). The details are as follows.

Shogi is played on a 9x9 rectangular board: the squares are not square in shape but slightly rectangular and they all have the same color. There are two players: **sente** (plays first) and **gote** (plays second); sente is also sometimes called black and gote white, like in chess (though unlike in chess black starts first here), but the pieces actually all have the same color (as they can be exchanged).

The pieces are weird pentagonal arrow-like shapes that have on them written a Chinese character identifying the piece; on the other side there is a symbol representing the promoted version of the piece (i.e. if you promote, you turn the piece over). The arrow of the piece is turned against the enemy and this is how it is distinguished which player a piece belongs to.

The table showing all the types of pieces follows. The movement rules are same as in chess, i.e. pieces cannot jump over other pieces except for the knight. (F, R, B, L mean forward, right, bottom, left.)

| piece          |symbol|letter|~value|  move rules                  | comment                                                           |
| -------------- | ---- | ---- | ---- | ---------------------------- | ----------------------------------------------------------------- |
| pawn           | 歩   | P    | 1    | 1 F                          | also takes forward (not complicated like in chess)                |
| lance          | 香   | L    | 4    | F (any distance)             | can't go backwards or sideways, just forward!                     |
| knight         | 桂   | N    | 5    | 2 F., then 1 L or R          | similar to knight in chess, only one that jumps over pieces       |
|silver general  | 銀   | S    | 7    | 1F1L, 1F, 1F1R, 1B1L, 1B1R   | like king but can't go directly back, left or right               |
|gold general    | 金   | G    | 8    | 1F1L, 1F, 1F1R, 1L, 1R, 1B   | similar to silver but has 6 squares (s. only has 5), can't promote|
| bishop         | 角   | B    | 11   | diagonal (any distance)      | same as bishop in chess                                           |
| rook           | 飛   | R    | 13   | horiz./vert. (any distance)  | same as rook in chess                                             |
| promoted pawn  | と   | +P   | 10   | like gold general            |more valuable than gold because when captured, enemy only gets pawn|
|promoted lance  | 杏   | +L   | 9    | like gold general            |                                                                   |
|promoted knight | 圭   | +N   | 9    | like gold general            |                                                                   |
|promoted silver | 全   | +S   | 9    | like gold general            |                                                                   |
|p. bishop       | 馬   | +B   | 15   | like both king and bishop    | can now move to other set of diagonals!                           |
|p. rook (dragon)| 龍   | +R   | 17   | like both king and rook      |                                                                   |
| king           | 王   | K    | inf  | any neighboring 8 squares    | same as king in chess, can't promote                              |

At the beginning the board is set up like this:

```
  9 8 7 6 5 4 3 2 1
  _________________
 |L N S G K G S N L| a   gote      promotion
 |. R . . . . . B .| b  (white)      zone
 |P P P P P P P P P| c ----------------------
 |. . . . . . . . .| d
 |. . . . . . . . .| e
 |. . . . . . . . .| f
 |p p p p p p p p p| g ----------------------   
 |. b . . . . . r .| h   sente     promotion
 |l n s g k g s n l| i  (black)      zone
  """""""""""""""""
```

So called *furigoma* is used to decide who starts (has the black pieces): one player throws 5 pawn pieces, if the number of unpromoted pawns ending up facing up is higher than the number of promoted ones, the player who tossed starts.

Then the players take turns in making moves, one can either:

- Move one own piece (according to its movement rules), possibly **capturing** (taking) one enemy piece by landing on it. If a piece is captured, it goes to the hand of that who captured it. After making this move the moved piece MAY (or may not) be promoted if: it can be **promoted** (as some pieces can't promote) AND its movement path went through the enemy promotion zone (his three starting rows, for example if the piece entered the zone or left it or moved within it). Promotion is mandatory if otherwise the piece would be unable to ever move again (e.g. a pawn entering the last row has to be always promoted). OR
- **Drop** one of the held pieces (captured from the enemy's army) anywhere on an empty square as a new piece of the player who drops it. After a drop the piece CANNOT be immediately promoted (it has to move to be promoted). One only cannot drop a piece so that it wouldn't ever have any legal move (e.g. a pawn to the last row), also a pawn mustn't be dropped into a column where there already is an unpromoted friendly pawn (there may only ever be at most one unpromoted pawn of each player in any column) AND one mustn't deliver an immediate checkmate by dropping a pawn (but can deliver a check etc.).

If a piece is immediately endangering the enemy king (so that it could capture it the next turn), a **check** happens. The player in check has to immediately avoid it, i.e. make a move that makes his king not be endangered by any enemy piece. If he cannot do that, he got **checkmated** and lost.

TODO

## Playing Tips

TODO

- Opening: moving the pawn right-up from your bishop seems to be the best first move, also most commonly played on top level.
- ...


## See Also

- [chess](chess.md)
- [go](go.md)
- [xiangqi](xiangqi.md)
- [backgammon](backgammon.md)
