# Whale

In online [pay to win](pay_to_win.md) [games](game.md) a whale is a player who spends enormous sums of money, much more than most of other players combined. They buy the most expensive items in the game stores daily, they highly engage in [microtheft](microtheft.md) and may throw even thousands of dollars at the game every day (cases of players spending over 1 million dollars on a casual game are known). In the playerbase there may exist just a handful of whale players but those are normally the main source of income for the game so that the developers actually focus almost exclusively on those few players, they craft the game to "catch the whales". The income from the rest of the players is negligible -- nevertheless the non-whales also play an important role, they are an attraction for the whales, they are there so that they can be owned by the whale players.

Existence of whale players is one of the demonstrations of the [pareto principle](pareto.md) (80/20 rule): 80% of the game's income comes from 20% of the players, out of which, by extension, 80% again comes from the 20% and so on.

The terminology can be extended further: besides **whales** we may also talk about **dolphins** (mid-spending players) and **minnows** (low spending players). Extreme whales are sometimes called **white whales** or **super whales** (about 0.2% generating 50% of income).

In some games, such as [WoW](wow.md), players may buy multiple accounts and practice so called [multiboxing](multiboxing.md). This means they control multiple characters at once, often using [scripts](script.md) to coordinate them, which of course gives a great advantage. Though using scripts or "hacking" the game in similar ways is in other cases considered unacceptable [cheating](cheating.md) that results in immediate ban, for obvious reasons (money) developers happily allow this -- of course this just shows they don't give a single shit about fairness or balance, they only thing they care about is $$$profit$$$.

## See Also

- [pay to win](pay_to_win.md)
- [landwhale](landwhale.md)