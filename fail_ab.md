# Type A/B Fail

Type A and type B fails are two very common cases of failing to adhere to the [LRS](lrs.md) politics/philosophy by only a small margin. Most people don't come even close to LRS politically or by their life philosophy -- these are simply general failures. Then there a few who ALMOST adhere to LRS politics and philosophy but fail in an important point, either by being/supporting [pseudoleft](pseudoleft.md) (type A fail) or being/supporting [right](left_right.md) (type B fail). The typical cases are following (specific cases may not fully fit these, of course):

- **type A fail**: Is anticapitalist, anticonsumerist, may incline towards minimalism, supports [free software](free_software.md) and [free culture](free_culture.md), may even be a vegan, [anarchist](anarchism.md), [C](c.md) programmer etc., however falls into the trap of supporting [pseudoleft](pseudoleft.md), e.g. [LGBT](lgbt.md) or [feminism](feminism.md) and things such as censorship ("[moderation](moderation.md)", [COCs](coc.md)), "just violence and bullying" (violence against fascists, e.g. [antifa](antifa.md)), falls for memes such as "[Rust](rust.md) is the new [C](c.md)".
- **type B fail**: Is against [pseudoleft](pseudoleft.md) bullshit and propaganda such as political correctness, is a [racial realist](racial_realism.md), highly supports [suckless](suckless.md) software, hacking and minimalism to achieve high freedom, usually also opposes [corporations](corporation.md) and state, however falls into the trap of being a [fascist](fascism.md), easily accepts violence, believes in "natural selection/wild west as a basis of society", supports and engages in [cryptocurrencies](crypto.md), believes in some form of [capitalism](capitalism.md) and that the current form of it can be "fixed" (["anarcho" capitalism](ancap.md) etc.)

Both types are furthermore prone to falling a victim to [privacy](privacy.md) obsession, [productivity](productivity_cult.md) obsession, [hero worshipping](hero_culture.md), use of violence, [diseases](disease.d) such as [distro hopping](distro_hopping.md), tech [consumerism](consumerism.md) and similar defects.

Type A/B fails are the "great filter" of the rare kind of people who show a great potential for adhering to LRS. It may be due to the modern western culture that forces a [right](right.md)-[pseudoleft](pseudoleft.md) false dichotomy that even those showing a high degree of non-conformance eventually slip into the trap of being caught by one of the two poles. These two fails seem to be a manifestation of an individual's true motives of [self interest](self_interest.md) which is culturally fueled with great force -- those individuals then try to not conform and support non-mainstream concepts like free culture or sucklessness, but eventually only with the goal of self interest. It seems to be extremely difficult to abandon this goal, much more than simply non-conforming. Maybe it's also the subconscious knowledge that adhering completely to LRS means an extreme loneliness; being type A/B fail means being a part of a minority, but still a having a supportive community, not being completely alone.

Even if someone has quite based views, he will 100% commit at least one of the following fatal errors:

- Even if he calls himself a "[pacifist](pacifism.md)", he will 100% make a "self defense" exception to this rule, which means he is actually not a pacifist, just wants to have that label.
- He'll support [privacy](privacy.md)/[security](security.md)/encryption, a form of [censorship](censorship.md) going against freedom of [information](information.md).
- He'll engage in [fight culture](fight_culture.md), will call his endeavor a "fight" or "battle" and by that will be on the edge of using violence or immoral means to "win" this fight.
- He'll be accepting [heroes and leader](hero_culture.md), i.e. social hierarchy, cults of personality and fascism.
- He will say he's not a nationalist but will support a "healthy" level of [nationalism](nationalism.md).
- He will oppose [defeatism](defeatism.md), [cynicism](cynicism.md), pessimism etc.
- He will think that [pedophilia](pedophilia.md) is bad and that pedophiles should be killed or at least "treated".
- He will be an [egoist](egoism.md), putting effort into looking certain way, getting [tattoos](tattoo.md), adopting some "style" or "[identity](identity_politics.md)".
- He will support good causes but for wrong reasons, for example "I'll help you because then next time you'll help me" (calculated decision based on self interest, not [selfless](selflessness.md) help) or "we shouldn't kill animals and eat them because it's not healthy and it contributes to destroying the planet" (again, calculated decision based on benefit of self and economic arguments, the only correct reason for not killing an animal is that it's a living being capable of feeling suffering). This matters because though at this moment his goals are aligned with our, tomorrow they may not be because he simply follows different interests and only by circumstance happened to momentarily go in the same direction as us.
- ...

{ Also he will be a [furry](furry.md), which is maybe not a fatal obstacle to LRS but it's pretty bad :D ~drummyfish }

A true LRS supporter mustn't fail at any of the above given points.

However these kinds of people may also pose a hope: if we could educate them and "fix their failure", the LRS community could grow rapidly. If realized, this step could even be seen as the main contribution of LRS -- uniting the misguided rightists and pseudoleftists by pointing out errors in their philosophies (errors that may largely be intentionally forced by the system anyway exactly to create the hostility between the non-conforming, as a means of protecting the system).

```

                  __
                .'  '.                  
               /      \                   drummyfish
            _.'        '._                    |
___....---''              ''---....___________v___
                               |           |
             normies           |    A/B    | LRS
               FAIL            |    fail   |
```

## See Also

- [idiot fallacy](idiot_fallacy.md)