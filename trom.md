# TROM

{ WIP, still being researched. Was trying to reach the Tio guy but he didn't respond. ~drummyfish }

TROM (The Reality Of Me) is a project running since 2011 that's educating mainly about the [harmfulness](harmful.md) of trade-based society and tries to show a better way: that of establishing so called [trade-free](trade_free.md) (in the sense of WITHOUT trade) society. It is similar to e.g. [Venus Project](venus_project.md) (with which TROM collaborated for a while), the [Zeitgeist Movement](zeigeist_movement.md) and our own [LRS](less_retaded_society.md) -- it shares the common core of opposing money and trade as a basis of society (which they correctly identify as the core issue), even though it differs in some details that we (LRS) don't see as wholly insignificant. The project website is at https://www.tromsite.com/.

TROM was started and is run by a Romanian guy called Tio (born June 12 1988, according to blog), he has a personal blog at https://www.tiotrom.com and now lives mostly in Spain. The project seems more or less run just by him.

The project is funded through [Patreon](patreon.md) and has created a very impressive volume of very good quality educational materials including books, documentaries, memes, even its own [GNU](gnu.md)/[Linux](linux.md) [distro](distro.md) ([Tromjaro](tromjaro.md)). The materials TROM creates sometimes educate about general topics (e.g. language), not just those directly related to trade-free society -- Tio says that he simply likes to learn all about the world and then share his knowledge.

The idea behind TROM is very good and similar to LRS, but just as the [Venus project](venus_project.md) it is a bit [sus](sus.md), we align with many core ideas and applaud many things they do, but can't say we 100% support everything about TROM.

## Summary

{ WATCH OUT, this is a work in progress sum up based only on my at the moment a rather brief research of the project, I don't yet guarantee this sum up is absolutely correct, that will require me to put in much more time. Read at own risk. ~drummyfish }

**good things about TROM**:

- It identifies trade/money/[capitalism](capitalism.md) as the root cause of most problems in society -- this we basically agree with (we officially consider [competition](competition.md) to be the root cause).
- It has TONS of very good quality educational materials exposing the truth, breaking corporate propaganda, even showing such things as corruption in [soyence](soyence.md) and [open source](open_source.md) etc. -- that's extremely great.

**bad things about TROM**: 

- It is based on [fight culture](fight_culture.md) and [hero culture](hero_culture.md) (judging by the content of the book *the origin of most problems*, creating superheroes and stating such things as "the world needs an enemy"), which we greatly oppose, we (LRS) don't believe a good and peaceful society can worship heroes and wars. Hopefully this is something the project may realize along the way, however at this point there is a great danger of falling into the trap of turning into a violent revolutionary [pseudoleftist](pseudoleft.md) movement.
- TROM refuses to use [free (as in freedom) licenses](free_culture.md)! Huge letdown. Tio gives some attempt at explanation at https://www.tiotrom.com/2022/08/free-software-nonsense/, but he doesn't seem to understand the concept of free culture/software very well, he's the "it don't need no license I say do whatever you want" guy.
- [Bloat](bloat.md), a lot of bloat everywhere, Javascript sites discriminating against non-consumerist computers. Tio isn't a programmer and seems to not be very aware of the issues regarding software, he uses the [Wordpress](wordpress.md) abomination with plugins he BUYS etc.
- There seems to be signs of [atheist](atheism.md)/iamverysmart/neil de grass overrationalism, some things are pretty cringe and retarded, see the following point.
- From Tio's blog TROM seems to be just his one-man project, even though he may employ a few helpers -- not that there's anything wrong about one man projects [:)](lrs.md) -- Tio is very determined, talented in certain areas such as "content creation" etc., however he dropped out of school and lacks a lot of knowledge and insight into important areas such as technology and free culture while talking about them as if he knew them very well, this is not too good and even poses some dangers. He really loves to write about himself, shows great signs of narcissism.  Hell, at times he seems straight from /r/iamverysmart, e.g. with this real cringe attempt at his own theory of black holes https://www.tiotrom.com/2021/11/my-theory-about-black-holes/. Of course it's alright to speculate and talk about anything, but sometimes it seems he thinks he's a genius at everything { Not wanting to make fun of anyone though, been there myself. :) ~drummyfish }. His eagerness is both his great strength and weakness, and as TROM is HIS project, it's the strength and weakness of it as well. Don't blindly follow TROM, takes the good out of it, leave the rest.

## See Also

- [Venus project](venus_project.md)
- [Trash Magic](trash_magic.md)