# Freedom

*Only when man loses everything he becomes actually free. Freedom is about letting go.*

Freedom denotes the possibility to act as one desires without suffering negative consequences.

TODO: more

People who seriously look for attaining mental/spiritual freedom often resort to [asceticism](asceticism.md), at least for a period of time (e.g. [Buddha](buddha.md)) -- this is very commonly not done with the intent of actually giving up all materialistic pleasures forevermore, but rather to let go of the [dependency](dependency.md) on the them, to know and see that one really can live without them if needed so that one becomes less afraid of losing them, which is often what internally enslaves us. Without even realizing it we are nowadays addicted to many things (games, social media, overeating, shiny gadgets, ...) like an alcoholic is to booze; it is not necessarily bad to drink alcohol, but it is bad to be addicted to it -- to free himself the alcoholic needs to abstain from alcohol for a long period of time. Our chains are often within ourselves: for example we often don't have the freedom to say what we want to say because that might e.g. ruin our career, preventing us from enjoying our expensive addictions -- once we don't worry about this, we gain the freedom to say what we want. Once you rid yourself of fear of jail, you gain the freedom to do potentially illegal things, and so on. Additionally going through the experience of letting go of pleasures very often opens up your eyes and mind, new thoughts emerge and one reevaluates what's really important in life.

Freedom is something promised by most (if not all) ideologies/movements/etc.; this is because without further specification the term is so wide it says very little -- the very basic thing to know is, of course, that **there is no such thing as general freedom**; one kind of freedom restricts other kinds of freedom -- for example so called freedom of market says that a rich capitalist is free to do whatever he wants, which leads to him enslaving people, killing the freedom of those people.

What kind of freedom is [LRS](lrs.md) interested in? Basically the freedom for living beings to do what makes them happy -- of course this can't be achieved 100% (if one desires to enslave others, their freedom would disappear), however we can get very close (make a [society](less_retarded_society.md) in which people don't wish to enslave others). For this goal we choose to support such freedoms as [free speech](free_speech.md), [free software](free_software.md), [free culture](free_culture.md), free love etc.

## See Also

- [FreeDoom](freedoom.md)
- [freedom distance](freedom_distance.md)