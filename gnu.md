# GNU

GNU (*"GNU is Not Unix"*, a [recursive](recursion.md) acronym) is a large software project established by [Richard Stallman](rms.md), the inventor of [free (as in freedom) software](free_software.md), running since 1983 with the goal of creating, maintaining and improving a completely free (as in freedom) [operating system](os.md), along with other free [software](software.md) that computer users might need. The project doesn't tolerate any [proprietary](proprietary.md) software (though it unfortunately tolerates other proprietary [data](data.md)). GNU achieved its goal of creating their free operating system when a [kernel](kernel.md) named [Linux](linux.md) became part of it in the 90s as the last piece of the puzzle -- the system should be called just GNU but is now rather known as GNU/Linux (watch out: most so called "Linux systems" nowadays aren't embraced by GNU as they diverge from GNU's strict policies on what the system should look like, only a handful of operating systems are recommended by GNU). However, the GNU project didn't end and continues to further develop the operating system, or rather a myriad of user software that runs under the operating system -- GNU develops a few of its projects itself and also offers hosting and support (such as free legal defense) for GNU projects developed by volunteers who dedicate their work to them. GNU gave rise to the [Free Software Foundation](fsf.md) and is one of the most important software projects in history of computing.

The mascot of GNU is literally gnu (wildebeest), it is available under a copyleft license. WARNING: ironically GNU is extremely protective of their brand's "intellectual property" and will rape you if you use the name GNU without permission (see the case of GNU boot). It's quite funny and undermines the whole project a bit.

The GNU/Linux operating system has several variants in a form of a few GNU approved "Linux" [ditributions](distro.md) such as [Guix](guix.md), [Trisquel](trisquel.md) or [Parabola](parabola.md). Most other "Linux" distros don't meet the strict standards of GNU such as not including any proprietary software. In fact the approved distros can't even use the standard version of [Linux](linux.md) because that contains proprietary [blobs](blob.md), a modified variant called [Linux-libre](linux_libre.md) has to be used.

**GNU greatly prefers [GPL](gpl.md) [licenses](license.md)**, i.e. it strives for [copyleft](copyleft.md) and largely recommends it, even though it will also accept projects under permissive licenses as those are still free. GNU also helps with enforcing these licenses legally and advises developers to transfer their [copyright](copyright.md) to GNU so that they can "defend" the software for them.

**If we still have a bit of freedom in computing nowadays, it is largely to GNU** -- this can't be stressed enough. But although GNU is great and has been one of the best things to happen in software ever, it also has **many flaws**, for example:

- **GNU programs are typically [bloated](bloat.md)** -- although compared to [Windows](windows.md) GNU programs are really light as a feather and though GNU programs are also in many cases (but not always) quite optimized, their source code, judged from strictly [suckless](suckless.md) perspective, is mostly huge, which many view as a big issue (it's a common theme, there are [jokes](joke.md) such as GNU actually meaning *Gigantic and Nasty but Unavoidable* and so on). This is likely because GNU chooses to [battle](fight_culture.md) proprietary programs, often by trying to beat them at their own game, so features are preferred over [minimalism](minimalism.md) to stay competitive.
- **GNU also doesn't mind proprietary non-functional data** (e.g. assets in video games). This goes against [free culture](free_culture.md) and many other free software groups, notably e.g. [Debian](debian.md). Justifications for this range from "data itself can't be harmful" (false), through "we just focus on software" to "we need GNU to be more popular" (i.e. compatible with proprietary games and so on). GNU is also generally **NOT supportive of [free culture](free_culture.md) and even uses copyright to prohibit modifications of their propaganda texts**: the [GFDL](gfdl.md) license they use for texts may contain sections that are prohibited from being modified and so are non-free by definition. They also try to "protect" their names, you can't use the name "GNU" without their permission and so on. This sucks big time and shows some of the movement's darker side.
- **GNU greatly pushes [copyleft](copyleft.md)**, which we, as well as many others, oppose.
- **GNU embraces complexity, plays the corporate game and rejects the true way of freedom through [minimalism](minimalism.md)**. GNU basically just makes a mantra of "license with 4 freedoms on every software" and will mostly ignore everything else, they'll just do whatever it takes to stick with the mantra, i.e. GNU tries to achieve popularity, it tries to [fight](fight_culture.md) corporations, gets into activism, it will abuse copyright -- basically GNU wants to become a "superpower of freedom", it doesn't mind hierarchy, state, control, it wants to replace corporations in holding the power over technology, naively believing that it will be using the power for good. That's why they embrace complexity and harmful ways of [capitalist software](capitalist_software.md) (e.g. "GUI in everything", "fuck Unix", ...), that is why they simply copy proprietary software 1 to 1, just with a free license, it helps them be popular (people can drop in replace their proprietary software with GNU software), it also helps them get a [monopoly](bloat_monopoly.md) they don't mind (remember, they even ask people to transfer their copyright to them) as they DO want to become a centralized superpower. Where corporations push JavaScript on websites, GNU will just try to make sure the JavaScript has a free license, instead of rejecting the idea of JavaScript on websites. Where a corporation makes a "smart home", GNU will try to do the same, just with free software, instead of rejecting such a dumb idea in the first place. Anyone who ever saw anything from [history](history.md) knows it's not possible for a good superpower to exist -- no matter how pure it starts, with power WILL come corruption no matter what, any superpower will ALWAYS become evil. The TRUE way of freedom is simply abolishing all superpowers, embracing minimalism and giving power to the people instead of trying to fix maximalism and believe a monopoly will somehow be good. Just take a look at [Wikipedia](wikipedia.md) as a recent example of how these things end. This philosophy is what helps GNU be big in short term but it's also what will kill it in the long term.
- ...

## History

The project officially started on September 27, 1983 by [Richard Stallman](rms.md)'s announcement titled *Free Unix!*. In it he expresses the intent to create a [free as in freedom](free_software.md) clone of the operating system [Unix](unix.md), and calls for people to join his effort (he also uses the term free software here). Unix was a good, successful de-facto standard operating system, but it was proprietary, owned by AT&T, and as such restricted by licensing terms. GNU was to be a similar system, compatible with the original Unix, but free as in freedom, i.e. freely available and allowing anyone to use it, improve it and share it.

In 1985 Richard Stallman wrote the GNU Manifesto, similar to the original project announcement, which further promoted the project and asked people for help in development. At this point the GNU team already had a lot of software for the new system: a text editor Emacs, a debugger, a number of utility programs and a nearly finished shell and [C](c.md) compiler ([gcc](gcc.md)).

At this point each program of the project still had its own custom license that legally made the software free as in freedom. The differences in details of these licenses however caused issues such as legal incompatibilities. This was addressed in 1989 by Richard Stallman's creation of a universal free software license: GNU General Public License ([GPL](gpl.md)) version 1. This license can be used for any free software project and makes these projects legally compatible, while also utilizing so called [copyleft](copyleft.md): a requirement for derived works to keep the same license, i.e. a legal mechanism for preventing people from making copies of a free project non-free. Since then GPL has become the primary license of the GNU project as well as of other unrelated projects.

## GNU Projects

GNU has developed an almost unbelievable amount of software, it has software for all basic and some advanced needs. As of writing this there are 373 software packages in the official GNU repository (at https://directory.fsf.org/wiki/Main_Page). Below are just a few notable projects under the GNU umbrella.

- [GNU Hurd](hurd.md) (OS [kernel](kernal.md), alternative to [Linux](linux.md))
- [GNU Compiler Collection](gcc.md) (gcc, compiler for [C](c.md) and other languages)
- [GNU C Library](glibc.md) (glibc, [C](c.md) library)
- [GNU Core Utilities](gnu_coreutils.md) (coreutils, basic utility programs)
- [GNU Debugger](gdb.md) (gdb, [debugger](debugger.md))
- [GNU Binary Utilities](gnu_binutils.md) (binutils, programs for working with binary programs)
- [GNU Chess](gnu_chess.md) (strong [chess](chess.md) engine)
- [GNU Go](gnu_go.md) ([go](go.md) game engine)
- [GNU Autotools](autotools.md) ([build system](build_system.md))
- [CLISP](clisp.md) (common [lisp](lisp.md) language)
- GNU Pascal ([pascal](pascal.md) compiler)
- [GIMP](gimp.md) (image manipulation program, a "free [photoshop](photoshop.md)")
- GNU Emacs ([emacs](emacs.md) text editor)
- [GNU Octave](octave.md) ([mathematics](math.md) software, "free Matlab")
- [GNU Mediagoblin](mediagoblin.md) (decentralized file hosting on the [web](web.md))
- GNU Unifont ([unicode](unicode.md) font)
- [GNU Privacy Guard](gpg.md) (gpg, OpenPGP encryption)
- [GNU Scientific Library] (GSL, a nice [C](c.md) library of mathematical functions)
- [GNU Collaborative International Dictionary of English](gcide.md)
- ...

## See Also

- [Free Software Foundation](fsf.md)
- [Richard Stallman](rms.md)
- [GNG](gng.md) (GNG is Not GNU)
- [GNAA](gnaa.md)
- [copyleft](copyleft.md)
- [free software](free_software.md)