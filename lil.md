# LIL

*There is an old language called LIL (little implementation language), but this article is about a different language also called LIL (little interpreted language by Kostas Michalopoulos).*

Little interpreted language (LIL) is a very nice [suckless](suckless.md), yet practically unknown interpreted [programming language](programming_language.md) by Kostas Michalopoulos which can very easily be embedded in other programs. In this it is similar to [Lua](lua.md) but is even more simple: it is implemented **in just two [C](c.md) source code files** (lil.c and lil.h) that together count about 3700 [LOC](loc.md). It is provided under [zlib](zlib.md) [license](license.md). More information about it is available at http://runtimeterror.com/tech/lil.

{ LIL is relatively amazing. I've been able to make it work on such low-specs hardware as Pokitto (32 kB RAM embedded). ~drummyfish }

LIL has two implementations, one in [C](c.md) and one in [Free Pascal](free_pascal.md), and also comes with some kind of [GUI](gui.md) and [API](api.md).

The language design is very nice, its interesting philosophy is that **everything is a string**, for example arithmetic operations are performed with a function `expr` which takes a string of an arithmetic expression and returns a string representing the result number.

For its simplicity there is no [bytecode](bytecode.md) which would allow for more efficient execution and [optimization](optimization.md).

TODO: example

{ I've been looking at the source and unfortunately there are some imperfections. The code uses [goto](goto.md) (may not be bad but I dunno). Also unfortunately stdlib, stdio, string and other standard libraries are used as well as [malloc](malloc.md). The code isn't really commented and I find the style kind of hard to read. }

## See Also

- [comun](comun.md)
- [Oberon](oberon.md)