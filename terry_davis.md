# Terry Davis

*"An idiot admires complexity, a genius admires simplicity."* --Terry Davis

Terry A. Davis, aka the *divine intellect*, born 1969 in Wisconsin, was a genius+[schizophrenic](schizo.md) [programmer](programming.md) that singlehandedly created [TempleOS](temple_os.md) in his own [programming language](programming_language.md) called [HolyC](holyc.md), and greatly entertained and enlightened an audience of followers until his tragic untimely death. For his programming skills and quality videos he became a legend and a [meme](meme.md) in the tech circles, especially on [4chan](4chan.org) which additionally valued his [autistic](autism.md) and [politically incorrect](political_correctness.md) behavior.

He was convinced he could talk to [God](god.md) and that God commanded him to make an operating system with certain parameters such as 640x480 resolution, also known as the God resolution. According to himself he was gifted a *divine intellect* and was, in his own words, the "best programmer that ever lived". Terry was making [YouTube](youtube.md) talking/programming videos in which God was an often discussed topic, alongside valuable programming advice and a bit of good old [racism](racism.md). He was also convinced that the government was after him and often delved into the conspiracies against him, famously proclaiming that **"CIA [niggers](nigger.md) glow in the dark"** ("glowing in dark" subsequently caught on as a phrase used for anything [suspicious](sus.md)). He was in mental hospital several times and later became homeless, but continued to post videos from his van. An entertaining fact is also that he fell in love with a famous female physics YouTuber Dianna Cowern which he stalked online. In 2018 he was killed by a train (officially a [suicide](suicide.md) but word has it CIA was involved) but he left behind tons of videos full of endless entertainment, and sometimes even genuine [wisdom](wisdom.md).

Terry, just as [us](lrs.md), greatly valued [simplicity](minimalism.md) and [fun](fun.md) in programming, he was a low-level programmer and saw that technology went to [shit](shit.md) and wanted to create something in the oldschool style, and he expressed his will to dedicate his creation to the [public domain](public_domain.md). This is of course extremely based and appreciated by [us](lrs.md) (though the actual public domain dedication wasn't executed according to our recommendations).

## See Also

- [schizo](schizo.md)
- [Henryk Lahola](henryk_lahola.md)
- [drummyfish](drummyfish.md) :D
- [Jesus](jesus.md)
- [RMS](rms.md)
- [Time Cube](time_cube.md)


