# LGBT

*This article is a part of series of articles on [fascism](fascism.md).*

LGBT, LGBTQ+, LGBTFURRYTRANSFAGCOCKS (lesbian, [gay](gay.md), [bisexual](bisexual.md), [transsexual](tranny.md), "[queer](queer.md)" and whatever is yet to be invented), also FGTS or TTTT (transsexual transsexual transsexual transsexual) is a toxic [pseudoleftist](pseudoleft.md) [fascist](fascist.md) political group whose ideology is based on superiority of certain selected minority sexual orientations. They are a highly [violent](violence.md), [toxic](toxic.md), [bullying](bully.md) movement (not surprisingly centered in the [US](usa.md) but already spread around the whole world) practicing [censorship](censorship.md), Internet lynching ([cancel culture](cancel_culture.md)), discrimination, spread of extreme [propaganda](propaganda.md), harmful [lies](soyence.md), culture poison such as [political correctness](political_correctness.md) and other [evil](evil.md). LGBT uses a flag with all colors of puke.

LGBT is related to the concept of equality in a similar way in which crusade wars were related to the nonviolent teaching of [Jesus](jesus.md), it shows how an idea can be completely twisted around and turned on its head as to be left completely contradicting its original premise.

Note that **not all gay people support LGBT**, even though LGBT wants you to think so and media treat e.g. the terms *gay* and *LGBT* as synonyms (this is part of [propaganda](propaganda.md), either conscious or subconscious). The relationship gay-LGBT is the same as e.g. the relationship White-WhitePride or German-Nazi: Nazis were a German minority that wanted to [fight](fight_culture.md) for more privileges for Germans of their own race (as they felt oppressed by other nations and races such as Jews), LGBT is a gay minority who wants to [fight](fight_culture.md) for more privileges for gay people (because they feel oppressed by straight people). LGBT isn't just about being gay but about approving of a very specific ideology that doesn't automatically come with being gay. LGBT frequently comments on issues that go beyond simply being gay (or whatever), for example LGBT openly stated disapproval of certain other orientation (e.g. [pedophilia](pedophilia.md)) and refuses to admit homosexuality is a disorder, which aren't necessarily stances someone has to take when simply being gay.

**LGBT is greatly embraced by [capitalism](capitalism.md)** as it serves it well, it creates more conflict and [competition](competition.md) that business feeds on, new areas of [bullshit](bullshit.md) business (gay movies, gay music, gay books, gay games, gay speakers, gay merchandise, antidepressants for people that are depressed for not being gay enough, ...), attention grabbing potential (LOOK LOOK AT OUR AD, WE SUPPORT GAYS), political capital to be mined (CEO wants more money and power so he wants to get into politics? Starting a gay supporting party is an awesome tool.) etcetc. Indeed, as of 2024 literally every ad you see is woke and gay, capitalism just loves this new "rebel movement".

**LGBT is a cult** that managed to actually get mainstream and embraced by the ruling powers ([states](state.md) and [corporations](corporation.md)) -- their [pseudoscience](soyence.md), called "[gender studies](gender_studies.md)", is not unlike the hilarious "science" of Ancient Aliens, the LGBT theories are not unlike the Nazi theories about underground Jewish societies secretly ruling the world. Just like some see everything a work of aliens or Jews, LGBT sees a secret gender oppression in everything, in 100 years old child cartoons, in primitive video games like [pacman](pacman.md), in colors of the butterfly wings, everything has a secret straight cis male oppression message embedded within it. If you ever wondered what it would look like if Scientology took over the world or if Nazis won the world war^([Hitler comparison committed but rightfully so]), you don't have to wonder anymore, it's right here (chances are just that you don't see it just as you wouldn't see Scientology as weird if you grew up in a culture completely controlled by it).

Gay fascists furthermore live off of attention so they love to wear bizarre clothes in all existing AND nonexisting [colors](color.md) at once, further combined with ugly hairstyles and [tattoos](tattoo.md) so that they literally look like clowns from mental asylum or that creepy McDonald's mascot. They also love to show their genitalia in the streets -- though they are pedophobes, they think it's a peer reviewed fact that it's natural for a child to see mommy have threesome with her frens.

LGBT works towards establishing [newspeak](newspeak.md) and [though crime](though_crime.md), their "pride" parades are not unlike military parades, they're meant to establish fear of their numbers. LGBT targets children and young whom their propaganda floods every day with messages like *"being gay makes you cool and more interesting"* so that they have a higher probability of developing homosexuality to further increase their ranks in the future. They also push the idea of children having same sex parents for the same reason.

LGBT oppose [straight](straight.md) people as they solely focus on gaining more and more ["rights"](rights_culture.md) and power only for their approved orientations. They also highly bully other, unpopular sexual orientations such as [pedophiles](pedophilia.md) (not necessarily child rapists), [necrophiles](necro.md) and [zoophiles](zoophilia.md), simply because supporting these would hurt their popularity and political power. They label the non-approved orientations a "disorder", they push people of such orientations to [suicide](suicide.md) and generally just do all the bad things that society used to do to gay people in the past -- the fact that these people are often gay people who know what it's like to be bullied like that makes it this even much more sad and disgusting. To them it doesn't matter you never hurt anyone, if they find some [loli](loli.md) images on your computer, you're gonna get lynched mercilessly.

In the world of technology they are known for supporting [toxic](toxic.md) [codes of conduct](coc.md) in [FOSS](foss.md) projects (so called [tranny software](tranny_sw.md)), they managed to push them into most mainstream projects, even [Linux](linux.md) etc. Generally they just killed [free speech](free_speech.md) online as well as [in real life](irl.md), every platform now has some kind of surveillance and censorship justified by "preventing offensive speech". They cancelled [Richard Stallman](rms.md) for merely questioning a part of their gospel. They also managed to establish things like "diversity" quotas in Hollywood that only allow Oscars to be given to movies made by specific number of gays, lesbians etc., and they started to insert gay characters into fairy tales and movies for children (Toy Story etc.) xD This is literally the same kind of cheap but effective propaganda Nazi Germany employed on children; it's just that now after nationalism has been demonized after the world war we replaced nationalism with gender identity, an exactly same thing in principle just with a different name. Apparently in the software development industry it is now standard to pretend to be a tranny on one's resume so as to greatly increase the chance of being hired for diversity quotas xD WTF if I didn't live in this shitty world I wouldn't believe that's even possible, in a dystopian horror movie this would feel like crossing the line of believability too far [lmao](lmao.md).

In the non-technological world they are known for example, besides others, for destroying all [art](art.md) by giving everything a twisted sexual context, for example there's now a retroactively injected LGBT propaganda in child stories like [Harry Potter](harry_potter.md): children reading about the old, wise Dumbledore now also have to read the asterisks about how they/thems is in fact a hexadecimal non fluid that had oral sex with Severus Snape, which is of course not relevant to the story at all, it's there to just compensate for the fact that he's a white male, so he can't at all be straight because he's supposed to represent good (straight white males can only represent evil [nowadays](21st_century.md)).

From the point of view of the evolutionary system of [capitalism](capitalism.md) LGBT is a very successful organism that adopted to live in symbiosis with [corporations](corporation.md): it is an entity spawning attention and grounds for business on which corporations can feast for which it is in return supported and fed by corporations, with the power of their [marketing](marketing.md) and propaganda, that's why you see every corporation go woke -- corporations even let them be entered by LGBT, they implement [codes of censorship](coc.md), they discriminate against LGBT opposition etc., they protect their milking cows. These organisms have common goals such as increasing the means of [censorship](censorship.md) and thought control on which they conspire and collaborate -- an LGBT tranny calling for censorship will be backed by corporations on Twitter, in turn a corporation setting up censorship technology justified by protecting LGBT will be backed by LGBT influencers and so on.

## Summary

```
BEEP BEEP NORTON ANITIVIRUS DETECT CONSPIRACY THEORY IN
YOUR WEB BROWSER, STOP READING RIGHT NOW! PLEASE, PLEASE
DON'T READ IT PLEASE! Browse will auto destruct in 10
seconds to protect you from badthink.
```

The following diagram captures roughly how LGBT operates and how it is able to get so much power, at the time of writing this article:

```
LGBT:
  push: gay marriage, "we just want rights"
    DONE, then push: gay pair child adoption (now more aggressivity can be used)
      => women don't need men anymore => mutual support with feminism strengthens
      => children raised by two women or two gay (feminine) men (still "better" than normal father)
        => boys will likely feel more feminine
          => more future gays/trans, increase ranks
          => more (trans) women => mutual support with feminism strengthens
        => children in depression, mental health and sexuality issues
          => "maybe it's because you're in wrong body"
            => gender switch: increase ranks
          => feeds psychiatry business: strengthens mutual support
  push + normalize: cancel culture, internet lynching
    => strengthens fear of LGBT
    => increase manipulation/brainwashing potential
    future: normalize physical violence
    ...
  infect culture:
    establish pride/parades
      => increase fanaticism, hysteria and fascist thinking
        => more conflict: mutual support with capitalism and politics strengthens
      => strengthen fear of LGBT
      => establish gayness as fashion, "modern", "cool", "interesting"
        => get attention capital
          => buy corporate support
          => buy politicians
      ...
    make big deal out of sex
      => creates more stigma around sex and sexual failures
        => more conflict, i.e. business, strengthen mutual support with capitalism and politicians
        => more depressed people
          => feed psychiatry business, strengthen mutual support etc.
    "coming outs"
      => reward with hero status, people will want to be heroes
      => reward with popularity (money, fame, ...) on social media
        => generate media activity: mutual support with capitalism strengthens
      ...
    infect media, "art" (child cartoons, books, Internet, ...)
      make "gender issues" constantly on minds of people
        => more conflict, depressions, business, politics, capital etc.
      promote gayness as cool, straightness as boring and/or enemy
        => push people into gayness, increase ranks
    infect science, e.g. medicine
      => allow prepubescent children to decide for irreversible gender change
        => will mostly lead to regret and depression but increases ranks
          => additionally feeds psychiatry business: strengthens mutual support
            => psychiatry opens up to propaganda, e.g. removing the "disorder" word, supporting the harmful gender switch
    establish that "being gay = supporting LGBT"
    ...
    => then infect laws
      => mandatory gay quotas: get LGBT people into positions of power
      => use law for censorship
      => use law for getting rid of opposition
      ...
  push + normalize: censorship
    => increase manipulation/brainwashing potential => mutual support with corporations strengthen
      (now negative reviews = "hate speech", e.g. "dislike buttons on YouTube", banning negative
       reviews stops people preventing corporations from implementing artificial obsolescence as
       only positive reviews of shitty products are now allowed etc.)
      => get corporate marketing support
        => more popularity = power
    => allow hiding lies, damage and unethical behavior of LGBT
    ...
  invent + push newspeak:
    => increase manipulation/brainwashing potential
    => enable easy loyalty check (not politically correct = enemy)
    => spawn new bullshit businesses: mutual support with capitalism strengthens
    ...
  apply brainwashing:
    => gay influencers, streamers, ...
    => make being gay/queer a fashion: push children into gayness
    => censor old non-gay media
    => push gay/queer characters into child media: push children into gayness
      => more gays: increase ranks
    ...
  loyalty checks:
    - If you're not with us, you're against us.
    - Prove NOW that you're with us: use newspeak, use our flag, establish quotas, ...
    - If you're not doing things our way (using newspeak, COCs, supporting our means, ...), you're against us.
    - If you're against us, we'll use all the normalized means (bullying, violence, ...) against you.
    => discover and psychologically break resisting individuals (OR otherwise remove them)
  official goal, stage 1: we only want equality
    society agrees; then change to phase 2: we actually want justice (revenge)
      => apply violence to opposition
      => strive for superiority
  ...
```