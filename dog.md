# Dog

Here is the dog! He doesn't judge you; dog [love](love.md) is unconditional. No matter who you are or what you ever did, this buddy will always love you and be your best friend <3 By this he is giving us a great lesson. The doggo lives on the [island](island.md).

He loves when you pet him and take him for walks, but most of all he probably enjoys to play catch :) Throw him a ball!

Send this to anyone who's feeling down :)

```
         __
  _     /  \
 ((    / 0 0)
  \\___\/ _o)
  (        |  WOOOOOOOF
  | /___| |(
  |_)_) |_)_)
```

Dog is the symbol of [cynics](cynicism.md).

## See Also

- [god](god.md)
- [woof](woof.md)
- [bark](bark.md)
- [Puppy Linux](puppy.md)
- [watchdog](watchdog.md)
- [cat](cat.md)
- [mouse](mouse.md)
- [bug](bug.md)
- [duck](duck.md)
- [catdog](catdog.md)
- [whale](whale.md)
- [dogma](dogma.md), aka a man's second best friend
- [cynicism](cynicism.md)
- [pepe](pepe.md)