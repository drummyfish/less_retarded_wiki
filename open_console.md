# Open Console

{ Open consoles are how I got into [suckless](suckless.md) programming, they taught me about the low-level, optimizations and how to actually program efficiently on very limited hardware. I recommend you grab one of these. ~drummyfish }

Open consoles (also indie handhelds etc.) are tiny [GameBoy](gameboy.md)-like [gaming](game.md) consoles mostly powered by [free software](free_software.md) and [free hardware](free_hardware.md), which have relatively recently (some time after 2015) seen a small boom. Examples include [Arduboy](arduboy.md), [Pokitto](pokitto.md) or [Gamebuino](gamebuino.md). These are **NOT** to be confused with the [Raspberry Pi](rpi.md) (and similar) handhelds that run GameBoy/PS1/DOS [emulators](emulator.md) (though some open consoles may use e.g. the RP2040 Raspberry pi processor) but rather custom, mostly [FOSS](foss.md) platforms running mostly their own community made [homebrew](homebrew.md) games. Open consoles are also similar to the old consoles/computers such as [NES](nes.md), [GameBoy](gameboy.md) etc., however again there is a difference in being more indie, released more recently and being "open", directly made for tinkering, so it's e.g. much easier to program them (old consoles/computers very often require some unofficial hacks, obscure libraries, gcc patches etc. to just get your code working).

In summary, open consoles are:

- **GameBoy-like gaming consoles** (but also allow and encourage non-gaming uses).
- Powered by **[free hardware](free_hardware.md) and [free software](free_software.md)** (usually [Arduino](arduino.md) plus a custom library, although mostly advertised as [open source](open_source.md) and not so strict about freedom). Schematics are a lot of times available.
- **Retro**.
- **Indie** (sometimes developed by a single guy), often [crowd-funded](crowd_funding.md).
- **Educational**.
- **[DIY](dyi.md)**, sometimes leaving assembly of the kit to the customer (assembled kits can usually be ordered for extra price).
- **Very cheap** (compared to proprietary mainstream consoles).
- **[Hacking](hacking.md) friendly**.
- Typically **[embedded](embedded.md) [ARM](arm.md)**.
- **[Bare metal](bare_metal.md)** (no operating system).
- Pretty **low spec** hardware ([RAM](ram.md) amount in kilobytes, CPU frequency in MHz).
- Relying on **user created games** which are many times also free-licensed.

Recommended consoles for starters are [Arduboy](arduboy.md) and [Pokitto](pokitto.md) which are not only very well designed, but most importantly have actual friendly active communities.

These nice little toys are great because they are anti-[modern](modern.md), [simple](minimalism.md), out of the toxic mainstream, like the oldschool bullshit-free computers. This supports (and by the low specs kind of "forces") [suckless](suckless.md) programming and brings the programmer the joy of programming (no headaches of resizable windows, multithreading etc., just plain programming of simple things with direct access to hardware). They offer an alternative [ISA](isa.md), a non-x86 platform without botnet and [bloat](bloat.md) usable for any purpose, not just games. Besides that, this hobby teaches low level, efficiency-focused programming skills.

**Watch out** (2024 update): having been successful on the market, the world of open consoles is now flooded by corporations and [SJWs](sjw.md) bringing in the [toxicity](toxic.md), they are going to go to shit very soon, get the old ones while you still can. New consoles already try to employ web-only IDEs in micropython, they're websites are full of suicide inducing diversity propaganda and unusable on computers with less than 1 TB of RAM.

## Programming

Open consoles can typically be programmed without proprietary software (though officially they may promote something involving proprietary software), GNU/[Linux](linux.md) mostly works just fine (sometimes it requires a bit of extra work but not much). Most of the consoles are [Arduino](arduino.md)-based so the Arduino IDE is the official development tool with [C++](cpp.md) as a language ([C](c.md) being thankfully an option as well). The IDE is "open-source" but also [bloat](bloat.md); thankfully [CLI](cli.md) development workflow can be set up without greater issues (Arduino comes with CLI tools and for other platforms [gcc](gcc.md) cross-compiler can be used) so comfy programming with [vim](vim.md) is nicely possible.

If normies can do it, you can do it too.

Some consoles (e.g. Arduboy, Pokitto and Gamebuino META) have their own [emulators](emulator.md) which make the development much easier... or rather bearable. Without an emulator you're forced to constantly reupload the program to the real hardware which is a pain, so you want to either use a nice [LRS](lrs.md) library such as [SAF](saf.md) or write your game to be platform-independent and just make it run on your development PC as well as on the console (just abstract the I/O and use SDL for the PC and the console's library for the console -- see how [Anarch](anarch.md) does it).

## Open Console List

Some notable open consoles (which fit the definition at least loosely) are listed here. Symbol meaning:

- `A` = Arduino
- `C` = great active community
- `*` = recommended
- `+` = many games/programs
- `-` = discontinued

| name                            | CPU       |RAM (K)| ROM (K)| display      | year | notes                             |
| ------------------------------- | --------- | ----- | ------ | ------------ | ---- | --------------------------------- |
|[Arduboy](arduboy.md)            |8b 16 MHz  | 2.5   | 32     | 64x32 1b     | 2015 | * A C +, tiny                     |
|[Gamebuino](gamebuino.md)        |8b 16 MHz  | 2     | 32     | 84x48 1b     | 2014 | + A -, SD                         |
|[Pokitto](pokitto.md)            |32b 48 MHz | 36    | 256    | 220x176      | 2018 |* C +, ext. hats, SD               |
|[ESPboy](espboy.md)              |32b 160 MHz| 80    | 4000   | 128x128      | 2019 |A                                  |
|[GB META](gamebuino.md)          |32b 48 MHz | 32    | 256    | 168x120      | 2018 |A + -, SD                          |
|[Nibble](nibble.md)              |32b 160 MHz| 80    | 4000   | 128x128      | 2021 |A, AAA bat.                        |
|[UzeBox](uzebox.md)              |8b 28 MHz  | 4     | 64     | 360x240      | 2008 |C, +                               |
|[Tiny Arcade](tiny_arcade.md)    |32b        |       |        |              |      |A                                  |
|[Thumby](thumby.md)              |32b 133 MHz| 264   | 2000   | 72x40 1b     | 2022 |RPI (RP2040), mainly web editor :( |
|[Pocket Arcade](pocket_arcase.md)|           |       |        |              |      |                                   |
|Ringo/[MakerPhone](makerphone.md)|32b 160 MHz| 520   | 4000   | 160x128      | 2018 | A -, phone, SD                    |
|[Agon](agon.md)                  |8b 18 MHz  | 512   |        | 640x480      |      |                                   |

TODO: BBC micro:bit, Vircon32 (fantasy console implementable in HW, not sure about license), Retro Game Tiny, Adafruit PyGamer, ... see also https://github.com/ESPboy-edu/awesome-indie-handhelds

## See Also

- programmable [calculator](calculator.md)
- [fantasy console](fantasy_console.md)
