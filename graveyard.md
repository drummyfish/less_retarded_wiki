# Graveyard

Welcome to the graveyard. Here we mourn the death of [technology](technology.md), [art](art.md), [science](science.md) and other deceased by the hand of [capitalism](capitalism.md) and its countless children such as [Feminism](feminism.md), [LGBT](lgbt.md), [consumerism](consumerism.md) and so on.

{ Sometimes we are very depressed from what's going on in this world, how technology is raped and used by living beings against each other. Seeing on a daily basis the atrocities done to the art we love and the atrocities done by it -- it is like watching a living being die. Sometimes it can help to just know you are not alone. ~drummyfish }

```
    _|"|_        .-.         .--.          ___          .--.
   |_   _|      |   |       |    |        /   \        |    |
    _| |_       |___|       |____|       |_____|       |____|
   /_____\     [_____]     /______\     /_______\     /______\

            __________             __..__        __...__
       ____/          \____      .'      '.    .'       '.     ..
      /                    \     |  free  |    | science |    |__|
    .'       R. I. P.       '.   | speech |    |         |   /____\
    |    ~~~~~~~~~~~~~~~~    |   |________|    |_________|
    |                        |  /__________\  /___________\
    |       TECHNOLOGY       |
    |                        |     _..._         __....__      ..
    |  long time ago - now   |   .'     '.     .'        '.   |__|
    |                        |   |  art  |     | humanity |  /____\
    |  Here lies technology  |   | R.I.P.|     |   ~~~~   |
    | who was helping people |   |_______|     |__________|
    | tremendously until its |  /_________\   [____________]
    |  last breath. It was   |
    |  killed by capitalism. |
  .-----------------------------.  
 |                               |
 |_______________________________|
```

Now we better go back [home](island.md).

## See Also

- [RIP](rip.md)