# Emoticon :)

*Not to be [confused](often_confused.md) with [emoji](emoji.md).*

Emoticon (emotion icon) is a short group of [text](text.md) characters that together by their look resemble a simple picture, very often a facial expression (but also other common symbols), for example `:-)`, `O_O` or `=[`. Emoticons are used to communicate feelings and emotions in text-only communication, such as [Internet](internet.md) chat, where body language is missing to provide this function. Emoticons are different from [emoji](emoji.md) (which serve basically the same purpose but are true, small pictures inserted in text) and smileys (smiling faces, both emoticon and emoji).

[History](history.md) of emoticons reaches far back before [computers](computer.md), possibly even to 17th century; invention of emoticons in computer communication is generally attributed to american Scott Fahlman who, as a teacher in 1982, suggested to mark [jokes](jokes.md) on the school [BBS](bbs.md) with `:-)`, exactly for the reason that some people didn't get jokes because of the absence of body language.

Using [Unicode](unicode.md) in emoticons is considered cheap [cheating](cheating.md), so we won't do that here, pure [ASCII](ascii.md) emoticons are best. There are literal emoji pictures in Unicode so if you allow its use, you can just use that and not bother with emoticons. Also multi-line emoticons don't count, that's already [ASCII art](ascii_art.md).

There are different types of emoticons, mainly **western**, which are 90 degrees flipped, e.g. `:-)` or `:{` (the nose symbol is sometimes missing), and **eastern**, which are not flipped, e.g. `<[o_o]>` or `(^.^)`. Western ones can additionally also be flipped, e.g. `(-:` instead of `:-)`, but doing it is not very common.

Here are some emoticons:

```
        :            ;            =            8            B            X
)   :)  :-) :o)  ;)  ;-) ;o)  =)  =-) =o)  8)  8-) 8o)  B)  B-) Bo)  X)  X-) Xo)
    :') :^) :~)  ;') ;^) ;~)  =') =^) =~)  8') 8^) 8~)  B') B^) B~)  X') X^) X~)
(   :(  :-( :o(  ;(  ;-( ;o(  =(  =-( =o(  8(  8-( 8o(  B(  B-( Bo(  X(  X-( Xo(
    :'( :^( :~(  ;'( ;^( ;~(  ='( =^( =~(  8'( 8^( 8~(  B'( B^( B~(  X'( X^( X~(
|   :|  :-| :o|  ;|  ;-| ;o|  =|  =-| =o|  8|  8-| 8o|  B|  B-| Bo|  X|  X-| Xo|
    :'| :^| :~|  ;'| ;^| ;~|  ='| =^| =~|  8'| 8^| 8~|  B'| B^| B~|  X'| X^| X~|
/   :/  :-/ :o/  ;/  ;-/ ;o/  =/  =-/ =o/  8/  8-/ 8o/  B/  B-/ Bo/  X/  X-/ Xo/
    :'/ :^/ :~/  ;'/ ;^/ ;~/  ='/ =^/ =~/  8'/ 8^/ 8~/  B'/ B^/ B~/  X'/ X^/ X~/
\   :\  :-\ :o\  ;\  ;-\ ;o\  =\  =-\ =o\  8\  8-\ 8o\  B\  B-\ Bo\  X\  X-\ Xo\
    :'\ :^\ :~\  ;'\ ;^\ ;~\  ='\ =^\ =~\  8'\ 8^\ 8~\  B'\ B^\ B~\  X'\ X^\ X~\
{   :{  :-{ :o{  ;{  ;-{ ;o{  ={  =-{ =o{  8{  8-{ 8o{  B{  B-{ Bo{  X{  X-{ Xo{
    :'{ :^{ :~{  ;'{ ;^{ ;~{  ='{ =^{ =~{  8'{ 8^{ 8~{  B'{ B^{ B~{  X'{ X^{ X~{
}   :}  :-} :o}  ;}  ;-} ;o}  =}  =-} =o}  8}  8-} 8o}  B}  B-} Bo}  X}  X-} Xo}
    :'} :^} :~}  ;'} ;^} ;~}  ='} =^} =~}  8'} 8^} 8~}  B'} B^} B~}  X'} X^} X~}
[   :[  :-[ :o[  ;[  ;-[ ;o[  =[  =-[ =o[  8[  8-[ 8o[  B[  B-[ Bo[  X[  X-[ Xo[
    :'[ :^[ :~[  ;'[ ;^[ ;~[  ='[ =^[ =~[  8'[ 8^[ 8~[  B'[ B^[ B~[  X'[ X^[ X~[
]   :]  :-] :o]  ;]  ;-] ;o]  =]  =-] =o]  8]  8-] 8o]  B]  B-] Bo]  X]  X-] Xo]
    :'] :^] :~]  ;'] ;^] ;~]  ='] =^] =~]  8'] 8^] 8~]  B'] B^] B~]  X'] X^] X~]
O   :O  :-O :oO  ;O  ;-O ;oO  =O  =-O =oO  8O  8-O 8oO  BO  B-O BoO  XO  X-O XoO
    :'O :^O :~O  ;'O ;^O ;~O  ='O =^O =~O  8'O 8^O 8~O  B'O B^O B~O  X'O X^O X~O
P   :P  :-P :oP  ;P  ;-P ;oP  =P  =-P =oP  8P  8-P 8oP  BP  B-P BoP  XP  X-P XoP
    :'P :^P :~P  ;'P ;^P ;~P  ='P =^P =~P  8'P 8^P 8~P  B'P B^P B~P  X'P X^P X~P
p   :p  :-p :op  ;p  ;-p ;op  =p  =-p =op  8p  8-p 8op  Bp  B-p Bop  Xp  X-p Xop
    :'p :^p :~p  ;'p ;^p ;~p  ='p =^p =~p  8'p 8^p 8~p  B'p B^p B~p  X'p X^p X~p
D   :D  :-D :oD  ;D  ;-D ;oD  =D  =-D =oD  8D  8-D 8oD  BD  B-D BoD  XD  X-D XoD
    :'D :^D :~D  ;'D ;^D ;~D  ='D =^D =~D  8'D 8^D 8~D  B'D B^D B~D  X'D X^D X~D
3   :3  :-3 :o3  ;3  ;-3 ;o3  =3  =-3 =o3  83  8-3 8o3  B3  B-3 Bo3  X3  X-3 Xo3
    :'3 :^3 :~3  ;'3 ;^3 ;~3  ='3 =^3 =~3  8'3 8^3 8~3  B'3 B^3 B~3  X'3 X^3 X~3
C   :C  :-C :oC  ;C  ;-C ;oC  =C  =-C =oC  8C  8-C 8oC  BC  B-C BoC  XC  X-C XoC
    :'C :^C :~C  ;'C ;^C ;~C  ='C =^C =~C  8'C 8^C 8~C  B'C B^C B~C  X'C X^C X~C
c   :c  :-c :oc  ;c  ;-c ;oc  =c  =-c =oc  8c  8-c 8oc  Bc  B-c Boc  Xc  X-c Xoc
    :'c :^c :~c  ;'c ;^c ;~c  ='c =^c =~c  8'c 8^c 8~c  B'c B^c B~c  X'c X^c X~c
S   :S  :-S :oS  ;S  ;-S ;oS  =S  =-S =oS  8S  8-S 8oS  BS  B-S BoS  XS  X-S XoS
    :'S :^S :~S  ;'S ;^S ;~S  ='S =^S =~S  8'S 8^S 8~S  B'S B^S B~S  X'S X^S X~S
*   :*  :-* :o*  ;*  ;-* ;o*  =*  =-* =o*  8*  8-* 8o*  B*  B-* Bo*  X*  X-* Xo*
    :'* :^* :~*  ;'* ;^* ;~*  ='* =^* =~*  8'* 8^* 8~*  B'* B^* B~*  X'* X^* X~*
1   :1  :-1 :o1  ;1  ;-1 ;o1  =1  =-1 =o1  81  8-1 8o1  B1  B-1 Bo1  X1  X-1 Xo1
    :'1 :^1 :~1  ;'1 ;^1 ;~1  ='1 =^1 =~1  8'1 8^1 8~1  B'1 B^1 B~1  X'1 X^1 X~1

        :            ;            =            8            X
)   ):  )-: )o:  );  )-; )o;  )=  )-= )o=  )8  )-8 )o8  )X  )-X )oX
    )': )^: )~:  )'; )^; )~;  )'= )^= )~=  )'8 )^8 )~8  )'X )^X )~X
(   (:  (-: (o:  (;  (-; (o;  (=  (-= (o=  (8  (-8 (o8  (X  (-X (oX
    (': (^: (~:  ('; (^; (~;  ('= (^= (~=  ('8 (^8 (~8  ('X (^X (~X
|   |:  |-: |o:  |;  |-; |o;  |=  |-= |o=  |8  |-8 |o8  |X  |-X |oX
    |': |^: |~:  |'; |^; |~;  |'= |^= |~=  |'8 |^8 |~8  |'X |^X |~X
/   /:  /-: /o:  /;  /-; /o;  /=  /-= /o=  /8  /-8 /o8  /X  /-X /oX
    /': /^: /~:  /'; /^; /~;  /'= /^= /~=  /'8 /^8 /~8  /'X /^X /~X
\   \:  \-: \o:  \;  \-; \o;  \=  \-= \o=  \8  \-8 \o8  \X  \-X \oX
    \': \^: \~:  \'; \^; \~;  \'= \^= \~=  \'8 \^8 \~8  \'X \^X \~X
{   {:  {-: {o:  {;  {-; {o;  {=  {-= {o=  {8  {-8 {o8  {X  {-X {oX
    {': {^: {~:  {'; {^; {~;  {'= {^= {~=  {'8 {^8 {~8  {'X {^X {~X
}   }:  }-: }o:  };  }-; }o;  }=  }-= }o=  }8  }-8 }o8  }X  }-X }oX
    }': }^: }~:  }'; }^; }~;  }'= }^= }~=  }'8 }^8 }~8  }'X }^X }~X
[   [:  [-: [o:  [;  [-; [o;  [=  [-= [o=  [8  [-8 [o8  [X  [-X [oX
    [': [^: [~:  ['; [^; [~;  ['= [^= [~=  ['8 [^8 [~8  ['X [^X [~X
]   ]:  ]-: ]o:  ];  ]-; ]o;  ]=  ]-= ]o=  ]8  ]-8 ]o8  ]X  ]-X ]oX
    ]': ]^: ]~:  ]'; ]^; ]~;  ]'= ]^= ]~=  ]'8 ]^8 ]~8  ]'X ]^X ]~X
O   O:  O-: Oo:  O;  O-; Oo;  O=  O-= Oo=  O8  O-8 Oo8  OX  O-X OoX
    O': O^: O~:  O'; O^; O~;  O'= O^= O~=  O'8 O^8 O~8  O'X O^X O~X
q   q:  q-: qo:  q;  q-; qo;  q=  q-= qo=  q8  q-8 qo8  qX  q-X qoX
    q': q^: q~:  q'; q^; q~;  q'= q^= q~=  q'8 q^8 q~8  q'X q^X q~X
C   C:  C-: Co:  C;  C-; Co;  C=  C-= Co=  C8  C-8 Co8  CX  C-X CoX
    C': C^: C~:  C'; C^; C~;  C'= C^= C~=  C'8 C^8 C~8  C'X C^X C~X
c   c:  c-: co:  c;  c-; co;  c=  c-= co=  c8  c-8 co8  cX  c-X coX
    c': c^: c~:  c'; c^; c~;  c'= c^= c~=  c'8 c^8 c~8  c'X c^X c~X
S   S:  S-: So:  S;  S-; So;  S=  S-= So=  S8  S-8 So8  SX  S-X SoX
    S': S^: S~:  S'; S^; S~;  S'= S^= S~=  S'8 S^8 S~8  S'X S^X S~X
*   *:  *-: *o:  *;  *-; *o;  *=  *-= *o=  *8  *-8 *o8  *X  *-X *oX
    *': *^: *~:  *'; *^; *~;  *'= *^= *~=  *'8 *^8 *~8  *'X *^X *~X
1   1:  1-: 1o:  1;  1-; 1o;  1=  1-= 1o=  18  1-8 1o8  1X  1-X 1oX
    1': 1^: 1~:  1'; 1^; 1~;  1'= 1^= 1~=  1'8 1^8 1~8  1'X 1^X 1~X

>-)  :))  :^}  :'-(  B{|}  >:(  xD

      _           -           ~           .           v           w           o           ...
OO   O_O  (O_O)  O-O  (O-O)  O~O  (O~O)  O.O  (O.O)  OvO  (OvO)  OwO  (OwO)  OoO  (OoO)  O...O  (O...O)
    [O_O] {O_O} [O-O] {O-O} [O~O] {O~O} [O.O] {O.O} [OvO] {OvO} [OwO] {OwO} [OoO] {OoO} [O...O] {O...O}
oo   o_o  (o_o)  o-o  (o-o)  o~o  (o~o)  o.o  (o.o)  ovo  (ovo)  owo  (owo)  ooo  (ooo)  o...o  (o...o)
    [o_o] {o_o} [o-o] {o-o} [o~o] {o~o} [o.o] {o.o} [ovo] {ovo} [owo] {owo} [ooo] {ooo} [o...o] {o...o}
Oo   O_o  (O_o)  O-o  (O-o)  O~o  (O~o)  O.o  (O.o)  Ovo  (Ovo)  Owo  (Owo)  Ooo  (Ooo)  O...o  (O...o)
    [O_o] {O_o} [O-o] {O-o} [O~o] {O~o} [O.o] {O.o} [Ovo] {Ovo} [Owo] {Owo} [Ooo] {Ooo} [O...o] {O...o}
00   0_0  (0_0)  0-0  (0-0)  0~0  (0~0)  0.0  (0.0)  0v0  (0v0)  0w0  (0w0)  0o0  (0o0)  0...0  (0...0)
    [0_0] {0_0} [0-0] {0-0} [0~0] {0~0} [0.0] {0.0} [0v0] {0v0} [0w0] {0w0} [0o0] {0o0} [0...0] {0...0}
QQ   Q_Q  (Q_Q)  Q-Q  (Q-Q)  Q~Q  (Q~Q)  Q.Q  (Q.Q)  QvQ  (QvQ)  QwQ  (QwQ)  QoQ  (QoQ)  Q...Q  (Q...Q)
    [Q_Q] {Q_Q} [Q-Q] {Q-Q} [Q~Q] {Q~Q} [Q.Q] {Q.Q} [QvQ] {QvQ} [QwQ] {QwQ} [QoQ] {QoQ} [Q...Q] {Q...Q}
PP   P_P  (P_P)  P-P  (P-P)  P~P  (P~P)  P.P  (P.P)  PvP  (PvP)  PwP  (PwP)  PoP  (PoP)  P...P  (P...P)
    [P_P] {P_P} [P-P] {P-P} [P~P] {P~P} [P.P] {P.P} [PvP] {PvP} [PwP] {PwP} [PoP] {PoP} [P...P] {P...P}
^^   ^_^  (^_^)  ^-^  (^-^)  ^~^  (^~^)  ^.^  (^.^)  ^v^  (^v^)  ^w^  (^w^)  ^o^  (^o^)  ^...^  (^...^)
    [^_^] {^_^} [^-^] {^-^} [^~^] {^~^} [^.^] {^.^} [^v^] {^v^} [^w^] {^w^} [^o^] {^o^} [^...^] {^...^}
;;   ;_;  (;_;)  ;-;  (;-;)  ;~;  (;~;)  ;.;  (;.;)  ;v;  (;v;)  ;w;  (;w;)  ;o;  (;o;)  ;...;  (;...;)
    [;_;] {;_;} [;-;] {;-;} [;~;] {;~;} [;.;] {;.;} [;v;] {;v;} [;w;] {;w;} [;o;] {;o;} [;...;] {;...;}
''   '_'  ('_')  '-'  ('-')  '~'  ('~')  '.'  ('.')  'v'  ('v')  'w'  ('w')  'o'  ('o')  '...'  ('...')
    ['_'] {'_'} ['-'] {'-'} ['~'] {'~'} ['.'] {'.'} ['v'] {'v'} ['w'] {'w'} ['o'] {'o'} ['...'] {'...'}
..   ._.  (._.)  .-.  (.-.)  .~.  (.~.)  ...  (...)  .v.  (.v.)  .w.  (.w.)  .o.  (.o.)  .....  (.....)
    [._.] {._.} [.-.] {.-.} [.~.] {.~.} [...] {...} [.v.] {.v.} [.w.] {.w.} [.o.] {.o.} [.....] {.....}
UU   U_U  (U_U)  U-U  (U-U)  U~U  (U~U)  U.U  (U.U)  UvU  (UvU)  UwU  (UwU)  UoU  (UoU)  U...U  (U...U)
    [U_U] {U_U} [U-U] {U-U} [U~U] {U~U} [U.U] {U.U} [UvU] {UvU} [UwU] {UwU} [UoU] {UoU} [U...U] {U...U}
--   -_-  (-_-)  ---  (---)  -~-  (-~-)  -.-  (-.-)  -v-  (-v-)  -w-  (-w-)  -o-  (-o-)  -...-  (-...-)
    [-_-] {-_-} [---] {---} [-~-] {-~-} [-.-] {-.-} [-v-] {-v-} [-w-] {-w-} [-o-] {-o-} [-...-] {-...-}
~~   ~_~  (~_~)  ~-~  (~-~)  ~~~  (~~~)  ~.~  (~.~)  ~v~  (~v~)  ~w~  (~w~)  ~o~  (~o~)  ~...~  (~...~)
    [~_~] {~_~} [~-~] {~-~} [~~~] {~~~} [~.~] {~.~} [~v~] {~v~} [~w~] {~w~} [~o~] {~o~} [~...~] {~...~}
><   >_<  (>_<)  >-<  (>-<)  >~<  (>~<)  >.<  (>.<)  >v<  (>v<)  >w<  (>w<)  >o<  (>o<)  >...<  (>...<)
    [>_<] {>_<} [>-<] {>-<} [>~<] {>~<} [>.<] {>.<} [>v<] {>v<} [>w<] {>w<} [>o<] {>o<} [>...<] {>...<}
**   *_*  (*_*)  *-*  (*-*)  *~*  (*~*)  *.*  (*.*)  *v*  (*v*)  *w*  (*w*)  *o*  (*o*)  *...*  (*...*)
    [*_*] {*_*} [*-*] {*-*} [*~*] {*~*} [*.*] {*.*} [*v*] {*v*} [*w*] {*w*} [*o*] {*o*} [*...*] {*...*}

<(o)_(o)>  \[@]_[@]/  <>_<>  [(O___O)]  (> 0 _0)> (~_~;) (=^.^=)  m(_ _)m

salutations:  o/  \o   o7   \o/

8====D   {|}   ,,|,,   (. (. )   ( . Y . )

@}->--  <3   </3

SOS: ...---...

hitler: :=)
```
