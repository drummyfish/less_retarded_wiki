# Minigame

Minigame is a very small and simple [game](game.md) intended to entertain the player in a simple way, usually for only a short amount of time, unlike a full fledged game. Minigames may a lot of times be embedded into a bigger game (as an [easter egg](easter_egg.md) or as a part of a game mechanic such as lock picking), they may come as an extra feature on primarily non-gaming systems, or appear in collections of many minigames as a bigger package (e.g. various party game collections). Minigames include e.g. [minesweeper](minesweeper.md), [sokoban](sokoban.md), the Google [Chrome](chrome.md) T-rex game, [Simon Tatham's Portable Puzzle Collection](stppc.md), as well as many of the primitive old games like [Pong](pong.md) and [Tetris](tetris.md). Minigames are nice from the [LRS](lrs.md) point of view as they are [minimalist](minimalism.md), simple to create, often [portable](portability.md), while offering a potential for great [fun](fun.md) nevertheless.

Minigame is an ideal project for learning [programming](programming.md).

Despite the primary purpose of minigames many players invest huge amounts of time into playing them, usually competitively e.g. as part of [speedrunning](speedrun.md).

Minigames are still very often built on the principles of old arcade games such as getting the highest score or the fastest time. For this they can greatly benefit from [procedural generation](procgen.md) (e.g. endless runners).

## List Of Minigames

Almost any traditional game idea can be made into a minigame if we simplify it enough, but for inspiration here is a list of some common minigames and minigame types.

- **[2048](2048.md)**
- **[arkanoid](arkanoid.md)**
- **[asteroids](asteroids.md)**
- **[backgammon](backgammon.md)**
- **button smasher**: Games whose goal is achieved mainly by smashing a button as quickly as possible, usually e.g. sprint simulators. This may perhaps even include a game that requires you to press a button as quickly as possible (achieve fastest reaction time).
- **[card games](card_game.md)**
- **[checkers](checkers.md)**
- **[chess](chess.md)**, its variants and chess puzzles
- **city bomber**: A plane is descending on the screen, player has to drop bombs to destroy building so that it can land.
- **concentration**
- **[donkey kong](donkey_kong.md)**
- **[dots and boxes](dots_and_boxes.md)**
- **[endless runner](endless_runner.md)**
- **[fifteen](fifteen.md)**
- **[flappy bird](flappy_bird.md)**
- **[game of life](gol.md)**
- **[go](go_game.md)**, especially a small board one or variants such as atari go
- **guess a number**
- **[hangman](hangman.md)**
- **[invaders](invaders.md)**
- **[jigsaw puzzle](jigsaw.md)**
- **knowledge quiz**
- **[loderunner](loderunner.md)**
- **[ludo](ludo.md)**
- **[lunar lander](lunar_lander.md)**
- **[mahjong](mahjong.md)**
- **maze**
- **[minigolf](minigolf.md)**
- **[minesweeper](minesweeper.md)**
- **[pacman](pacman.md)**
- **[pinball](pinball.md)**
- **[poker](poker.md)**
- **[pong](pong.md)**
- **[racetrack](racetrack.md)**
- **[rock-paper-scissors](rock_paper_scissors.md)**
- **[shoot'em up](shmup.md)**
- **[snake](snake.md)**
- **[sokoban](sokoban.md)**
- **[solitaire](solitaire.md)**
- **[sprouts](sprouts.md)**
- **[sudoku](sudoku.md)**
- **[tangram](tangram.md)**
- **[tetris](tetris.md)** (block game, "tetris" is trademarked)
- **[The Witness](the_witness.md) puzzles**: The kind of puzzles that appear in the game The Witness.
- **[tic-tac-toe](tic_tac_toe.md)**
- **[tower of hanoi](tower_of_hanoi.md)**
- **[tron](tron.md)**
- **[untangle](untangle.md)**
- **[QWOP](qwop.md)**
- ...