# Ronja

{ I was informed about this by a friend over email <3 I basically paraphrase here what he told me. See also http://www.modulatedlight.org/. ~drummyfish }

Ronja (reasonable optical near joint access) is a [free/open](foss.md) [KISS](kiss.md) device for wireless connection of two devices using light (i.e. optical communication) and the [ethernet](ethernet.md) protocol; it can be [made at home](diy.md) (for about $100), doesn't require any [MCUs](mcu.md) and as such can be considered a [LRS](lrs.md)/[suckless](suckless.md) alternative to traditional [WiFi](wifi.md) [routers](router.md) that are de-facto owned and controlled by [corporations](corporation.md). It works full [duplex](duplex.md) up to the distance of 1400 meters with a speed of 10 Mbps, which is pretty amazing. One can also imagine Ronja as a kind of ethernet cable, just implemented with light instead of electricity. The design is released under [GFDL](gfdl.md). The project website is at http://ronja.twibright.com/.

There are many advantages in Ronja -- besides the mentioned KISS design and all its implications (freedom, repairability, cheap price, compatibility, ...), Ronja doesn't use radio so there are no bullshit issues with legal bands etc., it also works with just an ethernet card, offers a stable and constant transmission speed with very low latency, can be potentially harder to block with jammers and to spy on: besides visible light the transmission can also use infrared spectrum and narrow direction of transmission, as opposed to radiating to all directions like wi-fis, also the fast flickering of the LED is unnoticable by human or even normal cameras, therefore Ronja transmission is expensive to detect. Also note that some kind of protocol-level [encryption](encryption.md) can be used above Ronja, if one so desires. This makes it a nice communication tool for people under oppresive regimes like those in China or [USA](usa.md).

## See Also

- [li-fi](li_fi.md)