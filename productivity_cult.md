# Productivity Cult

*"PRODUCE PRODUCE PRODUCE PRODUCE PRODUCE"* --[capitalism](capitalism.md)

Productivity cult (often connected to terms such as *self improvement*, *personal growth* etc.) is one of [modern](modern.md) [capitalist](capitalism.md) religions which praises human productivity above everything, even happiness, well being, sanity etc. Kids nowadays are all about "how to be more motivated and productive", they make daily checklists, analyze tables of their weekly performance, count how much time they spend taking a shit on the toilet, give up sleep to study some useless bullshit required by the current market fluctuation. Productivity cult is all about voluntarily making oneself a robot, a slave to the system that worships capital. As [Froge](froge.md) put it: "The world of self improvement gurus is a circlejerk of such magnitude it rivals several world religions".

A human is living being, not a machine, he should live a happy relaxed life, dedicated to spending time with his close ones, raising children, enjoying the beauties of nature, exploring secrets of the universe, without stress; he should create when inspiration or a truly great necessity comes to him and when he does, he should take his time to carefully make the art great, without hasting it or forcing it. Productivity cult goes all against this, it proclaims one should be constantly spitting out "something", torturing and forcing himself, maximizing quantity on detriment of quality, undergo constant stress while suppressing rest -- that one should all the time be preoccupied with competitive [fight](fight_culture.md), deadlines, that art he creates is something that can be planned on schedule, made on deadline and assigned a calculated price tag to be an ideal consumerist product. If such stance towards life doesn't make you wanna puke, you most likely lack a soul.

Do not produce. Create. Art takes time and can't be scheduled.

The name of the cult itself [says a lot about it](name_is_important.md). While a name such as *efficiency* would probably be better, as efficiency means doing less work with the same result and therefore having more free time, it is not a surprise that capitalism has chosen the word *productivity*, i.e. producing more which means working more, e.g. for the price of free time and mental health.

Productivity obsessed people are mostly idiots without the ability to think for themselves, they have desktops with "[motivational](motivation.md)" wallpapers saying shit like "the word impossible doesn't exist in my dictionary" and when you tell them if it wouldn't be better to rather establish a society where people wouldn't have to work they start screeching "HAHAA THATS IMPOSSIBLE IT CANT WORK". Productivity maximalists bully people for taking rest or doing what they otherwise enjoy in moderation -- they invent words such as "[procrastination](procrastination.md)" to create a feeling of ever present guilt induced by doing what one truly enjoys.

Productivity freaks are often the ones who despise consumers, i.e. brainless zombies that consume goods, but somehow don't seem to mind being producers, a similar kind of brainless zombies that just stands on the other end of this retarded system.

One of the funniest examples of productivity cult gone too far is so called "[life couching](life_couching.md)" in which the aspiring producer robots hire bullshit cult leaders, so called "life couches", to shout at them to be more productive. At least in the past slaves were aware of being slaves and tried to free themselves. I literally want to [kill myself](suicide.md).

Productivity is such a big deal because **programmers are in fact actually getting exponentially less productive** due to time needed to spend on [bullshit](bullshit.md) nowadays, on overcomplicated buggy [bloat](bloat.md) and billions of frameworks needed to get basic things done -- this has been pointed out by [Jonathan Blow](jonathan_blow.md) in his talk *Preventing the Collapse of Civilization* in which he refers to the video of [Ken Thompson](ken_thompson.md) talking about how he developed the [Unix](unix.md) operating system in **three weeks**.

A considerable number of people are attracted to [suckless](suckless.md) software due to its positive effects on productivity thanks to the elimination of bullshit. These are mostly the kind of above mentioned dumbasses who just try to exploit anything they encounter for [self interest](self_interest.md) without ever aiming for greater good, they don't care about Unix philosophy beyond its effects on increasing their salary. Beware of them, they poison society.

## See Also

- [motivation](motivation.md)
- [procrastination](procrastination.md)
- [laziness](laziness.md)
- [antiwork](antiwork.md)