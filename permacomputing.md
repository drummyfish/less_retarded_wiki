# Permacomputing

Permacomputing is a new term invented by [Viznut](viznut.md), it's inspired by the term *[permaculture](permaculture.md)* and means something like "sustainable, ecological minimalist computing"; see [permacomputing wiki](permacomputing_wiki.md).