# Throw Away Script

Throw away script is a script for one-time job which you "throw away" after its use. Such scripts may be [ugly](ugly.md), badly written and don't have to follow [LRS](lrs.md) principles as their sole purpose is to quickly achieve something without any ambition to be good, future-proof, readable, reusable etc.

For example if you have a database in some old format and want to convert it to a new format, you write a throw away script to do the conversion, or when you're mocking an idea for a [game](game.md), you write a quick throw away prototype in JavaScript to just test how the gameplay would feel.

For throw aways cripts it is acceptable and often preferrable to use languages otherwise considered bad such as [Python](python.md) or [JavaScript](javascript.md).
