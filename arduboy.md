# Arduboy

Arduboy is an [Arduino](arduino.md)-based, extremely tiny indie/"retro" handheld [open gaming console](open_console.md), about the size of a credit card, with monochrome (black&white) display; it was one of the earliest during the open console boom and is at the same time among the best in many aspects (construction, hardware, community, games, price, ...). Not only is it one of the top open consoles out there, it is also one of the most [minimalist](minimalism.md) and a great way to get into low level [programming](programming.md), learning [C](c.md), [embedded](embedded.md) development etc. Even for a normie not intending to program it it's just a super cool toy to play old nostalgic games on and flex with around friends. [We](lrs.md) can really recommend getting Arduboy ASAP to anyone remotely interested in this kind of stuff. Arduboy is a bit similar to the original [Gamebuino](gamebuino.md) (the one with monochrome screen), it may have been inspired by it. The official website is https://www.arduboy.com/.

```
  _________
 |.-------.|
 ||ARDUBOY||
 ||_______||
 |         |
 | _||_ /O |
 | ";;" O/ |
 |_________|
```

*Hasted (but very good) Arduboy ASCII art.*

Arduboy has a number of [hack](hacking.md)/mods, both official and unofficial, see e.g. Arduboy FX (version that comes with memory for games so one doesn't have to use external PC to switch between them) or Arduboy Mini (yet physically smaller version).

{ Let's make it clear I AM NOT PAID for this article :D Reading through it it sounds like I'm shilling it super hard but it's really just that I like Arduboy, it was my first open console and I owe it my change in direction in programming. Of course, Arduboy does have its flaws, it's still something designed for profit, it shills [open soars](open_source.md), forums need [JavaScript](js.md) and it's very possible it will become more spoiled in the future, however at the moment it's something quite nice with the amount of capitalist [bullshit](bullshit.md) being still tolerable. That may change but as of writing this it still seems to be so. ~drummyfish }

Arduboy is not very expensive, one can even build it at home, there is documentation. The one you buy has excellent construction, it doesn't suffer from any shortcomings often seen in other such devices (hard to press buttons, display visibility angles, ... ), the monochrome display is tiny but very good, with great contrast, it is joy to just look at; some people even managed to "fake" multiple shades of gray by fast pixel flickering. Seeing games on this display is somehow magical.

As can be judged from the name, Arduboy is based on **[Arduino](arduino.md)** (the famous [free hardware](free_hardware.md) platform), seems like Arduino Leonardo. The console is very small not only physically but also by its [hardware](hardware.md) specification, and that even compared to other open consoles -- Arduboy only has **2.5 KB of [RAM](ram.md)** which is really low, however this is good for learning [good programming](lrs.md) and testing minimalist software; [less is more](less_is_more.md). Besides this there is 1 KB of [EEPROM](eeprom.md) (for things like game saves, ...) and 32 KB of flash memory (memory for the program itself -- as RAM is so low, one often tries to store data here). The [CPU](cpu.md) is 8bit ATmega32u4 clocked at **16 MHz** (also not very much, good for minimalism). It's an [AVR](avr.md) CPU which has a **Harvard architecture**, i.e. program resides in different memory than the data; this can be something new to PC programmers as you have to think what (and how) to place into the program memory (flash) vs the [RAM](ram.md) (as there is very little RAM); basically you have to use the `PROGMEM` macro to tell Arduino you want something placed in the flash (however that data will then be read-only; the whole PROGMEM thing can be a bit of annoyance, but in the end this is how small computers work). The vanilla version has no SD card. There are **6 buttons**: arrows, *A* and *B* and that's it -- one often has to think of how to make clever controls with this limited number of buttons (another kind of minimalist exercise). The display is **monochrome, 128x64 pixels**. For programming one typically uses Arduino IDE and the official Arduboy library ([FOSS](foss.md)), but it's easy to do everything from the [command line](cli.md). There is also some kind of speaker for making beeps. Arduboy also has a good **[emulator](emulator.md)**, something that greatly helps with development which also isn't standard in the open console world.

Arduboy way designed by Kevin Bates through Kickstarter in 2015, He still keeps improving it and regularly gets involved in discussions on the official forums, that's pretty cool, he shares details and ideas about what he's currently doing, he gets involved in discussing [hacks](hacking.md) etc. The community is also very nice and greatly active itself -- yes, there are probably some rules on the forums, most people are absolute uber noobs, but it just seems like politics, furriness and similar poison just doesn't get discussed there, everyone just shares games, advice on programming etc., it's pretty bearable. In this it's similar to [Pokitto](pokitto.md) -- there the community is also nice, active, with the creator getting involved a lot.

**Games**, mostly made by the users themselves, can either be found on the official website or in Erwin's Arduboy game collection (https://arduboy.ried.cl/) which as of writing this lists **something over 300 games** -- some are pretty impressive, like Arduventure, a pokemon-look-alike RPG game with quite huge world (very impressive for such tiny amount of memory), MicroCity, Catacombs of the Damned or various other similar [raycasting](raycasting.md) 3Dish games. If you don't have Arduboy yet, you can play any game in the emulator (which even runs in the browser), however the experience of playing on the small device cannot indeed be replicated. The console can only hold one game at a time, so you always have to upload another one through USB if you want to switch, though the Arduboy FX mod addresses this by adding additional game memory to the console. Game progress is saved in EEPROM so you shouldn't lose your save states by uploading a new game (unless that game carelessly uses the same space in EEPROM for its own savestates). Great many of the games are [FOSS](foss.md), i.e. come with a free [license](license.md) or at least with the source code, only a minority has secret source code. Some [LRS](lrs.md) software run on Arduboy, e.g. [SAF](saf.md), so also games like [microTD](utd.md) and [smolchess](smallchesslib.md), though for example [Anarch](anarch.md) is too much for this small hardware. [Comun](comun.md) [C](c.md) interpreter also ran on Arduboy.

**How [free](free_software.md) is it?** Quite a lot, though probably not 100% (if spying is your concern then know this small thing probably won't even be capable of trying to hurt you this way). Arduboy Schematics are available { Not sure about license. ~drummyfish }, forums have tons of additional documentation and tutorials, Arduboy library is free licensed (BSD 3 clause), Arduino itself also uses free licenses (though of course it won't likely be free down to individual transistors...), games are very often free too. Being a minimalist computer there is a great deal of practical freedom. All in all, it's one of the most "overall" free things you can get.









