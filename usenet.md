# Usenet

Usenet (User's Network) is an ancient digital discussion network -- a [forum](forum.md) -- that existed long before the [World Wide Web](www.md). At the time it was very popular, it was THE place to be, but nowadays it's been forgotten by the mainstream, sadly hardly anyone remembers it.

Back in the day there were no [web browsers](browser.md), there was no web. Many users were also **not connected through Internet** as it was expensive, they normally used other networks like [UUCP](uucp.md) working through phone lines. They  could communicate by some forms of electronic mail or by directly connecting to servers and leaving messages for others there -- these servers were called [BBS](bbs.md)es and were another popular kind of "[social network](social_network.md)" at the time. Usenet was a bit different as it was [decentralized](decentralization.md) (see also [federation](federation.md)) -- it wasn't stored or managed on a single [server](server.md), but on many independent servers that provided users with access to the network. This access was (and is) mostly paid (to [lurk](lurk.md) for free you can search for Usenet archives online). To access Usenet a **newsreader** program was needed, it was kind of a precursor to web browsers (nowadays newsreaders are sometimes built into e.g. email clients). Usenet was lots of time not moderated and anonymous, i.e. kind of free, you could find all kinds of illegal material there.

Usenet invented many things that survive until today such as the words *[spam](spam.md)* and *[FAQ](faq.md)* as well as some basic concepts of how discussion forums even work. It was also generally quite a lot for the [free speech](free_speech.md), which is good.

Usenet was originally [ASCII](ascii.md) only, but people started to post binary files encoded as ASCII and there were dedicated sections just for posting binaries, so you co go [piiiiiiiiirating](piracy.md).

It worked like this: there were a number of Usenet servers that all collaborated on keeping a database of *articles* that users posted (very roughly this is similar to how [blockchain](blockchain.md) works nowadays); the servers would more or less mirror each other's content. These servers were called *providers* as they also allowed access to Usenet but this was usually for a fee. The system uses a [NNTP](nntp.md) (Network News Transfer Protocol) protocol. The articles users posted were also called *posts* or *news*, they were in [plain text](plain_text.md) and were similar to email messages ([mailing lists](mailing_list.md) actually offer a similar experience). Other users could reply to posts, creating a discussion thread. Every post was also categorized under certain **newsgroup** that formed a hierarchy (e.g. *comp.lang.java*). After so called *Big Renaming* in 1987 the system eventually settled on 8 top level hierarchies (called the *Big 8*): comp.* (computers), news.* (news), sci.* (science), rec.* (recreation), soc.* (social), talk.* (talk), misc.* (other) and humanities.* (humanities). There was also another one called alt.* for "controversial" topics (see especially alt.tasteless). According to [Jargon File](jargon_file.md), by 1996 there was over 10000 different newsgroups.

Usenet was the pre-[web](www.md) web, kind of like an 80s [reddit](reddit.md) which contained huge amounts of [historical](history.md) [information](information.md) and countless discussions of true computer [nerds](nerd.md) which are however not easily accessible anymore as there aren't so many archives, they aren't well indexed and direct Usenet access is normally paid. It's a shame. It is possible to find e.g. initial reactions to the [AIDS](aids.md) disease, people asking what the [Internet](internet.md) was, people discussing future technologies, the German cannibal (Meiwes) looking for someone to eat (which he eventually did), [Bezos](bezos.md) looking for [Amazon](amazon.md) programmers, a heated debate between [Linus Torvalds](torvalds.md) and [Andrew Tanenbaum](tanenbaum.md) about the best OS architecture (the "Linux is obsolete" discussion) or [Douglas Adams](douglas_adams.md) talking to his fans. There were [memes](meme.md) and characters like [BIFF](biff.md), a kind of hilarious noob wannabe cool personality. Some users became kind of famous, e.g. Scott Abraham who was banned from Usenet by court after an extremely long flame war, Alexander Abian, a mathematician who argued for blowing up the Moon (which according to his theory would solve all Earth's issues), Archimedes Plutonium who suggested the Universe was really a big plutonium atom (he later literally changed his name to Plutonium lol) or John Titor (pretend time traveler). There are also some politically incorrect groups like *alt.niggers* [lol](lol.md).

{ I mean I don't remember it either, I'm not that old, I've just been digging on the Internet and in the archives, and I find it all fascinating. ~drummyfish }

## The Newsgroup Hierarchy

Here are some notable groups, placed in the group hierarchy:

- *comp*: computers, part of Big 8
  - *ai*: [AI](ai.md)
  - *answers*
  - *bbs*: [BBS](bbs.md)
  - *binaries*
  - *compression*: [compression](compression.md)
  - *editors*: [text editors](text_editor.md)
  - *graphics*: [computer graphics](graphics.md)
  - *internet*: [Internet](internet.md)
    - *services*
      - *wiki*: [wikis](wiki.md)
  - *lang*: [programming languages](programming_language.md)
    - *asm*: [assembly](assembly.md)
    - *c*: [C](c.md) language
    - *forth*: [Forth](forth.md)
    - *lisp*: [Lisp](lisp.md)
    - *python*: [Python](python.md) language
  - *object*: [OOP](oop.md)
  - *os*: [operating systems](os.md)
    - *linux*: [Linux](linux.md)
      - *misc*
  - *programming*: [programming](programming.md)
  - *society*: technology as related to society
  - *theory*: theoretical [compsci](compsci.md)
  - *unix*: [Unix](unix.md)
- *humanities*: humanities, part of Big 8
  - *lit*: literature
  - *music*: [music](music.md)
- *misc*: miscellaneous, part of Big 8
  - *books*: [books](books.md)
  - *business*: [business](business.md)
  - *education*: [education](education.md)
  - *fitness*
  - *news*: news from various regions
  - *survival*
- *news*: about the network itself, part of Big 8
  - *announce*
  - *software*
- *rec*: recreational activity, part of Big 8
  - *animals*
  - *arts*: [art](art.md)
  - *drugs*
  - *food*
  - *games*
  - *humor*: [funny stuff](jokes.md)
  - *knives*: [knives](knife.md)
- *sci*: [science](science.md), part of Big 8
  - *answers*: answers and questions
  - *bio*: [biology](biology.md)
  - *chem*: [chemistry](chemistry.md)
  - *crypt*: [cryptography](cryptography.md)
  - *fractals*: [fractals](fractal.md)
  - *lang*: [languages](natural_language.md), [linguistics](linguistics.md)
  - *logic*: [logic](logic.md)
  - *math*: [math](math.md) { See, it's science. ~drummyfish }
  - *physics*: [physics](physics.md)
  - *space*: [space](space.md)
- *soc*: social issues and socializing, part of Big 8
  - *atheism*: [atheism](atheism.md)
  - *bi*: [bisexual](bisexual.md)
  - *culture*: [culture](culture.md)
  - *feminism*: [feminism](feminism.md)
  - *history*: [history](history.md)
  - *politics*: [politics](politics.md)
  - *religion*: [religion](religion.md)
    - *christian*: [Christianity](christianity.md)
    - *islam*: [Islam](islam.md)
  - *women*: [women](woman.md)
- *talk*: talk, part of Big 8
  - *euthanasia*
  - *philosophy*
  - *politics*
  - *rumors*
- *alt*: alternative, weird/controversial/NSFW/NSFL
  - *alien*
  - *anarchism*: [anarchism](anarchism.md)
  - *ascii-art*: [ASCII art](ascii_art.md)
  - *atheism*: [atheism](atheism.md)
  - *abortion*
  - *binaries*: binary file sharing, [piracy](piracy.md)
    - *games*
    - *pictures*: picture sharing
      - *erotica*: erotic pictures
        - *animals*
        - *child*: [pedo](pedophilia.md)
          - *female*
          - *male*
        - *lolita*: [loli](loli.md)
        - *pre-teen*
        - *tasteless*
        - *teen*
  - *drugs*
  - *freedom*
  - *fun*: [fun](fun.md)
  - *games*: [games](game.md)
    - *doom*: [Doom](doom.md)
      - *ii*
    - *duke3d*: [Duke3D](duke3d.md)
    - *video*: vidya
      - *game-boy*: [GameBoy](gameboy.md)
  - *humor*
  - *jokes*: [jokes](jokes.md)
  - *niggers*: [niggers](nigger.md)
  - *sex*: [sex](sex.md)
    - *pedophilia*: [pedo](pedo.md)
      - *pictures*
      - *swaps*
    - *sheep*
    - *zoophilia*
  - *suicide*: [suicide](suicide.md)
  - *tasteless*
    - *jokes*
  - *ufo*: [UFO](ufo.md)
  - *tv*: [TV](tv.md)

## Where To Freely Browse Usenet

Search for Usenet archives, I've found some sites dedicated to this, also [Internet archive](internet_archive.md) has some newsgroups archived. [Google](google.md) has Usenet archives on a website called "Google groups" (now sadly requires login). There is a nice archive at https://www.usenetarchives.com. Possibly guys from Archive Team can help (https://wiki.archiveteam.org/index.php/Usenet, https://archive.org/details/usenet, ...). See also http://www.eternal-september.org. There is an archive from 1981 accessible through [gopher](gopher.md) at gopher://gopher.quux.org/1/Archives/usenet-a-news. Also https://yarchive.net/. Also search for *Henry Spencer's UTZOO NetNews Archive*.

{ Also here's some stuff https://old.reddit.com/r/usenet/wiki/index. ~drummyfish }

## See Also

- [BBS](bbs.md)
- [modem world](modem_world.md)
- [multi user dungeon](mud.md)
- [mailing list](mailing_list.md)
- [FidoNet](fidonet.md)