# SAF

SAF is a [LRS](lrs.md) [C](c.md) [library](library.md) for small, very [portable](portability.md) [games](game.md) (and possibly other kinds pf programs); it can also be seen as a [fantasy console](fantasy_console.md). It was made by [drummyfish](drummyfish.md). The repository is available e.g. at https://codeberg.org/drummyfish/SAF. [MicroTD](utd.md) is an example of a LRS game made with SAF, but even more complex games such as [Anarch](anarch.md) have been ported to it.

The whole SAF library is implemented in a [single header](single_header.md) file (currently a bit under 5000 [lines of code](loc.md), which is mostly due to including all the [boilerplate](boilerplate.md) for all possible platforms it supports, the actual SAF logic is pretty small) and offers a simple [API](api.md) for programming games, i.e. functions for drawing pixels to the screen, playing sounds, reading buttons, computing the [sine](sin.md) function etc., and it handles boring/annoying issues such as the game main loop. It also has built-in a number of [frontends](frontend.md) that allow compiling a correctly made SAF games to any supported platform among which are [SDL](sdl.md), [SFML](sfml.md), [X11](x11.md), [ncurses](ncurses.md) (terminal), even [open consoles](open_console.md) such as [Pokitto](pokitto.md), [Arduboy](arduboy.md) or [Gamebuino META](gamebuino.md). There is an option to auto-convert a color game to black-and-white only displays. Some PC frontends add emulator-like features such as time manipulation, [pixelart upscaling](pixelart_upscale.md), [TAS](tas.md) support and so on.

Games made with SAF run in 64x64 resolution, with 256 colors ([332 palette](rgb332.md)) and 25 [FPS](fps.md). They can use 7 input buttons (arrows, A, B and C), play 4 different sound effects and use 32 [bytes](byte.md) of persistent memory, e.g. for savegames or highscores. These relatively low specifications are set on purpose so as to help [portability](portability.md), reduce [bloat](bloat.md), frustration and [friction](friction.md). Many times good, entertaining games can be very simple, as is the case e.g. with [Tetris](tetris.md), [chess](chess.md), various [shmups](shmup.md), [roguelikes](roguelike.md) and so on -- these are the kinds of games SAF is ideal for.

```
      []  [][][][][]
      [][][]      [][]
      [][]          []
      []    XX    XX[]
      []      XXXX  []
      [][]          []
      [][][]      [][]
      []  [][][][][] 
```

*SAF logo*

```
 microTD
  ________________________________________________________________
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;' ';; ' ' ;;' ';;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;' ';,   ,;,   ,;,   ,;;';;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;,;''   '''   '''   '', ,;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;',''''''''''''''''''',';;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;; ' ,;';;';, ;';'; ;';, ; ;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;; ' ;; ;; ;;   ;   ;  ; ; ;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;; ' ;; ,,;;;   ;   ; ,; ; ;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;,', ''''''    '   ''' ,',;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;,' ' ' ' ' ' ' ' ' ',;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 | ';;;' , ';;;' , ';;;' , ';;;' , ';;;' , ';;;' , ';;;' , ';;;' ,|
 |;, ' ,;;;, ' ,;;;, ' ,;;;, ' ,;;;, ' ,;;;, ' ,;;;, ' ,;;;, ' ,;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |                  ,, ,  ,,,   ,,,          ,,                   |
 |                  ;'';  ;,;   ;,;           ;                   |
 |                  '  '  ' '   '            '''                  |
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|
 |;';;;;;;'';;;;;;;;;;';;;;;;;;;;;;;;;;''';;;;;;;;;;;;;;;;;;;;;;;;|
 |; ,';;; ',,;;; ,;;;, ,;;;;,;;;;;;;;;;,, ;;;;;;;;;;;;;;;;;;;;;;;;|
 |;,,;;;;;,,;;;,,;;;;;,,;;;;,;;;;;;;;;;,,,;;;;;;;;;;;;;;;;;;;;;;;;|
  ----------------------------------------------------------------
```

*screenshot of a SAF game ([uTD](utd.md)) running in terminal with [ncurses](ncurses.md)*

## See Also

- [comun shell](comun_shell.md)
- [MIX](mix.md)
- [uxn](uxn.md)