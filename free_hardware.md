# Free/Freedom-Friendly Hardware

Free (as in freedom) hardware is a form of ethical [hardware](hardware.md) aligned with the philosophy of [free (as in freedom) software](free_software.md), i.e. having a free [licensed](license.md) designed that allows anyone to study, use, modify and share such designs for any purpose and so prevent abuse of users by technology. Let us note the word *free* refers to user freedom, not price! Sometimes the term may be more broadly and not completely correctly used even for hardware that's just highly compatible with purely free software systems -- let us rather call these a **freedom friendly hardware** -- and sometimes people misunderstand the term *free* as meaning "gratis hardware"; to avoid misunderstandings [GNU](gnu.md) recommends using the term **free design hardware** or **libre hardware** for free hardware in the strict sense, i.e. hardware with free licensed design. Sometimes -- nowadays maybe even more often -- the term *"[open source](open_source.md)" hardware* or *open hardware* with very similar meaning is encountered, but that is of course a [harmful](harmful.md) terminology as open source is an inherently harmful [capitalist](capitalism.md) movement ignoring the ethical question of freedom -- hence it is recommended to prefer using the term free hardware. Sometimes the acronym FOSH (free and open source hardware) is used neutrally, similarly to [FOSS](foss.md). Indeed we will find many different definitions of *free hardware* and *free technology*, usually with same core ideas but sometimes in disagreement; for example the [Trash Magic](trash_magic.md) manifesto states that free hardware and technology is that which "gives more than it takes", that it's only that which can be made from the available waste stream of current society even by a non-expert (i.e. being free not just legally but also [practically](de_facto.md)), which is further not encumbered by any intellectual property etc.; for this it excludes "open source" hardware from inclusion under free hardware (as "open source" hardware may require factories, high expertise etc.).

[GNU](gnu.md), just like [us](lrs.md), highly advocates for free hardware, though, unlike with software, they don't completely reject using non-free hardware nowadays, not just for practical reasons (purely free hardware basically doesn't exist), but also because hardware is fundamentally different from software and it is possible to use *some* non-free hardware (usually the older one) relatively safely, without sacrificing freedom. The [FSF](fsf.md) issues so called **[Respects Your Freedom](ryf.md)** (RYF) certification for non-malicious hardware products, both free and non-free, that can be used with 100% free software (even though RYF has also been a target of some criticism of free software activists).

We, [LRS](lrs.md), advocate for more strict criteria than just a free-licensed hardware design, for example we prefer complete [public domain](public_domain.md) and advocate high [simplicity](kiss.md) which is a prerequisite of true freedom -- see [less retarded hardware](less_retarded_hardware.md) for more. We also stress that [freedom distance](freedom_distance.md) has to be minimized.

The topic of free hardware is a bit messy, free hardware definition is not as straightforward as that of free software because hardware, a physical thing, has some inherently different properties than software and it is also not as easy to design and create so it evolves more slowly than software and it is much more difficult to create hardware completely from the ground up. Now consider the very question "what even is hardware"? There is a grey area between hardware and software, sometimes we see [firmware](firmware.md) as hardware, sometimes as software, sometimes pure software can be hardwired into a circuit so it basically behaves like hardware etc. Hardware design also has different levels, a higher level design may be free-licensed but its physical implementation may require existing lower level components that are non-free -- does such hardware count as free or not? How much down does free go -- do peripherals have to be free? Do the chips have to be free? Do the transistors themselves have to be free? We have to keep these things in mind. While in the software world it is usually quite easy to label a piece of software as free or not (at least legally), with hardware we rather tend to speak of different levels of freedom, at least for now.

## Existing Free And Freedom-Friendly Hardware And Firmware

{ I'm not so much into hardware, this may be incomplete or have some huge errors, as always double check and please forgive :) Report any errors you find, also send me suggestions, thanks. ~drummyfish }

TODO, WORK IN PROGRESS, UNDER CONSTRUCTION

The following is a list of hardware whose design is **at least to some degree** free/open (i.e. for example free designs that however may be using a non-free CPU, this is an issue discussed above):

- **[Arduino](arduino.md)**: Extremely popular single board microcontrollers that can be easily used to make various devices. Designs and software tools are free, however the name Arduino is trademarked AND the hardware designs are using existing proprietary components, e.g. the [AVR](avr.md) MCUs, i.e. Arduino is not 100% free from the ground up, but the degree of freedom is high and the hardware is kind of simple, i.e. friendly to tinkering and hacking.
- **[RISC-V](risc_v.md)**: Big project creating a free-licensed [instruction set architecture](isa.md), usable by anyone for anything etc. (however the RISC-V brand is [trademarked](trademark.md)). A number of free CPUs/SOC implementations exist (alongside many proprietary implementations), for example [PicoRV32](picorv32.md) or [Sodor](sodor.md).

The following is a list of some "freedom friendly" hardware, i.e. hardware that though partly or fully proprietary is not or can be made non-malicious to the user (has documented behavior, allows fully free software, librebooting, battery replacement, repairs etc.):

- **[Agon](agon.md)**: Simple game console.
- **[Ben NanoNote](ben_nanonote.md)**: tiny [GNU](gnu.md)/[Linux](linux.md) laptop whose design is free, however it utilizes e.g. a proprietary CPU.
- **[DragonBox Pyra](pyra.md)**: Upcoming small handheld computer running [GNU](gnu.md)/[Linux](linux.md) that *almost* meets the RYF criteria, schematics will be available, GPU drivers are sadly proprietary. Successor to OpenPandora.
- **[Librem 5](librem5.md)**: WARNING, this device has been criticized a lot. It's an "open"/privacy-friendly smartphone with free-licensed design running [GNU](gnu.md)/[Linux](linux.md), however it uses proprietary firmware (loaded from secondary CPU to sneakily comply with RYF) and the functionality is, according to reviews, horrible.
- **[MNT Reform](mnt_reform.md)**: "Open hardware" (free-licensed design but using proprietary components) laptop with [NXP](nxp.md) [ARM](arm.md) CPU and [Vivante](vivante.md) GPU that can run with free drivers, has no camera or microphone. Pretty expensive.
- **[Talos ES](talos_es.md)**: Very simple usable [CPU](cpu.md) that can be made at home.
- **Old [Thinkpad](thinkpad.md) laptops**: Old thinkpads such as [X200](x200.md), [T400](t400.md) and [T500](t500.md) are construction-wise superior to maybe any other laptop ever made, however despite being proprietary they are compatible with [libreboot](libreboot.md) and can be purchased with [Intel ME](intel_me.md) CPU backdoor disabled, offering complete control over the device, plus they can be bought relatively cheap. Very popular, some even certified ["Respects Your Freedom"](ryf.md) by the [FSF](fsf.md).
- **[OLinuXino](olinuxino.md)**: TODO
- **[OpenPandora](openpandora.md)**: Game console/tiny computer.
- **[Open consoles](open_console.md)** such as [Arduboy](arduboy.md), [Pokitto](pokitto.md) and [Gamebuino](gamebuino.md) usually utilize a lot of simple free hardware such as [Arduino](arduino.md), provide schematics, free libraries and encourage hacking.
- [Raspberry Pi](rpi.md) is not really free hardware but with free firmware such as [librerpi](librerpi.md) it can be quite freedom friendly.
- **Other proprietary laptops**: many mostly older laptops are freedom friendly, e.g. Asus C201 Chromebook. You can usually find these in the libreboot compatibility list.
- **[Pinephone](pinephone.md)**: Another "free/open" smartphone running GNU/Linux, probably better than Librem5, also uses some proprietary firmware (e.g. for Wifi), design is only source-available.
- **[Ringo MakerPhone](ringo.md)**: Educational Arduino dumbphone running on free software, by [Circuitmess](circuitmess.md). { I own one, is a bit buggy but [works](just_werks.md) for calls and messages. ~drummyfish }
- **[Ronja](ronja.md)**: Device for optical communication using ethernet protocol.
- **[Uzebox](uzebox.md)**: Very simple TV [game](game.md) [console](open_console.md).
- ...

The following is a list of [firmware](firmware.md), [operating systems](os.md) and software tools that can be used to liberate freedom-friendly proprietary devices:

- **[coreboot](coreboot.md), [libreboot](libreboot.md), [GNU boot](gnu_boot.md), [nonGeNUine Boot](nongenuine_boot.md) etc.**: More or less libre replacements for proprietary [BIOS](bios.md) in personal computers. Different projects here take different roads and tolerate different amounts of non-free binary blobs, just as different [Linux](linux.md) distros, so check out each one to pick whichever you like best.
- **[librerpi](librerpi.md)**: Libre boot firmware for [RPI](rpi.md).
- **[PostmarketOS](postmarketos.md)**: Mobile [GNU](gnu.md)/[Linux](linux.md) distribution that can be used to liberate smartphones.
- **[Replicant](replicant.md)**: Fork of [Android](android.md) mobile OS that replaces proprietary components with free software, can be used to liberate smartphones, though it is still [bloat](bloat.md).
- **[Rockbox](rockbox.md)**: Free firmware for digital audio players allowing replacement of the proprietary firmware and even improving on functionality and [GUI](gui.md).
- ...

## See Also

- [free software](free_software.md)
- [salvage computing](salvage_computing.md)
- [RYF](ryf.md)
- [public domain computer](public_domain_computer.md)
- [less retarded hardware](less_retarded_hardware.md)