# Everyone Does It

"Everyone does it" is an argument quite often used by simps to justify their unjustifiable actions. It is often used alongside the "[just doing my job](just_doing_my_job.md)" argument. This is not to be confused with ["monkey see monkey do"](monkey_see_monkey_do.md) behavior which just signifies mindless imitation; "everyone does it" is rather used as an excuse by a normie to do something that even he knows is bad, but which he just wants to do and he needs to say some words when someone tells him it's indeed bad -- it's similar to dog barking or [autist screeching](reeeeeeeee.md).

The argument has a valid use, however it is rarely used in the valid way. We humans, as well as other higher organisms, have evolved to mimic the behavior of others because such behavior is tried, others have tested such behavior for us (for example eating a certain plant that might potentially be poisonous) and have survived it, therefore it is likely also safe to do for us. So we have to realize that "everyone does it" is an **argument for safety, not for morality**. But people nowadays mostly use the argument as an excuse for their immoral behavior, i.e. something that's supposed to make bad things they do "not bad" because "if it was bad, others wouldn't be doing it". That's of course wrong, people do bad things and the argument "everyone does it" helps people do them, for example during the Nazi holocaust this excuse partially allowed some of the greatest atrocities in history. Nowadays during [capitalism](capitalism.md) it is used to excuse taking part unethical practices, e.g. those of corporations.

So if you tell someone "You shouldn't do this because it's bad" and he replies "Well, everyone does it", he's really (usually) saying "I know it's bad but it's safe for me to do".

The effect is of course abused by politicians: once you get a certain number of people moving in a certain shared direction, others will follow just by the need to mimic others. Note that just creating an illusion (using the tricks of [marketing](marketing.md)) of "everyone doing something" is enough -- that's why you see 150 year old grandmas in ads using modern smartphones -- it's to force old people into thinking that other old people are using smartphones so they have to do it as well.

Another potentially valid use of the argument is in the meaning of "everyone does it so I am FORCED to do it as well". For example an employer could argue "I have to abuse my employees otherwise I'll lose the edge on the market and will be defeated by those who continue to abuse their employees". This is very true but it seems like many people don't see or intend this meaning.

## See Also

- [excuse](excuse.md)
- [they do it too](they_do_it_too.md)
- [just doing my job](just_doing_my_job.md)
- [someone has to do it](someone_has_to_do_it.md)
- [if I didn't do it someone else would](someone_else_would.md)