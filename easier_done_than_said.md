# Easier Done Than Said

Easier done than said is the opposite of [easier said than done](easier_said_than_done.md).

Example: exhaling, as saying the word "exhaling" requires exhaling plus doing some extra work such as correctly shaping your mouth.