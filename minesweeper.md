# Minesweeper

Minesweeper is a simple yet entertaining singleplayer puzzle [game](game.md) whose objective is to deduce locations of all mines hidden in a square grid given certain numeric clues. You will most often see the game treated as a [minigame](minigame.md) but indeed it can be taken to much higher level for example with [speedrunning](speedrunning.md) or by expanding the concept to create a more complex game; as a matter of fact there is a dedicated community around competitive play of the game (see minesweepergame.com, their wiki nicely explains advanced strategies and techniques such as so called "1.5 click"). Minesweeper falls under the kind of games highly appreciated by [LRS](lrs.md) perspective -- it's very simple, easy to program (try it as an [exercise](exercises.md)) and learn but still highly [fun](fun.md) to play, it's mathematically [interesting](interesting.md) (example: the *Minesweeper theorem* states that any given board has the sum of its clue numbers same as that of its complement board) and can in theory be played even without a [computer](computer.md) (if someone else prepares you the board). Current form of the game seems to have evolved from 1980s games such as *Mined-Out* or *Relentless Logic* (which were in turn likely inspired by older games like *Battleships*) or even 1973 mainframe game called *Cube*, later it became popular mainly due to being one of default games that came with [Windows](windows.md).

{ Look up the video called *Minesweeper The Movie*, it's a funny mockup trailer of a Hollywood style movie based on this game. ~drummyfish }

```
 _ _ _ _ _ _ _ _ _
|_|_|_|_|_|_|_|_|_|
|_|_|#|_|_|_|_|_|_|
|_|_|3 2 1 1|#|2|_|
|_|#|1 . . 1 1 2|_|
|1 1 1 . . . . 1|_|
|. . . . . . . 1 1|
|. . . . . . . . .|
|. . . 1 2 2 1 . .|
|._._._1|_|_|1_._.|
```

*In progress game, `#` are marked mines.*

The **rules** are following: initially a *W* times *H* grid is created and *N* mines are placed on [random](randomness.md) squares (size of board and number of mines can affect difficulty). All squares are obscured so that the player doesn't see where the mines are. The player can dig on any square by clicking it -- if he clicks a mine square, it detonates and he loses (here all mines are revealed to him). Implementations normally make it so that the first click can never land on a mine (if it does, the mine is relocated), because that would be just sad. If the click is on empty square, then this square as well as all other mine-free squares reachable from this square (by walking in 4 principal directions) are revealed as mine-free. All revealed squares have a clue number which says the number of mines in its immediate 8 neighbor squares -- except for squares with clue number 0, all squares have their clue number written on them (often each number has its own color to help with pattern recognition). These clues help the player deduce where the mines are. Player can mark any hidden square with a mine symbol -- once all mine-free squares are revealed, the game is won (it's a bit funny that in some versions of the game, especially on low difficulty, one can win with a single click; more serious implementations put limits on the minimum number of required left clicks, so called 3BV number).

The game requires deductive thinking -- at least initially when one is learning, later on it turns almost entirely into a pattern recognition game. The patterns are very local and there isn't that many of them, so one learn them quickly. The game however DOES involve a bit of chance and luck because the player doesn't have complete information, situations may arise in which taking a guess is necessary, but these are relatively few and risk can be minimized with clever deduction.

Some patterns can be seen in the above image. For example a square with clue number *N* which has *N* neighbors mean that all its neighbors have mines -- this is often seen e.g. with 1s near corners (seen in the picture). We can also see at the bottom of the board that the two squares will both clearly contain mines. A more intricate pattern is seen near the square with number 3 -- the mine above it was deduced from the number 2 at its right and the number 1 next to the 2: according to the 2 square there have to be 2 mines somewhere in the 3 squares above it, but it's impossible for one mine to be above the 2 with the other being top-right from the 2 as that would place 2 mines in the area of the square with clue number 1, therefore one of the mines has to be top-left from the square with number 2 (above the square with 3). For more patterns see e.g. [this](https://minesweepergame.com/website/authoritative-minesweeper/wiki/Strategy).