# Cryptocurrency

Cryptocurrency, or just *crypto*, is a [digital](digital.md) [virtual](virtual.md) (non-physical) [currency](money.md) used on the [Internet](internet.md) which uses [cryptographic](cryptography.md) methods (electronic signatures etc.) to implement a [decentralized](decentralization.md) system in which there is no authority to control the currency (unlike e.g. with traditional currencies that are controlled by the state or systems of digital payments controlled by the banks that run these systems). Cryptocurrencies traditionally use so called **[blockchain](blockchain.md)** as the underlying technology and are practically always implemented as [FOSS](foss.md). Example of cryptocurrencies are [Bitcoin](bitcoin.md), [Monero](monero.md) or [Dogecoin](dogecoin.md).

The word *crypto* in *crpytocurrency* **doesn't imply that the currency provides or protects "[privacy](privacy.md)"** -- it rather refers to the cryptographic algorithms used to make the currency work -- even though thanks to the decentralization, anonymity and openness cryptocurrencies actually are mostly "privacy friendly" (up to the points of being considered the currency of criminals).

[LRS](lrs.md) sees cryptocurrencies as not only unnecessary [bullshit](bullshit.md), but downright as an **unethical** technology because [money](money.md) itself is unethical, plus the currencies based on proof of work waste not only human effort but also enormous amount of electricity and computing power that could be spent in a better way. Keep in mind that cryptocurrencies are a way of digitizing harmful concepts existing in society. Crypto is just an immensely expensive game in which people try to fuck each other over money that have been stolen from the people.

# History

TODO

# How It Works

Cryptocurrency is build on top of so called [blockchain](blockchain.md) -- a kind structure that holds records of transactions (exchanges of money or "coins", as called in the crypto world). Blockchain is a [data structure](data-structure.md) serving as a [database](database.md) of the system. As its name suggests, it consists of **blocks**. Each block contains various data, most important of which are performed transactions (e.g. "A sent 1 coin to B"), and each block points to a previous one (forming a [linked list](linked_list.md)). As new transactions are made, new blocks are created and appended at the end of the blockchain.

But where is the blockchain stored? It is not on a single computer; many computers participating in the system have their own copy of the blockchain and they share it together (similarly to how people share files via [torrents](torrent.md)).

But how do we know which one is the "official" blockchain? Can't just people start forging information in the blockchain and then distribute the fake blockchains? Isn't there a chaos if there are so many copies? Well yes, it would be messy -- that's why we need a **consensus** of the participants on which blockchain is the *real* one. And there are a few algorithms to ensure the consensus. Basically people can't just spam add new blocks, a new block to be added needs to be validated via some process (which depends on the specific algorithm) in order to be accepted by others. Two main algorithms for this are:

- [proof of work](proof_of_work.md): For a block to be confirmed it has to have a specific cryptographic puzzle solved, e.g. it may need to have appended some string that makes the block's hash some predetermined value. Participants try to solve this puzzle: finding the string is difficult and has to be done by [brute force](brute_force.md) (which wastes electricity and makes this method controversial). Once someone finds a solution, the block is confirmed and the solver gets a reward in coin -- this is therefore called **mining**.
- [proof of stake](proof_of_stake.md): This methods tries to waste less energy by not solving cryptographics puzzles but rather having some chosen participants validate/confirm the blocks. Basically participants can give some of their money at stake which then gives them a chance (proportional to the amount of money put at stake) to be chosen as validators. A validator is then chosen at random who will check the transactions and sign the block. For this they will get a small reward in coins. If they try to confirm fraudulent transactions (e.g. money sent from people without any money), the network will punish them by taking away the money they put at stake (so there is a financial motivation to not "cheat").

Can't people just forge transactions, e.g. by sending out a record that says someone else sent them money? This can be easily prevented by [digitally signing](digital_signature.md) the transactions, i.e. if there is e.g. a transaction "A sends 1 coint to B", it has to be signed by A to confirm that A really intended to send the money. But can't someone just copy-paste someone else's already signed transactions and try to perform them multiple times? This can also be prevented by e.g. numbering the transactions, i.e. recording something like "A sent 1 coin to B as his 1st transaction".

But where are one's coins actually stored? They're not explicitly stored anywhere; the amount of coins any participant has is deduced from the list of transactions, i.e. if it is known someone joined the network with 0 coins and there is a record of someone else sending him 1 coin, it is clear he now has 1 coin. For end users there are so called **wallets** which to them appear to store their coins, but a wallet is in fact just the set of cryptographic keys needed to perform transactions.

But why is blockchain even needed? Can't we just have a list of signed transactions without any blocks? Well, blockchain is designed to ensure coherency and the above mentioned consensus.






