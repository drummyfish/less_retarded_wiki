# Interplanetary Internet

Interplanetary Internet is at this time still a hypothetical extension of the [Internet](internet.md) to multiple planets. As mankind is getting closer to starting living on other planets and bodies such as [Mars](mars.md) and [Moon](moon.md), we have to start thinking about the challenges of creating a communication network between all of them. The greatest challenge is posed by the vast distances that increase the communication delay (which arises due to the limited [speed of light](speed_of_light.md)) and make errors such as [packet loss](packet_loss.md) much more painful. Two-way communication (i.e. request-response) to Moon and Mars can take even 2 seconds and 40 minutes respectively. Also things like planet motions, eclipses etc. pose problems to solve.

We can see that e.g. [real time](real_time.md) [Earth](earth.md)-Mars communication (e.g. [chat](chat.md) or videocalls) are physically impossible, so not only do we have to create new [network](network.md) [protocols](protocol.md) that minimize the there-and-back communication (things such as [handshakes](handshakes.md) are out of question) and implement great [redundancy](redundancy.md) for reliable recovery from loss of data traveling through space, we also need to design **new [user interfaces](ui.md)** and communication paradigms, i.e. we probably need to create a new messaging software for "interplanetary chat" that will for example show the earliest time at which the sender can expect an answer etc. [Interesting](interesting.md) shit to think about.

{ TFW no [Xonotic](xonotic.md) deathmatches with our Moon friends :( ~drummyfish }

For things like [Web](web.md), each planet would likely want to have its own "subweb" (distinguished e.g. by [TLDs](tld.md)) and [caches](cache.md) of other planets' webs for quick access. This way a man on Mars wouldn't have to wait 40 minutes for downloading a webpage from the Earh web but could immediately access that webpage's slightly delayed version, which is of course much better.

Research into this has already been ongoing for some time. InterPlaNet is a protocol developed by [NASA](nasa.md) and others to be the basis for interplanetary Internet.

## See Also

- [world broadcast](world_broadcast.md)