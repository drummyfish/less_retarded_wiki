# Racism

The term racism has nowadays two main definitions, due to the onset of [newspeak](newspeak.md):

- **original definition**: Great hatred and/or hostility towards specific [races](race.md) of people. For example the [Nazi](nazism.md) genocide of [Jews](jew.md) was an act of racism in the sense of the term's original meaning. Nowadays racism in this meaning is targetted especially against [white](white.md) people.
- **[newspeak](newspeak.md) definition**: Disagreement with the mainstream [pseudoleftist](pseudoleft.md) propaganda regarding the question of human [race](race.md), or just performing certain prohibited sins connected to it, e.g. saying the word [nigger](nigger.md). For example anyone who claims human race has a basis in biology or that there are any statistical differences between races at all is a racist in the modern meaning of the term.

## See Also

- [political correctness](political_correctness.md)
- [nigger](nigger.md)