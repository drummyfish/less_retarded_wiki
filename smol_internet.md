# Smol Internet

Smol Internet, smol web, small web, smol net, dork web, dumb web, poor man's web, web revival, web 1.0 and similar terms refer to [Internet](internet.md) and [web](www.md) [technology](tech.md) (such as [gopher](gopher.md), [gemini](gemini.md), plain [HTML](html.md) etc.) and communities that are smaller (see [minimalism](minimalism.md)), [simpler](kiss.md), less controlled/centralized and less [toxic](toxic.md) than the "big" mainstream/commercial Internet (especially the [web](www.md)) which due to [capitalism](capitalism.md) became littered with [shit](shit.md) like [ads](marketing.md), unbearable [bloat](bloat.md), [censorship](censorship.md), [spyware](spyware.md), corporate propaganda, masses of [retarded people](influencer.md), [bullshit](bullshit.md) ugly visuals like animations etc. Consider this analogy: the mainstream, i.e. [world wide web](www.md), [Discord](discord.md), [Facebook](facebook.md) etc., is like a big shiny city, but as the city grows and becomes too stressful, overcrowded, stinky with smog, hasted, overcontrolled with police and ads on every corner, people start to move to the countryside where life is simpler and happier -- smol Internet is the countryside.

What EXACTLY constitutes the Smol Internet? Of course we don't really have exact definitions besides what people write on blogs, it also depends on the exact term we use, e.g. smol web may refer specifically to lightweight self-hosted websites while smol net will also include different protocols than [HTTP(s)](http.md) (i.e. things outside the Web). But we are usually talking about simpler ([KISS](kiss.md), [suckless](suckless.md), ...), alternative, [decentralized](decentralization.md), [self hosted](self_hosting.md) technology (protocols, servers, ...), and communities that strive to escape commercially spoiled spaces. These communities don't aim to grow to big sizes or compete with the mainstream web, they do not seek to replace the web or achieve the same things (popularity, profitability, ...) but rather bring back the quality the web (and similar services such as [Usenet](usenet.md)) used to have in the early days such as relative freedom, unrestricted sharing, [free speech](free_speech.md), [simplicity](minimalism.md), [decentralization](decentralization.md), creative personal sites, [comfiness](comfy.md), [fun](fun.md) and so on. It is for the people, not for companies and [corporations](corporation.md). Smol Internet usually refers to [gopher](gopher.md) and [gemini](gemini.md), the alternative protocols to [HTTP](http.md), the basis of the web. Smol Web on the other hand stands for simple, plain [HTML](html.md) web 1.0 static personal/community sites on the web itself which are however hosted independently, often on one's own server (self hosted) or a server of some volunteer or non-profit -- such sites can be searched e.g. with the [wiby](wiby.md) search engine. It may also include small communities such as [pubnixes](pubnix.md) like [SDF](sdf.md) and [tildeverse](tildeverse.md). Other [KISS](kiss.md) communication technology such as [email](email.md) and [IRC](irc.md) may also fall under Smol Internet.

BEWARE: even the Smol Net gets its fair share of toxicity, nowadays especially gemini is littered with [SJW](sjw.md) fascists and [pseudominimalists](pseudominimalism.md). Avoid such places if you can.

## See Also

- [web 1.0](web_10.md)
- [web 0.5](web_05.md)
- [Fediverse](fediverse.md)
- [tildeverse](tildeverse.md)
- [Usenet](usenet.md)
- [geocities](geocities.md)
- [neocities](neocities.md)
- [soynet](soynet.md)
- [webring](webring.md)
- [incelosphere](incelosphere.md)