# Pseudo 3D

The term pseudo 3D, also 2.5D or primitive 3D, is used for [computer graphics](graphics.md) that tries to create the illusion of [3D](3d.md) [rendering](rendering.md) while in fact only utilizing simpler techniques; genuine 3D rendering is in this case called [true 3D](true_3d.md). On consumer computers it is nowadays mostly a thing of the past as everything including cell phones now has a powerful [GPU](gpu.md) capable or most advanced 3D rendering, nevertheless for [suckless](suckless.md)/[KISS](kiss.md)/[LRS](lrs.md) programming the techniques used in the past are very interesting and useful.

For example [BSP rendering](bsp.md) rendering in early games such as [Doom](doom.md) is generally called pseudo 3D in the mainstream, however it is pretty debatable what exactly should classify as true 3D and what not because any computer rendering technique will inevitably have some kind of simplification of the true 3D reality of real life. And so the debate of "was Doom really 3D" arises. One side argues that in Doom's BSP rendering it for example wasn't possible to look up and down or have rooms above other rooms, all due to the limitations of the rendering system which this side sees as "not real 3D". However even modern 3D renderers have limitations such as mostly being able to only render models made out of triangles (while reality can have completely smooth shapes) or having a limited resolution of textures. Where to draw the line for "true 3D" is subjective -- we see it as reasonable to say that **if it looks 3D, it IS 3D**, i.e. we think Doom's graphics WAS really 3D, albeit limited. For this reason we also advise to rather use the term **primitive 3D** rather than pseudo 3D.

Techniques associated with primitive 3D are for example [2D raycasting](raycasting.md), [BSP rendering](bsp.md), [mode7](mode7.md), [parallax scrolling](parallax.md), [voxel space](voxel_space.md) terrain rendering or perspective-scaled [sprites](sprite.md).

## See Also

- [software rendering](sw_rendering.md)
- [bsp rendering](bsp.md)
- [raycasting](raycasting.md)
