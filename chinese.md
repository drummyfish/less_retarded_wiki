# Chinese

Chinese is one of the most [bloated](bloat.md) natural human languages, spoken in China.

Any text in chinese basically looks like this:

```
#########################
#########################
#########################
#########################
```

It is so bloated that some Chinese people literally don't understand other Chinese people.