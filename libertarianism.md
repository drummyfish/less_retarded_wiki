# Libertarianism

*Not to be [confused](often_confused.md) with [liberalism](liberalism.md).*

Libertarianism, also known as the *redneck ideology*, is a [harmful](harmful.md) political ideology whose definition is quite broad and not very clear, but which in essence gives highest priority to individual "liberty" and seeks to minimize the role of [state](state.md) (but typically without wanting to remove it). A bit like [anarchism](anarchism.md), libertarianism has many branches which frequently greatly diverge and even oppose each other, some are called more "leftist", some more "rightist" -- libertarianism usually tries to pretend to be focusing on the people, i.e. their "liberties", pseudoequality ("equality before law", "equality of opportunity", ...), oppose "the kind of corporate [capitalism](capitalism.md) we have today", believing some kind of "saner" version of it can work (which it can't), and claims that people can form a working, decentralized society by loose associations, however, unlike anarchism which opposes state and any kind of hierarchy altogether (with [true anarchism](anpac.md) also opposing any violence), libertarianism typically wants to preserve some functions of the state such as courts and justice for protection against crime, and it acknowledges property as a sacred thing that may even be defended by violence, i.e. libertarianism just replaces the rule of states by rule of private subjects, getting quite close to ["anarcho" capitalism](ancap.md), the stupidest idea yet conceived. Libertarians basically adopts the **"law of the jungle"** or **"wild west"** mindset. So it's [shit](shit.md), do not subscribe.

The [color](color.md) associated with libertarianism is yellow, which symbolizes piss.

[USA](usa.md) is essentially just a land where libertarians [battle](fight_culture.md) with [liberals](liberalism.md). Both camps are similarly stupid.

{ Some bashing by digdeeper: https://digdeeper.neocities.org/articles/libertarianism. ~drummyfish }

