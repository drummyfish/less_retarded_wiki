# "Anarcho" Capitali$m

*Not to be confused with [anarchism](anarchism.md).*

So called "anarcho capitalism" (ancap for short, not to be confused with [anpac](anpac.md) or any form of [anarchism](anarchism.md)) is probably the worst, most retarded and most dangerous idea in the [history](history.md) since the Big Bang, and that is the idea of supporting [capitalism](capitalism.md) absolutely unrestricted by state or anything else. You'd have to have some worms in your brain to support that. No one with at least 10 brain cells and/or anyone who has spent at least 3 seconds observing the world could come up with such a stupid, stupid idea. We, of course, completely reject this shit.

It has to be noted that **"anarcho capitalism" is not real [anarchism](anarchism.md)**, despite its name. Great majority of anarchists strictly reject this ideology as any form of capitalism is completely incompatible with anarchism -- anarchism is defined as opposing any social hierarchy and oppression, while capitalism is almost purely based on many types of hierarchies (internal corporate hierarchies, hierarchies between companies, hierarchies of social classes of different wealth etc.) and oppression (employee by employer, consumer by corporation etc.). Why do they call it *anarcho* capitalism then? Well, partly because they're stupid and don't know what they're talking about (otherwise they couldn't come up with such an idea in the first place) and secondly, as any capitalists, they want to deceive and ride on the train of the *anarchist* brand -- this is not new, [Nazis](nazi.md) also called themselves *socialists* despite being the complete opposite.

The colors on their flag are black and yellow (this symbolizes shit and piss).

It is kind of another bullshit kind of "anarchism" just like  "[anarcha feminism](anarcha_feminism.md)" etc.

## The Worst Idea In History

As if [capitalism](capitalism.md) wasn't extremely bad already, "anarcho" capitalists want to get rid of the last mechanisms that are supposed to protect the people from corporations -- [states](state.md). We, as anarchists ourselves, of course see states as eventually harmful, but they cannot go before we get rid of capitalism first. Why? Well, imagine all the bad things corporations would want to do but can't because there are laws preventing them -- in "anarcho" capitalism they can do them.

Firstly this means anything is allowed, any unethical, unfair business practice, including slavery, physical violence, blackmailing, rape, worst psychological torture, nuclear weapons, anything that makes you the winner in the jungle system. Except that this jungle is not like the old, self-regulating jungle in which you could only reach limited power, this jungle offers, through modern technology, potentially limitless power with instant worldwide communication and surveillance technology, with mass production, genetic engineering, AI and weapons capable of destroying the planet.

Secondly the idea of getting rid of a state in capitalism doesn't even make sense because **if we get rid of the state, the strongest corporation will become the state**, only with the difference that state is at least *supposed* to work for the people while a corporation is only by its very definition supposed to care solely about its own endless profit on the detriment of people. Therefore if we scratch the state, McDonalds or Coca Cola or Micro$oft -- whoever is the strongest -- hires a literal army and physically destroys all its competition, then starts ruling the world and making its own laws -- laws that only serve the further growth of that corporation such as that everyone is forced to work 16 hour shifts every day until he falls dead. Don't like it? They kill your whole family, no problem. 100% of civilization will experience the worst kind of suffering, maybe except for the CEO of McDonald's, the world corporation, until the planet's environment is destroyed and everyone hopefully dies, as death is what we'll wish for.

A typical "anarcho"capitalist is a redneck who probably thinks the only thing preventing him from beating a corporation at its own game is state holding him back with high taxes -- he thinks that if he's allowed to keep all his crops he'll become a beast starting his own business that will go on beating Facebook, i.e. he thinks that if he can keep all five of all five carrots he grows on his field, he will somehow become so powerful he can bring down an army of ten thousand men with the most advanced technology who by the way can now also keep all money it makes, because he heard this in some fairy tale about Henry Ford or something. Of course that's highly laughable and pathetic, but indeed typically [American](usa.md).

All in all, "anarcho" capitalism is advocated mostly by children who don't know a tiny bit about anything, by children who are being brainwashed daily in schools by capitalist propaganda, with no education besides an endless stream of ads from their smartphones, or capability of thinking on their own. However, these children are who will run the world soon. It is sad, it's not really their fault, but through them the system will probably come into existence. Sadly "anarcho" capitalism is already a real danger and a very likely future. It will likely be the beginning of our civilization's greatest agony. We don't know what to do against it other than provide education.

God be with us.

## See Also

- [capitalism](capitalism.md)
- [libertarianism](libertarianism.md)