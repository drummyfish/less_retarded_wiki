# Small3dlib

Small3dlib (S3L) is a very portable [LRS](lrs.md)/[suckless](suckless.md) [single header](signle_header.md) 3D [software renderer](software_rendering.md) [library](library.md) written by [drummyfish](drummyfish.md) in the [C](c.md) programming language. It is very efficient and runs on many resource-limited computers such as embedded [open consoles](open_console.md). It is similar to [TinyGL](tinygl.md), but yet more simple. Small3dlib is [public domain](public_domain.md) [free software](free_software.md) under [CC0](cc0.md), written in under 3000 [lines of code](loc.md).

The repository is available at https://codeberg.org/drummyfish/small3dlib and https://gitlab.com/drummyfish/small3dlib.

Small3dlib can be used for rendering 3D graphics on almost any device as it is written in pure [C99](c99.md) without any software and hardware [dependencies](dependency.md); it doesn't use the standard library, [floating point](float.md) or [GPU](gpu.md). It is also very flexible, not forcing any preprogrammed [shaders](shader.md) -- instead it only computes which pixels should be rasterized and lets the programmer of the main application decide himself what should be done with these pixels (this is typically applying some [shading](shading.md) and writing them to screen).

Some of the rendering features include:

- different drawing strategies ([z-buffer](z_buffer.md), sorted rendering, ...)
- top-left rasterization rule (no holes between or overlaps of triangles)
- [perspective correction](perspective_correction.md) (either none, full or approximate)
- different near plane collision handling strategies (culling, clipping, shifting, ...)

```
                                                            ##x
                                                         ####xx
                                                      ######xxxx
                ..xx                               ########xxxxx
             .....xxx                           ##########xxxxxx
          .......xxxxx                       #############xxxxxxx
          .......xxxxxxx                    #############xxxxxxxx
          .......xxxxxxxx                  #############xxxxxxxxxx
         .......xxxxxxxxxx                #############xxxxxxxxxxx
         .......xxxxxxxxxxx              #############xxxxxxxxxxxx
        ........xxxxxxxxxxxxx           ##############xxxxxxxxxxxxx
        .......xxxxxxxxxxxxxxx         ##############xxxxxxxxxxxxxx
        .......xxxxxxxxxxxxxxxx       ##############xxxxxxxxxxxxxxxx
       ........xxxxxxxxxxxxxxxxx     ##############xxxxxxxxxxxxxxxxx
       .......xxxxxxxxxxxxxxxxxx    ##############xxxxxxxxxxxxxxxxxx
      ........xxxxxxxxxxxxxxxxxx   ................xxxxxxxxxxxxxxxxxx
      ........xxxxxxxxxxxxxxxxxx    ................xxxxxxxxxxxxxxxxx
      .......xxxxxxxxxxxxxxxxxx      ...............xxxxxxxxxxxxxxxxx
     ........xxxxxxxxxxxxxxxxxx      ................xxxxxxxxxxxxxxxx
     ........xxxxxxxxxxxxxxxxxx       ...............xxxxxxxxxxxxxxx
    ........xxxxxxxxxxxxxxxxxxx        ...............xxxxxxxxxxxxxx
    ........xxxxxxxxxxxxxxxxxx         ................xxxxxxxxxxxx
    ........xxxxxxxxxxxxxxxxxx          ...............xxxxxxxxxxxx
   ........xxxxxxxxxxxxxxxxxxx           ...............xxxxxxxxxx
   ........xxxxxxxxxxxxxxxxxxx           ...............xxxxxxxxxx
  .........xxxxxxxxxxxxxxxxxx             ...............xxxxxxxx
         #xxxxxxxxxxxxxxxxxxx              ..............xxxxxxxx
               #xxxxxxxxxxxxx              ...............xxxxxx
                      xxxxxxx                 .............xxxxx
                                                 ..........xxxx
                                                    ........xxx
                                                       .....xx
                                                          ...x
```

*Simple ASCII rendering made with small3dlib.*
