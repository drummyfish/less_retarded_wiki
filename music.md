# Music

Music is an auditory [art](art.md) whose aim is to create [pleasant](beautiful.md) sound of longer duration that usually adheres to some rules and structure, such as those of melody, harmony, rhythm and repetition. Music has played a huge role throughout all history of human [culture](culture.md). It is impossible to precisely define what *music* is as the term is [fuzzy](fuzzy.md), i.e. its borders are unclear; what one individual or culture considers music may to another one sound like [noise](noise.md) without any artistic value, and whatever rule we set in music is never set in stone and will be broken by some artists (there exists music without chords, melody, harmony, rhythm, repetition... even without any sound at all, see *Four Minutes Thirty Three Seconds*). Music is mostly created by singing and playing musical instruments such as [piano](piano.md), guitar or drums, but it may contain also other sounds; it can be recorded and played back, and in all creation, recording and playing back [computers](computer.md) are widely used nowadays.

**Music is deeply about [math](math.md)**, though most musicians don't actually have much clue about it and just play "intuitively", by feel and by the ear. Nevertheless the theory of scales, musical intervals, harmony, rhythm and other elements of music is quite complex;

**[Copyright](copyright.md) of music**: TODO (esp. soundfonts etc.).

## Modern Western Music + How To Just Make Noice Music Without PhD

{ I don't actually know that much about the theory, I will only write as much as I know, which is possibly somewhat simplified, but suffices for some kind of overview. Please keep this in mind and don't eat me. ~drummyfish }

Our current western music is almost exclusively based on major and minor diatonic scales with 12 equal temperament tuning -- i.e. basically our scales differ just by their transposition and have the same structure, that of 5 whole tone steps and 2 semitone steps in the same relative places, AND a semitone step always corresponds to the multiplying factor of 12th root of 2 -- this all is basically what we nowadays find on our pianos and in our songs and other compositions. 4/4 rhythm is most common but other ones appear, e.g. 3/4. Yeah this may sound kinda too nerdy, but it's just to set clear what we'll work with in this section. Here we will just suppose this kind of music. Also western music has some common structures such as verses, choruses, bridges etc.; lyrics of music follow many rules of poetry, they utilizes rhymes, rhythm based on syllables, choice of pleasant sounding words etc.

**Why are we using this specific scale n shit, why are the notes like this bruh?** TODO

**Music is greatly about breaking the rules**, like most other [art](art.md) anyway -- you have to learn the rules and respect them most of the time, but respecting them all the time results in sterile, soulless music; a basic rule is therefore to break the rules IN APPROPRIATE PLACES, i.e. where such a rule break will result in emotional response, in something interesting, unique, ... This includes for example leaving the scale for a while, adding disharmony, adding/skipping a beat in one bar, ...

**If you wanna learn music, firstly you should get something with piano keyboard**: musical keyboard, electronic piano, even virtual software piano, ... The reason being that the keys really help you understand what's going on, the piano keyboard quite nicely visually represent the notes (there is a reason every music software uses the piano roll). Guitar or flute on the other hand will seem much more confusing; of course you can learn these instruments, but first start with the piano keyboard.

```
    | |C#| |D#| | |F#| |G#||A#| | |
    | |Db| |Eb| | |Gb| |Ab||Bb| | |
    | |__| |__| | |__| |__||__| | |__
... |   |   |   |   |   |   |   |   | ...
    | C | D | E | F | G | A | B | C |
   _|___|___|___|___|___|___|___|___|__   
```

*Tones on piano keyboard, the "big keys" are [white](white.md), the "smaller keys on top" are [black](black.md).*

OK so above we have part of a piano keyboard, tones go from lower (left) to higher (right), the keyboard tones just repeat the same above and below. The white keys are named simply A, B, C, ..., the black keys are named by their neighboring white key either by adding *#* (sharp) to the left note or by adding *b* (flat) to the right note (notes such as C# and Db can be considered the same within the scales we are dealing with). Note: it is convenient to see C as the "start tone" (instead of A) because then we get a nice major scale that has no black keys in it and is easy to play on piano; just ignore this and suppose we kind of "start" on C for now.

Take a look at the C note at the left for example; we can see there is another C on the right; the latter C is one **octave** above, i.e. it is the "same" note by name but it is also higher (for this we sometimes number the notes as C2, C3 etc.). The same goes for any other tone, each one has its different versions in different octaves. Kind of like the color red has different versions, a lighter one, a darker one etc. Octave is a basic interval we have to remember, **a tone that's one octave above another tone has twice its frequency**, so e.g. if C2 has 65 hertz, C3 has 130 hertz etc. This means that **music intervals are [logarithmic](log.md), NOT linear!** I.e. an interval (such as octave) says a number by which we have to MULTIPLY a frequency to get the higher frequency, NOT a number which we would have to add. This is extremely important.

Other important intervals are **tone** and **semitone**. Semitone is a step from one key to the immediately next key (even from white to black and vice versa), for example from C to C#, from E to F, from G# to A etc. A tone is two semitones, e.g. from C to D, from F# to G# etc. There are 12 semitones in one octave (you have to make 12 steps from one tone to get to that tone's higher octave version), so a semitone has a multiplying factor of 2^1/12 (12th root of two). For example C2 being 65 hertz, D2 is 65 * 2^1/12 ~= 69 hertz. This makes sense as if you make 12 steps then you just multiply 12th root of two twelve times and are left simply with multiply by 2, i.e. one octave.

TODO: chords, scales, melody, harmony, beat, bass, drums, riffs, transpositions, tempo, polyphony ...

## Music And Computers/Programming

TODO: midi, bytebeat, tracker music, waveforms, formats, procedural music, AI music, ...

## LRS Music

TODO

**What's the most [suckless](suckless.md) musical instrument?** Definitive answer is hard to give, but here are a few suggestions for very [KISS](kiss.md) ones:

- **drums**: Extremely satisfying to play, not so difficult to learn, quite easy to make yourself (for inspiration see how street drummers in poor countries do it: it's enough to take a few plastic buckets, two sticks and you have nice basic drums).
- **keyboard percussion instruments** like xylophone, malimbe etc.: Potentially easy to play and make at home, just take sticks and then take some kind of objects that can be tuned (pieces of wood, metal, glasses with water, ...) and make a keyboard out of them.
- **melodica** (kind of "flute with piano keys"): Not the simplest but still super cheap to get, small to carry around anywhere, sounds nice and you get kind of a mini piano with that super nice and convenient keyboard, it can be easy to learn, especially if you've already played piano.
- **single string instruments (monochords)**: Potentially easier to learn and play and make at home.
- **voice**: You automatically have it, you always have it with you everywhere and it sounds beautiful, on top of music can also communicate AND can even be used as percussive instrument (see [beatbox](beatbox.md)).
- **wind instruments** like flutes, ocarinas etc.: They're very small, have beautiful sound and can even be made at home (from a pipe, wood, whatever...).
- Something like a [music box](music_box.md)?
- ...

What's the most [bloated](bloat.md) instrument? Theatre organ looked like one but pipe organs may be on a similar level. Orchestrion also. We have to ask ourselves what counts as an instrument, for example if we consider orchestra to be the conductor's instrument or if we consider a super bloated DAW with hundreds of VSTs an instrument, then these may aspire for most bloated ones.