#!/bin/sh
# Creates a PDF version of the wiki.

echo "making PDF"

./make_html_single.sh

cat lrs_wiki.html | sed "s/<code> /<code>\n /" > _tmp.html
htmldoc --bodyfont sans --fontsize 10 --gray --left 8mm --right 8mm --top 8mm --bottom 8mm --continuous --header ... --header1 ... --footer ct/ -t pdf -f lrs_wiki.pdf _tmp.html
rm _tmp.html

echo "done"
