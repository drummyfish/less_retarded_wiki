# SIGBOVIK

SIGBOVIK ([special interest group](sig.md) on Harry Q. Bovik) is a [computer science conference](compsci.md) running since 2007 that focuses on researching and presenting [fun](fun.md) ideas in fun ways, [scientifically](science.md) but in a lighthearted [hacker](hacking.md) spirit similar to e.g. [esoteric programming languages](esolang.md) research or the [IOCCC](ioccc.md). SIGBOVIK has its own proceedings just like other scientific conferences, the contributors are usually professional researchers and experts in computer science. The name seems to be a reference to the "serious" conferences such as [SIGGRAPH](siggraph.md), SIGMOD etc. (SIGBOVIK is organized by the *Association for Computational Heresy* while the "serious" SIGs are run by *Asscoiation for Computing Machinery*, ACM).

A famous contributor to the conference is for instance Tom7, a [PhD](phd.md) who makes absolutely lovely [youtube](youtube.md) videos about his fun research (e.g. this one is excellent https://www.youtube.com/watch?v=DpXy041BIlA).

{ Skimming through the proceedings sadly most of the stuff seems rather silly, though there are a few good papers, usually those by Tom7. Maybe I'm just dumb. ~drummyfish }

## See Also

- [ioccc](ioccc.md)
- [NaNoGenMo](nanogenmo.md)