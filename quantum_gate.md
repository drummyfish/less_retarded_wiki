# Quantum Gate

{ Currently studying this, there may be errors. ~drummyfish }

Quantum (logic) gate is a [quantum computing](quantum.md) equivalent of a traditional [logic gate](logic_gate.md). A quantum gate takes as an input *N* [qubits](qubit.md) and transforms their states to new states (this is different from classical logical gates that may potentially have a different number of input and output values).

Quantum gates are represented by [complex](complex_number.md) [matrices](matrix.md) that transform the qubit states (which can be seen as points in multidimensional space, see Bloch sphere). A gate operating on *N* qubits is represented by a *2^N*x*2^N* matrix. These matrices have to be **unitary**. Operations performed by quantum gates may be reversed, unlike those of classical logic gates.

We normally represent a single qubit state with a **column** [vector](vector.md) *|a> = a0 * |0> + a1 * |1> => [a0, a1]* (look up bra-ket notation). Multiple qubit states are represented as a [tensor product](tensor_product.md) of the individual state, e.g. *|a,b> =  [a0 * b0, a0 * b1, a1 * b0, a1 * b1]*. Applying a quantum gate *G* to such a qubit vector *q* is performed by simple matrix multiplication: *G * v*.

## Basic gates

Here are some of the most common quantum gates.

### Identity

Acts on 1 qubit, leaves the qubit state unchanged.

```
1 0
0 1
```

## Pauli Gates

Act on 1 qubit. There are three types of Pauli gates: X, Y and Z, each one rotates the qubit about the respective axis by [pi](pi.md) radians.

The X gate is:

```
0 1
1 0
```

The Y gate is:

```
0 -i
i  0
```

The Z gate is:

```
1  0
0 -1
```

## NOT

The not gate is identical to the Pauli X gate. It acts on 1 qubit and switches the probabilities of measuring 0 vs 1.

## CNOT

Controlled NOT, acts on 2 qubits. Performs NOT on the second qubit if the first qubit is *|1>*.

```
1 0 0 0
0 1 0 0
0 0 0 1
0 0 1 0
```

TODO
