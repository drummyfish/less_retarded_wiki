# Smart

*Smart, smells like fart.*

The adjective "smart", as in e.g. *smartphone*, is in the context of [modern](modern.md) [capitalist technology](capitalist_technology.md) used as a euphemism for malicious features that include [spyware](spyware.md), [bloat](bloat.md), obscurity, [DRM](drm.md), [ads](marketing.md), programmed [planned obsolescence](planned_obsolescence.md), unnecessary [dependencies](dependency.md) (such as required Internet connection), anti-repair design and others; it is the opposite of [dumb](dumb.md). "Smart" technology is far inferior to the traditional [dumb](dumb.md) technology and usually just downright [harmful](harmful.md) to its users and society as a whole, but normal (i.e. retarded) people think it's good because it has a cool name, so they buy and support such technology. They are [slowly boiled](slowly_boiling_the_frog.md) to accept "smart" technology as the standard. Orwell is rotating in his grave.

[Richard Stallman](rms.md) has called smart phones "Stalin's dream" -- a device Stalin would wish every citizen of [Soviet Union](ussr.md) had so that he could be tracked and spied on.

## See Also

- [dumb](dumb.md)