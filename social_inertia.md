# Social Inertia

Social inertia appears when a social group continues to behave in established ways chiefly because it has behaved that way for a long time, and that even if such behavior is no longer well rationally justified.