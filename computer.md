# Computer

The word *computer* may be defined in countless ways and can also adopt many different meanings; a somewhat common definition may be this: computer is a machine that automatically performs mathematical computations. We can also see it as a machine for processing [information](information.md), manipulating symbols or, very generally, as any tool that helps computation, in which case not just laptops, desktops and cellphones fit the definition, but also primitive computers like a [sundial](sundial.md), one's fingers or even a [mathematical](math.md) formula. But nowadays the word of course implicitly implies an electronic [digital](digital.md) computer.

The electronic digital computer turned out to be one of the greatest [technological](tech.md) inventions in [history](history.md) for numerous reasons -- firstly computers allowed creation of many other things which previously required too complex calculations, such as highly complex planes, space rockets and undreamed of factories (and, of course, yet more powerful computers which is why we've seen the exponential growth in computer power), they also allow us to crunch extreme volumes of data and accelerate [science](science.md); secondly they offered extremely advanced work tools like [robots](robotics.md), virtual 3D visualizations, [artificial intelligence](ai.md) and physics simulators, and they also gave us high quality, cheap [multimedia](multimedia.md) and entertainment like [games](game.md) -- with computers anyone can shoot video, record music, carry around hundreds of movies in his pocket or fly a virtual plane. Most important however is probably the fact that computers enabled the [Internet](internet.md) -- by this they forever changed the world.

We can divide computers based on many attributes, e.g.:

- by continuous or discrete **representation of data**: [digital](digital.md) vs [analog](analog.md)
- by **way of existing**:
  - **physical**: existing as a physical object
    - by **[hardware](hw.md) technology**: [electronic](electronics.md) ("lightning in sand"), [mechanical](mechanical.md), [quantum](quantum.md), biological etc.
  - **virtual/abstract**: existing as an abstract idea of a computer, e.g. [Turing machine](turing_machine.md), [MIX](mix.md) or various [fantasy consoles](fantasy_console.md); see also [ISA](isa.md)
- by **purpose**: special purpose vs general purpose, [personal](pc.md), [server](server.md), [calculator](calculator.md), [embedded](embedded.md), [workstation](workstation.md), [supercomputers](supercomputer.md), [gaming](game.md) computer etc.
- by **[programmability](programming.md)**: non-programmable, partially or fully programmable
- by the theoretical **[model of computation](model_of_computation.md)** it is based on: [Turing machine](turing_machine.md), [lambda calculus](lambda_calculus.md) etc.
- by **computational power**: how difficult problems the computer is able to solve, i.e. where in the [Chomsky hierarchy](chomsky_hierarchy.md) it stands (typically we want [Turing complete](turing_completeness.md) computers)
- by **other criteria**: price, reliability, durability etc.

```
            ______________
           |  ________  | \_
           | |>..     | | : \                        ||
           | |        | | :  ]                   |:==-'              
           | |________| | :_/                 ___||___
    ___    |____________|_/'.    ___         /########\
   / \ \        \...../      '. / \ \        |""""""""|
  |;:;| |  _____/_____\_____  :|;:;| |       |O O O O |
  |;:;| |:|[o][o],,, === |  |.'|;:;| |       || | | | |
  |___|_| |______________|__|:.|___|_|       || | | | |
                      __...--':              ||,|,|,|,|
                 .-'''    .-''               \########/
     ___________;__     _:_
    /:::::::.::'::/|   /-'-)
   /:::::::'.:.:://   (___/ 
   """""""""""""""   
```

*On the left typical personal computer, with case, monitor, keyboard, mouse and speakers; on the right a pocket mechanical calculator of the Curta type.*

Computers are theoretically studied by [computer science](compsci.md). The kind of computer we normally talk about consists of two main parts:

- **[hardware](hw.md)**: physical parts
- **[software](sw.md)**: [programs](program.md) executed by the hardware, made by [programmers](programmer.md)

**The power of computers is mathematically limited**, [Alan Turing](turing.md) mathematically proved that there exist problems that can never be completely solved by any [algorithm](algorithm.md), i.e. there are problems a computer (including our [brain](brain.md)) will never be able to solve (even if solution exists). This is related to the fact that the power of mathematics itself is limited in a similar way (see [Godel's theorems](incompleteness_theorems.md)). Turing also invented the theoretical model of a computer called the [Turing machine](turing_machine.md). Besides the mentioned theoretical limitation, many solvable problems may take too long to compute, at least with computers we currently know (see [computational complexity](computational_complexity.md) and [P vs NP](p_vs_np.md)).

And let's also mention some [curious](interesting.md) **statistics** and facts about computers as of the year 2024. The fist computer in modern sense of the word is frequently considered to have been the Analytical Engine designed in 1837 by an Englishman Charles Babbage, a general purpose [mechanical computer](mechanical_computer.md) which he however never constructed. After this the computers such as the Z1 (1938) and Z3 (1941) of a German inventor Konrad Zuse are considered to be the truly first "modern" computers. Shortly after the year 2000 the number of US households that had a computer surpassed 50%. The fastest [supercomputer](supercomputer.md) of today is Frontier (Tennessee, [USA](usa.md)) which achieved computation speed of 1.102 exaFLOPS (that is over 10^18 [floating point](float.md) operations per second) with power 22.7 MW, using [Linux](linux.md) as its kernel (like all top 500 supercomputers). Over time transistors have been getting much smaller -- there is the famous **[Moore's law](moores_law.md)** which states that number of transistors in a chip doubles about every two years. Currently we are able to manufacture [transistors](transistor.md) as small as a few nanometers and chips have billions of them. { There's some blurriness about exact size, apparently the new "X nanometers" labels are just [marketing](marketing.md) lies. ~drummyfish }

## Typical Computer

Computers we ordinarily talk about in everyday conversations are [electronic](electronics.md) [digital](digital.md) mostly personal computers such as [desktops](desktop.md) and [laptops](laptop.md), possibly also [cell phones](phone.md), [tablets](tablet.md) etc.

Such a computer consists of some kind of [case](case.md) (chassis), internal [hardware](hardware.md) plus [peripheral devices](peripheral.md) that serve for [input and output](io.md) -- these are for example a [keyboard](keyboard.md) and [mouse](mouse.md) (input devices), a [monitor](monitor.md) (output device) or [harddisk](hdd.md) (input/output device). The internals of the computer normally include:

- **[motherboard](motherboard.md)**: The main electronic circuit of the computer into which other components are plugged and which creates the network and interfaces that interconnect them (a [chipset](chipset.md)). It contains slots for expansion cards as well as connectors for external devices, e.g. [USB](usb.md). In a small memory on the board there is the most basic software ([firmware](firmware.md)), such as [BIOS](bios.md), to e.g. enable installation of other software. The board also carries the [clock](clock.md) generator for synchronization of all hardware, heat sensors etc. 
- **[CPU](cpu.md)** (central processing unit): Core of the computer, the chip plugged into motherboard that performs general calculations and which runs [programs](program.md), i.e. [software](sw.md).
- **[RAM](ram.md)/working memory/main memory**: Lower capacity volatile (temporary, erased when powered off) working memory of the computer, plugged into motherboard. It is used as a "pen and paper" by the CPU when performing calculations.
- **[disk](disk.md)**: [Non-volatile](volatile.md) (persisting when powered off) large capacity memory for storing [files](file.md) and other [data](data.md), connected to the motherboard via some kind of [bus](bus.md). Different types of disks exist, most commonly [hard disks](hdd.md) and [SSDs](ssd.md).
- **expansion cards ([GPU](gpu.md), sound card, network card, ...)**: Additional hardware cards plugged into motherboard for either enabling or accelerating specific functionality (e.g. GPU for graphics etc.).
- **[PSU](psu.md)** (power supply unit): Converts the input electrical power from the plug to the electrical power needed by the computer.
- other things like fans for [cooling](cooling.md), batteries in laptops etc.

## Notable Computers

Here is a list of notable computers.

{ Some nice list of ancient computers is here: https://xnumber.com/xnumber/frame_malbum.htm. ~drummyfish }

| name                                    | year | specs (max, approx)                               | comment                                           |
| --------------------------------------- | ---- | ------------------------------------------------- | ------------------------------------------------- |
| [brain](brain.md)                       |-500M | 86+ billion neurons                               | biological computer, developed by nature          |
| [abacus](abacus.md)                     |-2500 |                                                   | one of the simplest digital counting tools        |
| Antikythera mechanism                   | -125 | ~30 gears, largest with 223 teeth                 | 1st known analog comp., by Greeks (mechanical)    |
| [slide rule](slide_rule.md)             | 1620 |                                                   | simple tool for multiplication and division       | 
| Shickard's calculating clock            | 1623 | 17 wheels                                         | 1st known calculator, could multiply, add and sub.| 
| [Arithmometer](arithmometer.md)         | 1820 | 6 digit numbers                                   | 1st commercial calculator (add, sub., mult.)      | 
| Difference Engine                       | 1822 | 8 digit numbers, 24 axles, 96 wheels              | mech. digital comp. of polynomials, by Babbage    | 
| Analytical Engine design                | 1837 | ~16K RAM, 40 digit numbers                        | 1st general purpose comp, not realized, by Babbage| 
| [nomogram](nomogram.md)                 | 1884 |                                                   | graphical/geometrical tools aiding computation    |
| [Z3](z3.md)                             | 1941 | 176B RAM, CPU 10Hz 22bit 2600 relays              | 1st fully programmable electronic digital computer| 
| [ENIAC](eniac.md)                       | 1945 | ~85B RAM, ~5KHz CPU, 18000 vaccum tubes           | 1st general purpose computer                      | 
| [PDP](pdp.md) 11                        | 1970 | 4M RAM, CPU 1.25Mhz 16bit                         | legendary [mini](minicomputer.md)                 |
| [Apple II](apple_ii.md)                 | 1977 | 64K RAM, 1MHz CPU 8bit                            | popular TV-attached home computer by Apple        |
| [Atari](atari.md) 800                   | 1979 | 8K RAM, CPU 1.7MHz 8bit                           | popular TV-attached home computer by Atari        |
| [VIC 20](vic_20.md)                     | 1980 | 32K RAM, 1MHz CPU 8bit, 20K ROM                   | successful TV-connected home computer by Commodore| 
| [IBM PC](ibm_pc.md)                     | 1981 | 256K RAM, CPU 4.7MHz 16bit, BASIC, DOS            | 1st personal computer as we know it now, modular  |
| [Commodore 64](c64.md)                  | 1982 | 64K RAM, 20K ROM, CPU 1MHz 8bit                   | very popular TV-connected home computer           |
| [ZX Spectrum](zx_spectrum.md)           | 1982 | 128K RAM, CPU 3.5MHz 8bit, 256x192 screen         | successful UK TV-connected home comp. by Sinclair |
| [NES](nes.md)/Famicom                   | 1983 | 2K RAM, 2K VRAM, CPU 1.7MHz 8bit, PPU             | TV-connected Nintendo game console                |
| [Macintosh](macintosh.md)               | 1984 | 128K RAM, CPU 7MHz 32bit, floppy, 512x342         | very popular personal computer by Apple           |
| [Amiga](amiga.md)                       | 1985 | 256K RAM, 256K ROM, CPU 7MHz 16bit, AmigaOS       | personal computer by Commodore, ahead of its time |
| [NeXT](next.md)                         | 1988 | 8M RAM, 256M drive, CPU 25MHz 32bit, NeXTSTEP OS  | famous workstation, used e.g. for Doom dev.       |
| [SNES](snes.md)                         | 1990 | 128K RAM, 64K VRAM, CPU 21MHz 16bit               | game console, NES successor                       |
| [PlayStation](playstation.md)           | 1994 | 2M RAM, 1M VRAM, CPU 33MHz 32bit, CD-ROM          | popular TV-connected game console by Sony         |
| [TI](texas_instruments.md)-80           | 1995 | 7K RAM, CPU 980KHz, 48x64 1bit screen             | famous programmable graphing calculator           | 
| [Deep Blue](deep_blue.md)               | 1995 | 30 128MHz CPUs, ~11 GFLOPS                        | 1st computer to defeat world chess champion       |
| [Nintendo 64](n64.md)                   | 1996 | 8M RAM, CPU 93MHz 64bit, 64M ROM cartr.           | famous TV-connected game console                  |
| [GameBoy Color](gbc.md)                 | 1998 | 32K RAM, 16K VRAM, CPU 2MHz 8bit, 160x144         | handheld gaming console by Ninetendo              |
| [GameBoy Advance](gba.md)               | 2001 | ~256K RAM, 96K VRAM, CPU 16MHz 32bit ARM, 240x160 | successor to GBC                                  |
| [Xbox](xbox.md)                         | 2001 | 64M RAM, CPU 733MHz Pentium III                   | TV-connected game console by Micro$oft            |
| [Nintendo DS](nds.md)                   | 2004 | 4M RAM, 256K ROM, CPU ARM 67MHz, touchscreen      | famous handheld game console by Nintendo          | 
| [Nintendo Wii](wii.md)                  | 2006 | 24M RAM, 512M ROM, SD, CPU PPC 729M               | famous family TV console with "stick" controllers |
| [iPhone](iphone.md) (aka spyphone)      | 2007 | 128M RAM, CPU ARM 620MHz, GPU, cam., Wifi, 480x320| 1st of the harmful Apple "smartphones"            |
| [ThinkPad](thinkpad.md) X200            | 2008 | 8G RAM, CPU 2.6GHz, Wifi                          | legendary laptop, great constr., freedom friendly |
| [ThinkPad](thinkpad.md) T400            | 2008 | 8G RAM, CPU 2.8GHz, Wifi                          | legendary laptop, great constr., freedom friendly |
| [Raspberry Pi 3](rpi.md)                | 2016 | 1G RAM, CPU 1.4GHz ARM, Wifi                      | very popular tiny inexpensive SBC                 |
| [Arduboy](arduboy.md)                   | 2016 | 2.5K RAM, CPU 16MHz AVR 8bit, 1b display          | tiny Arduino [open console](open_console.md)      |
| [Pokitto](pokitto.md)                   | 2017 | 36K RAM, 256K ROM, CPU 72MHz ARM                  | indie educational [open console](open_console.md) |
| [Raspberry Pi 4](rpi.md)                | 2019 | 8G RAM, CPU 1.5GHz ARM, Wifi                      | tiny inexpensive SBC, usable as desktop           |
| [Frontier](frontier.md)                 | 2021 | 9000+ 64 2GHz CPUs, 37000+ GPUs                   |fastest supercomputer to date, 1st with 1+ exaFLOPS|
| [Deep Thought](deep_thought.md)         |      |                                                   | fictional computer from Hitchhiker's Guide ...    |
| [HAL 9000](hal_9000.md)                 |      |                                                   | fictional AI computer (2001: A Space Oddysey)     |
|[PD computer](public_domain_computer.md) |      |                                                   | planned LRS computer                              |
| [Turing machine](turing_machine.md)     |      | infinite RAM                                      | important theoretical computer by Alan Turing     |

TODO: mnt reform 2, pinephone, 3DO, ti-89, quantum?

## See Also

- [MIX](mix.md)
