# WikiWikiWeb

WikiWikiWeb (also *c2 Wiki* or just *Wiki*) was the first ever created [wiki](wiki.md) (user editable) website, created in 1995 in [Perl](perl.md) by [Ward Cunningham](cunningham.md). It was focused on [software engineering](sw_engineering.md) and [computer](computer.md) [technology](tech.md) in general but included a plethora of discussion and pages touching other topics as well, e.g. politics, [humor](jokes.md) or nerd and [hacker](hacking.md) culture. The principles on which it stood, most importantly allowing users to edit its the highly [hyperlinked](hyperlink.md) pages, largely influenced thousands of subsequently emerging sites which made use of the same concepts -- these sites are now collectively called [wikis](wiki.md), most famous of which is [Wikipedia](wikipedia.md). The style of WikiWikiWeb was partly an inspiration for our [LRS wiki](lrs_wiki.md) too.

The project quite impressively spawned over 36000 pages (http://c2.com/cgi/wikiPages). Since 2014 the wiki can no longer be edited due to vandalism, but it's still online. It was originally available at http://www.c2.com/cgi/wiki, now at http://wiki.c2.com/ (sadly now requires [JavaScript](js.md), WTF how is this a hacker site???).

The site's engine was kind of [suckless](suckless.md)/[KISS](kiss.md), even Wikipedia looks [bloated](bloat.md) compared to it. It was pure unformatted [HTML](html.md) that used a very clever system of [hyperlinks](hypertext.md) between articles: any [CamelCase](camelcase.md) multiword in the text was interpreted as a link to an article, so for example the word `SoftwareDevelopment` was automatically a link to a page called *Software Development*. This presented a slight issue e.g. for single-word topics but the creativity required for overcoming the obstacle was part of the [fun](fun.md), for example the article on [C](c.md) was called `CeeLanguage`.

Overall the site was also very different from [Wikipedia](wikipedia.md) and allowed informal comments, [jokes](jokes.md) and subjective opinions in the text. It was pretty entertaining to read. There's a lot of old hacker wisdom to be found there. On the other hand it was a bit retarded too though, a bit like [hacker news](hacker_news.md) of its time, except a tiny bit less stupid maybe. The people were not as much focused on pure hacking but rather on "software engineering", i.e. manipulating and "managing" people, they were obsessed with [OOP](oop.md) patterns and things like that.

There are other wikis that work in similar spirit, e.g. CommunityWiki (https://communitywiki.org, a wiki "about communities"), MeatBallWiki (http://meatballwiki.org/wiki/) or EmacsWiki.

## Interesting Pages

These are some interesting pages found on the Wiki.

{ NOTE: To see all pages in given category go to the category page and CLICK THE PAGE TITLE. ~drummyfish }

- **CategoryCategory**: List of categories of pages.
- **CategoryHumor**: Humorous pages.
- **ComputerGame**
- **ExtinctionOfHumanity**: Discussing end of humanity and a possible [collapse](collapse.md).
- **GameOfChess**: About [chess](chess.md).
- **LanguageGotchas**
- **WeirdErrorMessages**
- **WikiWikiWebFaq**
- **WithinTwentyYears**: Mostly pre-2005 predictions about what technology would be like in 20 years, a lot of hits and misses.
- **WikiHistory**

## See Also

- [Jargon File](jargon_file.md)
- [Wikipedia](wikipedia.md)