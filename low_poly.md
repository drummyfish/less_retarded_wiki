# Low Poly

The term *low poly* (also low-poly or lowpoly) is used for polygonal [3D models](3d_model.md) whose polygon count is relatively low -- so low that one can see the model [approximates](approximation.md) the ideal shape only very roughly. For typical models (animals, cars, guns, ...) the polygon count under which they are correctly called low poly is usually a few dozens or few hundreds at most. The opposite of low poly is *[high poly](high_poly.md)*.

**WATCH OUT**: Retards nowadays use the term "low poly" for stylized/untextured high poly models; they even use the term for models whose polygon count is lower than the number of atoms in observable universe, or they use the term completely randomly just to put a cool label to their lame shit models. **STOP THIS FUCKING INSANITY, DON'T CALL HIGH POLY MODELS LOW POLY**.

The exact threshold on polygon count from which we call a model low poly can't be objectively set because firstly there's a subjective judgment at play and secondly such threshold depends on the ideal shape we're approximating. This means that not every model with low polygon count is low poly: if a shape, for example a [cube](cube.md), can simply be created with low number of polygons without it causing a distortion of the shape, it shouldn't be called low poly. And similarly a model with high polygon count can still be classified as low poly if even the high number of polygons still causes a significant distortion of the shape. However let's say that if it has more than 100 triangles it's most likely not low poly.

The original purpose of creating low poly models was to improve performance, or rather to make it even possible to render something in the era of early computer graphics. Low poly models take less space in memory and on good, non-capitalist computers render faster. As computers became able to render more and more polygons, low poly models became more and more unnecessary and eventually ended up just as a form of **"retro" art style** -- many people still have nostalgia for [PS1](ps1.md) graphics with very low poly models and new games sometimes try to mimic this look. In the world of capitalist consoomer computing/[gayming](game.md) nowadays no one really cares about saving polygons on models because "[modern](modern.md)" [GPUs](gpu.md) aren't really affected by polygon count anymore, everyone just uses models with billions of polygons even for things that no one ever sees, soydevs don't care anymore about the art of carefully crafting models on a low polygon budget. However in the context of [good, non-capitalist technology](lrs.md) low poly models are still very important.

Low poly models are intended to be used in interactive/[real-time](real_time.md) [graphics](graphics.md) while high poly ones are for the use in offline (non-realtime) rendering. Sometimes (typically in games) a model is made in both a low poly and high poly version: the low poly version is used during gameplay, the high poly version is used in cutscenes. Sometimes even more than two versions of models are made, see [level of detail](lod.md).

## See Also

- [pixel art](pixel_art.md)
- [ASCII art](ascii_art.md)
- [low fidelity](low_fidelity.md)
- [retro](retro.md)