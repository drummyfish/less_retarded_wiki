# Jargon File

Jargon File (also Hacker's Dictionary) is a computer [hacker](hacking.md) dictionary/compendium that's been written and updated by a number of prominent hackers, such as [Richard Stallman](rms.md) and [Erik S Raymond](esr.md), since 1970. It is a chiefly important part of hacker culture and has also partly inspired [this very wiki](lrs_wiki.md).

{ A similar but smaller encyclopedia is at https://www.erzo.org/shannon/writing/csua/encyclopedia.html (originally and encyclopedia at soda.csua.berkeley.edu). ~drummyfish }

The work informally states it's in the [public domain](pd.md) and some people have successfully published it commercially, although there is no standard [waiver](waiver.md) or [license](license.md) -- maybe because such waivers didn't really exist at the time it was started -- and so we have to suppose it is NOT formally [free as in freedom](free_culture.md). Nevertheless it is freely accessible e.g. at [Project Gutenberg](gutenberg.md) and no one will bother you if you share it around... we just wouldn't recommend treating it as true public domain.

It is fairly nicely written with high amount of humor and good old political incorrectness, you can for example successfully find the definition of terms such as *[rape](rape.md)* and *clit mouse*. Some other nice terms include *smoke emitting diode* (broken diode), *notwork* (non-functioning [network](network.md)), [Internet Exploiter](internet_explorer.md), *binary four* (giving a finger in binary), *Kamikaze packet* or *Maggotbox* ([Macintosh](mac.md)). At the beginning the book gives some theory about how the hacker terms are formed (overgeneralization, comparatives etc.).

## See Also

- [GNU Collaborative International Dictionary of English](gcide.md)