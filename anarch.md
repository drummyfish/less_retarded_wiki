# Anarch

*Poor man's [Doom](doom.md)?*

Anarch is a [LRS](lrs.md)/[suckless](suckless.md), [free as in freedom](free_software.md) ([public domain](public_domain.md)) first man shooter [game](game.md) similar to [Doom](doom.md), created completely from scratch by [drummyfish](drummyfish.md). It has been designed to follow the LRS principles very closely and set an example of how games, and software in general, should be written. It also tries to be compatible with principles of [less retarded society](less_retarded_society.md), i.e. it promotes [anarchism](anarchism.md), anti-[capitalism](capitalism.md), pacifism etc.

There are now also additional official mods available for the game that for example add multiplayer, increase texture resolution, improve the overall look, allow recording demos, replace assets with those from [Freedoom](freedoom.md) and so on.

To make Anarch no advanced [bullshit](bullshit.md) was used such as multiple monitors, [IDE](ide.md)s, [UML](uml.md), workstations, graphic tablets, programming socks, expensive chairs and so on. It was made mostly with one old laptop, one old desktop computer and only basic [free software](free_software.md) such as [vim](vim.md), [GIMP](gimp.md) and so on.

The repo is available at https://codeberg.org/drummyfish/Anarch or https://gitlab.com/drummyfish/anarch. Some info about the game can also be found at the [libregamewiki](lgw.md): https://libregamewiki.org/Anarch. The 1.0 version was released on 1st December 2020, it was officially in the making since September 2019, but we may also see the inception of the game to be the start of [raycastlib](raycastlib.md) development in 2018.

```
h@\hMh::@@hhh\h@rrrr//rrrrrrrrrrrrrrrrrrrr@@@@hMM@@@M@:@hhnhhMnr=\@hn@n@h@-::\:h
hMhh@@\\@@@\\h:M/r/////rrrrrrrrrrrrrrr//r@@@@@MMh@@hhh\\\=rMr=M@hh\hn\:\:h::\@\:
@nh==hhhMM@hrh\M/r/////rrrrrrrrrrrrrrr//@@@@@@hhM@h\MhhhMM\@@@@@M\hh\\\Mhh\\\\hh
:hh=@Mh/;;;@hr:M,///;;/////rrr//rrrrrr//@@@@@@hh\h@@hM:==h\@@::\\\:M\@\h\M:\:=@h
\=MhM@hr  `hMhhM///@@@@@@@@@@@@@@@@@@@//@@@@@@rMM@n\M=:@M\\\\Mh\\\hr\n\--h-::r:r
:Mh@M@@`  `rh@\@///@@@@@@@@@@@@@@@@@@@@@@@@@@@Mr\@@\h@:\h\h@\Mhh@@\M@@@@-n\rn@:h
:MhhMn@//r;;@/hM@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@MhMhh:M@MhMhMh@\\rM/@h@nn=-MrnM@:h
:nhhhhh\\//\::@M@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@rMM@@nh@M\=nh@@@M@..=hM@n@-@@@@@:h
\@\h@@rrrr/rr@=M@@@@@@@@@@@@@@nr@@@@@@@@@@@@@@Mrhn@\M@:nMh\@@@@@...h:::::@---::h
-M\h=h\`   rhM\M@@@@@@@@@@@@@@@=@@@@@@@@@@@@@@MhM@\hh@M@Mhh@-\MMhrr\\\:MMh::\\-\
h@hhh\h`  `rMh\M@@@@@@@@@@@@@@nr;;;;rn@@@@@@@@r@r///=@\@\r\\hM@nrrr@\n\h\M\\\\\:
hn===hhM=;hhhh\MrMnrr=rrr=r@rhhr;.r,/hr=r=r=h=r@=/-;/MhhMr:h\@h=...r\@hMhM:/\h\=
@n==M\h@=;hhh\\Mrr=r=r=rMr=hrMMr;;;,;========MM@r=./;@:MMM\h=r=rM/rh@@@M-n---:-h
:\=hMn@@@=\hhh:M===============;/. ,,==========@r-/--@:@M\\@@@n@Mn:hM@n@-=\hr=-h
\hhnM@=@::@MM/h================;;;;.,======\h==M=/;r,//;;r=r=r=r@\=r=r=r=@rnMn:r
:Mrrr=rr==@rr=rrr=rrr=/=r===r==/:; ..===r\\-h==@r-,;-=r/;/;;;;;;rnrrr=rrr=rrr=r;
rrrrrrrr@=rrrrrrrrrrr//r=r=r=r=r;. ,.r=r\---hr=@r===-r=r=;;;r;;;hh@:;;;;;;;;;;-;
r=rrr=rr\\@rr=rrr=r/;/:rr=rrr=rr;r,..=r\--.-h=r@r----=rrr=rrr--:,;;:,;;;,;;;,;--
rrrr:-@=====:,;,;-/;/:rrrrrrrrr;;....r\--.,\hrrrrrrrrrrrrrrrrrrrrr-----rrrrrrrrr
,;,:,; ;,;;;-;;;,;/:-rrrrrrrrrrrrrrrrr\-.,;\@rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
;,;,;,.,;,;,;,;,;,:rrrrrrrrrrrrrrrrrr\--.;,\Mrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
,;,;.-.-.-::-:::--rr/rrr/rrr/rrr/rrr/\-.:;::@rrr/rrr/rrr/rrr/rrr/rrr/rrr/rrr/rrr
-.-.r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/\---;::\@/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/
/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r\-.,;:,:@r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r/r
///////////////////////////////////\::-,;,-:@///////////////////////////////////
;///;///;///;///;///;///;///;///;//,::-:,.,-@///;///;///;///;///;///;///;///;///
//////////////////////////////////\----:-.,-h///////////////////////////////////
,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
nn..nnn...nn...nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn...nnnnnnnnnnnnnn
nnn.nnn.n.nn.n.nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn.n.nnnnnnnnnnnnnn
nnn.nnn.n.nn.n.nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn.n.nnnnnnnnnnnnnn
nn...nn...nn...nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn...nnnnnnnnnnnnnn
nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
```

*screenshot from the terminal version*

Anarch has these features:

- It is completely **[public domain](public_domain.md)** [free software](free_software.md)/[free culture](free_culture.md) under [CC0](cc0.md), including all code and assets which were all made from scratch by drummyfish.
- It has extremely low hardware demands, fitting into **256 kB** (WITH assets, with mods and compression even 60 kB) and requiring only about **32 kB of [RAM](ram.md)** and **50 MHz [CPU](cpu.md)**. [GPU](gpu.md)s need not apply.
- It is written in pure [C99](c.md) without any [dependencies](dependency.md) (not even the standard library). It uses no [dynamic heap allocation](dynamic_allocation.md) and no [floating point](float.md).
- It is **extremely [portable](portability.md)**, written against a very tiny [I/O](io.md) layer. As such it has been ported to many platforms such as [GNU](gnu.md)/[Linux](linux.md), [BSD](bsd.md), [Pokitto](pokitto.md), browser [JavaScript](javascript.md), [Raspberry Pi](rpi.md) and many others.
- It is written in a [single compilation unit](scu.md) and without any [build system](build_system.md).
- It is created with only free software.
- It is well documented.
- Mods and configs follow [suckless](suckless.md) philosophy: mods are just [diffs](diff.md), config is part of source code.
- Gameplay-wise Anarch offers 10 levels and multiple enemy and weapon types. It supports mouse where available.

## Technical Details

Anarch is written in [C](c.md)99.

The game's engine uses [raycastlib](raycastlib.md), a [LRS](lrs.md) [library](library.md) for advanced 2D [raycasting](raycasting.md); this would be called "pseudo 3D" graphics. The same method (raycasting) was used by [Wolf3D](wolf3d.md) but Anarch improves it to allow different levels of floor and ceiling which makes it look a little closer to [Doom](doom.md) (which however used a different methods called [BSP](bsp.md) rendering).

The whole codebase (including raycastlib AND the assets converted to C array) has fewer than 15000 [lines of code](loc.md). Compiled binary is about 200 kB big, though with [compression](compression.md) and replacing assets with [procedurally generated](procgen.md) ones (one of several Anarch mods) the size was made as low as 57 kB.

The music in the game is [procedurally generated](procedural_generation.md) using [bytebeat](bytebeat.md).

All images in the game (textures, sprites, ...) are 32x32 pixels, compressed by using a 16 color subpalette of the main 256 color palette, and are stored in source code itself as simple [arrays](array.md) of bytes -- this eliminates the need for using [files](file.md) and allows the game to run on platforms without a [file system](file_system.md).

The game uses a tiny custom-made 4x4 bitmap [font](font.md) to render texts.

Saving/loading is optional, in case a platform doesn't have persistent storage. Without saving all levels are simply available from the start.

In the suckless fashion, mods are recommended to be made and distributed as [patches](patch.md).

### Bad Things Plus Comments, Retrospective (/Post Mortem?)

*written by [drummyfish](drummyfish.md)*

Though retrospectively I can of course see many mistakes and errors about the game and though it's not nearly perfect, I am overall quite happy about how it turned out and for what it is overall, it got some attention among the niche of suckless lovers and many people have written me they liked the game and its philosophy. Many people have ported it to their favorite platforms, some have even written me their own expansions of the game lore, tricks they found etc. Some even created small mods. If you're among them, thank you :)

There were also a few people I've heard criticism from -- and that's absolutely fine -- that the game is just shit, too simple, badly designed, boring and so on. Indeed, about the quality of the game as such (from the "consumer's" point of view) I actually agree, the game isn't the most fun one to play, but when judging it please keep in mind the following. Firstly it was ALL made by a single man, absolutely from scratch -- if you've ever tried to create something from nothing, you know that despite it not looking that hard, it is indeed extremely difficult and time consuming to make something from the ground up, AND I couldn't even use any free assets, sound samples, game engines, not even fonts -- except for the programming language I had to create EVERYTHING from nothing, which required not only great amount of time, but also skills which I cannot master all: that of a visual artist, engineer, level designer, sound artist and so on and so forth. You only see the result but not the dozens of blind paths, hundreds of lines of rewritten code that was thrown to trash, hours and hours spent hunting down silly little bugs. All the skills needed for making a complete game are so vastly different that unless one is a genius, they cannot be mastered at once, one would simply have to be an excellent painter, writer, musician, mathematician, programmer, psychologist and many different things at once. Secondly I didn't work on it full time, I worked on other things, went to work, I was even hospitalized etc. Thirdly the goal wasn't to make a game that would be excellent by its gameplay AT ALL, the context of its creation was absolutely different from that of a typical game that has to try to appeal to many people, Anarch was focused on showing the technological side, prove a point of a certain development philosophy, and that mostly to rather a few people who want to make games themselves. By its philosophy it's also not meant to be a "fixed product" as is, again, common to think of games nowadays, it is really meant to be a basis for improvement, mods, a starting codebase for [forks](fork.md) and so on. So even though there are many shortcomings and things done plainly wrong, I think the main goal was more or less achieved, though some people miss to see that goal. If this philosophy of simplicity allows a single guy (quite average and in many ways retarded and that) to make a WHOLE game completely from scratch, with zero budget, only over weekend evenings, and that game is additionally very well made (extremely small, portable, modifiable, free, ...), what if let's say three brilliant people worked on it? What if five people worked on something similar full time? What marvels would we see? This is the number one question I wanted to highlight.

The following is a retrospective look on what could have been done better (a potential project for someone might be to implement these and so make the game even more awesome):

- I kinda overdid it with fixed width types, tried to always only use variables of the smallest necessary size but now it looks a bit ridiculous, it would be better to just use normal integers for most local variables, I probably didn't achieve much, maybe it's even a bit slower this way.
- It might have been better to write it in C89 than C99 for better portability.
- Sound effects would likely be better procedurally generated just like music. It would be less code and data in ROM and the quality probably wouldn't be that much worse as the samples are shitty quality anyway.
- The palette used might have rather been [RGB 332](rgb332.md) instead of the custom palette, again less code and less data in ROM, though visual quality might suffer a bit and things like diminishing colors might require some extra code or look up tables (like in Doom).
- The python scripts for data conversion should be rewritten to C. Using python was just laziness.
- Raycastlib itself has some issues, but those should be addressed separately.
- The game is really a bit too hard (tho this can easily be changed in settings) and gameplay is not great (like explosions pushing player instantly, not too good, too few items, player often lacks ammo/health, ...), movement inertia could be in vanilla game to make it feel nicer etc.
- Some of the code is awkward, like `SFG_recomputePlayerDirection` was an attempt at optimization but it's probably optimization in a wrong place that does nothing, ...
- Some details like having separate arrays for different types of images -- there is no reason for that, it would be better to just have one huge array of all images; maybe even have ALL data in one huge array of bytes.
- ...

## See Also

- [Doom](doom.md)
- [Licar](licar.md)
- [SAF](saf.md)