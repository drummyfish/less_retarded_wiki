# Soydev

Sodevs are incompetent wanna-be programmers that usually have these characteristics:

- Being [pseudoleftist](left_right.md) (fascist) political activists pushing [tranny software](tranny_software.md) and [COCs](coc.md) while actually being mainstream centrists in the tech world (advocating "[open-source](open_source.md)" instead of [free_software](free_software.md), being okay with [proprietary](proprietary.md) software, [bloat](bloat.md) etc.).
- Trying to be "cool", having friends and even spouses and kids, wearing T-shirts with "[coding](coding.md) [jokes](jokes.md)", having [tattoos](tattoo.md), piercing and colored hair (also trimmed bear in case of males), having that ugly [snowflake](snowflake.md) (i.e. bizarre) kind of look to catch attention, [signaling virtues](virtue_signaling.md) on social networks.
- Only being hired in tech for a no-brainer job such as "coding websites" or because of diversity quotas.
- Being actually bad at programming, using meme high-level languages like [JavaScript](javascript.md), [Python](python.md) or [Rust](rust.md). { I shit you not, I learned from a friend who lives in India that "universities" there produce "security experts" who don't even have to know any programming and math. They just learn some sysadmin stuff and installing antiviruses, without having any clue about how encryption works etc. These people get regular degrees. Really makes me wanna kys myself. ~drummyfish }
- Using a [Mac](mac.md).
- Thinking they're experts in technology because they are familiar with the existence of [Linux](linux.md) (usually some mainstream distro such as [Ubuntu](ubuntu.md)) and can type `cd` and `ls` in the terminal.
- Having only shallow awareness of tech culture, telling big-bang-theory HTML jokes (*sudo make sandwich*, [42](42.md) hahahahahahaha).
- Being highly active on social networks, probably having a pixel-art portrait of their ugly face and "personal pronouns" on their profile.
- Believing in and engaging in [capitalism](capitalism.md), believing corporations such as [Microsoft](microsoft.md), wanting to create "startups", being obsessed with [productivity](productivity_cult.md), inspirational quotes and masturbating to tech "visionaries" like [Steve Jobs](steve_jobs.md). The "rebels" among these are advocating [FOSS](foss.md), however they always promote huge [bloat](bloat.md) and de-facto [capitalist software](capitalist_software.md) which is no different from proprietary software.
- Using buzzwords like "solution", "orchestration" etc.
- ...

Here is a quick rough comparison of seydevs and actual good programmers (nowadays mostly an extinct species):

| characteristic    | good programmer                                | soydev                                                                                |
| ----------------- | ---------------------------------------------- | ------------------------------------------------------------------------------------- |
| math skills       | deep knowledge of math                         | "I don't need it", "there's library for that", memorized math interview questions     |
| computer knowledge| all-level, big-picture knowledge of principles | knowledge of trivia ("This checkbox in this framework has to be unchecked.", ...)     |
| specialization    | generalist                                     | hyperspecialized, knows one language/framework                                        |
| prog. languages   | C, assembly, FORTRAN, Forth, comun, lisp, ...  | Python, JavaScript, Rust, Java, C#, C++2045, ...                                      |
| mostly does       | thinking about algorithms and data structures  | typing glue code for different libraries, updates/maintains systems, talks to people  |
| political opinions| politically incorrect hippie anarcho pacifist  | liberal capitalist feminist pro black lesbian LGBT fascist anti Nazi                  |
| hardware          | 640x480 1990s laptop, no mouse                 | 2023 touchscreen 1080K macbook with stickers all over, wireless $1000 AI gaming mouse |
|memorized knowledge| 10000 digits of pi                             | 10000 genders plus offensive words he mustn't say                                     |
| text editor       | vim, ed, ...                                   | Microsoft AI blockchain VSCode with 10000 plugins running in 10000 virtual sandboxes  |
| looks             | fat, unwashed, unkept beard, dirty clothes     |pink hair, fake glasses, $1000 T-shirt "sudo make sandwich HAHA BAZINGA", 10000 tattoos|
| gender            | male                                           | depends on mood                                                                       |
| race              | white                                          | prefers not to specify, offended by the question                                      |
| hobbies           | reading encyclopedias, chess, rocket science   | distrohopping, browserhopping, githopping, editorhopping, tiktok, partying            |

## See Also

- [SJW](sjw.md)
- [soyboy](soyboy.md)
- [snowflake](snowflake.md)
- [zoomer](zoomer.md)