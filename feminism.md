# Feminism

*Sufficiently advanced stupidity is indistinguishable from feminism.* --old Chinese proverb

Feminism, also feminazism or femifascism, is a [fascist](fascism.md) [terrorist](terrorism.md) [pseudoleftist](pseudoleft.md) cult aiming for establishing [female](woman.md) as the superior gender, for social revenge on men and gaining political power, such as that over [language](political_correctness.md). Similarly to [LGBT](lgbt.md), feminism is violent, [toxic](toxic.md) and [harmful](harmful.md), based on [brainwashing](brainwashing.md), mass hysteria, [bullying](bullying.md) (e.g. the [metoo](metoo.md) campaign) and [propaganda](propaganda.md). As always, women have fucked everything up.

Let us start by prefacing the feminist motto:

*"Castrate all men!"* --feminist love speech

A quite nice article on feminism can also be found on the [incel](incel.md) wiki at https://incels.wiki/w/Feminism. { A friend also recommended a text called *Counter-Advice From The Third Sex*, possibly check it out. ~drummyfish }

If anything's clear, then that feminism is not at all about gender equality but about hatred towards men and female superiority. Firstly feminism is not called *gender equality movement* but *feminism*, i.e. for-female, literally "womanism", and as we know, [name plays a huge role](name_is_important.md). Imagine this: if you asked feminists if they could right now implement matriarchy in society, i.e. female ruling over man, how many of them do you think would answer "no"? There is not even a shadow of a doubt a vast majority would absolutely answer "yes", we may at best argue about if it would be 85% or 99% of them. So the question of feminist goals is absolutely clearly answered, there is no point in trying to deny it. To a feminist a man is what a [jew](jew.md) was to the Nazi or what the Christian was to the Romans who famously hunted Christians down and fed them to the lions because they refused to bow to their polytheist ideology (nowadays analogous to e.g. refusing to practice [political correctness](political_correctness.md)). The whole story is repeated again, we have yet again not learned a bit from our [history](history.md). Feminism is exactly the same as [Nazism](nazism.md), just replace "Aryan race" with "woman gender", "jew" with "man" and Nazi uniforms with pink hair. Indeed, women have historically been oppressed and needed support, but once women reach social equality -- which has basically already happened a long time ago now -- feminist movement will, if only by [social inertia](social_inertia.md), keep pursuing more advantages for women (what else should a movement called *feminism* do?), i.e. at this point the new goal has already become female superiority. In the age of capital no one is going to just dissolve a movement because it has already reached its goal, such a movement present political capital one will simply not throw out of window, so feminists will forever keep saying they're being oppressed and will forever keep inventing new bullshit issues to keep [fighting](fight_culture.md). Note for example that feminists care about things such as wage gap but of course absolutely don't give a damn about opposite direction inequality, such as men dying on average much younger than women etc. -- feminism cares about women, not equality. If the wage gap became reversed, i.e. women earned on average more than men, do you think a Feminist wouldn't be happy? No answer is needed. And of course, when men establish "men rights" movements, suddenly feminists see those as "fascist", "toxic" and "violent" and try to destroy such movements.

Closing gaps is not how you achieve equality -- on the contrary it's only how you stir up hostility and physically reshape women into men (by closing the height gap, boob size gap, penis length gap, brain size gap and any kind of gap that may potentially have any significance in sports, art or culture at all). [Making gaps not matter](less_retarded_society.md) is how you truly achieve equality. but Feminists won't go that way exactly because they are against equality.

Since feminism became [mainstream](mainstream.md) in about 2010s, it also became the main ideology of populists and opportunists, i.e. all politicians and [corporations](corporation.md), it is now a milking cow movement and a vehicle for pushing all kinds of evil such as censorship laws, creation of bullshit jobs, discrediting opposition and so on.

{ I really have no issues with women, I truly love everyone, but I do pay attention to statistics. One of the biggest things feminism achieved for me in this regard is that now it's simply not enough for me to see a woman achieve success in society to be convinced she is skilled or capable, a woman getting PhD to me nowadays automatically just means she got it because she's a woman and we need more quotas of "strong women in SCIENCE". In the past I didn't see it this way, a woman that did something notable back then was mostly convincing to me. Nowadays I just require much better evidence to believe she is good at something, e.g. seeing something truly good she created -- to be honest, I now don't recall any woman in "modern times" to have convinced me, but I am really open to it and just waiting to be proven wrong. ~drummyfish }

Some notable things feminists managed to achieve are:

- Women hate men.
- Men hate women.
- Women are now [slaves](work.md) too, they have to pursue career instead of being able to enjoy stress-free time at home.
- People are scared of physical touch, eye contact, even talking. Touching a stranger by accident can mean a lawsuit.
- Men are pushed to forming fascist counter movement such as [MGTOW](mgtow.md).
- Men actually being nice to women, e.g. holding a door open for them, is seen as hostility.
- Actual good achievements of women are now dismissed because everyone supposes the success was fabricated as part of ever present feminist propaganda, hurting the few truly skilled women.
- Women look and behave like men.
- Women refuse to have children.
- Even if a woman has a child, she has it late and doesn't take proper care of it, all because she is supposed to pursue a career and compete with men.
- Women refuse to date men, men are depressed and commit [suicides](suicide.md) (see [incel](incel.md)).
- Stronger [sexism](sexism.md), people now believe women are better than men, that man is automatically something to fear etc.
- There are more [bullshit](bullshit.md) jobs such as diversity departments etc.
- Industries such as those of [technology](tech.md), [science](science.md), movies etc. all go to absolute shit because of incompetent women FORCED there because "we need more strong women everywhere".
- Strong propaganda everywhere, destroying all [art](art.md), truth about history etc.
- Women are more stressed because their capabilities are overestimated by the propaganda, a young girl is told she is better than a man and she is expected to beat men; in reality she finds out she can't beat a man and becomes depressed, thinking she is extremely inferior while she is just a normal woman.
- [Censorship](censorship.md) of basically all old art such as movies without enough women in them, movies that make any kind of fun of any woman, movies that show any woman as weak etc. Tiny bits of [free speech](free_speech.md) are disappearing completely.
- Women marry women and raise children who lack fathers, something that's objectively extremely bad from psychological point of view. Ask literally anyone who grew up without a father if he missed having one.
- People believe literal lies such as that a woman is physically stronger and more intelligent than man.
- Eye contact is perceived as [rape](rape.md).
- More people want to castrate all men than before.
- Language is becoming ugly, ridiculous and retarded by [political correctness](political_correctness.md), forcing things like "personkind" instead of "mankind" or abolishment of the word "history" (because it contains the substring "his").
- Stronger [fight culture](fight_culture.md), cults of personalities, [toxicity](toxic.md) of whole society etc.
- Because all the above everyone is hostile, stressed, scared, depressed, society is in tension, leading to even faster downfall.
- ...

{ [LMAO](lmao.md), **a supposed woman writer who won 1 million euro prize turned out to actually be three men writers**, see Carmen Mola :) Also the recent "historically first all female space walk" during which they managed to lose $100K worth of equipment :D ~drummyfish }

Feminism prospers largely thanks to [capitalism](capitalism.md) -- women with privileges, e.g. those of not having to work as much as men, are not accepted under capitalism; everyone has to be exploited as much as possible, everyone has to be a work slave. Therefore capitalist propaganda promotes ideas such as "women not having to work is oppression by men and something a woman should be ashamed of", which is of course laughable, but with enough brainwashing anything can be established, even the most ridiculous and obvious bullshit. Furthermore under capitalism **being a woman is a form of capital**, this capital is to be utilized and traded and so you can no longer get a woman just because you love each other, a woman dating you just out of love would be like her giving all her life savings to a complete stranger just to make him happy. And so women rather start dating other women, just as rich people hang around with other rich people, and that's why Feminism is so closely tied to [LGBT](lgbt.md) and vice versa -- LGBT helps women get rid of men and women in turn support LGBT back, e.g. by supporting men switching genders to female. Feminism is a product of capitalism induced [fight culture](fight_culture.md) and escalation of conflict. Women being in war with men is just natural development of capitalism as the system naturally puts more and more social groups against each other to fuel conflict as a ground and fuel for business -- a very advanced business that no longer works just with money and traditional property, but newly with human needs, emotion and suffering. Eventually mothers will end up being in conflict with their sons, parents will become enemies of their children, tall people enemies of short people and so on. If it seems ridiculous, just wait a few years -- the current insanity of feminism would have seemed absolutely ridiculous as well not that long ago.

Apparently in Korea feminists already practice segregation, they separate parking spots for men and women so as to prevent women bumping into men or meeting a man late at night because allegedly men are more aggressive and dangerous. Now this is pretty ridiculous, this is exactly the same as if they separated e.g. parking lots for black and white people because black people are statistically more often involved in crime, you wouldn't want to meet them at night. So, do we still want to pretend feminists are not fascist?

Nicole Kidman is a whore.

LMAO 2024 Olympic is already woman only :D (Men are "officially" allowed but they don't broadcast men sports lol.)