#!/bin/sh
# Creates a single HTML version of the wiki.

echo "making single HTML"

DATE=`date +"%D"`

echo "<html><head><link rel=\"stylesheet\" href=\"style.css\"><title>LRS Wiki</title></head><body><h1>less_retarded_wiki</h1><p>by drummyfish, generated on $DATE, available under <a href=\"https://creativecommons.org/publicdomain/zero/1.0/\">CC0 1.0</a> (public domain) </p><hr />" > lrs_wiki.html

echo "$HEADER <ul>" > html/$FILELIST

for f in `ls *.md | sort`; do
  echo "processing $f"
  fname=$(echo "$f" | sed "s/\.md//g")

  echo "<span class=\"article\" id=\"$fname\">$fname</span><br />" >> lrs_wiki.html

  cmark-gfm -e table $f | sed "s/\.md\"/\"/g" | sed "s/ href=\"/ href=\"#/g" >> lrs_wiki.html

  echo "<hr />" >> lrs_wiki.html
done

echo "</body></html>" >> lrs_wiki.html

echo "done"
