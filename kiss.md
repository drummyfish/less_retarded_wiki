# KISS

*See also [minimalism](minimalism.md).*

KISS (Keep It Simple, Stupid!; also KISR, *keep it simple, retard*) is a [minimalist](minimalism.md) design philosophy that favors simplicity, both internal and external, [technology](tech.md) that is **as simple as possible** to achieve given task. This philosophy doesn't primarily stem from [laziness](laziness.md) or a desire to rush something (though these are completely valid reasons too), but mainly from the fact that higher [complexity](complexity.md) comes with increasingly negative effects such as the cost of development, cost of [maintenance](maintenance.md), greater probability of [bugs](bug.md) and failure, more [dependencies](dependency.md) etc.

WATCH OUT: various scum has started to ride on the wave of the "KISS" trend and abuse the term, twisting its true meaning; for example GNU/Linux Mint has started to market itself as "KISS" -- that's of course ridiculous and all Mint developers are cretins and idiots. **Maximum INTERNAL simplicity is a necessary prerequisite for the KISS philosophy**, anything that's just simple on the outside is a mere harmful [pseudominimalism](pseudominimalism.md) -- you may as well use a [Mac](mac.md).

Under dystopian [capitalism](capitalism.md) simple [technology](tech.md), such as simple [software](software.md), has at least one more advantage connected to "[intellectual property](intellectual_property.md)": a simple solution is less likely to step on a [patent](patent.md) landmine because such a simple solution will either be hard to patent or as more obvious will have been discovered and patented sooner and the patent is more likely to already be expired. So in this sense KISS technology is legally safer.

Apparently the term *KISS* originated in the US Army plane engineering: the planes needed to be repairable by *stupid* soldiers with limited tools under field conditions.

**Examples** of KISS "[solutions](solution.md)" include:

- Using a [plain text](plain_text.md) file instead of a dedicated [bug](bug.md) tracker (TODO.txt), note taking program etc.
- Creating website in plain [HTML](html.md) instead of using some complex web framework such as [Wordpress](wordpress.md).
- Using solar panels directly, without a battery, if it's [good enough](good_enough.md).
- Implementing a web left-right sweeping image gallery with HTML [iframe](iframe.md) instead of some overcomplicated [JavaScript](js.md) library. { Example stolen from [reactionary software](reactionary_software.md) website. ~drummyfish }
- Supporting only [ASCII](ascii.md) (or something like [ISO 8859-1](iso88591.md)) instead of [Unicode](unicode.md), it is [good enough](good_enough.md).
- Using a plain text flat file instead of a [database](database.md) system.
- Using [markdown](markdown.md) for creating documents, as opposed to using office programs such as [Libreoffice](libreoffice.md).
- Using a trivial [shell](shell.md) script for compiling your programs rather than a complex build system such as [CMake](cmake.md).
- Using [ASCII art](ascii_art.md) instead of bitmap images.
- [Beatboxing](beatbox.md) to produce a back track for rap songs, as opposed to using (or even paying for) some kind of music software.
- Using hammock instead of bed -- it is much simpler, cheaper, portable and has an extra swinging feature for which it is considered by many to even be superior in terms of comfort.
- In the world of [coffee](coffee.md) so called *Turkish coffee* without any milk and sugar is probably the most KISS option, you just put coffee ground in a mug and pour hot water on it. Compare this to Espresso with milk and sugar which needs a quite complex and expensive machine. { Yeah, I drink the most KISS coffee, though sometimes I also use French press -- that one is still quite simple. ~drummyfish }
- ...

Compared to [suckless](suckless.md), [Unix philosophy](unix_philosophy.md) and [LRS](lrs.md), KISS is a more general term and isn't tied to any specific group or movement, it doesn't imply any specifics but rather the general overall idea of simplicity being an advantage ([less is more](less_is_more.md), [worse is better](worse_is_better.md), ...).

[KISS Linux](kiss_linux.md) is an example of software developed under this philosophy and adapting the term itself.

## See Also

- [minimalism](minimalism.md)
- [suckless](suckless.md)
- [KILL](kill.md)
- [primitivism](primitivism.md)
- [LRS](lrs.md)
- [KISP](kisp.md)