#include <stdio.h>

int main(void)
{
  int c, state = 0, filenameLen = 0;
  char filename[256];

  while ((c = getchar()) != EOF)
  {
    putchar(c);

    switch (state)
    {
      case 0: if (c == '<') state = 1; break;      
      case 1: state = c == 'a' ? 2 : 0; break;      
      case 2: state = c == ' ' ? 3 : 0; break;      
      case 3: state = c == 'h' ? 4 : 0; break;      
      case 4: state = c == 'r' ? 5 : 0; break;      
      case 5: state = c == 'e' ? 6 : 0; break;      
      case 6: state = c == 'f' ? 7 : 0; break;      
      case 7: state = c == '=' ? 8 : 0; break;      
      case 8: state = c == '"' ? 9 : 0; break;      
      case 9:
        if (c == '"')
        {
          filename[filenameLen] = 0;

          FILE *f = fopen(filename,"r");

          if (!f)
            printf(" class=\"dead\"");
          else
            fclose(f);

          filenameLen = 0;
          state = 0;
        }
        else if (filenameLen < 128)
        {
          filename[filenameLen] = c;
          filenameLen++;
        }

        break;

      default: break;
    }

  }

  return 0;
}
