# Toxicity

A social environment is said to be toxic if it induces a high psychological discomfort, toxic individuals are members of such environment that help establish it. Examples of toxic environments include a [capitalist](capitalism.md) society and [SJW](sjw.md) social networks.