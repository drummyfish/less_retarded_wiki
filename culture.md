# Culture

Culture is an [abstract](abstraction.md) term that includes behavioral norms, common beliefs, [moral](morality.md) values, habits and similar concept within certain society or some smaller group within it. It's the unwritten rules, what people generally do, what they like and appreciate, what's considered rude, what they strive for, what they dislike, how they react to things and so on -- culture is therefore connected to other attributes of society such as [language](language.md), [art](art.md) and [law](law.md) -- they all influence each other.

**Culture is more important than laws** as culture is the strongest force defining how we live the majority of our lives, what actions we take and how they are judged by others; law may try to capture some cultural demands, albeit in quite simplified and limited way, and enforce them, however courts, police and prison only step in in absolute extreme cases. Culture determines what you want and what you do, it is the rules that you truly follow, laws just pose arbitrary obstacles that you try to overcome, bypass or sometimes just ignore: police randomly beats people who don't respect laws but they can't beat everyone at once, you just try to reduce the probability you'll be beaten while following your cultural desires, so you try to not be noticed breaking legal laws while following cultural laws -- importance of culture versus law can be observed on different cultures of people living in the same country: consider e.g. orthodox Jews, capitalist businessmen, black gangs, all living under same legal laws. You do many more things (such as eating meat, cutting your hair, watching TV or living with a single partner) because of culture, not because you are obliged by law. There aren't even enough policemen to guarantee law enforcement in all cases and all states rely (by basically not even having any other choice) on culture doing most of the job in keeping society working (which is also exploited by states and corporations when they try to manipulate culture with [propaganda](propaganda.md) rather than changing laws). Consider for example that you download a random photo from the internet and set it as a wallpaper on your computer -- officially you have committed a crime of [piracy](piracy.md) as you had no rights for downloading the image, however culturally no one sees this as harmful, no one is going to bully you, sue you and even if someone tried to sue you, no judge would actually punish such a laughable "crime". On the other hand if you do a legal but culturally unacceptable thing, such as making a public art exhibition of non-sexual photos of naked children (also notice this might have been culturally OK to do in the previous century, but not [now](21st_century.md)), you will be culturally punished by everyone distancing themselves from you and someone perhaps even illegally physically attacking you. A sentence, such as "black people aren't as intelligent as white people", spoken half a century ago may nowadays be judged by a court in a much different way just by the context of today's culture and even under the same set of laws in the past you would not have been convicted of a crime while nowadays you would, as legal terms are eventually at some level defined in a plain language, which is permeated by culture. Therefore in trying to change society we should remember two things:

1. Focus on laws is a short term necessary evil.
2. Focus on culture (and eventual elimination of law as such) is our long term focus.

## Types Of Culture

Here are some notable, named cultural patterns:

- **[cancel culture](cancel_culture.md)**
- **[fear culture](fear_culture.md)**
- **[fight culture](fight_cutlure.md)**
- **[free culture](free_culture.md)**
- **[hacker culture](hacker_culture.md)**
- **[hero culture](hero_culture.md)**
- **[offended culture](offended_culture.md)**
- **[permission culture](permission_culture.md)**
- **[remix culture](remix_culture.md)**
- **[rights culture](rights_culture.md)**
- **[update culture](update_culture.md)**
- ...