# Zen

Zen, a term coming from zen [Buddhism](buddhism.md) (the word itself from *dhyana*, meaning *meditation*), means emphasis on meditation that leads to enlightenment; in a wider sense it hints on related things and feelings such as tranquility, spiritual peace, sudden coming to realization and understanding. In [hacker](hacking.md) speech *zen* is a very frequent term, according to [Jargon file](jargon_file.md) "to zen" means to understand something by simply meditating about it or by sudden realization (as opposed to e.g. active [trial and error](trial_and_error.md)).

[Wikiwikiweb](wikiwikiweb.md) has a related discussion under *ZenConcepts*.

TODO

## See Also

- [tao](tao.md)
- [fu](fu.md)
- [Buddhism](buddhism.md)
- [hacker culture](hacking.md)
- [nirvana](nirvana.md)
- [guru](guru.md)