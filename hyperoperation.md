# Hyperoperation

*WARNING: brain exploding article*

*UNDER CONSTRUCTION*

{ This article contains unoriginal research with errors and TODOs, read at own risk. Some really interesting and more in-dept information can be found at this nice site: http://mrob.com/pub/math/largenum.html. Also the rabbithole of big numbers and googology is so deep I can't even see the end of it. ~drummyfish }

Hyperoperations are [mathematical](math.md) operations that are generalizations/continuations of the basic arithmetic operations of addition, multiplication, exponentiation etc. Basically they're like the basic operations like plus but on steroids. When we realize that multiplication is just repeated addition and exponentiation is just repeated multiplication, it is possible to continue in the same spirit and keep inventing new operations by simply saying that a new operation means repeating the previously defined operation, so we define repeated exponentiation, which we call tetration, then we define repeated tetration, which we call pentation, etc.

There are infinitely many hyperoperations as we can go on and on in defining new operations, however we start with what seems to be the simplest operation we can think of: the successor operation (we may call it *succ*, *+1*, *++*, *next*, *increment*, *zeration* or similarly). In the context of hyperoperations we call this operation *hyper0*. Successor is a [unary](unary.md) operator, i.e. it takes just one number and returns the number immediately after it (suppose we're working with [natural numbers](natural_number.md)). In this successor is a bit special because all the higher operations we are going to define will be binary (taking two numbers). After successor we define the next operation, addition (*hyper1*), or *a + b*, as repeatedly applying the successor operation *b* times on number *a*. After this we define multiplication (*hyper2*), or *a * b*, as a chain of *b* numbers *a*s which we add together.  Similarly we then define exponentiation (*hyper3*, or raising *a* to the power of *b*). Next we define tetration (*hyper4*, building so called [power towers](power_tower.md)), pentation (*hyper5*), hexation (*hyper6*) and so on (heptation, octation, ...).

Indeed the numbers obtained by high order hyperoperations grow quickly as [fuck](fuck.md).

An important note is this: there are multiple ways to define the hyperoperations, the most common one seems to be by supposing the **right associative** evaluation, which is what we're going to implicitly consider from now on. This means that once associativity starts to matter, we will be evaluating the expression chains FROM RIGHT, which may give different results than evaluating them from left (consider e.g. `2^(2^3) != (2^2)^3`). The names tetration, pentation etc. are reserved for right associativity operations.

The following is a sum-up of the basic hyperoperations as they are commonly defined (note that many different symbols are used for these operations throughout literature, often e.g. up arrows are used to denote them):

| operation              |symbol    | meaning                                         |commutative|associative|
|------------------------|----------|-------------------------------------------------|-----------|-----------|
|successor (hyper0)      |`succ(a)` |next after *a*                                   |           |           |
|addition (hyper1)       |`a + b`   |`succ(succ(succ(...a...)))`, *b* succs           | yes       | yes       |
|multiplication (hyper2) |`a * b`   |`0 + (a + a + a + ...)`, *b* *a*s in brackets    | yes       | yes       |
|exponentiation (hyper3) |`a ^ b`   |`1 * (a * a * a * ...)`, *b* *a*s in brackets    | no        | no        |
|tetration (hyper4)      |`a ^^ b`  |`1 * (a ^ (a ^ (a ^ (...)`, *b* *a*s in brackets | no        | no        |
|pentation (hyper5)      |`a ^^^ b` |`1 * (a^^ (a^^ (a^^ (...)`, *b* *a*s in brackets | no        | no        |
|hexation (hyper6)       |`a ^^^^ b`|`1 * (a^^^(a^^^(a^^^(...)`, *b* *a*s in brackets | no        | no        |
|...                     |          |                                                 | no more   | no more   |

The following ASCII masterpiece shows the number [2](two.md) in the territory of these hyperoperations:

{ When performing these calculations, use some special calculator that allows extremely high numbers such as HyperCalc (http://mrob.com/pub/comp/hypercalc/hypercalc-javascript.html) or Wolfram Alpha. ~drummyfish }

```
 2    +1    +1    +1    +1    +1    +1    +1  ...     successor
 |        __/   ________/           /       9
 |       /     /     ______________/
 |      /     /     /
 2  +  2  +  2  +  2  +  2  +  2  +  2  +  2  ...     addition
 |     |4       __/                       / 16
 |     |       /     ____________________/
 |     |      /     /
 2  *  2  *  2  *  2  *  2  *  2  *  2  *  2  ...     multiplication
 |     |4     8 __/ 16    32    64    128   256           
 |     |       /     
 |     |      /                 ~10^(6 * 10^19728)
 2  ^ (2  ^ (2  ^ (2  ^ (2  ^ (2  ^ (2  ^ (2  ...     exponentiation
 |     |4     16__/ 65536 ~10^19728   ~10^(10^(10^19728))
 |     |       /             not sure about arrows here, numbers get too big, TODO
 |     |      /
 2  ^^(2  ^^(2  ^^(2  ^^(2  ^^(2  ^^(2  ^^(2  ...     tetration
 |     |4    |65536
 |     |     |         not sure about arrows here either
 |     |     |
 2 ^^^(2 ^^^(2 ^^^(2 ^^^(2 ^^^(2 ^^^(2 ^^^(2  ...     pentation
 ...    4     65536                         a lot
```

Some things generally hold about hyperoperations, for example for any operation *f = hyperN* where *N >= 3* and any number *x* it is true that *f(1,x) = 1* (just as raising 1 to anything gives 1).

[Hyperroot](hyperroot.md) is the generalization of [square root](sqrt.md), i.e. for example for tetration the *n*th hyperroot of number *a* is such number *x* that *tetration(x,n) = a*.

**Left associativity hyperoperations**: Alternatively left association can be considered for defining hyperoperations which gives different operations. However this is usually not considered because, as mentioned in the webpage above, e.g. left association tetration *a ^^ b* can be simplified to *a ^ (a ^ (b - 1))* and so it isn't really a new operation. Anyway, here is the same picture as above, but for left associativity -- we see the numbers don't grow THAT quickly (but still pretty quickly).

```
 2    +1    +1    +1    +1    +1    +1    +1  ...     successor
 |        __/   ________/           /       9
 |       /     /     ______________/
 |      /     /     /
 2  +  2  +  2  +  2  +  2  +  2  +  2  +  2  ...     addition
 |     |4       __/                       / 16
 |     |       /     ____________________/
 |     |      /     /
 2  *  2  *  2  *  2  *  2  *  2  *  2  *  2  ...     multiplication
 |     |4       __/ 16    32    64    128 / 256           
 |     |       /     ____________________/
 |     |      /     /
(2  ^  2) ^  2) ^  2) ^  2) ^  2) ^  2) ^  2  ...     left exponentiation
 |     |4     16__/ 256   65536             ~3*10^38
 |     |       /     ____________________________
 |     |      /     /
(2  ^^ 2) ^^ 2) ^^ 2) ^^ 2) ^^ 2) ^^ 2) ^^ 2  ...     left tetration
 |     |4     256   2^1048576   
 |     |                        TODO: arrows?
 |     |
(2 ^^^ 2)^^^ 2)^^^ 2)^^^ 2)^^^ 2)^^^ 2)^^^ 2  ...     left pentation
 ...    4     ~3*10^38
```

In fact we may choose to randomly combine left and right associativity to get all kinds of weird hyperoperations. For example we may define tetration with right associativity but then use left associativity for the next operation above it (we could call it e.g. "right-left pentation"), so in fact we get a binary [tree](tree.md) of hyperoperations here (as shown by M. Muller in his paper on this topic).

Of course, we can now go further and start inventing things such as hyperlogarithms, hyperfactorials etc.

## Code

Here's a [C](c.md) implementation of some hyperoperations including a general hyperN operation and an option to set left or right associativity (however note that even with 64 bit ints numbers overflow very quickly here):

```
#include <stdio.h>
#include <inttypes.h>
#include <stdint.h>

#define ASSOC_R 1 // right associativity?

// hyper0
uint64_t succ(uint64_t a)
{
  return a + 1;
}

// hyper1
uint64_t add(uint64_t a, uint64_t b)
{
  for (uint64_t i = 0; i < b; ++i)
    a = succ(a);

  return a;
  // return a + b
}

// hyper2
uint64_t multiply(uint64_t a, uint64_t b)
{
  uint64_t result = 0;

  for (uint64_t i = 0; i < b; ++i)
    result += a;

  return result;
  // return a * b
}

// hyper(n + 1) for n > 2
uint64_t nextOperation(uint64_t a, uint64_t b, uint64_t (*operation)(uint64_t,uint64_t))
{
  if (b == 0)
    return 1;

  uint64_t result = a;

  for (uint64_t i = 0; i < b - 1; ++i)
    result = 
#if ASSOC_R
      operation(a,result);
#else
      operation(result,a);
#endif

  return result;
}

// hyper3
uint64_t exponentiate(uint64_t a, uint64_t b)
{
  return nextOperation(a,b,multiply);
}

// hyper4
uint64_t tetrate(uint64_t a, uint64_t b)
{
  return nextOperation(a,b,exponentiate);
}

// hyper5
uint64_t pentate(uint64_t a, uint64_t b)
{
  return nextOperation(a,b,tetrate);
}

// hyper6
uint64_t hexate(uint64_t a, uint64_t b)
{
  return nextOperation(a,b,pentate);
}

// hyper(n)
uint64_t hyperN(uint64_t a, uint64_t b, uint8_t n)
{
  switch (n)
  {
    case 0: return succ(a); break;
    case 1: return add(a,b); break;
    case 2: return multiply(a,b); break;
    case 3: return exponentiate(a,b); break;
    default: break;
  }

  if (b == 0)
    return 1;

  uint64_t result = a;

  for (uint64_t i = 0; i < b - 1; ++i)
    result = hyperN(
#if ASSOC_R
      a,result
#else
      result,a
#endif
      ,n - 1);

  return result;
}

int main(void)
{
  printf("\t0\t1\t2\t3\n");

  for (uint64_t b = 0; b < 4; ++b)
  {
    printf("%" PRIu64 "\t",b);

    for (uint64_t a = 0; a < 4; ++a)
      printf("%" PRIu64 "\t",tetrate(a,b));

    printf("\n");
  }

  return 0;
}
```

In this form the code prints a table for right associativity tetration:

```
        0       1       2       3
0       1       1       1       1
1       0       1       2       3
2       1       1       4       27
3       0       1       16      7625597484987
```

## See Also

- [googology](googology.md)
- [p-adic numbers](p_adic.md)