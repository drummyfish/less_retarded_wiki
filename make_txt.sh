#!/bin/sh
# Creates an ASCII version of the wiki.

echo "making TXT"

./make_stats.sh

rm -rf txt
mkdir txt

for f in *.md; do
  echo $f

  fname=$(echo "$f" | sed "s/\.md//g")
  f2="txt/${fname}.txt"

  cat $f | cmark-gfm -e table > __tmp.html && links -html-numbered-links 1 -dump __tmp.html | sed "s/file:\/\/.*less_retarded_wiki\///g" >> $f2
done

rm __tmp.html

echo "making single TXT"

echo "LESS RETARDED WIKI" > lrs_wiki.txt
echo "by drummyfish, released under CC0 1.0, public domain" >> lrs_wiki.txt

for f in txt/*.txt; do
  echo $f

  fname=$(echo "$f" | sed "s/\.txt//g" | sed "s/txt\///g")

  echo "--------------------------------------------------------------------------------" >> lrs_wiki.txt
  echo "$fname:" >> lrs_wiki.txt

  cat $f >> lrs_wiki.txt
done

echo "done"
