#!/bin/sh
# Convert the wiki to all formats.

echo "making all"

./make_html.sh
./make_txt.sh
./make_pdf.sh

mkdir lrs_wiki
mkdir lrs_wiki/md
cp *.md lrs_wiki/md
cp lrs_wiki.html lrs_wiki
cp lrs_wiki.txt lrs_wiki
cp lrs_wiki.pdf lrs_wiki
7z a lrs_wiki.7z lrs_wiki -t7z -mfb=64 -md=32m -ms=on
cp lrs_wiki.7z html

echo "done"
