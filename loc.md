# Lines of Code

Lines of code (LOC, KLOC = 10K LOC, MLOC = 1M LOC etc., also SLOC = source LOC) are a metric of software [complexity](complexity.md) that simply counts the number of lines of program's [source code](source_code.md). It is not a perfect measure but despite some [soyboys](soydev.md) shitting on it it's actually pretty good, espcially when using only one language ([C](c.md)) with consistent [formatting style](code_formatting.md).

Of course the metric becomes shitty when you have a project in 20 programming languages written by 100 pajeets out of which every one formats code differently. Also when you use it as a [productivity](productivity_cult.md) measure at [work](work.md) then you're guaranteed your devs are gonna just shit our as much meaningless code as possible in which case the measure fails again. Fortunately, at [LRS](lrs.md) we don't have such problems :)

When counting lines, we need to define what kind of lines we count. We can either count:

- raw (physical) lines: every single one
- lines that actually "matter" (*logical* lines), e.g. excluding comments, blank lines etc.

A comfy tool for counting lines is [`cloc`](cloc.md), but you can also just use `wc -l` to count raw lines.