# Operating System

Operating System (OS) is usually a quite complex [program](program.md) that's typically installed on a [computer](computer.md) before any other user program and serves as a platform for running other programs as well as handling [low level](low_level.md) functions, managing resources ([CPU](cpu.md) usage, [RAM](ram.md), [files](file.md), [network](network.md), ...) and offering services, protection and [interfaces](interface.md) for humans and programs. If computer was a city, an OS is its center that was built first and where its government resides. As with most things, the definition of an OS can differ and be stretched greatly -- while a typical OS will include features such as [graphical interface](gui.md) with windows and mouse cursor, [file system](file_system.md), [multitasking](multitasking.md), [networking](network.md), [audio](audio.md) system, safety mechanisms or user accounts, there exist OSes that work without any said feature. Though common on mainstream computers, operating system isn't necessary; it may be replaced by a much simpler program (something akin a program loader, BIOS etc.) or even be absent altogether -- programs that run without operating system are called "[bare metal](bare_metal.md)" programs (these can be encountered on many simple computers such as [embedded](embedded.md) devices).

There is a nice [CC0](cc0.md) wiki for OS development at https://wiki.osdev.org/.

Examples of operating systems are [Unix](unix.md) (one of the first and most influential systems), [GNU](gnu.md) ([free software](free_software.md) clone of Unix), various [BSD](bsd.md) systems, [Windows](windows.md) (harmful parody of an operating system) or [Android](android.md) (another highly harmful software, this time for mobile platforms), [TempleOS](templeos.md) etc.

From programmer's point of view a serious OS is one of the most difficult pieces of software one can pursue to develop. The task involves an enormous amount of [low-level](low_level.md) programming, development of own tools from scratch and requires deep and detailed knowledge of all components of a computer, of established standards as well as many theoretical subjects such as [compiler](compiler.md) design.

**Which OS is the best?** Currently there seems to be almost no good operating system in existence, except perhaps for [Collapse OS](collapseod.md) and [Dusk OS](duskos.md) which may be the closest to [LRS](lrs.md) at the moment, but aren't widely used yet and don't have many programs running on them. Besides this there are quite a few relatively usable OSes, mostly [Unix](unix.md) like systems. For example [OpenBSD](openbsd.md) seems to be one of them, however it is [proprietary](proprietary.md) (yes, it contains some code without valid licenses, for example [this](https://cvsweb.openbsd.org/cgi-bin/cvsweb/src/sys/dev/microcode/tigon/tigon-license?rev=1.5&content-type=text/x-cvsweb-markup), [this](https://cvsweb.openbsd.org/cgi-bin/cvsweb/src/sys/dev/microcode/yds/yds-license?rev=1.3&content-type=text/x-cvsweb-markup) etc.) and too obsessed with MUH [SECURITY](security.md), and still a bit overcomplicated. [HyperbolaBSD](hyperbolabsd.md) at least tries to address the freedom issue of OpenBSD but suffers from many others. [Devuan](devuan.md) is pretty usable, [just werks](just_werks.md) and is alright in not being an absolute apeshit of consoomerist bloat. [FreeDOS](freedos.md) seemed nice too: though it's not Unix like, it is much more [KISS](kiss.md) than Unices, but it will probably only work on [x86](x86.md) systems. 

An OS, as a software, consists of two main parts:

- **[kernel](kernel.md)**: The base/core of the system, running in the most privileged mode, offering an environment for user applications.
- **[userland](userland.md) (applications)**: The set of programs running on top of the kernel. These run in lower-privileged mode and use the services offered by the kernel via so called [system calls](syscall.md).

For example in GNU/Linux, [Linux](linux.md) is the kernel and [GNU](gnu.md) is the userland.

## Attributes/Features

TODO

## Notable Operating Systems

Below are some of the most notable OSes.

{ Some more can be found here: https://wiki.osdev.org/Projects. ~drummyfish }

- [Android](android.md): extremely badly designed malicious system
- [BSD](bsd.md) systems such as [OpenBSD](openbsd.md) and [freeBSD](freebsd.md): Unix-like OSes
- [Collapse OS](collapseos.md): [finished](finished.md), extremely minimalist OS that will help us survive the [collapse](collapse.md)
- [DuskOS](duskos.md): kind of continuation of Collapse OS aiming for more comfort
- [DOS](dos.md)
- [FreeDOS](freedos.md)
- [GNU](gnu.md)/[Linux](linux.md) very popular systems existing in many different [distributions](distro.md), some completely [free](free_software.md)
- [Haiku](haiku.md)
- [HyperbolaBSD](hyperbolabsd.md)
- [Inferno](inferno.md): OS in the style of Plan 9
- [MacOS](macos.md)
- [Minix](minix.md)
- [Oberon](oberon.md): old, kinda KISS OS with [TUI](tui.md), made using custom language
- [Plan 9](plan9.md): research OS, continuing the ideas of [Unix](unix.md)
- [PostmarketOS](pmos.md)
- [ReactOS](reactos.md)
- [Replicant](replicant.md)
- [Solaris](solaris.md)
- [TempleOS](templeos.md): simple [meme](meme.md) OS written by a [Terry Davis](terry_davis.md)
- [Unix](unix.md)
- [Windows](windows.md): very bad [proprietary](proprietary.md) [capitalist](capitalist_software.md) OS
- [9front](9front.md): OS based on Plan 9
- ...

## LRS Operating System

*UPDATE: This role may be taken by [comun shell](comun_shell.md).*

What would an operating system designed by [LRS](lrs.md) principles look like? There may be many different ways to approach this challenge. Multiple operating systems (or multiple versions of the same system) may be made, such as as an "extremely KISS bare minimum featureless system", a "more advanced but still KISS system", a "special-purpose safe system for critical uses" etc. The following is a discussion of ideas we might employ in designing such systems.

The basic idea for a universal LRS operating system is to be something more akin a mere **text [shell](shell.md)** (possibly [comun](comun.md) shell), we wouldn't probably even call it an operating system. A rough vision is something like **"[DOS](dos.md) plus a bit of [Unix philosophy](unix_philosophy.md)"**; we may also imagine it like [GRUB](grub.md) or something similar really. The system would probably seem primitive by "[modern](modern.md) standards", but in a [good society](less_retarded_society.md) it would be sufficient as a universal operating system (i.e. not necessarily suitable for ALL purposes). The OS would in fact be more of a **program loader** (like e.g. the one seen in [Pokitto](pokitto.md)), running with the same privileges as other programs -- its purpose would NOT be to provide a safe environment for programs to run in, to protects user's data and possibly not even to offer a platform for programs to run on (for abstracting hardware away a non-OS [library](library.md) might be used instead), but rather to allow switching between different programs on a computer without having to reupload the programs externally, and to provide basic tools for managing the computer itself (such as browsing files, testing hardware etc.). This shell would basically allow to browse files, load them as programs, and maybe run simple scripts (e.g. in mentioned comun language), allowing things such as automatization of running several program (NOT in parallel but rather one by one) to collaborate on computing something.

An idea worth mentioning is also the possibility to have a have a distribution of this "operating system" that works completely without a file system, i.e. something akin a "big program" that has all the tools compiled into it, without the possibility to install or uninstall programs. Of course this doesn't mean ALL operating systems would in the world would work like this, it would just be a possibility for those that could benefit from it, e.g. very small wrist watch computers that don't wouldn't want and need to include hardware and software required for a mutable filesystem to work, since all they would need would be a few tools like stopwatch and calculator, plus they would gain the advantage of loading a program instantly. The tools to be "compiled in" could be chosen by the user before compilation to make a personalized "immutable distro".

Let's keep in mind that true LRS computers would be different from the current capitalist ones -- an operating system would only be optional, programs would be able to run on [bare metal](bare_metal.md) as well as under an OS, and operating systems would be as much compatible as possible. By this an OS might be seen as more of an extra tool rather than a platform.

The system might likely lack features one would nowadays call essential for an OS, such as multiple user support (no need if [security](security.md) is not an issue, everyone can simply make his own subdirectory for personal files), [virtual memory](virtual_memory.md), complex [GUI](gui.md) etc. There might even be no [multitasking](multitasking.md); a possibility to make a multitasking OS exists, but let's keep in mind that even such things as programs interacting via [pipes](pipe.md) may be implemented without it (using temporary buffer files into which one program's output is stored before running the next program).

The universal OS would assume well behaved programs, as programs would likely be given full control over the computer when run -- this would greatly simplify the system and also computing in general. Doing so would be possible thanks to non-existence of malicious programs (as in good society there would be no need for them) and elimination of [update culture](update_culture.md). Users would only install a few programs they choose carefully -- programs that have been greatly tested and don't need to be updated.

**On user interface**: the basic interaction mode would of course be the text interface. Programs would have the option to switch to a graphical mode in which they would be able to draw to screen. There would be no such [bloat](bloat.md) as [window managers](window_manager.md) or [desktop environments](desktop_environment.md) -- these are capitalist inventions that aren't really needed as users practically always interacts with just one program at a time. Even in a multitasking system only one program would be drawing to the screen at a time, with user having the option to "alt-tab" between them. This would also simplify programs greatly as they wouldn't have to handle bullshit such as dynamically resizing and rearranging their window content. If someone REALLY wanted to have two programs at the screen at the same time, something akin a "screen splitter" might be made to create two virtual screens on one physical screen.

**A bit more details**: the universal OS could simply be a program that gets executed after computer restart. This program would offer a [shell](shell.md) (textual, graphical, ...) that would allow inspecting the computer, configuring it, and mainly running other programs. Once the user chose to run some program, the OS would load the program to memory and jump to executing it. To get back to the OS the program could hand back control to the OS, or the computer could simply be restarted. If the program crashes, the computer simply restarts back to OS.

TODO: more