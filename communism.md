# Communism

*"Imagine no possession"* --John Lennon

Communism (from *communis* -- common, shared) is a very wide term which most generally stands for the idea that sharing and equality should be the basic values and principles of a society; as such it is a [leftist](left_right.md) idea which falls under [socialism](socialism.md) (i.e. basically focusing on people at large). There are very many branches, theories, political ideologies and schools of thought somewhat based on communism, for example [Marxism](marxism.md), [Leninism](leninism.md), [anarcho communism](ancom.md), primitive communism, Christian communism, Buddhist communism etc. -- of course, some of these are good while others are evil and only abuse the word communism as a kind of *brand* (as also happens e.g. with [anarchism](anarchism.md)). Sadly after the disastrous failure of the violent pseudocommunist revolutions of the 20th century, most people came to equate the word communism with oppressive militant regimes, however we have to stress that **communism is NOT equal to [USSR](ussr.md), Marxism-Leninism, Stalinism or any other form of pseudocommunism**, on the contrary such regimes were rather hierarchical, nonegalitarian and violent, we might even say downright [fascist](fascism.md). We ourselves embrace true communism and build our [LRS](lrs.md) and [less retarded society](less_retarded_society.md) on ideas of unconditional sharing. **Yes, large communist societies have existed and worked**, for example the [Inca](inca.md) empire worked without [money](money.md) and provided FREE food, clothes, houses, health care, education and other products of collective work to everyone, according to his needs. Many other communities also work on more or less communist principles, see e.g. Jewish kibbutz, Sikhist [langar](langar.md), [free software](free_software.md), or even just most families for that matter. Of course, no one says the mentioned societies and groups are or were [ideal](less_retarded_society.md), just that the principles of communism DO work, that communism should be considered a necessary attribute of an ideal society and that ideal society is not impossible due to impossibility of communism because as we see, it is indeed possible. The color [red](red.md) is usually associated with communism and the "hammer and sickle" (U+262D) is taken as its symbol, though that's mostly associated with the evil communist regimes and so its usage by LRS supporters is probably better be avoided.

Common ideas usually associated with communism are (please keep in mind that this may differ depending on the specific flavor of communism):

- Ending [capitalism](capitalism.md) and similar rightist oppressive hierarchical systems which are the polar opposite of communism and are incompatible with it. Along with these also things like [consumerism](consumerism.md), worker exploitation, crime and poverty will disappear.
- Abolishment of private property, establishing common ownership, for example a factory shouldn't have a single owner who makes profit off of it, it should rather be collectively managed by those who work in the factory and they should collectively share what they make there.
- Sharing and collaboration as opposed to [competition](competition.md).
- Equality, seizing of division of people into social classes (such as workers, [bourgeoisie](bourgeoisie.md), rich, poor, aristocracy, ...).
- Eventual abolishment of [state](state.md), as again in a good society that benefits people there shouldn't be any crime, theft, abuse of workers etc. However some "communists" see state and its control of economy as a necessary intermediate step towards this goal.
- Abolishment of [money](money.md) as that is a means of dividing people into classes (rich and poor), means of abuse (wage slavery) and a tool of systems such as capitalism. In a good society money is unnecessary, everyone gets what he needs.
- Sometimes [revolution](revolution.md) (and even [war](war.md), temporary dictatorship etc.) is seen by some "communists" as a necessary way of achieving a change, however many others oppose this as revolution means violence, dominating man by another man (inequality) etc. -- peaceful voluntary evolutionary approach is also an option of achieving communism.
- Focus on workers and common people.
- Intellectual endeavor and idealism -- many communists are intellectuals, scientifically examining society and seeking models of an ideal society, a "[utopia](utopia.md)" as opposed to accepting life in a [dystopia](dystopia.md).
- ...

TODO

## See Also

- [less retarded society](less_retarded_society.md)
- [socialism](socialism.md)
- More’s [Utopia](utopia.md)
- [capitalism](capitalism.md)
