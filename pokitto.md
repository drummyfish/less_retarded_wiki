# Pokitto

Pokitto is a very nice educational [open gaming console](open_console.md) friendly to [hacking](hacking.md) and [FOSS](foss.md). It is also very family friendly, aiming to be used as an educational device for kids on schools, which doesn't at all take away any of its value for hardcore hackers. Its website is https://www.pokitto.com/. As of writing this Pokitto is unavailable for purchase as a new version is being developed.

Its great advantage is its nice, active and friendly community that's constantly writing software, documenting Pokitto and helping newcomers. There have even appeared a few issues of Pokitto magazine.

The console was created by Jonne Valola from Finland. He started the project on Kickstarter on April 28 2017, pledged over $27000 and released Pokitto in February 2018. { Jonne is a really nice guy who loves the project, puts his soul into the project and always personally helps people and shares technical details of the console. ~drummyfish }

Pokito, unlike most other open consoles, is NOT based on [Arduino](arduino.md), but on [NXP](nxp.md)'s LPC11U6x [microcontroller](mcu.md) (MCU). Some features and specs of Pokitto are:

- Up to **220x176 color [TFT](tft.md) display** (ST7775R). (Resolution and color depth depends on chosen mode and how much [RAM](ram.md) you want to dedicate to [screen buffer](screen_buffer.md)).
- Up to **72 MHz [ARM](arm.md) CPU** (LPC11U6x). The base frequency is 48 MHz but the hardware is easily [overclocked](overclocking.md).
- **256 kB [ROM](rom.md)** (program storage space).
- **36 kB [RAM](ram.md)** (working memory).
- **4 kB [EEPROM](eeprom.md)** (persistent storage).
- **7 buttons**.
- **Speaker and headphone jack**.
- Both **[emulator](emulator.md) and simulator** which make programming much more efficient and comfortable.
- **Custom library** -- PokittoLib -- [free](free_software.md)-licensed { even though it contains a few small "fair use" files from the MCU vendor. ~drummyfish }. It has many features, unfortunately it's also kind of [bloated](bloat.md).
- **[SD](sd.md) card support**.
- Hardware extensions called **hats**. Available is e.g. a hat with [joystick](joystick.md) and extra buttons.
- Programming via [USB](usb.md), works on [GNU](gnu.md)/[Linux](linux.md) with [gcc](gcc.md) ARM cross compiler. Supports a few languages: **[C++](cpp.md), [C](c.md), [MicroPython](micropython.md) and [Java](java.md)**.
- Custom [IDE](ide.md) for [noobs](noob.md): FemtoIde.
- Schematics and 3D print files available.
- A huge number of games and other software has already been written.

**How [free](free_software.md) is Pokitto?** Quite freedom friendly, but not nearly 100% free; It is made out of [proprietary](proprietary.md) hardware, but it's quite [KISS](kiss.md), the Pokitto library, emulator and most tools as well as many games are [FOSS](foss.md), however the library contains a few proprietary pieces of code (short vendor source code without license), though these are almost certainly not harmful and could easily be replaced. Schematics and printable STL files are available, though license seems to be non-present. No Pokitto trademarks were surprisingly found during brief search.

**Downsides** of Pokitto are that the community is an [open source](open_source.md) community rather than [free software](free_software.md) one, purists like us will find they lean towards [bloated](bloat.md) solutions even though the technical limitation of the console largely prevent their implementation. The web forums runs on [discourse](discourse.md) and requires [JavaScript](js.md) for interactivity. [Discord](discord.md) is also actively used for communication, even though some community members bridged it to free alternatives. The official library is relatively bloated and even contains some small pieces of unlicensed code from the [MCU](mcu.md) manufacturer -- they are very simple assembly snippets that may be easily replaceable, but we should be cautious even about this. Anyway, a reasonably dedicated programmer might create a suckless Pokitto library without greater problems.

Some quite nice [hacks](hacking.md) were achieved with Pokitto, e.g. using it as a display for a PC or even running [GameBoy](gameboy.md) games on it -- this was done thank to a small [FOSS](foss.md) GameBoy emulator and a tool that packs this emulator along with selected GameBoy ROM into a Pokitto executable -- this of course comes with some limitations, e.g. on sound or game size. Yes, Pokitto quite comfortably runs [Anarch](anarch.md).

## How To, Tips'N'Tricks

TODO

**Uploading** programs to Pokitto under [GNU](gnu.md)/[Linux](linux.md) can be done e.g. with [dd](dd.md) (or mcopy etc.) like this:

```
sudo mount /dev/sdb ~/mnt
sudo dd bs=256 conv=nocreat,notrunc,sync,fsync if=~/git/Anarch/bin/Anarch_pokitto_nonoverclock_1-01.bin of=~/mnt/firmware.bin
sudo umount ~/mnt
```
