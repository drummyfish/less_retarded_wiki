# Shortcut Thinking

Shortcut thinking means making conclusions by established associations (such as "theft = bad", "[fair](fair.md)" = "good") rather than making the extra effort of inferring actual conclusions based on new context such as different circumstances or newly discovered facts. This isn't bad in itself, in fact it is a great and necessary [optimization](optimization.md) of our thinking process, a kind of [cache](cache.md), and it's really why we have long term memory -- imagine we'd have to deduce all the facts from scratch each time we thought about anything. However shortcut thinking can be a weakness in many situations and leaves people prone to manipulation by [propaganda](propaganda.md) which twists meanings of words (such as "open mind", ["rationality"](pseudoskepticism.md), "[progress](progress.md)", ["theft"](intellectual_property.md), ["science"](soyence.md) etc.), relying on people accepting the unacceptable by having them bypass the thinking process with the mental shortcut. As such this phenomenon is extremely abused by politicians, i.e. they for example try to shift the meaning of a certain negative word to include something they want to get rid of, to have it rejected just based on its name.

Some commonly held associations appearing in shortcut thinking of common people nowadays are for example "piracy = theft = bad", "laziness = bad", "[fairness](fair.md) = good", "pedophiles = child rapists = bad", "[competition](competition.md) = [progress](progress.md) = good", "more [jobs](work.md) = good", "more [complex technology](capitalist_software.md) = better", "open mind = blindly trusting those officially declared smarter than myself = good" etc. Of those most are of course either extremely simplified or just plain wrong. however some association may still of course be correct, such as "murder = bad", which is an association that e.g. [military](military.md) tries to get rid of by calling the acts of murder they commit something else, e.g. "defense", "[justice](justice.md)" etc.

Let's focus on the specific **example** of the association "fair = good" -- when we hear something is *fair*, we automatically give it a positive meaning, we see it as good. It is so because our society is, and mostly has been, based on [competition](competition.md) and in that fairness is key so as to ensure that the stronger wins and weaker loses, i.e. fairness is an attribute that any competitive system needs in order to work correctly. However when we start to aim to remove competitive society and replace it with [altruistic one](less_retarded_society.md) -- a society in which one doesn't have to prove his worth or be slave in order to deserve the right to live -- fairness becomes a BAD attribute. If we want the same well being for everyone, fairness is bad because it goes against this goal, it gives more to some (the stronger) than to other (the weaker).

Another example is e.g. the association "theft = bad". Indeed it has some sense in it -- if we turn shortcut thinking off, we may analyze why this association exists. For most of our history the word theft has meant *taking a physical personal possession of someone else against his will*. Indeed, in a society of people of which most weren't rich, this was bad in most cases as it hurt the robbed man, he has lost something he probably needed. However the society evolved, the meaning of property itself has changed from "personal property" to "private property", i.e. suddenly there were people who could own a whole forest or a factory even if they have never seen it, and there were people who had much more than they needed. If a poor starving man steals food from the rich to his family and the rich doesn't even notice this, suddenly the situation is different and many will say this is no longer bad. Nevertheless the word theft stayed in use and now included even such cases that were ethical because of the shifted meaning of the word "property" and due to changes in conditions of people. Recently the word property was shifted to the extreme with the invention of **[intellectual property](intellectual_property.md)**, i.e. the concept of being able to own information such as ideas or stories in books. Intellectual property is fundamentally different from physical property as it can't be stolen in the same way, it can only be copied, duplicated, but this copying doesn't rid the "owner" of the original information. Indeed it may prevent the author from making a lot of money under [capitalism](capitalism.md), but that's only thanks to the artificially established system inventing ways of bullying recipients of information into paying money, the system was deliberately made so as to make non harmful things into harmful ones to fuel "[competition](competition.md)" and the situation is no longer as simple as with physical property/stealing, it's actually the underlying system that's wrong here, not actions that aren't aligned with the harmful system. And so nowadays the word "theft", or one of its modern forms, "[piracy](piracy.md)", includes also mere copying of information or even just reusing an idea ([patent](patent.md)) for completely good purposes, for example writing computer programs in certain (patented) ways is considered a theft, even if doing so is done purely to help other people at large. Those arguing that illegal downloads or reuses prevent the "owner's" profit must know that again society is completely different nowadays and this so called "theft" actually doesn't hurt anyone but some gigantic billion dollar corporation that doesn't even notice if loses one or two million dollars, no actual human gets hurt, only a legal entity, and these so called "thefts" actually give rise to good, helpful things by at least a little more balance to society, hurting a virtual, non-living entity to help millions of actual living people. In fact, hurting a corporation, by definition a [fascist](fascism.md) entity hostile to people, may yet further be seen as a good thing in itself, so stealing from corporation is also good by this view. The illusion of profit theft here is arbitrarily made, the "theft" exists only because we've purposefully created a system which allows selling copies of information and restricting ideas and therefore enables this "theft", i.e. this is no longer a natural thing that would exist without something "preventing it", it's something miles away from the original meaning of the word "theft". With all this in mind we may, in today's context of the new meaning of old words, reconsider theft to no longer be generally bad.

When confronted with a new view, political theory etc., we should try to turn shortcut thinking off; we should also do this every time someone tries to have us make a decision under a competitive system such as [capitalism](capitalism.md) as that someone is most likely trying to manipulate us. Doing this can be called **being open minded**, i.e. opening one's mind to reinterpretation of very basic, possibly strongly rooted concepts by a new view, however be careful as the meaning of the term "open mind" is often twisted too. Also we should probably update our association from time to time just to keep them up with the new state of the world, only by sitting down and thinking about the world and discussing it with the right people.

The politics and views of [LRS](lrs.md) requires extreme open mindedness to be accepted by someone indoctrinated by the standard capitalist fascist propaganda of today.

## Example Of Abuse

Let's take a look at a simple example of how our shortcuts can be abused -- in recent times people in first world started to hate mass religion (let's now ignore [fedora extremists](atheism.md) that hate any religion whatsoever). Whether it's good or bad is now irrelevant, the important thing is there was some kind of reasoning behind this connection, for example that mass religion very often leads to abuse of power, brainwashing, corruption and so on. The thinking looks like this:

```
          dislike
 we ------------------->  abuse of power
    :                    and brainwashing
    :                            ^
    :      THEREFORE             |
    :       .----------- usually |
    :       |           leads to |
    :       |                    |
    :       V                    |
    :  we also dislike           |
    '- - - - - - - - - - >  mass religion
   (establishing shortcut)
```

After the initial step we end up with a convenient shortcut that simply states "we hate mass religion":

```        
 we -.
     |
     | dislike
     | *proven shortcut*
     |
     V
  mass religion
```

We should remember that this shortcut in fact means we oppose the evil behind it, i.e. abuse of power, brainwashing etc., but over time we forget this, we base our personality on simply hating the words "mass religion" as an axiom. We are furthermore told to always stand behind our opinions no matter what, so we harden the shortcut so much that we will never change it -- we are told it's a shame to deny something we used to think in the past (it will be called something like "integrity", identity, personality, ...). Now it's very easy to manipulate us -- even if the situation changed and mass religion no longer presented the dangers we originally opposed, we will still hate it just because we hate those words now -- but more importantly, it is now easy to for anyone to start doing the evils that mass religions used to do simply by calling themselves something else, as we only hate words and we'll never change (capitalists tell us it is a shame to change opinions). If for example [capitalism](capitalism.md) takes over the old role of mass religion, with all the things such as abuse of power, brainwashing, corruption, discrimination etc., we accept them easily, we are only focused on kicking the corpse of mass religion. [Our goalpost has been shifted](shifting_the_goalpost.md) due to the weakness in our thinking -- our shortcut we will never question because we don't want to change our values. This simple psychological trick works on 99.9999999999999% of people and is further perfected by applying a bit of brainwashing and cheap tricks such as [corporations](corporation.md) pretending to "stand on our side" by "hating the same thing" we do (when in fact corporation cannot love or hate anything, it is a robotic entity that can only strategically claim something) and fueling our hatred of the mass religion corpse, e.g. by applying [fear culture](fear_culture.md) (BEWARE PRIEST [PEDOPHILES](pedophilia.md)!), [influencers](influencer.md) (BE A COOL ATHEIST LIKE THIS YOUTUBER!), [ads](marketing.md), [fashion](fashion.md) (BUY THIS COOL HAT THAT TELLS EVERYONE YOU'RE AN ATHEIST!) etcetc. The situation now looks possibly like this:

```
          "DON'T THINK HERE"
      "FOCUS ON HATING RELIGION!"
         brainwashing barrier
               |     
 we -.         |          abuse of power
     |         |_        and brainwashing
     |         | \_              ^
     |             \_            |
     | dislike       \_   always |
     |                 \_leads to|
     |                   \_      |
     |                     \_    |
     V                       \   |
  mass religion <----------- capitalism
           "WE ALSO DISLIKE THIS" 
             "WE ARE WITH YOU!"
```

This is how capitalism will be able to sustain itself no matter what -- even if people slowly realized capitalism is bad, it will just be renamed to something else like "Progressive Lesbian Smart Communist [Modern](modern.md) Technologianism" -- it will be exactly the same as capitalism but people will accept it because it won't have the letters C, A, P, I, T, A, L, I, S and M in this exact succession in it. China for example has already done this by just renaming capitalism to "communism".

## See Also

- [idiot fallacy](idiot_fallacy.md)