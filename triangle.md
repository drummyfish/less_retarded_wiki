# Triangle

Triangle is a three sided [polygon](polygon.md), one of the most basic [geometric](geometry.md) shapes. It is a [convex](convex.md) shape. Furthermore it is a 2-[simplex](simplex.md), i.e. the simplest ["shape composed of sides"](polytope.md) in [2 dimensions](2d.md). Triangles are very important, they for example help us to compute [distances](distance.md) or define functions like [sine](sin.md) and [cosine](cos.md) (see [trigonometry](trigonometry.md)).

{ In my favorite book [Flatland](flatland.md) triangles represent the lowest class of men with isoscele triangles being the lowest as they are most similar to [women](woman.md) who are just straight [lines](line.md). ~drummyfish }

Triangle consists of three [vertices](vertex.md) (usually labeled *A*, *B* and *C*), three sides (usually labeled *a*, *b* and *c* according to the side's opposing vertex) and three angles (usually labeled *alpha*, *beta* and *gamma* according to the closest vertex):

```
       B                           B
       .                           .
      / \                 c =     /|
   c /   \ a          hypotenuse / | a
    /     \                     /  |
   /_______\                   /___|
 A     b     C               A   b  C
    triangle               right triangle
```

**Right triangle**, a triangle with one angle equal to 90 degrees ([pi](pi.md) / 2 [radians](radian.md)), is an especially important type of triangle. Right triangles are used to define trigonometric functions, such as [sine](sin.md), [cosine](cos.md) and [tangent](tan.md), as ratios of side lengths as a function of the triangle angle. For example in a right triangle (as drawn above) it holds that *sin(alpha) = a / c*.

**Similar triangles** are triangles that "have the same shape" (but may be of different sizes, positions and rotations). Two triangles are similar if the lengths of corresponding sides have the same ratios, or, equally, if they have the same inside angles. E.g. a triangle with side lengths 1, 2 and 3 is similar to a triangle with side lengths 2, 4 and 6. This fact is very useful in some geometric computations as it can help us determine unknown side lengths.

**Equilateral triangle** is a triangle whose sides have the same length, which means all its angles are also equal (60 degrees, pi / 3 radians). Equilateral triangles are of course all similar to each other. An **isoscele triangle** is a triangle with two sides of the same length. We can also distinguish acute and obtuse triangles (obtuse having one angle greater than 90 degrees).

In a triangle there exist two important types of helper line segments: **median** and **altitude**. Median goes from a triangle's vertex to the opposite side's center. Altitude goes from a vertex to the opposite side in a perpendicular direction to that side. Each triangle has three medians and three altitudes.

Some basic facts, features and equations regarding triangles are following (beware: many of them only hold in [Euclidean geometry](euclidean_geometry.md)):

- **Triangle angles add up to 180 degrees** ([pi](pi.md) [radians](radian.md)). This can be used to determine unknown side angles.
- Center of weight: average the three coordinates, or take the intersection of the triangle's medians.
- **[area](area.md)**:
  - general triangle: *a * altitude(a) / 2*
  - right triangle: *a * b / 2*
- **[Pythagorean theorem](pythagorean_theorem.md)**: For the lengths of the sides of a RIGHT triangle (i.e. one angle is 90 degrees) it always holds that *a^2 + b^2 = c^2*. This is extremely important and can be used to determine unknown side lengths of right triangles.
- **[Thales's theorem](thales_theorem.md)**: if points *A*, *B* and *C* lie on a [circle](circle.md), then they form a right triangle with hypotenuse equal to the circle diameter (and the center of the circle lying in the middle of the hypotenuse).
- **Triangle inequality**: Sum of any two side lengths can't be greater than the length of the third side, i.e. *a + b <= c*. That means that e.g. a triangle with side lengths 1, 2 and 4 can't exist because 1 + 4 > 2. If one side of a triangle is exactly the sum of the other two, the triangle is called **degenerate**, its vertices lie on the same line and it is completely "squashed".
- **Law of sines**: *a / sin(alpha) = b / sin(beta) = c / sin(gamma)*
- **Law of cosines** (generalization of Pythagorean theorem to any triangle, not just right one): *a^2 = b^2 + c^2 - 2 * b * c * cos(alpha)*.
- Triangle [tessellation](tessellation.md) is one of only three possible regular plane tilings (the other two being [square](square.md) and [hexagon](hexagon.md)).
- Every triangle has two special associated [circles](circle.md):
  - **[incircle](incircle.md)**: circle inside the triangle which touches each of its sides at one point, its center (incenter) lies on the intersection of all angle bisectors.
  - **[circumcircle](circumcircle.md)**: circle outside the triangle which touches each of its vertices, its center (circumcenter) lies on the perpendicular bisectors of each side.
- Triangle vertices always line in a single [plane](plane.md) (unlike other polygons).

In non [Euclidean](euclidean.md) ("crazy") geometries triangles behave weird, for example we can draw a triangle with three right angles on a surface of a [sphere](sphere.md) (i.e. its angles add to more than 180 degrees). This fact can be exploited by inhabitants of a space (e.g. our [Universe](universe.md)) to find out if they in fact live in a non Euclidean space (and possibly determine the space's exact [curvature](curvature.md)).

Constructing triangles: if we are to construct (draw) triangles with only partial knowledge of its parameters, we may exploit the above mentioned attributes to determine things we don't explicitly know. For example if we're told to construct a triangle with knowing only the lengths of the sides but not the angles, we can determine an angle of one side using the law of cosines at which point we can already draw all three vertices and just connect them. In other words just use your brain.

Triangles also play a big role e.g. in [realtime](realtime.md) [3D rendering](3d_rendering.md) where they are used as a basic building block of [3D models](3d_model.md), i.e. we [approximate](approximation.md) more complex shapes with triangles because triangles are simple (thanks to being a [simplex](simplex.md)) and so have nice properties such as always lying in a [plane](plane.md) so we cannot ever see both its front and back side at once. They are relatively easy to draw ([rasterize](rasterization.md)) so once we can draw triangles, we can also draw complex shapes composed of triangles. In general triangles, just as other simple shapes, can be used to [approximate](approximation.md) measures and attributes -- such as area or center of mass -- of more complex shapes, even outside computer graphics. For example to determine an area of some complex shape we approximate it by triangles, then sum up the areas of the triangles.

**[Barycentric coordinates](barycentric_coords.md)** provide a [coordinate](coordinate.md) system that can address specific points inside any triangle -- these are used e.g. in [computer graphics](graphics.md) for [texturing](texture.md). The coordinates are three numbers that always add up to 1, e.g. [0.25, 0.25, 0.5]. The coordinates can be though of as ratios of areas of the three subtriangles the point creates. Points inside the triangle have all three numbers positive. E.g. the coordinates of the vertices *A*, *B* and *C* are [1, 0, 0], [0, 1, 0] and [0, 0, 1], and the coordinates of the triangle center are [1/3, 1/3, 1/3].

**Winding** of the triangle says whether the ordered vertices of the triangle go clockwise or counterclockwise. I.e. winding says whether if we were to go in the direction *A -> B -> C* we'd be going clockwise or counterclockwise around the triangle center. This is important e.g. for [backface culling](backface_culling.md) in computer graphics (determining which side of a triangle in 3D we are looking at). Determining of the winding of triangle can be derived from the sign of the z-component of the [cross product](cross_product.md) of the triangle's sides. For the lazy: compute *w = (y1 - y0) * (x2 - x1) - (x1 - x0) * (y2 - y1)*, if *w > 0* the points go clockwise, if *w < 0* the points go counterclockwise, otherwise (*w = 0*) the points lie on a line.

[Sierpinski triangle](sierpinski_triangle.md) is a [fractal](fractal.md) related to triangles.

**Testing if point lies inside 2D triangle**: one way to do this is following. For each triangle side test whether the winding of the tested point and the side is the same as the winding of whole triangle -- if this doesn't hold for any side, the point is outside the triangle, otherwise it is inside. In order words for each side we are testing whether the tested point and the remaining triangle point are on the same side (in the same half plane). Here is a [C](c.md) code:

```
int pointIsInTriangle(int px, int py, int tp[6])
{
  // winding of the whole triangle:
  int w = (tp[3] - tp[1]) * (tp[4] - tp[2]) - (tp[2] - tp[0]) * (tp[5] - tp[3]);
  int sign = w > 0 ? 1 : (w < 0 ? -1 : 0);

  for (int i = 0; i < 3; ++i) // test winding of point with each side
  {
    int i1 = 2 * i;
    int i2 = i1 != 4 ? i1 + 2 : 0;

    int w2 = (tp[i1 + 1] - py) * (tp[i2] - tp[i1]) - (tp[i1] - px) * (tp[i2 + 1] - tp[i1 + 1]);
    int sign2 = w2 > 0 ? 1 : (w2 < 0 ? -1 : 0);
      
    if (sign * sign2 == -1) // includes edges
    //if (sign != sign2) // excludes edges
      return 0;
  }
    
  return 1;
}
```