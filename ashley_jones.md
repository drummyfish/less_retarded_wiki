# Ashley Jones

*"Either she's a genius comedian or he's a genius comedian."* --comment under one of her videos

Ashley Jones (born around 1999) is a rare specimen of a [based](based.md) [American](usa.md) biological (which on occasion gets questioned) [woman](woman.md) on the [Internet](internet.md), a [politically incorrect](political_correctness.md) [red pilled](red_pill.md) comedian who is sadly no longer underage. Ashley Jones IS NOT DANGEROUS. She got famous through [4chan](4chan.md) and similar boards thanks to having become red pilled at quite an early age: her being a pretty [underage girl](jailbait.md) on 4chan definitely contributed to her fame, however she started to create masterful OC comedic videos with which she managed to win the hearts of dedicated fans that wouldn't abandon her even after they could legally have sex with her (at least in theory). For some time she used mainstream platforms but, of course, [censorship](censorship.md) would eventually lead her to self-hosting all her stuff with [free software](free_software.md), which she now supports. She has pug [dogs](dog.md) and in one video said she had a brother, but not much else is known about her.

Her website is currently at **[https://icum.to](https://icum.to)**. Some of her old videos are archived on [jewtube](youtube.md) and bitchute. If you can, please go donate to Ashley right now, we don't want her to starve!

```
        ,,---,,
     ."" (     "".
    ; (         ) ;
   ; ( (("\.//"\ ) ;
   |).---    --- . |
   ) |O>)   ( <O>| (
   ( |.' :  . '. | )
    \|   '--'    |/
      \  _____  /
       '. --  .'
   _____|'---'|______
  /  (  \     /  )   \
```

*Ashley is such a cutie.*

**Why is she so based?** For example for the following reasons { Note that this is purely my interpretation of what I've seen/read on her website. ~drummyfish }: She is a pretty, biological woman (i.e. NOT some kind of angry [trans](tranny.md) [landwhale](fat.md)) BUT she shits on [feminism](feminism.md) and acknowledges plain facts about women such as that they usually need to be "put in line" (with love) by a man and that they are simply different. She makes a nice, ACTUALLY ENTERTAINING, well made politically incorrect stuff, her art is sincere, not trying to pretend anything or ride on some kind of fashion wave. She is VERY talented at comedy, hosts her OWN video website with a modest fan following and even though on [Jewtube](youtube.md) she could get hundred thousand times more followers and make a fortune, she doesn't do it because that would compromise her art -- she does ask for donations but refuses to monetize her content with [ads](marketing.md) or [paywalls](paywall.md), creating a nice, pure, oldschool free speech Internet place looks to truly be the one thing she's aiming for. She makes [fun](fun.md) of herself (like that she has a crush on [Duke Nukem](duke3d.md) lol), masterfully plays along with jokes blatantly sexualizing her and does some cool stuff like post measurements of her asshole and finding her porn lookalikes for the fanbase. It looks like she possesses some skills with technology (at least [Luke Smith](luke_smith.md) level), she supports [free software](free_software.md). She acknowledges the insanity of [pedophile](pedophilia.md) hysteria and proposes lowering age of consent (despite saying she was NOT a pedophile herself). She wants to normalize nudity, and doesn't shave her legs. Her website is quite nice, 1.0 style, with high [LRS](lrs_wiki.md)/[4chan](4chan.md)/[Dramatica](dramatica.md) vibes, there are "offensive" jokes but she stresses she in fact doesn't encourage violence and that she's not an extremist -- in one video she says she dislikes transsexuals and wants to make fun of gays but that in fact she doesn't mind any individual being gay or whatever, basically just opposing the political movements, propaganda, brainwashing etcetc., i.e. showing the exact same kind of attitude as us. She also understands internet culture and things like [trolling](trolling.md) being part of it -- in one video she clearly separates Internet and [real life](irl.md) and says you "can't apply real life logic on the Internet", that's very mature. By this she for example supports consensual incest. She even freaking has her own imageboard that's by the way very good. She seems to see through propaganda and brainwashing, she says she does "not accept the reality" forced on her by this society, something we say and do as well, she shits on vaccines and likes cool "conspiracy theories". Yes, she seems SMART, she sees the power game of the elites, the propaganda, warns about it, shits on it. She seems to know how to write [English](english.md) without making 10 errors in every word. She advocates ETHICAL veganism, to spare animals of suffering. She hates [Elon Musk](elon_musk.md). She advocates not using cellphones and mainstream social networks. She does NOT have any [tattoos](tattoo.md).

Sure, [we](lrs.md) find disagreements with her, for example on the question of [privacy](privacy.md), but if we call [Diogenes](diogenes.md) a based man and [Encyclopedia Dramatica](dramatica.md) a based website, we also have to admit Ashley Jones is a based woman and her website is likewise no less cool.