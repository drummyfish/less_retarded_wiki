# Chess

Chess (from Persian *shah*, *king*) is a very [old](old.md) two-player board [game](game.md), perhaps most famous and popular among all board games in [history](history.md). In video game terms we could call it a turn-based strategy, in mathematical terms it's a [zero sum](zero_sum.md), [complete information](complete_information.md) game with no element of [randomness](randomness.md), that simulates a battle of two armies on an 8x8 board with different battle pieces, also called *chessmen* or just *men* (also stones, pieces or juicers). Chess is also called the King's Game, it has a world-wide competitive community and is considered an intellectual [sport](sport.md) but it's also been a topic of research and [programming](programming.md) (many chess engines, [AI](ai.md)s and frontends are being actively developed). Chess is similar to games such [shogi](shogi.md) ("Japanese chess"), [xiangqi](xiangqi.md) ("Chinese chess") and [checkers](checkers.md). As the estimated number of chess games is bigger than [googol](googol.md), it is unlikely to ever be solved; though the complexity of the game in sheer number of possibilities is astronomical, among its shogi, go and xiangqi cousins it is actually considered one of the "simplest" (the board is relatively small and the game tends to simplify as it goes on as there are no rules to get men back to the game etc.).

{ There is a nice black and white indie movie called *Computer Chess* about chess programmers of the 1980s, it's pretty good, very oldschool, starring real programmers and chess players, check it out. ~drummyfish }

[Drummyfish](drummyfish.md) has created a suckless/[LRS](lrs.md) chess library [smallchesslib](smallchesslib.md) which includes a simple engine called *smolchess* (and also a small chess game in [SAF](saf.md) with said library).

**At [LRS](lrs.md) we consider chess to be one of the best games** for the following reasons:

- It is just a greatly interesting and deep game in which luck plays minimal role.
- **It is greatly [suckless](suckless.md)**, the rules are very simple, it can be implemented on simple 8bit computers. Of course the game doesn't even require a computer, just a board and a few men -- chess masters don't even need a board to play (they can completely visualize the games in memory). In the end one can in theory just play against himself in his head, achieving the ultimate freedom: the only dependency of the game is one's brain, i.e. it becomes a [brain software](brain_software.md). Chess is extremely inexpensive, doesn't discriminate against poor people and will survive even the most extreme technological [collapse](collapse.md).
- **No one owns chess**, the game is hundreds of years old and many books about it are also already in the [public domain](public_domain.md). It is extremely free.
- It is a basis for other derived games, for example many different chess variants or chess puzzles which can be considered a "singleplayer chess game".
- It is a source of many interesting [mathematical](math.md) and programming challenges.
- It seems to strike the right balance of simplicity and complexity, it is very simple but not so trivial as to be ever solved in a foreseeable future.

Many however see [go](go.md) as yet a more [beautiful](beauty.md) game: a more minimal, yet more difficult one, with a completely unique experience.

**Where to play chess online?** There exist many servers such as https://chess.com or https://chess24.com -- however these ones are proprietary, so don't use them. For us a possible one is **Lichess** (libre chess) at https://lichess.org which not only [FOSS](foss.md), but is also gratis (it also allows users to run bots under special accounts which is an amazing way of testing engines against people and other engines), however it requires JavaScript. Another server, a more [suckless](suckless.md) one, is **Free Internet Chess Server** (FICS) at https://www.freechess.org/ -- on this one you can play through telnet (`telnet freechess.org 5000`) or with graphical clients like pychess. Online servers usually rate players with Elo/Glicko just like FIDE, sometimes there are computer opponents available, chess puzzles, variants, analysis tools etc.

Chess as a game is not and cannot be [copyrighted](copyright.md), but **can chess games (moves played in a match) be copyrighted?** Thankfully there is a pretty strong consensus and precedence that say this is not the case, even though [capital worshippers](capitalism.md) try to play the intellectual property card from time to time (e.g. 2016 tournament organizers tried to stop chess websites from broadcasting the match moves under "trade secret protection", unsuccessfully).

**Chess and [IQ](iq.md)/intelligence**: there is a debate about how much of a weight general vs specialized intelligence, IQ, memory and pure practice have in becoming good at chess. It's not clear at all, everyone's opinion differs. A popular formula states that *highest achievable Elo = 1000 + 10 * IQ*, though its accuracy and validity are of course highly questionable. All in all this is probably very similar to language learning: obviously some kind of intelligence/talent is needed to excel, however chess is extremely similar to any other sport in that putting HUGE amounts of time and effort into practice (preferably from young age) is what really makes you good -- without practice even the biggest genius in the world will be easily beaten by a casual chess amateur, and even a relatively dumb man can learn chess very well under the right conditions (just like any dumbass can learn at least one language well); many highest level chess players admit they sucked at math and hated it. As one starts playing chess, he seems to more and more discover that it's really all about studying and practice more than anything else, at least up until the highest master levels where the genius gives a player the tiny nudge needed for the win -- at the grandmaster level intelligence seems to start to matter more. Intelligence is perhaps more of an accelerator of learning, not any hard limit on what can be achieved, however also just having fun and liking chess (which may be just given by upbringing etc.) may have similar accelerating effects on learning. Really the very basics can be learned by literally ANYONE, then it's just about learning TONS of concepts and principles (and automatizing them), be it tactical patterns (forks, pins, double check, discovery checks, sacrifices, smothered mates, ...), good habits, positional principles (pawn structure, king safety, square control, piece activity, ...), opening theory (this alone takes many years and can never end), endgame and mating patterns, time management etcetc.

**Fun fact**: chess used to be played over [telegraph](telegraph.md), first such game took place probably in 1844.

**How to play chess with yourself?** If you have no computer or humans to play against, you may try playing against yourself, however playing a single game against yourself doesn't really work, you know what the opponent is trying to do -- not that it's not interesting, but it's more of a search for general strategies in specific situations rather than actually playing a game. One way around this could be to play many games at once (you can use multiple boards but also just noting the positions on paper as you probably won't be able to set up 100 boards); every day you can make one move in some selected games -- randomize the order and games you play e.g. with dice rolls. The number of games along with the randomized order should make it difficult for you to remember what the opponent (you) was thinking on his turn. Of course you can record the games by noting the moves, but you may want to cover the moves (in which case you'll have to be keeping the whole positions noted) until the game is finished, so that you can't cheat by looking at the game history while playing. If this method doesn't work for you because you can keep up with all the games, at least you know got real good at chess :) { This is an idea I just got, I'm leaving it here as a note, haven't tried it yet. ~drummyfish }

## Chess In General

Chess evolved from ancient board games in India (most notably Chaturanga) in about 6th century -- some sources say that in chess predecessor games dice was used to determine which man a player was allowed to move but that once dice were banned because of hazard games, we got the variant without any element of chance. Nowadays the game is internationally governed by **FIDE** which has taken the on role of an authority that defines the official rules: FIDE rules are considered to be the standard chess rules. FIDE also organizes tournaments, promotes the game and keeps a list of registered players whose performance it rates with so called Elo system --⁠ based on the performance it also grants titles such as **Grandmaster** (GM, strongest), **Internation Master** (IM, second strongest) or **Candidate Master** (CM). A game of chess is so interesting in itself that chess is usually not played for money like many other games ([poker](poker.md), [backgammon](backgammon.md), ...).

The mastery of chess is often divided into two main areas (it is also common to divide strong players into these two categories depending on where their main strength lies):

- **positional play**: Long term, big picture patterns that offer many advantages and opportunities for playing good moves, trying to get a "good position" with men on strong squares, controlling key parts of the board, putting pressure on enemy, ensuring safety of own king etc.
- **tactical play**: Short term, quick action, tricks and calculation skills that win advantages, often material, with tools like forks, pins, discovery checks, sacrifices etc.

Of course this is not the only possible division, another one may be for example offensive vs defensive play etc., but generally chess revolves around position and tactics.

A single game of chess is seen as consisting of three stages: **opening** (starting, theoretical "book" moves, developing men), **middlegame** (seen as the pure core of the game) and **endgame** (ending in which only relatively few men remain on the board). There is no clear border between these stages and they are sometimes defined differently, however each stage plays a bit differently and may require different skills and strategies; for example in the endgame king becomes an active man while in the opening and middlegame he tries to stay hidden and safe.

The study of chess openings is called **opening theory** or just *theory*. Playing the opening stage is special by being based on memorization of this theory, i.e. hundreds or even thousands of existing opening lines that have been studied and analyzed by computers, rather than by performing mental calculation (logical "thinking ahead" present in middlegame and endgame). Some see this as weakness of chess that makes players spend extreme energy on pure memorization. One of the best and most famous players, Bobby Fischer, was of this opinion and has created a chess variant with randomized starting position that prevents such memorization, so called *chess 960*.

**[Elo](elo.md) rating** is a mathematical system of numerically rating the performance of players (it is used in many sports, not just chess). Given two players with Elo rating it is possible to compute the probability of the game's outcome (e.g. white has 70% chance of winning etc.). The FIDE set the parameters so that the rating is roughly this: < 1000: beginner, 1000-2000: intermediate, 2000-3000: master. More advanced systems have also been created, namely the Glicko system.

The rules of chess are quite simple ([easy to learn, hard to master](easy_to_learn_hard_to_master.md)) and can be found anywhere on the Internet. In short, the game is played on a 8x8 board by two players: one with **[white](white.md)** men, one with **[black](black.md)** (LOL IT'S [RACIST](racism.md) :D). Each man has a way of moving and capturing (eliminating) enemy men, for example bishops move diagonally while pawns move one square forward and take diagonally. The goal is to **checkmate** the opponent's king, i.e. make the king attacked by a man while giving him no way to escape this attack. There are also lesser known rules that noobs often miss and ignore, e.g. so called en-passant or the 50 move rule that declares a draw if there has been no significant move for 50 moves.

At the competitive level **clock** (so called *time control*) is used to give each player a limited time for making moves: with unlimited move time games would be painfully long and more a test of patience than skill. Clock can also nicely help balance unequal opponent by giving the stronger player less time to move. Based on the amount of time to move there exist several formats, most notably **correspondence** (slowest, days for a move), **classical** (slow, hours per game), **rapid** (faster, tens of minutes per game), **blitz** (fast, a few seconds per move) and **bullet** (fastest, units of seconds per move).

Currently the best player in the world is pretty clearly Magnus Carlsen from Norway with Elo rating 2800+.

During [covid](covid.md) chess has experienced a small boom among normies and [YouTube](youtube.md) chess channels have gained considerable popularity. This gave rise to [memes](meme.md) such as the bong cloud opening popularized by a top player and streamer Hikaru Nakamura; the bong cloud is an intentionally shitty opening that's supposed to taunt the opponent (it's been even played in serious tournaments lol).

**White is generally seen as having a slight advantage over black** (just like in [real life](irl.md) lol). It is because he always has the first move -- statistics also seems to support this as white on average wins a little more often. This doesn't play such as big role in beginner and intermediate games but starts to become apparent in master games. How big the advantages is is a matter of ongoing debate, most people are of the opinion there exists a slight advantage for the white (with imperfect play, i.e. that white plays easier, tolerates slightly less accurate play), though most experts think chess is a draw with perfect play (pro players can usually quite safely play for a draw and secure it if they don't intend to win; world championships mostly consist of drawn games as really one player has to make a mistake to allow the other one to win). Minority of experts think white has theoretical forced win. Probably only very tiny minority of people think white doesn't have any advantage. Some people argue black has some advantages over white, as it's true that sometimes the obligation to make a move may be a disadvantage. Probably no one thinks black has a forced win, though that's not disproved yet so maybe someone actually believes it.

On **perfect play**: as stated, chess is unlikely to be ever solved so it is unknown if chess is a theoretical forced draw or forced win for white (or even win for black), however many simplified endgames and some simpler chess variants have already been solved. Even if chess was ever solved, it is important to realize one thing: **perfect play may be unsuitable for humans** and so even if chess was ever solved, it might have no significant effect on the game played by humans. Imagine the following: we have a chess position in which we are deciding between move *A* and move *B*. We know that playing *A* leads to a very good position in which white has great advantage and easy play (many obvious good moves), however if black plays perfectly he can secure a draw here. We also know that if we play *B* and then play perfectly for the next 100 moves, we will win with mathematical certainty, but if we make just one incorrect move during those 100 moves, we will get to a decisively losing position. While computer will play move *B* here because it is sure it can play perfectly, it is probably better to play *A* for human because human is very likely to make mistakes (even a master). For this reason humans may willingly choose to play mathematically worse moves -- it is because a slightly worse move may lead to a safer and more comfortable play for a human.

Fun fact: there seem to be **almost no black people in [chess](chess.md)** :D the strongest one seems to be Pontus Carlsson which rates number 1618 in the world; even [women](woman.md) seem to be much better at chess than black people. But how about black women? [LMAO](lmao.md), it seems like there haven't even been any black female masters :'D The web is BLURRY on these facts, but there seems to be a huge excitement about one black female, called Rochelle Ballantyne, who at nearly 30 years old has been sweating for a decade to reach the lowest master rank (the one which the nasty oppressive white boys get at like 10 years old) and MAYBE SHE'LL DO IT, she seems to have with all her effort and support of the whole Earth overcome the 2000 rating, something that thousands of amateurs on the net just causally do every day without even trying too much. But of course, it's cause of the white male oppression =3 lel

## Chess And Computers

{ [This](https://www.youtube.com/watch?v=DpXy041BIlA) is an absolutely amazing video about weird chess algorithms :) ~drummyfish }

Chess is a big topic in computer science and programming, computers not only help people play chess, train their skills, analyze positions and perform research of games, but they also allow mathematical analysis of chess as such and provide a platform for things such as [artificial intelligence](ai.md).

Chess software is usually separated to **[libraries](library.md)**, **chess engines** and **[frontends](frontend.md)**. Chess engine is typically a [CLI](cli.md) program capable of playing chess but also doing other things such as evaluating arbitrary position, hinting best moves, saving and loading games etc. -- commonly the engine has some kind of custom CLI interface (flags, interactive commands it understands, ...) plus a support of some standardized text communication protocol, most notably XBoard (older one, more [KISS](kiss.md)) and UCI (newer, more [bloated](bloat.md)). There is also typically support for standardized formats such as FEN (way of encoding a chess position as a text string), PGN (way of encoding games as text strings) etc. Frontends on the other hand are usually [GUI](gui.md) programs (in this case also called *boards*) that help people interact with the underlying engine, however there may also be similar non-GUI programs of this type, e.g. those that automatically run tournaments of multiple engines.

Computers have already surpassed the best humans in their playing strength (we can't exactly compute an engine's [Elo](elo.md) as it depends on hardware used, but generally the strongest would rate high above 3000 FIDE). As of 2023 the strongest chess engine is widely agreed to be the [FOSS](foss.md) engine [Stockfish](stockfish.md), with other strong engines being e.g. Leela Chess Zero (also FOSS), AlphaZero ([proprietary](proprietary.md) by [Google](google.md)) or Komodo Dragon (proprietary). [GNU Chess](gnu_chess.md) is a pretty strong [free software](free_software.md) engine by [GNU](gnu.md). There are world championships for chess engines such as the *Top Chess Engine Championship* or *World Computer Chess Championship*. [CCRL](https://ccrl.chessdom.com/ccrl/4040/) is a list of chess engines along with their Elo ratings deduced from tournaments they run. Despite the immense strength of modern engines, there are still some specific artificial situations in which a human beats the computer (shown e.g. in [this](https://www.youtube.com/watch?v=R9IZWgArWUE) video); this probably won't last long though.

The first chess computer that beat the world champion (at the time Gary Kasparov) was famously [Deep Blue](deep_blue.md) in 1997. [Alan Turing](turing.md) himself has written a chess playing algorithm but at his time there were no computers to run it, so he executed it by hand -- nowadays the algorithm has been implemented on computers (there are bots playing this algorithm e.g. on lichess).

Playing strength is not the only possible measure of chess engine quality, of course -- for example there are people who try to make the **smallest chess programs** (see [countercomplex](countercomplex.md) and [golfing](golf.md)). As of 2022 the leading programmer of smallest chess programs seems to be Óscar Toledo G. (https://nanochess.org/chess.html). Unfortunately his programs are [proprietary](proprietary.md), even though their source code is public. The programs include Toledo Atomchess (392 [x86](x86.md) instructions), Toledo Nanochess (world's smallest [C](c.md) chess program, 1257 non-blank C characters) and Toledo Javascript chess (world's smallest [Javascript](javascript.md) chess program). He won the [IOCCC](ioccc.md). Another small chess program is micro-Max by H. G. Muller (https://home.hccnet.nl/h.g.muller/max-src2.html, 1433 C characters, Toledo claims it is weaker than his program). Other engines try to be strong while imitating human play (making human moves, even mistakes), most notably Maia which trains several neural networks that play like different rated human players.

{ Nanochess is actually pretty strong, in my testing it easily beat [smallchesslib](smallchesslib.md) Q_Q ~drummyfish }

### Programming Chess

NOTE: our [smallchesslib](smallchesslib.md)/smolchess engine is very simple, educational and can hopefully serve you as a nice study tool to start with :)

There is also a great online wiki focused on programming chess engines: https://www.chessprogramming.org.

Programming chess is a [fun](fun.md) and enriching experience and is therefore recommended as a good exercise. There is nothing more satisfying than writing a custom chess engine and then watching it play on its own.

The core of chess programming is writing the [AI](ai.md). Everything else, i.e. implementing the rules, communication protocols etc., is pretty straightforward (but still a good programming exercise). Nevertheless, as the chess programming wiki stresses, one has to pay a great attention to eliminating as many [bugs](bug.md) as possible; really, the importance of writing automatic tests can't be stressed enough as debugging the AI will be hard enough and can become unmanageable with small bugs creeping in. Though has to go into choosing right [data structures](data_structure.md) so as to allow nice [optimizations](optimization.md), for example board representation plays an important role (two main approaches are a 64x64 2D array holding each square's piece vs keeping a list of pieces, each one recording its position).

The AI itself works traditionally on the following principle: firstly we implement so called static **evaluation function** -- a function that takes a chess position and outputs its evaluation number which says how good the position is for white vs black (positive number favoring white, negative black, zero meaning equal, units usually being in pawns, i.e. for example -3.5 means black has an advantage equivalent to having extra 3 and a half pawns; to avoid fractions we sometimes use centipawns, i.e. rather -350). This function considers a number of factors such as total material of both players, pawn structure, king safety, men mobility and so on. Traditionally this function has been hand-written, nowadays it is being replaced by a learned [neural network](neural_network.md) ([NNUE](nnue.md)) which showed to give superior results (e.g. Stockfish still offers both options); for starters you probably want to write a simple evaluation function manually.

Note: if you could make a perfect evaluation function that would completely accurately state given position's true evaluation (considering all possible combinations of moves until the end of game), you'd basically be done right there as your AI could just always make a move that would lead to position which your evaluation function rated best, which would lead to perfect play. Though neural networks got a lot closer to this ideal than we once were, as far as we can foresee ANY evaluation function will always be just an approximation, an estimation, heuristic, many times far away from perfect evaluation, so we cannot stop at this. We have to program yet something more.

So secondly we need to implement a so called **search** algorithm -- typically some modification of [minimax](minimax.md) algorithm, e.g. with alpha-beta pruning -- that recursively searches the game tree and looks for a move that will lead to the best result in the future, i.e. to position for which the evaluation function gives the best value. This basic principle, especially the search part, can get very complex as there are many possible weaknesses and optimizations. Note now that this search kind of improves on the basic static evaluation function by making it [dynamic](dynamic.md) and so increases its accuracy greatly (of course for the price of CPU time spent on searching).

Exhaustively searching the tree to great depths is not possible even with most powerful hardware due to astronomical numbers of possible move combinations, so the engine has to limit the depth quite greatly and use various [hacks](hacking.md), [approximations](approximation.md), [heuristics](heuristic.md) etc.. Normally it will search all moves to a small depth (e.g. 2 or 3 half moves or *plys*) and then extend the search for interesting moves such as exchanges or checks. Maybe the greatest danger of searching algorithms is so called **horizon effect** which has to be addressed somehow (e.g. by detecting quiet positions, so called *quiescence*). If not addressed, the horizon effect will make an engine misevaluate certain moves by stopping the evaluation at certain depth even if the played out situation would continue and lead to a vastly different result (imagine e.g. a queen taking a pawn which is guarded by another pawn; if the engine stops evaluating after the pawn take, it will think it's a won pawn, when in fact it's a lost queen). There are also many techniques for reducing the number of searched tree nodes and speeding up the search, for example pruning methods such as **alpha-beta** (which subsequently works best with correctly ordering moves to search), or **transposition tables** (remembering already evaluated position so that they don't have to be evaluated again when encountered by a different path in the tree).

**Alternative approaches**: most engines work as described above (search plus evaluation function) with some minor or bigger modifications. The simplest possible stupid AI can just make random moves, which will of course be an extremely weak opponent (though even weaker can be made, but these will actually require more complex code as to play worse than random moves requires some understanding and searching for the worst moves) -- one might perhaps try to just program a few simple rules to make it a bit less stupid and possibly a simple training opponent for complete beginners: the AI may for example pick a few "good looking" candidate moves that are "usually OK" (pushing a pawn, taking a higher value piece, castling, ...) and aren't a complete insanity, then pick one at random only from those (this randomness can further be improved and gradually controlled by scoring the moves somehow and adding a more or less random value from some range to each score, then picking the moves with highest score). One could also try to just program in a few generic rules such as: checkmate if you can, otherwise take an unprotected piece, otherwise protect your own unprotected piece etc. -- this could produce some beginner level bot. Another idea might be a "Chinese room" bot that doesn't really understand chess but has a huge database of games (which it may even be fetching from some Internet database) and then just looking up what moves good players make in positions that arise on the board, however a database of all positions will never exist, so in case the position is not found there has to be some fallback (e.g. play random move, or somehow find the "most similar position" and use that, ...). As another approach one may try to use some **non neural network [machine learning](machine_learning.md)**, for example [genetic programming](genetic_programming.md), to train the evaluation function, which will then be used in the tree search. Another idea that's being tried (e.g. in the Maia engine) is **pure neural net AI** (or another form of machine learning) which doesn't use any tree search -- not using search at all has long been thought to be impossible as analyzing a chess position completely statically without any "looking ahead" is extremely difficult, however new neural networks have shown to be extremely good at this kind of thing and pure NN AIs can now play on a master level (a human grandmaster playing ultra bullet is also just a no-calculation, pure pattern recognition play). Next, **[Monte Carlo](monte_carlo.md) tree search** (MCTS) is an alternative way of searching the game tree which may even work without any evaluation function: in it one makes many random playouts (complete games until the end making only random moves) for each checked move and based on the number of wins/losses/draws in those playouts statistically a value is assigned to the move -- the idea is that a move that most often leads to a win is likely the best. Another Monte Carlo approach may just make random playouts, stop at random depth and then use normal static evaluation function (horizon effect is a danger but hopefully its significance should get minimized in the averaging). However MCTS is pretty tricky to do well. MCTS is used e.g. in Komodo Dragon, the engine that's currently among the best. Another approach may lie in somehow using several methods and [heuristics](heuristic.md) to vote on which move would be best.

Many other aspects come into the AI design such as opening books (databases of best opening moves), endgame tablebases (precomputed databases of winning moves in simple endgames), clock management, pondering (thinking on opponent's move), learning from played games etc. For details see the above linked chess programming wiki.

### Notable Chess Engines/Computers/Entities

*See also ratings of computer engines at https://www.computerchess.org.uk/ccrl/4040/.*

Here are some notable chess engines/computers/entities, as of 2024:

- **[Stockfish](stockfish.md)** (SF): FOSS engine (written in [C++](cpp.md)), without any doubt **the strongest chess engine** that's been reliably winning all the computer tournaments for years now; its strength is far beyond any human, even if run on quite a weak device -- it actually caused some trouble because it's extremely easy to just download onto a cellphone and [cheat](cheating.md) even in OTB tournaments. Currently the engine is using a [neural network](neural_network.md) for evaluating positions but still also uses the tree search algorithm (a greatly optimized one so that it searches gigantic numbers of positions per second). Important part of the development is so called *Fishtest*, a distributed framework for testing and improving the engine's performance, it's one of the reasons why it good so strong. Stockfish's current CCRL Elo rating is 3639 (warning: this is incomparable to human Elos).
- **Magnus Carlsen**: Human, most likely the strongest player ever, has been quite comfortably winning every tournament he entered including the world championship until he quit, basically because he got "bored". His top FIDE Elo was 2882.
- **Komodo Dragon**: [Proprietary](proprietary.md), currently seems to be the second strongest engine, it's main feature is [Monte Carlo] ("randomized") search algorithm. Current CCRL Elo is 3624.
- **[Leela Chess Zero](lc0)** (lc0): FOSS engine (written in C++), among top strongest engines (currently top 50 on CCRL), it is interesting mainly for how it works: it is a neural network engine that's **completely self-taught** from the ground up, i.e. it didn't learn chess by watching anyone else play, it was only allowed to learn by playing against itself. Current CCRL Elo is 3441.
- **[Deep Blue](deep_blue.md)**: A historically famous supercomputer, the first one to have beaten the human world chess champion in 1997.
- **[GNU chess](gnu_chess.md)** Free engine by [GNU](gnu.md), not among absolute top by strength but still very strong. Current CCRL Elo is 2825.
- **Maia**: FOSS engine, or rather neural network, notable by not trying to be the strongest, but rather most human-like, i.e. tries to imitate human play, even with errors. There are several versions, each trained for different strength. It is also notable by using pure neural network, i.e. it doesn't perform any search, it's a pure "pattern recognition"/static engine that still manages to play quite well.
- **Toledo Nanochess**: Seems to be the world's smallest [C](c.md) chess engine, with only 1257 non-blank characters of source code.
- **[smallchesslib](smallchesslib.md)/smolchess**: Tiny LRS [C](c.md) library/engine, very weak but is very simple, small and portable, may be [good enough](good_enough.md) in many situations.
- **Chessmaster**: A famous proprietary chess video games with its own engine, it was strong for a video game of its time (around 2000 Elo) but nowadays would be considered rather weak for an engine -- its significance is cultural, it's used for comparisons, many people played against it and still use it to test their engines against.
- **Turochamp**: Probably the first chess program ever, made by David Champernowne and [Alan Turing](turing.md) himself in 1948, in times when computers still couldn't execute it! It was very primitive, looking only two moves ahead, and was only ever executed manually -- of course, it got raped pretty bad the human opponent.
- ...

## Stats And Records

Chess stats are pretty [interesting](interesting.md).

{ Some chess world records are here: https://timkr.home.xs4all.nl/records/records.htm. ~drummyfish }

**Number of possible games** is not known exactly, Shannon estimated it at 10^120 (lower bound, known as *Shannon number*). Number of possible games by plies played is 20 after 1, 400 after 2, 8902 after 3, 197281 after 4, 4865609 after 5, and 2015099950053364471960 after 15.

Similarly the **number of possibly reachable positions** (position for which so called *proof game* exists) is not known exactly, it is estimated to at least 10^40 and 10^50 at most. Numbers of possible positions by plies is 20 after 1, 400 after 2, 5362 after 3, 72078 after 4, 822518 after 5, and 726155461002 after 11.

**Shortest possible checkmate** is by black on ply number 4 (so called *fool's mate*). As of 2022 the **longest known forced checkmate** is in 549 moves -- it has been discovered when computing the Lomonosov Tablebases.

Average game of chess lasts 40 (full) moves (80 plies). **Average [branching factor](branching_factor.md)** (number of possible moves at a time) is around 33. **Maximum number of possible moves in a position** seems to be 218 (FEN: `R6R/3Q4/1Q4Q1/4Q3/2Q4Q/Q4Q2/pp1Q4/kBNN1KB1 w - - 0 1`).

White wins about 38% of games, black wins about 34%, the remaining 28% are draws (38.7%, 31.1%, 30.3% respectively on Computer Chess Rating Lists).

What is the **longest possible game**? It depends on the exact rules and details we set, for example if a 50 move rule applies, a player MAY claim a draw but also doesn't have to -- but if neither player ever claims a draw, a game can be played infinitely -- so we have to address details such as this. Nevertheless the longest possible chess game upon certain rules has been computed by [Tom7](tom7.md) at 17697 half moves in a paper for [SIGBOVIK](sigbovik.md) 2020. Chess programming wiki states 11798 half moves as the maximum length of a chess game which considers a 50 move rule (1966 publication).

The **longest game played in practice** is considered to be the one between Nikolic and Arsovic from 1989, a draw with 269 moves lasting over 20 hours. For a shortest game there have been ones with zero moves; serious decisive shortest game has occurred multiple times like this: `1.d4 Nf6 2.Bg5 c6 3.e3 Qa5+` (white resigned).

**Best players ever**: a 2017 paper called *Who is the Master?* analyzed 20 of the top players of history based on how good their moves were compared to Stockfish, the strongest engine. The resulting **top 10** was (from best): Carlsen (born 1990 Norway, peak Elo 2882), Kramnik (born 1975 Russia, peak Elo 2817), Fischer (born 1943 USA, peak Elo 2785), Kasparov (born 1963 Russia, peak Elo 2851), Anand (born 1969 India, peak Elo 2817), Khalifman, Smyslov, Petrosian, Karpov, Kasimdzhanov. It also confirmed that the quality of chess play at top level has been greatly increasing. The **best [woman](woman.md) player** in history is considered to be Judit Polgar (born 1976 Hungary, peak Elo 2735), which still only managed to reach some 49th place in the world; by Elo she is followed by Hou Yifan (born 1994 China, peak Elo 2686) and Koneru Humpy (born 1987 India, peak Elo 2623). **Strongest players of black [race](race.md)** (NOT including brown, e.g. India): lol there don't seem to be many black players in chess :D The first black GM only appeared in 1999 (!!!) -- Maurice Ashley (born 1966 Jamaica, peak rating 2504) who is also probably the most famous black chess player, though more because of his commentator skills; Pontus Carlsson (peak Elo 2531) may be strongest. { Sorry if I'm wrong about the strongest black player, this information is pretty hard to find as of course you won't find a race record in any chess player database. So thanks to political correctness we just can't easily find good black players. ~drummyfish }

What's **the most typical game**? We can try to construct such a game from a game database by always picking the most common move in given position. Using the lichess database at the time of writing, we get the following incomplete game (the remainder of the game is split between four games, 2 won by white, 1 by black, 1 drawn):

```
1. e4 e5 2. Nf3 Nc6 3. Bc4 Bc5 4. c3 Nf6 5. d4 exd4
6. cxd4 Bb4+ 7. Nc3 Nxe4 8. O-O Bxc3 9. d5 Bf6 10. Re1 Ne7
11. Rxe4 d6 12. Bg5 Bxg5 13. Nxg5 h6 14. Qe2 hxg5
15. Re1 Be6 16. dxe6 f6 17. Re3 c6 18. Rh3 Rxh3
19. gxh3 g6 20. Qf3 Qa5 21. Rd1 Qf5 22. Qb3 O-O-O
23. Qa3 Qc5 24. Qb3 d5 25. Bf1
```

You can try to derive your own stats, there are huge free game databases such as the Lichess [CC0](cc0.md) database of billions of games from their server.

{ TODO: Derive stats about the best move, i.e. for example "best move is usually by queen by three squares" or something like that. Could this actually help the play somehow? Maybe could be used for move ordering in alpha-beta. ~drummyfish }

## Rules

The exact rules of chess and their scope may depend on situation, this is just a sum up of rules generally used nowadays.

The start setup of a chessboard is following (lowercase letters are for black men, uppercase for white men, on a board with colored squares A1 is black):

```
        _______________
    /8 |r n b q k b n r|
 r | 7 |p p p p p p p p|
 a | 6 |. . . . . . . .|
 n | 5 |. . . . . . . .|
 k | 4 |. . . . . . . .|
 s | 3 |. . . . . . . .|
   | 2 |P P P P P P P P|
    \1 |R N B Q K B N R|
        """""""""""""""
        A B C D E F G H
        \_____________/
             files
```

Players take turns in making moves, white always starts. A move consists of moving one (or in special cases two) of own men from one square to another, possibly capturing (removing from the board) one opponent's man -- except for a special *en passant* move capturing always happens by moving one man to the square occupied by the opposite color man (which gets removed). Of course no man can move to a square occupied by another man of the same color. A move can NOT be skipped. A player wins by giving a **checkmate** to the opponent (making his king unable to escape attack) or if the opponent resigns. If a player is to move but has no valid moves, the game is a draw, so called **stalemate**. If neither player has enough men to give a checkmate, the game is a draw, so called **dead position**. There are additional situation in which game can be drawn (threefold repetition of position, 50 move rule). Players can also agree to a draw. A player may also be declared a loser if he cheated, if he lost on time in a game with clock etc.

The individual men and their movement rules are (no man can move beyond another, except for knight who jumps over other men):

| man            | symbol | ~value | movement                                                           |     comment           |
| -------------- | -------| ------ | ------------------------------------------------------------------ | --------------------- |
| pawn           | P      | 1      |1F, may also 2F from start, captures 1F1L or 1F1R, also *en passant*| promotes on last row  |
| knight         | N      | 3      |L-shape (2U1L, 2U1R, 2R1U, 2R1D, 2D1R, 2D1L, 2L1U, 2L1D), jumps over|                       |
| bishop         | B      | 3.25   | any distance diagonally                                            |stays on same color sq.|
| rook           | R      | 5      | any distance orthogonally (U, R, D or L)                           |can reach all sq.      |
| queen          | Q      | 9      | like both bishop and rook                                          |strongest piece        |
| king           | K      | inf    | any of 8 neighboring squares                                       |                       |

{ Cool players call knights *horses* or *ponies* and pawns *peasants*, rook may be called a *tower* and bishop a *sniper* as he often just sits on the main diagonal and shoot pieces that wonder through. Also pronounce *en passant* as "en peasant". Nakamura just calls all pieces a *juicer*. ~drummyfish }

**Check**: If the player's king is attacked, i.e. it is immediately possible for an enemy men to capture the king, the player is said to be in check. A player in check has to make such a move as to not be in check after that move.

A player cannot make a move that would leave him in check!

**Castling**: If a player hasn't castled yet and his king hasn't been moved yet and his kingside (queenside) rook hasn't been moved yet and there are no men between the king and the kingside (queenside) and the king isn't and wouldn't be in check on his square or any square he will pass through or land on during castling, short (long) castling can be performed. In short (long) castling the king moves two squares towards the kingside (queenside) rook and the rook jumps over the king to the square immediately on the other side of the king.

**Promotion**: If a pawn reaches the 1st or 8th rank, it is promoted, i.e. it has to be switched for either queen, rook, bishop or knight of the same color.

**Checkmate**: If a player is in check but cannot make any move to get out of it, he is checkmated and lost.

**En passant**: If a pawn moves 2 squares forward (from the start position), in the immediate next move the opponent can take it with a pawn in the same way as if it only moved 1 square forward (the only case in which a men captures another man by landing on an empty square).

Threefold repetition is a rule allowing a player to claim a draw if the same position (men positions, player's turn, castling rights, en passant state) occurs three times (not necessarily consecutively). The 50 move rule allows a player to claim a draw if no pawn has moved and no man has been captured in last 50 moves (both players making their move counts as a single move here).

### Variants

Besides similar games such as [shogi](shogi.md) there are many variants of chess, i.e. slight modifications of rules, foremost worth mentioning is for example chess 960. The following is a list of some variants:

- **antichess** ([suicide](suicide.md), ...): The goal is to lose all men or get stalemated, rules are a bit changed, e.g. castling and checks are removed and taking is forced.
- **chess 960** aka **Fischer's random**: Starting position is randomly modified by shuffling the non-pawn rows (with these rules: king must be between rooks, bishops on opposite colors and black/white's positions are mirrored). The rules are the same with a slight modification to castling. This was invented by Bobby Fischer to emphasize pure chess skill as opposed to memorizing the best opening moves, he saw the opening theory as harmful to chess. Chess 960 is nowadays even advocated by some to become the "main" version of chess.
- **[chess boxing](chess_boxing.md)**: Chess combined with box, players switch between the two games, one wins either by checkmate or knockout.
- **crazyhouse**: When a player captures a man, it goes into his reserve. From the reserve a man can be dropped (as a man of the current player's color) to an empty square instead of making a normal move. This is a rule taken from [shogi](shogi.md).
- **different men**: Some variants use different men, e.g. empress (moves like rook and knight) or amazon (queen/knight).
- **duck chess**: After each move players place a duck on an empty square, the duck blocks the square. The duck cannot be left on the same square, it has to be moved. There are no checks, players win by capturing the king.
- **fog of war**: Makes chess an incomplete-information game by allowing players to only see squares they can immediately move to (this is similarly to some strategy video games).
- **horde chess**: Asymmetric starting position: large number of black pawns vs a white army of traditional men. Rules are slightly modified, e.g. black can only be defeated by having all pawns captured (there is no black king).
- **[infinite](infinity.md) chess**: Infinite chessboard. { Huge rabbithole with things like "mate in [omega](omega.md)" etc. ~drummyfish }
- **minichess**: Smaller chessboard, e.g. 4x4, 4x8 etc. Los Alamos chess is played at 6x6 board without bishops (also no promotion to bishop, no pawn double step, no en passant, no castling). Some are already solved (e.g. 3x3).
- **more players**: E.g. 3 man chess or 4 player chess allow more than two players to play, some use different boards.
- **old chess**: The rules of chess itself have been changing over time (e.g. adding the 50 move rule etc.). The older rule sets can be seen as variants as well.
- **puzzle**: For single player, chess positions are presented and the player has to find the best move or sequence of moves.
- **racing kings**: The starting position has both players on the same side, the goal is to get one's king to the other side first.
- **different board geometries/topologies:** e.g. [non-Euclidean](non_euclidean.md) ([hyperbolic](hyperbolic.md), [spherical](spherical.md), [torus](torus.md), ...), hexagonal chess (had some considerable following) etc.
- **3D chess**: [3D](3d.md) generalization of chess, possible are also other dimensions (4D, 5D, ... maybe even 1D?).
- **randomly chosen variant**: Here a chess variant to be played is chosen at random before the game, e.g. by dice roll. { This is an idea I got, not sure if this exists or has a different name. ~drummyfish }

## Playing Tips

Some general tips and rules of thumb, mostly for beginners:

- Try to control the center of the board (D4, D5, E4, E5).
- Don't bring the queen out too early, the opponent can harass it and get ahead in development.
- Learn some universal setup openings or "systems" to play, e.g. London, King's Indian, the hippo etc.
- Develop your men before attacking, usually knights go out before bishops, bishops are well placed on the longest diagonals as "snipers".
- Learn basic tactics, especially **forks** (attacking two or more men at once so that one of them cannot escape capture) and **pins** (attack one man so that if he moves out of the way he will expose another one to be captured).
- King safety is extremely important until endgame, castle very early but not extremely early. In the endgame (with queens out) king joins the battle as another active man.
- Pawn structure is very important.
- Watch out for back rank checkmates, make an escape square for your king.
- Rooks want to be on open files, you also want to CONNECT them (have both guard each other). Also a rook in the opponents second row (2nd/7th rank) is pretty good.
- Bishops are generally seen a bit more valuable than knights, especially in pairs -- if you can trade your knight for opponent's bishop, it's often good. If your opponent has two bishops and you only have one, you want to trade yours for his so he doesn't have the pair. A knight pair is also pretty powerful though, especially when the knights are guarding each other.
- "Knight on a rim is dim" (knights are best placed near the center).
- An extremely strong formation is both rooks and the queen on the same open file.
- Blocking the opponents man so that it can't move is almost as good as taking it. And vice versa: you want to activate all your men if possible.
- Nubs are weak against long range bishops, they can't see them. Place a bishop to corner on the long diagonal and just snipe the opponent's material.
- Don't play "hope chess", always suppose your opponent will play the best move he can.
- If you can achieve something with multiple men, usually it's best to do it with the weakest one.
- TODO: moar

## How To Disrespect Your Opponent And Other Lulz In Chess

*see also [unsportmanship](unsportmanship.md)*

WORK IN PROGRESS, pls send me more tips :)

- OTB (over the board) only:
  - Turn your knights to face backwards or in another weird way (always face the opponent's king etc.). Also place the pieces unevenly on the squares to piss off opponents with OCD and autism.
  - Behave weird, make weird faces, walk extremely far away from the board and walk in circles (or just get up and stand up directly behind your opponent in a completely upright position staring into the distance without moving at all like a robot lol), constantly sneeze (try to sneeze every time the opponent touches a piece), make very long unbroken eye contact with the opponent while smiling as if you know what he's thinking, call the referee constantly, go to the toilet after every move, pretend to fall asleep from boredom etc. Overeat on beans before the game so you fart a lot and always try to fart as loud as possible. Wear nice clothes but right before the game go sweat to the gym so that you smell like a pig and distract the opponent with toxic fume. If you're a [wimmin](woman.md) behave sexually, keep grabbing your boobs, lick your lips and opponent's captured pieces and silently moan sometimes as if you're having an orgasm, pretend to masturbate under the table; if your opponent is male he is almost definitely smarter than you, you gotta use your woman weapons, but it will probably work easily on the chess virgins.
  - In a tournament change play based on opponent's [race](race.md) or sex, for example play only one opening against white people and another opening against black people, see if anyone notices the pattern :D
  - Outside tournament take advantage of the fact that you can do whatever the fuck you want: have one hand constantly on the clock and play with the other hand (considered rude and often forbidden), touch and knock over your opponent's pieces, take back your moves, ... and of course when you're losing, "accidentally" knock over the whole board and be like "oops, let's consider it a draw then" :D
  - Trash talk the referee.
  - Correct the opponent's pronunciation of *en passant*, insist it's pronounced "en peasant".
  - ...
- online only:
  - Be annoying and offensive in chat, if opponent blunders write `gg`, spam `ez` when you win. If he wins say it was a shit game and accuse him of [cheating](cheating.md).
  - Constantly ask for takebacks, offer draws, report legit opponents for cheating and offensive behavior.
  - ...
- Play the bongcloud, fool's mate, 1. h3 or similar offensive opening, especially against a stronger player. Offer a draw after 1st move. Just play knight F3 and back constantly. Castle manually even if you don't have to. Play the exact mirror of opponent's moves -- if he tries to break it then just always try to get back to mirrored position.
- When losing constantly offer draws, prolong the game AS MUCH AS POSSIBLE, before the very last move just let the clock run out.
- Repeatedly try to make swastikas on the board, especially against colored opponents.
- Underpromote pawns e.g. to knights or bishops.
- When playing a noob, don't just mate him but absolutely rape him, promote all pawns to knights before winning, then say you didn't even have to try and that he should look into another game as chess is clearly not his game.
- Look up chess etiquette and do the exact opposite of what it says.
- ...

## LRS Chess

{ Has someone already made this tho? Seems like a pretty obvious simplification to make. ~drummyfish }

Chess is only mildly [bloated](bloat.md) but what if we try to unbloat it completely? Here we propose the LRS version of chess. The rule changes against normal chess are:

- No castling.
- No en passant.
- Promotion is always to queen.
- No checks or checkmates, king is just another man.
- Whoever takes the opponent's king first wins.
- If a player has no available moves, he loses.
- Only a single draw rule: if game doesn't end in 1024 half moves or fewer, it is a draw. I.e. there are no weird draw rules (50 move, repetition, ...). Of course players may still agree on draw anytime.
- Random: optionally random variant of LRS chess can be played. Here we randomly shuffle the white player's back row men in the starting position and mirror it for black (no weird conditions on men positions like in chess 960).

## See Also

- [shogi](shogi.md)
- [go](go.md)
- [hexapawn](hexapawn.md)
- [hex game](hex_game.md)
- [checkers](checkers.md)
- [advance wars](advance_wars.md)
- [backgammon](backgammon.md)
- [Catan](catan.md)
- [Deep Blue](deep_blue.md)
- [stockfish](stockfish.md)
- [anal bead](anal_bead.md)
