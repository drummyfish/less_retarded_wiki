# Freedom Distance

*The main article about this is found in a section of [free software](free_software.md) article.*

Tl;dr: Freedom distance is the average minimum distance to someone who can practically exercise ALL legally defined freedoms (such as the four essential freedoms of [free software](free_software.md)) or similarly satisfy some legal conditions; it is a metric that can be applied to measuring PRACTICAL effects of legal definitions.