# Coding

*Not to be [confused](often_confused.md) with [programming](programming.md).*

Coding nowadays means low quality attempt at [programming](programming.md), usually practiced by [soydevs](soydev.md) and barely qualified coding monkeys. Coder is to programmer what a bricklayer is to architect. Coding is to programming what building houses in Minetest is to building real houses.

Traditionally coding meant encoding and decoding of information as in e.g. video coding -- this is the only non-[gay](gay.md) meaning of the word