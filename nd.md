# ND

*See also [NC](nc.md).*

In the context of [licenses](license.md) the acronym ND stands for *no derivatives* and means "no derivative works allowed" -- this is an unpopular limitation that makes such a license **by definition [proprietary](proprietary.md) (i.e. NOT a [free cultural](free_culture.md) license)**. A work licensed under a license with ND clause -- most notably the [CC BY-NC-ND](cc_by_nc_nd.md) license -- prohibits anyone from making derivative (i.e. modified) works from the original work, on grounds of [copyright](copyright.md), which goes against one of the very fundamental ideas of [free culture](free_culture.md) (and just any sane culture), that of free [remixing](remix.md), improvement, combining and reuse of [art](art.md); it kills artistic freedom, [culture](culture.md), opens the door to legal bullying and strengthens the [harmful](harmful.md) [capitalist](capitalism.md) idea of "[intellectual property](intellectual_property.md)". All in all **ND licenses are [cancer](cancer.md), NEVER USE THEM**.

The ND clause is similarly harmful to the [NC](nc.md) (non-commercial-only) clause -- see the NC article for more detail.