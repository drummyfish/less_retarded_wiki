# CSS

{ Check out our cool CSS styles in the wiki consoomer edition. ~drummyfish }

Cascading Style Sheets (CSS, *cascading* because of the possible style [hierarchy](hierarchy.md)) is a [computer](computer.md) [language](language.md) for styling documents (i.e. defining their visual appearance), used mainly on the [web](www.md) for giving websites ([HTML](html.md) documents) their look. The language is standardized by [W3C](w3c.md) (the consortium established for making such standards). CSS is NOT a [programming language](programming_language.md), it's merely a language that defines attributes of visual presentation such as "headings should use this font" or "background should have this color"; it is one of the three main languages a website is written in: [HTML](html.md) (for writing the document), CSS (for giving the document a specific look) and [JavaScript](js.md) ([programming language](programming_language.md) for the website's scripts). As of 2024 the latest CSS specification is version 2.1 from 2016, version 3 is being worked on.

A website is not required to have a CSS style, without it it will just have the plain default look (which is mostly [good enough](good_enough.md) for communicating any [information](information.md), but won't impress normies), though only boomers and hardcore [minimalists](minimalism.md) nowadays have websites without any CSS at all (and we applaud them for such [minimalism](minimalism.md)). Similarly a single HTML website may use several styles or allow switching between them -- this is thanks to the fact that the style is completely separate from the underlying document (you can in theory take any document's style and apply it to any other document) AND thanks to the overriding rules that say which style will take precedence over which (based on which one is more specific etc.) -- using multiple style sheets at once creates the "cascades" the name refers to. In theory a web browser may even allow the user to apply his own CSS style to given website (e.g. a half blind guy may apply style with big font, someone reading in dark will apply "dark mode" style and so on), though for some reason browsers don't really do this (well, it seems like the original intent of being able to do good things like this was reworked by capitalists that rather see CSS more as a tool to apply more marketing styling and, of course, a capitalist won't want the user to change how his site looks because he might for example hide ads or annoying flashing buttons the capitalist paid hard money for).

Back in the boomer web days -- basically before the glorious year 2000 -- there was no CSS. Well, it was around, but support was poor and no one used it (or needed it for that matter). People cared more for sharing [information](information.md) than pimping muh graphics. Sometimes people needed to control the look of their website to some degree though, for example in an image gallery it's good to have thumbnail sizes the same, so HTML itself included some simple things to manipulate the looks (e.g. the `width` property in the `img` tag). People also did hacks such as raping tables or spamming the `<br />` tags or using [ASCII art](ascii_art.md) to somehow force displaying something how they wanted it. However as [corporations](corporation.md) started to invade the web, they naturally wanted more [consumerism](consumerism.md), flashing lights and brainwas... ummm... [marketing](marketing.md). They wanted to redefine the web from "collection of interlinked documents" or a "global database" to something more like "virtual billboard space" or maybe "gigantic electronic shopping center", which indeed they did. So they supported more work on CSS, more browsers started to support it and normies with blogs jumped on the train too, so CSS just became standard. On one hand CSS allows nice things, you can restyle your whole website with a single line change, but it is still [bloat](bloat.md), so beware, use it wisely (or rather don't use it -- you can never go wrong with that).

**Correct, [LRS](lrs.md) approved attitude towards this piece of [bloat](bloat.md)**: as a minimalist should you avoid CSS like the devil and never use it? Usual LRS recommendations apply but, just in case, let's reiterate. Use your brain, maximize [good](less_retarded_society.md), minimize damage, just make it so that no one can ever say "oh no, I wish this site didn't have CSS". You CAN use CSS on your site, but it mustn't become any burden, only something optional that will make life better for those using a browser supporting CSS, i.e. **your site MUSTN'T RELY on CSS**, CSS mustn't be its [dependency](dependency.md), the site has to work perfectly fine without it (remember that many browsers, especially the minimalist ones not under any corporation's control, don't even support CSS), the site must not be crippled without a style, i.e. firstly design your site without CSS and only add CSS as an optional improvement. Do not make your HTML bow to CSS, i.e. don't let CSS make you add tons of divs and classes, make HTML first and then make CSS bow to the HTML. Light CSS is better than heavy one. If you have a single page, embed CSS right into it ([KISS](kiss.md), site is self contained and browser doesn't have to download extra files for your site) and make it short to save bandwidth on downloading your site. Don't use heavy CSS features like animation, blurs, [color](color.md) transitions or wild repositioning, save the [CPU](cpu.md), save the planet (:D). Etcetc.

**PRO TIP: you can achieve some basic formatting completely without CSS** (i.e. even without the `style` attribute) -- for some things you can use the old HTML visual attributes. Whether it's better to do it this way or rather use CSS depends on situation, but it's good to have a choice, this may allow you to just completely avoid CSS (e.g. to get to the "nocss" website club) and maybe have better chance of supporting ancient browsers. Though some of the attributes are already deprecated in HTML5, browsers still support them and the worst that can happen is that they simply won't work. It's probably also possible to use CSS and also the HTML attributes as a fallback. The visual attributes include `bgcolor`, `border`, `color`, `cols`, `height`, `rows`, `shape`, `size`, `width` etc. You can also use tables to give the page a layout (this is how it used to be done back in the day). And of course you may use images for visuals as well, but that may already be an inferior, more bloated solution.

TODO: more more more

## How It Works

The CSS style can be put into three places:

- **separate file** (external CSS): Here the style is written in its own file with *.css* extension and any HTML file wanting to use the style links to this file (with *link* tag inside *head*). This is good if you have multiple HTML files (i.e. a whole website) that use the same style.
- **inside the HTML file itself** (internal CSS): The style is written right inside the same file as the HTML document, between *style* tags in *head*, so it's all nicely self contained. This is good if the style is used only by this one HTML document (e.g. you have a single webpage or some special page that just has its own style).
- **inside HTML tags** (inline CSS): Style can be specified also as HTML tag attributes (the *style* attribute), but this is discouraged as it intermixes HTML and CSS, something CSS wants to avoid.

The style itself is quite simple, it's just a list of styling rules, each one in format:

```
selectors
{
  style
}
```

Here *selectors* says which elements the rule applies to and *style* defines the visual attributes we want to define. For example

```
p
{
  color: blue;
  font-size: 20px;
}

h1, h2, h3
{
  color: red;
}
```

Specifies two rules. One says that all *p* tags (paragraphs) should have blue text color and font that's 20 pixels tall. The second rule says that *h1*, *h2* and *h3* tags (headings) should have red text color. Pretty simple, no?

Tho it can get more complex, especially when you start positioning and aligning things -- it takes a while to learn how this really works, but in the end it's no rocket science.

TODO: moar

## CSS Gore And Pissing People Off

A user running bloatbrowser that implements CSS and has it turned on by default is openly inviting you to freely remotely manipulate what his computer is showing to him, so let's take this opportunity to do some lighthearted [trolling](trolling.md) :) Here are some possible approaches:

- `cursor`: Change or even hide the mouse cursor :D You can set it to `none` (hide), `progress` (make the user think something's loading indefinitely, see how long it takes for him to realize), `wait`, `col-resize` or even specific image with `url(...)`.
- Make the site work only without CSS :D For example: `body * { display: none; } body:before { content: "This site only works without CSS." }`.
- CSS can do animation! This can be used to induce seizures. E.g.: `@keyframes lul { 0% { background-color: red; } 100% { background-color: green; } } body { animation-name: lul; animation-duration: 0.1s; animation-iteration-count: infinite; }`.
- Animate `<body>` size so that the scroll bars keep resizing.
- `a:hover { display: none; }`: Makes links disappear when they're pointed at with the cursor :D Can also be used for buttons etc.
- Make some huge clusterfuck of divs that get arranged in some intricate way, then make each div change its size with `:hover`, or better yet use `transform` to rotate or skew it, triggering a spectacular domino effect. You have to make it so that if one div reshapes on mouse over, another one gets under the cursor, triggering reshape of that one, which pushes another one under the cursor etc.
- Alternative to the previous: make one huge ass div covering the whole screen and make it resize to 1x1 pixels on `:hover`, this will cause some vomit inducing blinking whenever mouse is moved.
- Use animation to very slowly alter the site, e.g. keep making text more and more transparent, so that it can't be noticed immediately but will become apparent after having the site open for 15 minutes, or maybe just have the site normal but after 10 minutes just immediately rotate it 180 degrees, the user will be like WTF :D or maybe instead of this after 10 minutes just replace the site with some porn image -- there is a chance someone will open the site, then leave the computer for a while, leaving the innocent site open but in the meanwhile it will change to porn and suddenly he will look like the biggest pervert :D
- TODO: some shit that makes CPU burn aka bitcoin miner without bitcoin
- TODO: make the page 1 light year long or something
- TODO: more