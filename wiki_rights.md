# LRS Wiki Usage Rights

This page serves to establish usage rights for the whole LRS wiki. It is here to be part of the work so that the legal rights are always clear, even if e.g. the README gets lost somewhere along the way.

Everything on this wiki has been created from scratch solely by people listed in [wiki authors](wiki_authors.md). Great care has been taken to make sure no copyrighted content created by other people has been included in any way. This is because one of the goals of this wiki is to be completely in the public domain world wide.

Each contributor has agreed to release the whole LRS Wiki under the Creative Commons Zero 1.0 (CC0 1.0) waiver, available at https://creativecommons.org/publicdomain/zero/1.0/, with an additional option for you to also freely choose the following waiver instead:

The intent of this waiver is to ensure that this work will never be encumbered by any exclusive intellectual property rights and will always be in the public domain world-wide, i.e. not putting any restrictions on its use.

Each contributor to this work agrees that they waive any exclusive rights, including but not limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions conceived, discovered, made, designed, researched or developed by the contributor either solely or jointly with others, which relate to this work or result from this work. Should any waiver of such right be judged legally invalid or ineffective under applicable law, the contributor hereby grants to each affected person a royalty-free, non transferable, non sublicensable, non exclusive, irrevocable and unconditional license to this right.