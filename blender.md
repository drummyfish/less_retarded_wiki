# Blender 

Blender (also Blunder) is a greatly [complex](bloat.md) "[open-source](open_source.md)" 3D modeling and [rendering](rendering.md) [software](software.md) -- one of the most powerful and "feature-rich" ones, even compared to [proprietary](proprietary.md) competition -- used not only by the [FOSS](foss.md) community, but also the industry (commercial [games](game.md), movies etc.), which is an impressive achievement in itself, however Blender is also a [capitalist](capitalist.md) software suffering from many not-so-nice features such as [bloat](bloat.md), [update culture](update_culture.md), [hardware discrimination](hw_discrimination.md) and centralized control.

After version 2.76 Blender started REQUIRING [OpenGL](opengl.md) 2.1 due to its "[modern](modern.md)" [EEVEE](eevee.md) renderer, deprecating old machines and giving a huge fuck you to all users with incompatible hardware (for example the users of [RYF](ryf.md) laptops). This new version also stopped working with the [free](free_software.md) [Nouveau](nouvea.md) driver, forcing the users to use NVidia's proprietary drivers. Blender of course doesn't at all care about this. { I've been forced to use the extremely low FPS [software](sw_rendering.md) GL version of Blender after 2.8. ~drummyfish }

**Are there good alternatives to Blender?** Some programs to check out are [wings3d](wings3d.md) (seems like the best candidate), [k3d](k3d.md), [meshlab](meshlab.md) and [mm3d](mm3d.md) (looks like bloat), also for some things possibly [FreeCAD](freecad.md). Remember you can also make models manually :-) Formats like obj can be hand-written.

## See Also

- [3D modeling](3d_modeling.md)