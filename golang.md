# Go (Programming Language)

Go (also golang) is a [transsexual](tranny_software.md) compiled [programming language](programming_language.md) advertised as the the "[modern](modern.md)" successor to [C](c.md), it is co-authored by one of C's authors, [Ken Thompson](ken_thompson.md), and has been worked on by [Rob Pike](rob_pike.md), another famous Unix hacker (who however allegedly went insane and has been really doing some crazy shit for years). Of all the new language go is one of the least [harmful](harmful.md), however it's still quite [shit](shit.md). Some reasons for this are:

- It is developed by [Google](google.md) and presented as "[open-source](open_source.md)" (not [free software](free_software.md)).
- It employs a [code of censorship](coc.md) (https://go.dev/conduct) and is therefore [tranny software](tranny_software.md) embracing fascism.
- It downloads additional [dependencies](dependency.md) during compilation, a huge defect due to which it's been rejected e.g. from [HyperbolaBSD](hyperbolabsd.md) (https://wiki.hyperbola.info/doku.php?id=en:philosophy:incompatible_packages).
- It has (classless) [OOP](oop.md) features.
- It has [bloat](bloat.md) such as [garbage collection](garbage_collection.md), built-in [complex number](complex_number.md) type, [concurrency](concurrency.md) and something akin a [package manager](package_manager.md) (*go get/install*).
- It forces a programming style in which an opening function bracket (`{`) can't be on its own line. [LMAO](lmao.md)
- Huge standard library with shit like crypto, image and html.
- It is also slow (at least the tested implementations of course), on benchmarks it performs about a class lower than C/C++.
- ...

Anyway, it at least tries to stay *somewhat* simple in some areas and as such is probably better than other modern languages like [Rust](rust.md). It purposefully omits features such as [generics](generics.md) or static type conversions, which is good.

**How big is it really?** The official implementation by Google has whopping 2 million lines of code of self hosted implementation -- that's ginormous but keep in mind Google would likely implement minesweeper in two million lines of code too, so it may say little. Size of specification may be more informative -- that one has about 130 pages (after converting the official HTML specs to pdf), that's a bit smaller than that of C (the pure language part has about 160 pages), so that's not bad.
