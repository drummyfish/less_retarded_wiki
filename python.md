# Python

*What if [pseudocode](pseudocode.md) was actually code?*

Python (name being a reference to Monty Python) is an exceptionally [bloated](bloat.md), extremely popular [high level](abstraction.md) [interpreted](interpreter.md) [programming language](programming_language.md). Its priority is readability and making it easy and fast to bash together some code for anyone with at least half a brain hemisphere, so it is eminently popular among beginners, children, [women](woman.md), non-programmers such as scientists and unqualified [soydevs](soydev.md) who can't handle real languages like [C](c.md). Python [just werks](just_werks.md) and is comfortable, but any program written in it is forever doomed to be bloated, slow, ugly, big and will unavoidably die without [maintenance](maintenance.md), for Python's updates purposefully break [backwards compatibility](backwards_compatibility.md). At this moment it is the language most frequently used for programming "neural net [AI](ai.md)s".

**Python is extremely slow**, even much slower than [JavaScript](javascript.md) and [PHP](php.md) (according to *Computer Language Benchmarks Game*). If you want to make your python programs faster, use the *PyPy* implementation over the default *CPython*.

**Programming in python is not real programming**. Making a program in Python versus writing a real program is like making a house in Minetest vs making a real house. Python was in fact made exactly for people who CANNOT program, such as [women](woman.md), economists and professional programmers, it is basically a wheelchair -- those who can program don't need Python just like those who can walk don't need a wheelchair.

Python was conceived in 1991 by a Dutchman Guido van Rossum who announced it on [Usenet](usenet.md). Version 1.0 was released in 1994 and version 2.0 in 2000. A very important version was 2.7 released in 2010 -- this was used and supported for a long time but the support was ended in 2020 in favor of Python 3. As of writing this the latest version is 3.9.

**Can [we](lrs.md) use python?** There are certain use cases for it, mostly writing [throwaway scripts](throwaway_script.md) and other quick, temporary code. Python can easily help you get into programming as well, so it may well serve as an educational language, however be sure to transition to a better language later on. Remember, **python mustn't ever be used for a serious program**.

The reference implementation, *CPython*, is at the same time the one in most widespread use; it is written in [C](c.md) and python itself. There also exist different implementations such as *MicroPython* (for [embedded](embedded.md)), PyPy (alternative implementation, often faster), Jython and so on.

What follows is a summary of the python language:

- Emphasis is on **"readability"** and comfort, with a bit of stretch the aim is to create a "runnable [pseudocode](pseudocode.md)". To this end is sacrificed performance, elegance, maintainability and other important aspects.
- It is **[interpreted](interpreter.md) and highly dynamic**, i.e. data types of variables are dynamic, [lists](list.md), [strings](string.md) and [dictionaries](dict.md) are dynamic, since new versions there are even **arbitrary size integers** by default. There is automatic **[garbage collection](garbage_collection.md)**, code can be modified at run time and so on. All this of course makes the language **slow**, with big memory footprint.
- There is **class-based [OOP](oop.md)** which can at least be avoided, it is not enforced.
- Python **revolves around [dictionaries](dictionary.md)** (a [data type](data_type.md) capable of storing *key:value* pairs), i.e. most things are internally implemented with dictionaries.
- It **doesn't keep backwards compatibility**, i.e. new versions of Python won't generally be able to run programs written in old versions of Python. This is so that the devs can eliminate things that turned out to be a bad idea (probably happens often), but of course on the other hand you have to [keep rewriting](maintenance.md) your programs to keep them working (python provides scripts that help automatize this).
- Quite retardedly **indentation is part of syntax**, that's a [shitty](shit.md) design choice that complicates programming (one liners, minification, compact code, [code golf](golf.md), temporary debugging indentation, ...).
- There is **no specification** per se -- but at least there is online reference (*The Python Language Reference*) that kind of serves as one.
- It has a **gigantic standard library** which handles things such as [Unicode](unicode.md), [GUI](gui.md), [databases](database.md), [regular expressions](regex.md), [email](email.md), [html](html.md), [compression](compression.md), communication with operating system, [networking](network.md), [multithreading](multithreading.md) and much, much more. This means it's almost impossible to implement Python in all its entirety without 100 programmers working full time for at least 10 years.
- There are numerous other **smaller fails**, e.g. inconsistent/weird naming of built-in commands, absence of switch statement (well, in new versions there is one already, but only added later and looks kinda shitty) etc.

## Example

Here is the **[divisor tree](divisor_tree.md)** program implemented in Python3, it showcases most of the basic language features:

```
# recursive function, prints the divisor tree of a number
def printDivisorTree(x):
  a = -1
  b = -1

  for i in range(2,int(x / 2) + 1): # find closest divisors
    if x % i == 0:
      a = i
      b = int(x / i)
      
      if a >= b:
        break

  print("(",end="")

  if a > 1:
    printDivisorTree(a)
    print(" " + str(x) + " ",end="")
    printDivisorTree(b)
  else:
    print(x,end="")
  
  print(")",end="")

while True: # main loop, read numbers
  try:
    x = int(input("enter a number: "))
  except ValueError:
    break  

  printDivisorTree(x)
  print("")
```