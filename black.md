# Black

Black, a [color](color.md) whose [politically correct](political_correctness.md) name is *afroamerican*, is a color that we see in absence of any [light](light.md).