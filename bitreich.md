# Bitreich

{ Researching this on-the-go, send me corrections, thanks. ~drummyfish }

Bitreich is a small, obscure underground group/movement of programmers who greatly value [minimalism](minimalism.md)/[simplicity](simplicity.md), oppose the [evil](evil.md) and degeneration of [modern](modern.md) mainstream technology and aim for making the world a better place mainly through simpler [technology](tech.md). They seem to belong to the cluster of "minimalist programmer groups", i.e. they are similar to [suckless](suckless.md) (which in their manifesto they see as a failed project), [reactionary software](reactionary_software.md) and our very own [LRS](lrs.md), sharing many values such as [minimalism](minimalism.md), [Unix philosophy](unix_philosophy.md), preference and love of the [C](c.md) language, carrying on some of the [hacker culture](hacking.md) heritage, though of course they also have their own specifics that will make them different and even disagreeing with us and others on occasion, e.g. on [copyleft](copyleft.md) (unlike us, they seem to greatly prefer the [GPL](gpl.md)), terminology (yeah, they seems to prefer "[open source](open_source.md)") and probably also things like [privacy](privacy.md) (though the craze doesn't seem to go too far, many have listed their real names and addresses) etc.

According to the gopherhole Bitreich started on 17.8.2016 -- the founder (or at least one of them?) seems to be 20h (Christoph Lohmann according to the user profile), a guy formerly active in [suckless](suckless.md) (can be found on their website), who even gave an interview about Bitreich to some radio/magazine/whatever. It seems Bitreich originated in Germany. As of 2023 they list 12 official member profiles (the number of lurker followers will of course be a much high number, there seem to be even bitreich subcommunities in other countries such as Italy). They are mostly present on [gopher](gopher.md) (gopher://bitreich.org), which they greatly promote, and [IRC](irc.md) (ircs://irc.bitreich.org:6697/#bitreich-en). There are also [Tor](tor.md) hidden services etc.; their website at bitreich.org seems to be purposefully broken in protest of the [web](web.md) horror.

Some of their ideas and philosophy seems to be very based, e.g. preference of KISS/older protocols (gopher, ftp, IRC, ...), "users are programmers" (opposing division into users as consumers and developers as overlords), "bug reports are [patches](patch.md)", "programs can be [finished](finished.md)" etc.

Bitreich is also about humor and [fun](fun.md) (sometimes so much so that it's not clear if something is a joke or serious stuff -- maybe because it's partly both). They invented *[analgram](analgram.md)*, an authentication method based on analprints (alternative to fingerprint authentication). They put a snapshot of their source code into an actual Arctic vault in Greenland, to be preserved for millennia. Often there appear parodies of whatever is currently hyping in the mainstream, e.g. [NFT](nft.md)s, "big data", [AI](ai.md), [blockchain](blockchain.md) etc. { There's also some stuff going on with [memes](meme.md) and cooking recipes but TBH I didn't get it. ~drummyfish }

Some interesting projects they do:

- **Bitreichcon**: annual conference, running since 2017. Their slides can be downloaded in plain text.
- **Bitreich radio**
- **Day Of The GrParazyd**: point and click adventure [game](game.md). { Didn't even take a look at this yet, sorry, no idea what it really is :D ~drummyfish }
- **The Gopher Lawn**: directory/index of gopherspace, categorizing gopherhole links.
- **The Gopher Times**: a very cool printable magazine (in both [pdf](pdf.md) and [plain text](txt.md)), `git clone git://bitreich.org/tgtimes`.
- A number of smaller utilities/programs and parody stuff (see their gopherhole).
- Keeping infrastructure to host stuff they see as valuable.
- ...

## See Also

- [suckless](suckless.md)
- [reactionary software](reactionary_software.md)
- [less retarded software](lrs.md)
- [KISS](kiss.md)