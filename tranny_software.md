# Tranny Software

Tranny software is a harmful [software](software.md) developed within and infected by the culture of the toxic [LGBTFJJJGIIQWWQW](lgbt.md) [SJW](sjw.md) [pseudoleftists](pseudoleft.md), greatly characterized e.g. by [codes of conduct](coc.md), bad engineering and excluding straight white males from the development in the name of "inclusivity". Such software is retarded. It is practically always a high level of [bloat](bloat.md) with features such as [censorship](censorship.md), bugs and [spyware](spyware.md).

To be clear, *tranny software* does NOT stand for *software written by transsexuals*, it stands for a specific kind of software infected by [fascism](fascism.md) (in its features, development practices, culture, goals etc.) which revolves around things such as sexual identity. Of course with good technology it doesn't matter by whom it is made. For god's sake do NOT bully individuals for being transsexual! Refuse bad software.

Some characteristics of tranny software are:

- **It is typically [FOSS](foss.md)** because a big part of trannyism in software is the development process and interaction with public (and with [proprietary](proprietary.md) software the development is not open to public). In theory we may call a proprietary software *tranny* if it e.g. very aggressively promote some [SJW](sjw.md) bullshit -- in fact most companies are nowadays infected by [SJWism](sjw.md) -- but it's not so common to use this term for proprietary software.
- **Typically has a [code of conduct](coc.md)** or some equivalent "SJW guideline" (excluding the anti-COCs).
- Sometimes promotes SJW fascism in other ways, e.g. [LGBT](lgbt.md) flags etc.
- **It is typically [bloat](bloat.md) and [capitalist software](capitalist_software.md)**, and generally just very bad software at least from the [LRS](lrs.md) point of view. This is partly because devs try to be "inclusive" and afraid to refuse [PRs](pull_request.md) from "diverse people" (who are mostly incompetent, again trying software development e.g. as part of proving that "minorities can program too"), partly because their software doesn't even aim to be good but rather popular (as it is to serve to promote political ideas etc.).
- **It often has SJW "features"**, e.g. "slur filters", "diverse" characters in games etc.

Examples of tranny software are:

- [Rust](rust.md)
- [Lemmy](lemmy.md)
- [Linux](linux.md)
- [Firefox](firefox.md)
- [Chromium](chromium.md)
- ...

Example of software that doesn't seem to be tranny software:

- all official [LRS](lrs.md)
- likely most [suckless](suckless.md) software
- TODO