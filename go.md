# Go

*It is better to dip your fingers in the coffee than to try to drink the stones.* --Go proverb

*This article is about the game of go, for programming language see [golang](golang.md).*

{ I am still learning the beautiful game of go, please excuse potential unintentional errors here. ~drummyfish }

Go (from Japanese *Igo*, "surrounding board game", also *Baduk* or *Wei-qi*) is possibly the world's oldest original-form two-player board [game](game.md), coming from Asia, and is one of the most [beautiful](beauty.md), elegant, deep and popular games of this type in [history](history.md), whose cultural significance and popularity can be compared to that of [chess](chess.md), despite it largely remaining widely popular only in Asia (along with other games like [shogi](shogi.md), or "Japanese chess"). There however, especially in Japan, go is pretty big, it appears a lot in [anime](anime.md), there are TV channels exclusively dedicated to go etc., though in Japan [shogi](shogi.md) (the "Japanese chess") is probably a bit more popular; nevertheless go is likely the most intellectually challenging board games among all of the biggest board games. **Go is a bit difficult to get into** (kind of like [vim](vim.md)?) though the rules can be learned quite quickly; it is hard to make big-picture sense of the rule implications and it may take weeks to months before one can even call himself a beginner player. To become a master takes lifetime (or two).

{ There is a nice non-bloated site hosting everything related to go: Sensei's Library at https://senseis.xmp.net/. ~drummyfish }

**Compared to chess** (some purists dislike this, see https://senseis.xmp.net/?CompareGoToChess) the rules of go are much more simple -- which is part of the game's [beauty](beauty.md) (see [easy to learn, hard to master](easy_to_learn_hard_to_master.md)) -- though the emergent complexity of those few rules is grandiose; so much so that to play the game well is usually considered more challenging than learning chess well, as there are many more possibilities and mere calculation is not enough to be strong, one needs to develop a strong intuition; this is also the reason why it took 20 more years for [computers](computer.md) to beat the best humans in go than in chess. Many say that go is yet deeper than chess and that it offers a unique experience that can't be found anywhere else; go is more mathematical, something that just exists naturally as a side effect of logic itself, while chess is a bit of an arbitrary set of more complex rules fine-tuned so that the game plays well. The spirit of go is also more [zen](zen.md)-like and peaceful: while chess simulates [war](war.md) (something more aligned with western mentality), go is more about dividing territory, one could even see it not as a battle but rather a creation of [art](art.md), beautiful patterns (something better aligned with eastern mentality). Also the whole [culture](culture.md) around go is different, for example there is a strong tradition of go proverbs that teach you to play (there also exist many [joke](joke.md) proverbs).

**From [LRS](lrs.md) point of view go is one of the best games ever**, for similar reasons to chess (it's highly free, [suckless](suckless.md), cheap, [not owned by anyone](public_domain.md), [fun](fun.md), mathematically deep, nice for programming while the game itself doesn't even require a [computer](computer.md) etc.) plus yet greater [simplicity](minimalism.md) and beauty.

**Solving go:** similarly to chess the full game of go seems unlikely to be solved -- the 19x19 board makes the game state tree yet larger than that of chess, but the much simpler rules possibly give a bigger hope for mathematical proofs. Smaller boards however have been solved: Erik van der Werf made a program that confirmed win for black on boards up to (and including) 5x5 (best first move in all cases being in the middle of the board). Bigger boards are being researched, but a lot of information about them is in undecipherable Japanese/Korean gibberish, so we leave that for the future.

A famous proverb about go goes like this: what is the most perfect game man ever invented? Chess! But what about go? Go existed long before man...

TODO: rating, programming, stats, programs and sites for playing, ...

## Rules

The rules of go vary a bit more than those of chess, they are not as much unified, but usually the details don't play as much of a role because e.g. different scoring systems still mostly result in the same outcome of games. Here we'll describe possibly the most common rule set.

The game's **goal** is basically to surround a bigger territory than the enemy player. The formal rules are pretty simple, though their implications are very complex.

Go is played by a black and white player, black plays first (unlike in chess) and then both players take turns placing stones of one's own color on squares -- a square is the INTERSECTION of the lines on the board, NOT the place between them (consider the lines to be carved in stone, the intersection is where the stone stands with stability). The stones are all the same (there are no different types of stones like in chess) and they cannot move; once a stone is placed, it stays on its position until the end of the game, or until it is captured by the enemy player. The board size is **19x19**, but for for students and quick games 13x13 and 9x9 boards are also used. As black plays first, he has a slight advantage; for this white gets bonus points at the end of the game, so called **komi**, which is usually set to be 6.5 points (the half point eliminates the possibility of a draw). Komi may differ depending on board size or a specific scoring system.

Any player can **pass** on his move, i.e. making a move isn't mandatory. However you basically always want to make a move, one only passes when he feels there is nothing more to be gained and the game should end. If both players pass consecutively, the game ends.

The game considers **4-neighborhoods**, NOT 8-neighborhood, i.e. squares that don't lie on board edges have 4 neighbors: up, right, bottom and left; diagonal squares are NOT neighbors.

**Capturing:** a player can capture a group of connected (through 4-neighborhoods) enemy player's stones by completely surrounding them, or more precisely by taking away all so called **liberties** of that group -- *liberty* is an empty square that's immediately neighboring with the group (note that liberties may lie even inside the group). If a player places his stone so that it removes the enemy group's last liberty, then the group is removed from the board and all its stones are taken as captured. It is possible to capture stones by a move that would otherwise be forbidden as suicide, if after the removal of the captured group the placed stone gains a liberty.

**[Suicide](kys.md) is forbidden:** it is not allowed to place a stone so that it would immediately result in that stone (or a group it would join) being captured by enemy. I.e. if there is an enemy group with one empty square in the middle of it, you cannot put a stone there as that stone would simply have no liberties and would immediately die. Exception to this is the above mentioned taking of a group, i.e. if a suicidal move results in immediately taking enemy's group, it is allowed.

The **ko** rule states that one mustn't make a move that returns the board to the immediately previous state; this basically applies just to the situation in which the enemy takes your stone and you would just place it back, retaking his capturing stone. By the *ko* rule you cannot do this IMMEDIATELY, but you can still do this any further following round. Some rulesets extend this rule to so called *superko* which prohibits repetition of ANY previously seen position (this covers some rare cases that can happen).

**Territory:** at any time any EMPTY square on the board belongs either to white (no black stone can be reached from it by traveling over neighbors), black (no white stone can be reached from it) or none (belongs to neither). Squares that have stone on them aren't normally considered to belong to anyone (though some scoring systems do), i.e. if you surround a territory as white, only the VACANT surrounded squares count as your territory. The size of territory plays a role in final scoring. An alternative to territory is **area**, which is territory plus the squares occupied by player's stones and which is used under some rulesets.

**Prisoners** are enemy's stones that are OBVIOUSLY in your territory and so are practically dead. I.e. they are inside what's clearly not their territory and with further play would clearly be captured. Obvious here is a matter of agreement between players -- if players disagree whether some stones are obvious prisoners, they simply keep playing and resolve the situation.

**Scoring:** scoring assigns points to each player when the game is over, the one with more points win. There are multiple scoring systems, most common are these two (players basically universally agree the scoring system has almost no effect on the play so it's probably more of a convention):

- **Chinese** (area scoring): more [KISS](kiss.md), the score is just each player's area (surrounded empty square PLUS squares occupied by the player's stones), plus komi for white. { This one seems to me like a better option for beginners and also for programming, it's just simpler and makes you not afraid of putting stones anywhere. ~drummyfish }
- **Japanese** (territory scoring): At the end of the game we count the score for black as the size of black's territory PLUS one point for each stone black has captured PLUS one point for each white prisoner (a would be captured stone) in black's territory. Score for white is computed analogously but we also add the komi compensation.

**Handicaps:** TODO.

**Implications of rules and basic of strategy/tactics:** The very basic observation a beginner has to learn is that of life and death and especially so called **eyes** -- an eye is simply an empty square inside a group of stones, and it can easily be deduced that **if a group has at least two eyes, it can't be taken by the opponent** (the eyes have to be SEPARATE though, two empty squares next to each other aren't two eyes). One eye isn't enough to keep the group alive -- the opponent can still surround this group and then take it by placing one stone in the eye, so two eyes are necessary. From this it follows that players try to make groups with these two eyes and more tactics follow from it -- for example if it's clear a group can no longer form two eyes, usually the player just stops wasting more time on it and lets it be sacrificed. This is the number one "implied" rule and many, MANY more follow, but here is not a place to recount them all: for more please refer for example to the Sensei's library.

**Example:** the following is an example of the end state of a beginner game on a 9x9 board:

```
   _________________
9 |. # . . # # # O .|
8 |# . # . # O O . O|
7 |. . . # # O . O .|
6 |# . . # O . O . O|
5 |# . # O O O . . .|
4 |. # # # O O . . .|
3 |. . . # # O O . .|
2 |. . . . # # O # .|
1 |. . . . # O O O .|
  '-----------------'
   A B C D E F G H I
```

Here black's (`#`) territory is 23, and black made 9 captures during the game, giving together 32 points. White's (`O`) territory is 16 and he has one black prisoner (H2), giving 17 points; furthermore white made 6 captures during the game and gets 5.5 (smaller value due to only 9x9 board size) bonus points as komi, totalling 28.5 point. Therefore black wins.

TODO

## Play Tips

TODO

## Go And Computers, Programming

See also https://senseis.xmp.net/?ComputerGoProgramming and https://www.chessprogramming.org/Go.

**Board representation:** a straightforward representation of the go board is as a simple [array](array.md) of squares; each square can be either empty, white or black, that's 3 values that can be stored with 2 bits, which allow storing 4 values, leaving one extra value to be used for some other purpose (e.g. marking illegal ko squares, estimated dead stones, marking last move etc.). 1 byte allows us to store 4 squares this way so we need only 91 bytes to represent the whole 19x19 board. On computers with enough RAM it may be considered to store 1 square in a single byte or int, making the board take more space but gaining speed thanks to data alignment (we don't need extra instructions for squeezing bit from/to a single byte). Of course we furthermore have to keep track of extra things such as numbers of captured stones.

TODO

## Stats

Some [interesting](interesting.md) stats about go follow.

The longest possible game without passes has 4110473354993164457447863592014545992782310277120 moves. The longest recorded professional game seems to be mere 411 moves long (Hoshino Toshi vs Yamabe Toshiro, 1950). There are 2.08168199382 * 10^170 legal positions on a 19x19 board, 3.72497923077 * 10^79 for 13x13 and 1.03919148791 * 10^38 for 9x9. The number of possible games is estimated from 10^10^100 to 10^10^171. An average high-level game lasts about 150 moves. Average branching factor is 250 (compare to 35 in chess).

## See Also

- [chess](chess.md)
- [game of life](game_of_life.md)
- [hex game](hex_game.md)