# Dusk OS

Dusk OS (http://duskos.org/) is a work in progress non-[Unix](unix.md) extremely [minimalist](minimalism.md) 32 bit [free as in freedom](free_software.md) [operating system](operating_system.md) whose primary purpose is to be helpful during societal [collapse](collapse.md) but which will still likely be very useful even before it happens. It is made mainly by [Virgil Dupras](dupras.md), the developer of [Collapse OS](collapseos.md), as a bit "bigger" version of Collapse OS, one that's intended for the first stage of societal collapse and will be more "powerful" and comfortable to use for the price of increased complexity (while Collapse OS is simpler and meant for the use during later stages). But don't be fooled, Dusk OS is still light year ahead in simplicity than the most minimal [GNU](gnu.md)/[Linux](linux.md) distro you can imagine; by this extremely minimalist design Dusk OS is very close to the ideals of our [LRS](lrs.md), it is written in [Forth](forth.md) but also additionally (unlike Collapse OS) includes a so called "Almost [C](c.md)" compiler allowing [ports](port.md) of already existing programs, e.g. Unix [command line](cli.md) utilities. It can also be seen as a Forth implementation/system. The project is available under [CC0](cc0.md) [public domain](public_domain.md) just as official LRS projects, that's simply unreal.

The project has a private mailing list. Apparently there is talk about the system being useful even before the collapse and so it's even considering things like [networking](network.md) support etc. -- as [capitalism](capitalism.md) unleashes hell on [Earth](earth.md), any simple computer capable of working on its own and allowing the user complete control will be tremendously useful, even if it's just a programmable calculator. Once [GNU](gnu.md)/[Linux](linux.md) and [BSD](bsd.md)s sink completely (very soon), this may be where we find the safe haven.

The only bad thing about the project at the moment seems to be the **lurking [SJW](sjw.md) danger**, presence of some [pseudoleftists](pseudoleft.md) around the project, threatening political takeover etcetc. At the moment there thankfully seems to be no [code of censorship](coc.md), however it can be smelled in the air that there are many people with the eyes on the project extremely horny about adding one. It will most likely be forced very soon -- once this happens, we have to stop supporting the project immediately. Hopefully [forks](fork.md) will come if anything goes wrong. In any case, [we](lrs.md) will be continuing our independent work that will probably yield a similar system, thought much later.

{ I'm not 100% sure about everything that will follow as I'm still studying this project, please forgive mistakes, double check claims. ~drummyfish }

It really does look amazing and achieves some greatly awesome things, for example it's (as far as it seems) completely [self-hosted](self-hosting.md) from the ground up, containing no binary blobs (at least for the bootstrap it seems, driver state unchecked), being able to completely bootstrap itself from some 1000 lines of assembly, however it still keeps [portability](portability.md) by using a kind of simple abstract [assembly](assembly.md) called [HAL](hal.md), so it's not tied to any CPU architecture like most other simple OSes. The "user mode" system can also be compiled as a normal program on current operating systems, allowing to make so called Dusk packages, i.e. automatically export any Dusk OS program as a basically native program for current computers. You don't even need any virtual machine or emulator to try out the OS, you just download it, type `make` and run it as a normal program. The whole system is also very fast. There's some black [magic](magic.md) going on. Also the project documentation is very nice. It also seems they don't care about [security](security.md) and [safety](safety.md) at all, WHICH IS EXTREMELY GOOD, there is no place for such [bullshit](bullshit.md) in a truly minimalist project.

Let's sum up some of the interesting features of the system:

- "aggressive" simplicity, simplicity is above speed (but speed is still great and high among priorities)
- written in [Forth](forth.md), offering simplified [C](c.md) compiler as its main feature
- [portable](portability.md), using simple abstract [assembly](assembly.md) to run on different architectures
- no [concurrency](concurrency.md), linear computation only, this will probably imply no [multitasking](multitasking.md)
- no [virtual memory](virtual_memory.md)
- preferring static memory allocation
- making use of global states/variables (going against mainstream capitalist "best practices")
- 32 bit system, maximum 4 GB of memory
- no user friendliness, [hacker](hacking.md) "operator" assumed instead of a layman user
- no "security", "safety", "privacy", user accounts etc.
- graphics: primarily no [GUI](gui.md), simple graphics capabilities will be present, direct access to screen pixels (through so called "grid" interface)
- some nice text [shell](shell.md) will be made
- sound: ???
- networking: being considered
- NOT a Unix-like but will naturally be aligned with many of its concepts as per simplicity

## See Also

- [DuckOS](duckos.md)