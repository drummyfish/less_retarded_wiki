# Temple OS

Temple OS is a [funny](fun.md) [operating system](os.md) made by a [schizo](schizo.md) guy [Terry Davis](terry_davis.md) who has become a [meme](meme.md) and achieved legendary status for this creation in the [Internet](internet.md) tech circles as it's extremely impressive that a single man creates such a complex OS and also the OS features and the whole context of its creation are quite funny. It has a website at https://templeos.org.

According to Terry, God commanded him to write TempleOS and guided him in the development: for example it was demanded that the resolution be 640x480. It is written in [HolyC](holy_c.md), Terry's own [programming language](programming_language.md). The OS comes with GUI, 2D and 3D library, [games](game.md) and even a program for communicating with God.

Notable Temple OS features and programs are:

- [multitasking](multitasking.md) (non-preemptive)
- supported [file systems](file_system.md): [FAT32](fat32.md), ISO9660, RedSea (custom)
- HolyC compiler
- 2D/3D library
- oracle (communicate with God)
- [games](game.md)
- [IDE](ide.md) supporting images and [3D models](3d_model.md) embedded in text

In his video blogs Terry talked about how technology became spoiled and that TempleOS is supposed to be [simple](minimalism.md) and [fun](fun.md). For this and other reasons the OS is limited in many way, for example:

- no networking
- Only runs on [x64](x64.md).
- Only runs in 640x480 16 color display mode.
- single audio voice
- ring-0 only
- single address space
- multitasking is non-preemptive (programs have to yield CPU themselves)

Temple OS source code has over 100000 [LOC](loc.md). It is publicly available and said to be in the [public domain](public_domain.md), however there is no actual [license](license.md)/waiver in the repository besides some lines such as "100% public domain" which are legally questionable and likely ineffective (see [licensing](license.md)).

There still seems to be some people developing the OS and applications for it, e.g. Crunklord420.

## See Also

- [Timecube](timecube.md)
- [Sonichu](sonichu.md)