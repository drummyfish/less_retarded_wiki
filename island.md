# Welcome To The Island!

This is the freedom island where [we](less_retarded_society.md) live. Many would call it [utopia](utopia.md), we just call it the Island for now. If you feel like you don't fit anywhere in [today's world](21st_century.md), if you feel alone and alienated and just want to escape somewhere quiet and peaceful, you can be at home here. This island has [no owners](free_universe.md), no governments, politicians, [corporations](corporation.md), no businesses or [money](money.md), no need to [fight](fight_culture.md) for the place under the Sun, no [regulation of speech](political_correctness.md), no bullying. Simply be yourself and exist as best as you can. Take off your clothes if you want, no one wears them here. You can build yourself a hut on any free spot. Planting trees and making landscape works is allowed too. Basically we have [no laws](anarchism.md), just empathy and [love](love.md).

```
                                                distant Normieland
                                                        or
                                                 The Land of Soy
                                                __..----..__..._____
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'""~~~~~~~~~~~~~~~~~~~~""'~~
                        The Freedom Island
-~"--'"~'                      ____                        -~"--'"~'
                           __X/    '-X_
    '"-~-._          ____./  i#  X   xx'-__       
                __.-' [E]/'  XX   i:   "x   \_       '"-~-._
         ___,--' x  x_/'  X Xi     O:.       '-_
     ___/     ##__-''   X  X(   x# i    x#      '-._
  _-'     :x#x.'  [G]   i  i  ###     [T] #xX "x.   ''-._    '"-~-._
 (     ,:xx     .x:.     x  O##X#i :     'ixx; "         \
  '-   _.'"  :Xx"'      xx  x#xx  #X\_    '"":.:':,..''   )
    ''[__     '' ';:.  ':. xx x##x    '.  ____      ____-'
         ''--___    [D]  '** ;#X        \/[B] ''---' 
                ''--__     '** ;xX       \__
                      \    O .:*:iX         ''-__
   '"-~-._            /        :* i; O   _    [F]'--__     _.'\--_
                     |        o ' ."'i.XX#\           \   ;__  )Xx')
            '-~-.     \__        _.  ' ox##Xx -._      )     "".___/
'-~-.        ,..,_       ''____["        '" "'.  " ___/
           /"[I]':'""-,_..      ''--_______      ,"
           "..--""-/  : _/   _.--._,..._   )___.-"    '"-~-._
                   ".__)"   (_ /xX# ___.'
                              )__./"
```

- **`B`: [boat](boat.md)**: Here you can depart from the Island back to the cruel world. New people also arrive here, and sometimes we have to go to Normieland to retrieve stuff we can't yet make on our own.
- **`D`: [drummyfish's](drummyfish.md) house**: It's a plain bamboo hut near the beach, drummyfish lives here with the [dog](dog.md) who however often roams the whole Island and welcomes all the newcomers.
- **`E`: [education](education.md) area**: Place where people go to learn and teach, everyone is a teacher and student at the same time, flat rocks and cave walls on this side of island are conveniently used as a kind of blackboard.
- **`F`: the beach forum**: It's a beach where people naturally gather to discuss in groups.
- **`G`: [graveyard](graveyard.md)**
- **`I`: small island**: A small island on the coast, for those who want to be extra alone.
- **`T`: a modest [zen](zen.md) [temple](temple.md)**: It has nice view of the sea and we go meditate here.

TODO: food sources: fields and sea farms (only vegetarian), chickens for eggs, food can also be retrieved from normieland (stolen or bought for money we somehow make, maybe by selling some kinda shit we create on the island), anything that died by natural causes can also be eaten probably, food naturally found in nature like coconuts or something

## See Also

- [Utopia](utopia.md)
- [Atlantis](atlantis.md)
- [Loquendo City](loquendo.md)