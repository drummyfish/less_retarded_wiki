# Gemini

Gemini is a [shitty](shit.md) [pseudominimalist](pseudominimalism.md) network [soynet](soynet.md) [protocol](protocol.md) for publishing, browsing and downloading files, a simpler alternative to the [World Wide Web](www.md) and a more complex alternative to [gopher](gopher.md) (by which it was inspired). It is a part of so called [Smol Internet](smol_internet.md). Gemini aims to be a "[modern](modern.md) take on gopher", adding some new "features" and [bloat](bloat.md), it's also more of a [toxic](toxic.md), [SJW](sjw.md) [soydev](soydev.md) version of gopher; gemini is to gopher a bit like what [Rust](rust.md) it to [C](c.md). The project states it wants to be something in the middle between Web and gopher but doesn't want to replace either (but secretly it wants to replace gopher). Gemini is for zoomers in programming socks, gopher is for the real [neckbeards](neckbeard.md).

On one hand Gemini is kind of cool but on the other hand it's pretty [shit](shit.md), especially by **REQUIRING the use of [TLS](tls.md) [encryption](encryption.md)** for "muh security" because the project was made by privacy freaks that advocate the *ENCRYPT ABSOLUTELY EVERYTHIIIIIING* philosophy. This is firstly mostly unnecessary (it's not like you do Internet banking over Gemini) and secondly adds a shitton of [bloat](bloat.md) and prevents simple implementations of clients and servers. Some members of the community called for creating a non-encrypted Gemini version, but that would basically be just gopher. Not even the Web goes as far as REQUIRING encryption (at least for now), so it may be better and easier to just create a simple web 1.0 website rather than a Gemini capsule. And if you want ultra simplicity, we highly advocate to instead prefer using [gopher](gopher.md) which doesn't suffer from the mentioned issue.

## See Also

- [gopher](gopher.md)
- [smol internet](smol_internet.md)
- [Fediverse](fediverse.md)