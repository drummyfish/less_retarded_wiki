# Art

*There is no indecency in art.*

Art is an endeavor that seeks discovery and creation of [beauty](beauty.md) and primarily relies on intuition, its value is in feelings it gives rise to. While the most immediate examples of art that come to mind are for example [music](music.md) and painting, even the most [scientific](science.md) and rigorous effort like [math](math.md) and [programming](programming.md) becomes art when pushed to the highest level, to the boundaries of current knowledge where intuition becomes important for further development.

**Good art always needs time**, usually a lot of time, and you cannot predict how much time it will need, **art cannot be made on schedule** or as a product. By definition creating true art is never a routine (though it requires well trained skills in routine tasks), it always invents something new, something no one has done before (otherwise it's just copying that doesn't need an artist) -- in this sense the effort is the same as that of research and science or exploring previously unwalked land, you can absolutely never know how long it will take you to invent something, what complications you will encounter or what you will find in an unknown land. You simply do it, fail many times, mostly find nothing, you repeat and repeat until you find the good thing. For this art also requires a lot of effort -- yes, there are cases of masterpieces that came to be very casually, but those are as rare as someone finding a treasure by accident. Art is to a great degree a matter of chance, trial and error, the artist himself doesn't understand his own creation when he makes it, he is only skilled at searching and spotting the good, but in the end he is just someone who invests a lot of time into searching, many times blindly.

**Art is discovered**, not made. The author of art is merely a discovered of some beautiful pattern of nature, he may not even fully comprehend or understand that which he discovered, he must in no way be its owned or arbiter (as capitalism wants to make it with bullshit such as [copyright](copyright.md)). Author has no higher authority in interpretation of his art than anyone else.

Art, like a [woman](woman.md), is beautiful and just like a woman it too often sells itself and becomes a whore, it is too difficult to find sincere, pure art like it is difficult to find a sincere love of a woman.

## See Also

- [beauty](beauty.md)
- [databending](databending.md)
- [music](music.md)