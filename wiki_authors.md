# LRS Wiki Authors

Contributors, list yourselves here if you have made at least one contribution. This helps keep track of people for legal reasons etc.

Legally contributors to this wiki include:

- Miloslav Číž aka [drummyfish](drummyfish.md) (https://www.tastyfish.cz)

{ Though legally there are no other collaborators in terms of copyright -- I do my best to ensure I stay far away from the line of someone's copyrightable work making it here -- there are people who helped me with the wiki in a way that's practically significant, for example those who sent me links, donations, corrected my errors, commented on something or simply supported me in doing what I do. I would like to thank them from my whole heart <3 As it may be risky for anyone to associate with me, I will implicitly not name anyone, but if you would want your name or nick here, let me know. ~drummyfish }

Special thanks goes to my email friend Ramon (who agreed to be listed here) who heavily proofreads the wiki (thanks to which this wiki is peer reviewed :D), sends me interesting findings, links and amazing ideas for [jokes](jokes.md).