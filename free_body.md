# Free Body

Free (as in freedom) body, also libre body, is a body of a human who allows (legally and otherwise) everyone some basic rights to it, such as the right to touch any of its part. This is a concept inspired by that of [free software](free_software.md) and [free culture](free_culture.md), just applied to one's body.

TODO: waiver like CC0 but for a body?

{ Made my waiver here https://codeberg.org/drummyfish/my_text_data/src/branch/master/body_waiver.txt. ~drummyfish }

## See Also

- [free universe](free_universe.md)
- [free software](free_software.md)
- [free culture](free_culture.md)
