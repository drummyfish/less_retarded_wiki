# Software

Software (SW) are [programs](program.md) running on a [computer](computer.md), i.e. its non-physical parts (as opposed to [hardware](hw.md)); for example an [operating system](os.md), the Internet [browser](browser.md), [games](game.md) etc. Software is created by the act of [programming](programming.md) (and related activities such as [software engineering](sw_engineering.md) etc.).

Usually we can pretty clearly say what is software vs what is hardware, however there are also edge cases where it's debatable. Normally software is that about the computer which *can relatively easily be changed* (i.e. reinstalled by a typing a few commands or clicking a few buttons) while hardware is [hard-wired](hard_wired.md), difficult to modify and not expected or designed to be modified. Nevertheless e.g. some [firmware](firmware.md) is kind of software in form of instructions which is however many times installed in some special kind of memory that's difficult to reprogram and not expected to be reprogrammed often -- some software may be "burned in" into a circuit so that it could only be changed by physically rewiring the circuit (the [ME](intel_me.md) spyware in [Intel](intel.md) [CPU](cpu.md)s has a built-in [minix](minix.md) operating system). And this is where it may on occasion become difficult to judge where the line is to be drawn. This issue is encountered e.g. by the [FSF](fsf.md) which certifies some hardware that works with free software as *Respects Your Freedom* ([RYF](ryf.md)), and they have very specific definition what to them classifies software.

## See Also

- [algorithm](algorithm.md)
- [shitware](shitware.md)
- [rapeware](rapeware.md)