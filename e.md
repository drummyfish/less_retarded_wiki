# E

Euler's number (not to be confused with [Euler number](euler_number.md)), or *e*, is an extremely important and one of the most fundamental [numbers](number.md) in [mathematics](math.md), approximately equal to 2.72, and is almost as famous as [pi](pi.md). It appears very often in mathematics and nature, it is the base of natural [logarithm](log.md), its digits after the decimal point go on forever without showing a simple pattern (just as those of [pi](pi.md)), and it has many more interesting properties.

It can be defined in several ways:

- Number *e* is such number for which a [function](function.md) *f(x) = e^x* (so called [exponential function](exp.md)) equals its own [derivative](derivative.md), i.e. *f(x) = f'(x)*.
- Number *e* is a [limit](limit.md) of the infinite series 1/0! + 1/1! + 1/2! + 1/3! + ... (! signifies [factorial](factorial.md)). I.e. adding all these infinitely many numbers gives exactly *e*.
- Number *e* is a number greater than 1 for which [integral](integration.md) of function 1/x from 1 to *e* equals 1.
- Number *e* is the base of natural [logarithm](log.md), i.e. it is such number *e* for which *log(e,x) = area under the function's curve from 1 to x*.
- ...

*e* to 100 decimal digits is:

2.7182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274...

*e* to 100 binary digits is:

10.101101111110000101010001011000101000101011101101001010100110101010111111011100010101100010000000100...

Just as [pi](pi.md), *e* is a [real](real.md) [transcendental](transcendental.md) number (it is not a root of any polynomial equation) which also means it is an [irrational](irrational.md) number (it cannot be expressed as a fraction of integers). It is also not known whether *e* is a [normal](normal_number.md) number, which would means its digits would contain all possible finite strings, but it is conjectured to be so.

TODO