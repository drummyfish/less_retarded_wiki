# Fun

*See also [lmao](lmao.md).*

Fun is a rewarding lighthearted satisfying feeling you get as a result of doing or witnessing something playful. **We should make fun of anything** -- whenever it's forbidden to make fun of something, something is very wrong; in such case make fun of it even more.

## Things That Are Fun

This is subjective AF, even within a single man this depends on day, hour and mood. Anyway some fun stuff may include:

- the `#capitalistchallenge`: Try to win this game, you have as many shots as you want. Go to some tech store, seek the shop assistant and tell him you are deciding to buy one of two products, ask which one he would recommend. If he recommends the cheaper one you win.
- the [fight culture](fight_culture.md) drinking game: Watch some [modern](modern.md) documentary, take a drink every time someone says the word *fight*. Harder mode: also drink when they say the word *[right](rights_culture.md)*.
- [programming](programming.md)
- [games](game.md) such as [chess](chess.md), [go](go.md) and [shogi](shogi.md), [racetrack](racetrack.md), even vidya gaymes (programming them and/or playing them), but only old+libre ones
- [jokes](jokes.md)
- [open consoles](open_console.md), programmable [calculators](calculator.md) and [fantasy consoles](fantasy_console.md), cool [embedded](embedded.md) programming without [bullshit](bullshit.md) (see also [SAF](saf.md))
- [obfuscating C](ioccc.md), [steganography](steganography.md)
- [marble racing](marble_race.md)
- [Netstalking](netstalking.md)
- Checking out offensive domains like nigger.com, retard.edu etc.
- [trolling](trolling.md)
- [funny programming languages](esolang.md)
- vandalizing [Wikipedia](wikipedia.md), LMAO take a look at this https://encyclopediadramatica.online/Vandal/How-to
- hanging around with friends on the [Island](island.md)
- laughing at normies dealing with [bloat](bloat.md)
- randomly stumbling upon sites on [wiby](https://www.wiby.me/), wikiindex, finding [politically incorrect](political_correctness.md) stuff in old [encyclopedias](encyclopedia.md) and generally just digging out obscure data
- old [Nokia](nokia.md) phones were fun
- [cowsay](cowsay.md)
- [autostereograms](autostereogram.md)
- [math](math.md) and data visualizations, e.g. [fractals](fractal.md), [phase diagrams](phase_diagram.md) (https://yt.artemislena.eu/watch?v=b-pLRX3L-fg), strange [attractors](attractor.md), [procgen](procgen.md), [cellular automata](cellular_automaton.md), plotting wild 2D [functions](function.md), ...
- `#21stcenturychallenge`: If someone on [YouTube](youtube.md) tries to trick you into audience interaction by saying something like "tell me down in the comments...", you have to stop watching YouTube for 14 days. You can cancel this curse by finding a [transsexual](tranny.md) who has no [tattoos](tattoo.md), but you can use any tattooless transsexual only once (not that you will find any though).
- ...