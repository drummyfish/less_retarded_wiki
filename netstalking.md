# Netstalking

Netstalking means searching for obscure, hard-to-find and somehow valuable (even if only by its entertaining nature) information buried in the depths of the [Internet](internet.md) (and similar networks), for example searching for funny photos on Google Streetview (https://9-eyes.com/), unindexed [deepweb](deepweb.md) sites or secret documents on [FTP](ftp.md) servers. Netstalking is relatively unknown in the English-speaking world but is pretty popular in Russian communities.

Netstalking can be divided into two categories:

- **deli-search** (deliberate search): trying to find a specific information, e.g. a specific video that got lost.
- **net-random**: randomly searching for interesting information in places where it is likely to be found.

Techniques of netstalking include port scanning, randomly generating web domains, using advanced search queries and different [search engines](search_engine.md), searching caches and archives and obscure networks such as [darknet](darknet.md) or [gopher](gopher.md).