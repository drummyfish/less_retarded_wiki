# Tangram

{ I made a simple tangram game in [SAF](saf.md), look it up if you want to play some tangram. ~drummyfish }

Tangram is a simple, yet greatly amusing old puzzle [game](game.md) in which the player tries to compose a given shape (of which only silhouette is seen) out of given basic geometric shapes such as [triangles](triangle.md) and [squares](square.md). It is a rearrangement puzzle, a 2D game that's in principle similar e.g. to [Soma cube](soma_cube.md), a game in which, similarly, one makes shapes out of basic parts, but in which the shapes are three dimensional. In Tangram many thousands of shapes can be created from just a few geometric shapes, some looking like animals, people and man made objects. This kind of puzzles have been known for a long time -- the oldest recorded tangram is Archimedes' box (square divided into 14 pieces), over 2000 years old. In general any such puzzle is called tangram, i.e. it is seen as a family of puzzle games, however tangram may also stand for **modern tangram**, a one with 7 polygons which comes from 18th century China and which then became very popular also in the west and even caused a so called "tangram craze" around the year 1818. Unless mentioned otherwise, we will talk about this modern version from now on.

```
 _________________
|\_     big     _/|
|  \_   tri   _/  |
|tri_\_     _/    |
| _/   \_ _/  big |
|<_ sqr _X_   tri |
|  \_ _/tri\_     |
|mid \_______\_   |
| tri  \_ para \_ |
|________\_______\|
```

*Divide square like this to get the 7 tangram pieces. Note that the parallelogram is allowed to be flipped when creating shapes as it has no mirror symmetry (while all other shapes do).*

[LRS](lrs.md) considers tangram to be **one of the best games** as it is extremely [simple](kiss.md) to make and [learn](easy_to_learn_hard_to_master.md), it has practically no [dependencies](dependency.md) (computers, electricity, ... one probably doesn't even have to have the sense of sight), yet it offers countless hours of [fun](fun.md) and allows deep insight, there is [art](art.md) in coming up with new shapes, [math](math.md) in counting possibilities, good exercise in trying to [program](programming.md) the game etc.

Tangram usually comes as a box with the 7 pieces and a number of cards with shapes for the player to solve. Each card has on its back side a solution. Some shape are easy to solve, some are very difficult.

```
          _/|              
         /  |                           _/|\_
        |\_ |                          /  |  \_
        |  \|                         |   |    \_
        | _/                      _   |   |______\
        |/ \_  ________         _/ \_ | _/ \_
       /     \/       /       _/     \|/     \
       \_   _/_______/      _/         \_   _/
         \_/|              /_____________\_/   
        _/  |                  |         _/
      _/    |                  |       _/
    _/      |                  |     _/
  _/        |                  |   _/
 /__________|                  | _/
 \_         |                  |/|
   \_       |                 /  |
     \_     |                 \_ | 
     _/\_   |                  |\|
   _/    \_ |                  |  \_
  /________\|                  |____\            
```

*Two tangram shapes: bunny and stork (from 1917 book Amusements in Mathematics).*

{ I found tangram to be a nice practice for **letting go of ideas** -- sometimes you've got an almost complete solution that looks just beautiful, it looks like THE only one that just has to be it, but you can't quite fit the last pieces. I learned that many times I just have to let go of it, destroy it and start over, usually there is a different, even more beautiful solution. This experience may carry over to practical life, e.g. [programming](programming.md). ~drummyfish }

**Can tangram shapes be [copyrighted](copyright.md)?** As always nothing is 100% clear in law, but it seems many tangram shapes are so simple to not pass the threshold of originality for copyright. Furthermore tangram is old and many shapes have been published centuries ago, making them public domain, i.e. if you find some old, [public domain](public_domain.md) book (e.g. the book *The Fashionable Chinese Puzzle*, *Amusement in Mathematics* or *Ch'i ch'iao hsin p'u: ch'i chiao t'u chieh*) with the shape you want to use, you're most definitely safe to use it. HOWEVER watch out, a collection of shapes, their ordering and/or shapes including combinations of colors etc. may be considered non-trivial enough to spawn copyright (just as collections of colors may be copyrightable despite individual colors not being copyrightable), so do NOT copy whole shape collections.

**Tangram [paradoxes](paradox.md)** are an [interesting](interesting.md) discovery of this game -- a paradox is a shape that looks like another shape with added or substracted piece(s), despite both being composed of the same pieces. Of course geometrically this isn't possible, the missing/extra area is always compensated somewhere, but to a human eye this may be hard to spot (see also [infinite chocolate](infinite_chocolate.md)). New players get confused when they encounter a paradox for the first time, they think they solved the problem but are missing a piece, or have an extra one, while in fact they just made a wrong shape. TODO: example

**Tips for solving**:

- Start by placing pieces you know for certain where they belong (small details that can only be made with the smallest pieces, pieces you deduce that HAVE to be somewhere etc.). This reduces the problem to making a smaller shape from fewer pieces, making it much easier to solve. But BEWARE: sometimes you wrongfully assume some piece in some place because the silhouette "suggests" it, do not fall to this trap.
- At the beginning try to get a sense of scale, sometimes what in the silhouette looks like the big triangle may actually be the middle sized one etc.
- Learn some common patterns, e.g. you can make a rectangle out of the two small triangles and parallelogram. This comes with just solving more puzzles.
- If you have an "almost solution" and can't fit the last few pieces for some time, just destroy it and start over. There are many nicely looking blind paths.
- If in your partial solution you can replace some subshape composed of smaller pieces with the same subshape composed of one larger piece, do it. Having smaller pieces is preferable because you have more flexibility.
- Be careful to make the exact shape you see, sometimes it is possible to make a very similar looking shape that has just a tiny bit different proportions e.g. by rotating the parallelogram.
- ...

TODO: some PD shapes, math, stats, ...

## See Also

- [Soma cube](soma_cube.md)