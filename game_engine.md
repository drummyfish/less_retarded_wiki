# Game Engine

Game engine is a [software](software.md), usually a [framework](framework.md) or a [library](library.md), that serves as a base code for [games](game.md). Such an engine may be seen as a [platform](platform.md) allowing [portability](portablity.md) and offering preprogrammed functionality often needed in games ([3D rendering](3d_rendering.md), [physics engine](physics_engine.md), [I/O](io.md), networking, [AI](ai.md), audio, scripting, ...) as well as [tools](tool.md) used in game development (level editor, [shader](shader.md) editor, 3D editor, ...).

A game engine differs from a general multimedia engine/library, such as [SDL](sdl.md), by its specific focus on games. It is also different from generic rendering engines such as [3D engines](3d_engine.md) like [OpenSceneGraph](osg.md) because games require more than just rendering (audio, AI, physics, ...). While one may use some general purpose technology such as [C](c.md) or [SDL](sdl.md) for creating a game, using a game engine should make the process easier. However, **beware of [bloat](bloat.md)** that plagues most mainstream game engines. [LRS](lrs.md) advises against use of any frameworks, so try to at worst use a game library. Many game programmers such as [Jonathan Blow](jonathan_blow.md) advocate and practice writing own engines for one's games.

## Existing Engines

The following are some notable game engines.

- **[free as in freedom](free_software.md)**
  - **[Allegro](allegro.md)**: 2D [C](c.md) game library. 
  - **[BRender](brender.md)**: Old 3D engine that used mainly [software rendering](sw_rendering.md), used e.g. in Carmageddon, later released under [MIT](mit.md).
  - **[Cube2](cube2.md)**: 3D [voxel](voxel.md) outdoor shooter engine with real-time editable levels, used e.g. in Cube 2: Sauerbraten.
  - **[Godot](godot.md)**: A successful but [bloated](bloat.md) [FOSS](foss.md) ([MIT](mit.md)) [framework](framework.md) engine, alternative to the proprietary [Unity](unity.md) engine, written in [C++](cpp.md), supports many platforms, has 3D/2D graphics and physics engines, scripting in many languages and many "advanced" features. [Capitalist software](capitalist_software.md).
  - *id Tech* engines (engines by [Id software](id_software.md))
    - **id Tech 0**: Simple 2D [raycasting](raycasting.md) engine, written in [ANSI C](C.md), used mainly in [Wolf3D](wolf3d.md) (1992).
    - **id Tech 1**: [BSP](bsp.md) rendering engine used mainly in [Doom](doom.md) and Doom 2.
      - **[Chocolate Doom](chocolate_doom.md)**: Doom engine [fork](fork.md) aiming to be very similar to the vanilla version.
      - **[Crispy Doom](crispy_doom.md)**: Slight enhancement of Chocolate Doom: increased resolution ([640x480](640x480.md)) and removed hardcoded engine limits.
      - **[GZDoom](gzdoom.md)**: Another Doom fork, supports newer OpenGL etc.
      - **[PrBoom](prboom.md)**: Doom engine fork adding e.g. [OpenGL](ogl.md) support.
    - **id Tech 2**: 3D engine used mainly in [Quake](quake.md) and Quake 2, in a modified form ([GoldSrc](goldsrc.md), proprietary) also in [Half Life](half_life.md), features both GPU accelerated and [software rendering](sw_rendering.md).
      - **[Darkplaces](darkplaces.md)**: [Fork](fork.md) of id Tech 2, used e.g. in [Xonotic](xonotic.md).
    - **id Tech 3**: 3D engine used mainly in [Quake 3](quake3.md), sadly dropped [software rendering](sw_rendering.md) support.
      - **[ioquake3](ioquake3.md)**: Fork of id Tech 3 aiming for bugfixes and improvements, e.g. [SDL](sdl.md) integration.
      - **[OpenArena](openarena.md)**: Game-specific fork of id Tech 3.
    - **id Tech 4**: 3D engine used mainly in [Doom 3](doom3.md) and [Quake 4](quake4.md).
      - **[iodoom3](iodoom3.md)**: Fork of id Tech 4, in a similar spirit to ioquake3.
  - **[Irrlicht](irrlicht.md)**: [C++](cpp.md) cross-platform library for 3D games, includes a physics engine and many rendering backends ([OpenGL](ogl.md), [software](sw_rendering.md), [DirectX](directx.md), ...). Used e.g. by [Minetest](minetest.md).
  - **[OpenMW](openmw.md)**: [FOSS](foss.md) remake of the engine of a proprietary [RPG](rpg.md) game [TES: Morrowind](morrowind.md), can be used to make open-world 3D RPG games.
  - **[Panda3D](panda3d.md)**: 3D game engine, under [BSD](bsd_license.md), written in [Python](python.md) and [C++](cpp.md).
  - **[pygame](pygame.md)**: [Python](python.md) 2D game library.
  - **[Raylib](raylib.md)**: [C99](c99.md) 2D/3D game library, relatively minimalist.
  - **[SAF](saf.md)**: Official [LRS](lrs.md) library for tiny and simple portable games.
  - **[Torque3D](torque3d.md)**: 3D game engine in [C++](cpp.md).
- **[proprietary](proprietary.md)** (no go!):
  - **[Build Engine](build_engine.md)**: Old portal rendering "[pseudo 3D](pseudo_3d.md)" engine used mainly in [3D Realms](3d_realms.md) games such as [Duke3D](duke3d.md). It is [source available](source_available.md).
  - *id Tech* engines (engines by [Id software](id_software.md))
    - **id Tech 5**: 3D engine used e.g. in Rage and some shitty Wolfenstein games.
    - **id Tech 6**: 3D engine adding [Vulkan](vulkan.md) support, used e.g. in Doom 2016.
    - **id Tech 7**: 3D engine used e.g. in Doom: Eternal.
  - **[Jedi engine](jedi_engine.md)**: old 90s "2.5D/Pseudo3D" engine best known for being used in *Dark Forces * (Star Wars game).
  - **[GameMaker](gamemaker.md)**: Laughable toy for non-programmers.
  - **[RAGE](rage.md)**: 3D open-world engine developed and used by [Rockstar](rockstar.md) for games such as [GTA](gta.md).
  - **[Source](source_engine.md)**: 3D engine by [Valve](valve.md) used in games such as [Half Life 2](half_life2.md).
  - **[Source2](source2_engine.md)**: Continuation of Valve's source engine with added support of [VR](vr.md) and other shit.
  - **[Unity](unity.md)**: Shitty nub all-in-one 3D game engine, very [bloated](bloat.md) and [capitalist](capitalist_software.md), extremely popular among [coding monkeys](code_monkey.md), includes [ads](ad.md).
  - **[Unreal Engine](unreal_engine.md)**: One of the leading proprietary 3D game engines developed alongside [Unreal Tournament](unreal_tournament.md) games, EXTREMELY [BLOATED](bloat.md) and [capitalist](capitalist_software.md), known for hugely overcomplicated rendering (advertised as "[photorealistic](photorealism.md)").
