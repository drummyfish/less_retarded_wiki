# Bloat

Bloat is a very wide term that in the context of [software](software.md) and [technology](tech.md) means overcomplication, unnecessary complexity and/or extreme growth in terms of source code size, overall complexity, number of [dependencies](dependency.md), [redundancy](redundancy.md), unnecessary and/or useless features (e.g. [feature creep](feature_creep.md)) and resource usage, all of which lead to inefficient, badly designed technology with [bugs](bug.md) (crashes, unusable features, memory leaks, [security](security.md) vulnerabilities, ...), as well as great [obscurity](obscurity.md), ugliness, **loss of [freedom](free_software.md)** and waste of human effort. Simply put bloat is burdening [bullshit](bullshit.md). Bloat is extremely bad and one of the greatest technological issues of today. Creating bloat is bad engineering at its worst and unfortunately it is what's absolutely taking over all technology nowadays, mostly due to [capitalism](capitalism.md) causing commercialization, [consumerism](consumerism.md), rushed "[just works](just_werks.md)" products, creating demand for newer hardware and so on, also allowing incompetent people ("let's push more women/minorities into programming") trying to take on jobs they are in no way qualified to do.

A related but different term is **bloatware**; it's more commonly used among normie users and stands for undesirable programs that eat up computer resources, usually being preinstalled by the computer manufacturer etc. Further on we'll rather focus on bloat as defined before.

TODO: history of bloat?

[LRS](lrs.md), [suckless](suckless.md) and some others rather small groups are trying to address the issue and write software that is good, [minimal](minimalism.md), reliable, efficient and well functioning. Nevertheless our numbers are very small and in this endeavor we are basically standing against the whole world and the most powerful tech [corporations](corporation.md). The issue lies not only in capitalism pushing bloat but also in common people not seeing the issue (partly due to the capitalist propaganda promoting [maximalism](maximalism.md)), no one is supporting the few people who are genuinely trying to create good tools, on the contrary such people often face hostility from the mainstream.

The issue of bloat may of course appear outside of the strict boundaries of computer technology, nowadays we may already observe e.g. **[science bloat](science_bloat.md)** -- science is becoming so overcomplicated (many times on purpose, e.g. by means of [bullshit](bullshit.md) science) that 99% people can NOT understand it, they have to BELIEVE "scientific authorities", which does not at all differ from the dangerous blind religious behavior. Any time a new paper comes out, chances are that not even SCIENTISTS from the same field but with a different specialization will understand it in depth and have to simply trust its results. This combined with self-interest obsessed society gives rise to [soyence](soyence.md) and large scale brainwashing and spread of "science approved" propaganda.

Some metrics traditionally used to measure bloat include **[lines of source code](loc.md)**, **[cyclomatic complexity](cyclomatic_complexity.md)** (kind of "number of ways the code may take"), **[programming language](programming_language.md) used** (some languages are bloated themselves and inherently incapable of producing non-bloat, also choice of language indicates the developer's priorities, skills etc.), **number of [dependencies](dependency.md)** (packages, libraries, hardware, ...), **binary size** (size of the compiled program), **compile time**,  **resource usage** ([RAM](ram.md), CPU, network usage, ...), **performance** ([FPS](fps.md), responsiveness, ...), **anti features** ([GUI](gui.md), [DRM](drm.md), auto updates, file formats such as [XML](xml.md), ...), [portability](portability.md), number of implementations, size of specification, number of developers and others. Some have attempted to measure bloat in more sophisticated ways, e.g. the famous *web bloat score* (https://www.webbloatscore.com/) measures bloat of websites as its total size divided by the page screenshot size (e.g. [YouTube](youtube.md) at 18.5 vs suckless.org at 0.386). It has been observed that **software gets slower faster than hardware gets faster**, which is now known as [Wirth's law](wirths_law.md); this follows from [Moore's law](moores_law.md) (speed of hardware doubles every 24 months) being weaker than [Gate's](bill_gates.md) law (speed of software halves every 18 months); or in other words: the stupidity of [soydevs](soydev.md) outpaces the brilliancy of geniuses.

Despite this there isn't any completely objective measure that would say "this software has exactly X % of bloat", bloat is something judged based on what we need/want, what tradeoffs we prefer etc. The answer to "how much bloat" there is depends on the answer to **"what really is bloat?"**. To answer this question most accurately we can't limit ourselves to simplifications such as [lines of code](loc.md) or number of package dependencies -- though these are very good estimates for most practical purposes, a more accurate insight is obtained by carefully asking what *burdens* and *difficulties* of ANY kind come with given technology, and also whether and how much of a necessary evil they are. Realize for example that if your software doesn't technically require package X to run or be compiled, package X may be [de facto](de_facto.md) required for your software to exist and work (e.g. a pure multiplayer game client won't have the server as a dependency, but it will be useless without a server, so de facto all bloat present in the server is now in a wider sense also the client's burden). So if you've found a program that's short and uses no libraries, you still have to check whether the language it is written in isn't bloated itself, whether the program relies on running on a complex platform that cannot be implemented without bloat, whether some highly complex piece of hardware (e.g. [GPU](gpu.md) or 8GB of [RAM](ram.md)) is required, whether it relies on some complex Internet service etc. You can probably best judge the amount of bloat most objectively by asking the following: if our current technology instantly disappeared, how hard would it be to make this piece of technology work again? This will inevitably lead you to investigating how hard it would be to implement all the dependencies etc.

For a quick overview let us average some data over time -- the table that follows shows growth of system requirements and sizes and averages them to give an estimate of bloat ratio with respect to the first row. Please note some data in the table may not be completely accurate, interpolation/extrapolation was used for missing values, we're only making an estimate after all, but still notice our computing resource usage already grew almost 2000 times despite computers being generally slower and less responsive from the user's perspective.

| year  | avg. webpage size (KB) | Windows min RAM MB/CPU MHz/HDD MB | Debian min RAM MB/HDD MB | FPS game min RAM MB/CPU MHz/HDD MB | Blender (win zip KB) | % of base |
| ----- | ---------------------- | --------------------------------- | ------------------------ | ---------------------------------- | -------------------- | --------- |
| 1993  | 4                      | 3, 25, 9                          | 4, 20                    | 4, 30, 24 (Doom)                   | 100 (extrap.)        | 100       |
| 1994  | 8                      | 3, 25, 9                          | 4, 20                    | 4, 33, 15 (Heretic)                | 172                  | 114       |
| 1995  | 14                     | 12, 25, 90                        | 4, 20                    | 4, 33, 16 (Descent)                | 307                  | 263       |
| 1996  | 23                     | 16, 33, 128                       | 4, 80                    | 8, 66, 25 (Duke Nukem 3D)          | 442                  | 412       |
| 1997  | 34                     | 16, 33, 128                       | 4, 90                    | 16, 90, 25 (Quake II)              | 577                  | 486       |
| 1998  | 44                     | 16, 33, 128                       | 4, 90                    | 24, 133, 400 (Half Life)           | 712                  | 715       |
| 1999  | 53                     | 32, 133, 1000                     | 5, 100                   | 64, 233, 70, 8M GPU (Quake III)    | 849                  | 1817      |
| 2000  | 63                     | 32, 133, 1000                     | 5, 100                   | 32, 233, 200, 4M GPU (Daikatana)   | 1170                 | 1848      |
| 2001  | 74                     | 64, 233, 1500                     | 5, 100                   | 64, 300, 600, OGL GPU (Serious Sam)| 1323                 | 2863      |
| 2002  | 83                     | 64, 233, 1500                     | 12, 110                  | 256, 500, 2000, 32M GPU (UT 2003)  | 1501                 | 4055      |
| 2003  | 93                     | 64, 233, 1500                     | 12, 120                  | 128, 600, 1400, 32M GPU (COD)      | 1704                 | 3569      |
| 2004  | 115                    | 64, 233, 1500                     | 12, 150                  | 256, 1200, 6000, DX7 GPU (HL2)     | 4399                 | 6345      |
| 2005  | 189                    | 64, 233, 1500                     | 24, 450                  | 512, 1700, 5000, 64M GPU (FEAR)    | 6353                 | 7296      |
| 2006  | 212                    | 384, 800, 15000                   | 24, 450                  | 512, 2000, 2000, 64M GPU (Prey)    | 7277                 | 22589     |
| 2007  | 260                    | 384, 800, 15000                   | 64, 1000                 | 1024, 2000, 12000, 64M GPU (Crysis)| 8639                 | 28667     |
| 2008  | 312                    | 384, 800, 15000                   | 64, 1000                 | 1024, 2600, 12000, 256M GPU (FC2)  | 12778                | 29411     |
| 2009  | 443                    | 1024, 1000, 16000                 | 64, 1000                 | 2048, 2400, 13000, 128M GPU (LFD2) | 13683                | 36063     |
| 2010  | 481                    | 1024, 1000, 16000                 | 64, 1000                 | 2048, 2400, 11000, 256M GPU (BS2)  | 25059                | 36462     |
| 2011  | 657                    | 1024, 1000, 16000                 | 64, 1000                 |2048, 3000, 8000, 128M GPU (Portal2)| 32398                | 36586     |
| 2012  | 831                    | 1024, 1000, 16000                 | 64, 1000                 | 2048, 2600, 15000, 512M GPU (FC3)  | 45786                | 41143     |
| 2013  | 1102                   | 1024, 1000, 16000                 | 64, 1000                 |3000, 2400, 17000, 1G GPU (Crysis 3)| 67787                | 47168     |
| 2014  | 1249                   | 1024, 1000, 16000                 | 64, 1000                 | 4096, 2600, 30000, 1G GPU (FC4)    | 81676                | 57147     |
| 2015  | 1466                   | 1024, 1000, 32000                 | 128, 2000                | 6000, 2900, 60000, 1G GPU (CODBO3) | 104139               | 95734     |
| 2016  | 1502                   | 4096, 1000, 64000                 | 128, 2000                |8192, 3100, 45000, 2G GPU (Doom2016)| 107840               | 141286    |
| 2017  | 1681                   | 4096, 1000, 64000                 | 128, 2000                | 8192, 3300, 90000, 2G GPU (CODWW2) | 116121               | 161379    |
| 2018  | 1848                   | 4096, 1000, 64000                 | 128, 2000                | 8192, 3100, 40000, 2G GPU (FC5)    | 113915               | 140675    |
| 2019  | 1980                   | 4096, 1000, 64000                 | 550, 850                 | 6000, 3400, 75000, 2G GPU (BL3)    | 153290               | 154626    |
| 2020  | 2042                   | 4096, 1000, 64000                 | 550, 850                 |8192, 3100, 50000, 4G GPU (Doom: E) | 197632               | 154179    |
| 2021  | 2173                   | 4096, 1000, 64000                 | 780, 920                 |8192, 3100, 60000, 4G GPU (FC6)     | 221865               | 161706    |
| 2022  | 2280                   | 4096, 1000, 64000                 | 780, 920                 |8192, 3300, 125000, 2G GPU (CODMWF2)| 248477               | 191785    |

One of a very frequent questions you may hear a noob ask is **"How can bloat limit software freedom if such software has a [free](free_software.md) (or "[FOSS](foss.md)") [license](license.md)?"** Bloat [de-facto](de_facto.md) limits some of the four essential freedoms (to use, study, modify and share) required for a software to be free. A free license grants these freedoms legally, but if some of those freedoms are subsequently limited by other circumstances, the software becomes effectively less free. It is important to realize that **complexity itself goes against [freedom](freedom.md)** because a more complex system will inevitably reduce the number of people being able to execute freedoms such as modifying the software (the number of programmers being able to understand and modify a trivial program is much greater than the number of programmers being able to understand and modify a highly complex million [LOC](loc.md) program -- see [freedom distance](freedom_distance.md)). This is not any made up reason, it is actually happening and many from the free software community try to address the issue, see e.g. [HyperbolaBSD](hyperbolabsd.md) policies on accepting packages which rejects a lot of popular "legally free" software on grounds of being bloat ([systemd](systemd.md), dbus, zstd, protobuf, [mono](mono.md), https://wiki.hyperbola.info/doku.php?id=en:philosophy:incompatible_packages). As the number of people being able to execute the basic freedom drops, we're approaching the scenario in which the software is de-facto controlled by a small number of people who can (e.g. due to the cost) effectively study, modify and maintain the program -- and a program that is controlled by a small group of people (e.g. a corporation) is by definition [proprietary](proprietary.md). If there is a web browser that has a free license but you, a lone programmer, can't afford to study it, modify it significantly and maintain it, and your friends aren't able to do that either, when the only one who can practically do this is the developer of the browser himself and perhaps a few other rich corporations that can pay dozens of full time programmers, then such browser cannot be considered free as it won't be shaped to benefit you, the user, but rather the developer, a corporation.

**How much bloat can we tolerate?** We are basically trying to get the most for the least price. The following diagram attempts to give an answer:

```
        external
       "richness"
           A   
   shiny   |    :                :
  bullshit | NO :      YES       : NO
           |    :                :                         ____... .
   luxury  |    :                :             ___________/
           |    :                :    ________/
           |    :              __:___/        \__________
    very   |    :         ____/  :                       \______
   useful  |    :     ___/       :                              \_..
           |    :  __/           :                      path of degeneracy
           |    :_/              :
   useful  |   _:                :
           |  | :                :
           | /  :                :
    does   ||   :                :
   nothing +-----------------------------------------------------> internal complexity
           trivial  simple   solo        big        huge       gigantic
                          manageable

```

The **path of [degeneracy](degenerate_software.md)** drawn in the graph shows how from a certain breaking point (which may actually appear at different places, the diagram is simplified) many software projects actually start getting less powerful and useful as they get more complex -- not all, some project really do stay on the path of increasing their "richness", but this requires great skills, experience, expertise and also a bit of lucky circumstances; in the zone of huge complexity projects start to get extremely difficult to manage -- non-primary tasks such as organization, maintenance and documentation start taking up so many resources that the primary task of actually programming the software suffers, the project crumbles under its own weight and the developers just try to make it fall slower. This happens mostly in projects made by incompetent [soydevs](soydev.md), i.e. most today's projects. { Thanks to a friend for pointing out this idea. ~drummyfish }

Please note there may arise some disagreement among minimalist groups about where the band is drawn exactly, especially old Unix hackers could be heard arguing for allowing (or even requiring) even trivial programs, maybe as long as the source code isn't shorter than the utility name, but then the discussion might even shift to questions like "what even is a program vs what's just a 10 characters long line" and so on.

As a quick [heuristic](heuristic.md) for judging programs you can really take a look at the [lines of code](loc.md) (as long as you know it's a simplification that ignores dependencies, formatting style, language used etc.) and use the following classes (basically derived from how [suckless](suckless.md) programs are often judged):

- < 10: Extremely small but may be useful, may be also too trivial for such small size to be justifiable, can aim to be completely bug-free. Example could be the [cat](cat.md) program.
- 11 to 100: Very small, can be debugged to a great level, many greatly useful utilities, e.g. [compression](compression.md) programs, may fit this class.
- 101 to 1000: Small "bigger" kinds of programs, often very minimalist implementations of programs that are usually not minimalist in nature like window managers, interactive text editors, web browsers and so on. Simplified version of [comun](comun.md) language, called *minicomun*, fits here.
- 1001 to 5000: Still considered small, a bit more "feature rich" kind of previous class, you may find minimalist 3D games here, quite powerful programming languages, libraries handling complex file formats (that weren't designed to be minimalist) etc. Currently a lot of [LRS](lrs.md) programs like [SAF](saf.md), [small3dlib](small3dlib.md) and [comun](comun.md) would fall here.
- 5001 to 10000: Often imposed upper limit on suckless programs, these programs aren't the smallest possible but may still be called minimalist, they are easily manageable by a single man without much hassle, anyone can modify them and there is a comfortable margin for implementing many "comfort" and fancy features that aren't complete [BS](bullshit.md). [Anarch](anarch.md) might be given as an example (if we subtract lines of code taken by game data and count only pure engine code).
- 10001 to 100000: Here things start to be called real bloat but may still be accepted as a compromise, not an "insanely bloated" program, we have to judge on a case by case basis as the transition towards bloat is gradual, but generally projects here must focus on not growing bigger, priority should be on minimizing. We have to consider anything here bloat unless proven otherwise. Minimalist projects end up here when trying to combine minimalism with some mainstream concept, e.g. implementing a complete operating system with all the standard features, trying to reimplement some mainstream, non-minimalist program etc. Example is [tcc](tcc.md), the C compiler that has a little over 20000 LOC. Also many "good old" mainstream programs like [Doom](doom.md) fall here.
- more: Basically just [bloat](bloat.md), some operating systems can perhaps argue they are comparatively small even within this category, but as a matter of fact very few people can manage a codebase this big, issues of bloat start to play a very significant role here, the project should most likely be split or rewritten from scratch in much more simplified way.

Yes, **bloat is also unecological** and no, it cannot be fixed by replacing fossil fuel cars with cars that run on grass and plastic computers by computers made from recycled cardboards mixed with composted horse shit. It is the immense volume of human ACTIVITY that's required by the bloated technology all around the globe that's inherently unecological by wasting so much effort, keeping focus on maximalism, growth and preventing us from frugality and minimizing resource waste. Just as any other [bullshit](bullshit.md) that requires immense resources to just keep maintaining -- great complexity is just absolutely incompatible with ecology and as much as you dislike it, to achieve truly eco-friendly society we'll have to give up what we have now in favor of something orders of magnitude more simple and if you think otherwise, you are just yet too unexperienced (or remained purposefully ignorant) to have seen the big picture already. Consider that your program having bullshit dependencies such as [Python](python.md), [JavaScript](js.md), [C++](cpp.md), [Java](java.md), [OpenGL](opengl.md), [Vulkan](vulkan.md), [GPU](gpu.md), [VR](vr.md) sets, gigabytes of [RAM](ram.md) etcetc. requires having the inherently unecological system up, it needs millions of people doing bullshit jobs that are inherently wasting resources, increasing CO2 and making them not focus on things that have to be done -- yes, even if we replace plastic straws with [paper straws](greenwashing.md). All those people that make the thousand pages standards that are updated every year, reviews of those standards, writing tons and tons of tests for implementations of those standards, electing other people to make those standards, testing their tests, implementing the standards themselves, optimizing them, all of that collectively needing many billions of lines of code and millions of hours of non-programming activities, it all requires complex bureaucracy, organization and management (complex [version control systems](vcs.md), wikis, buildings, meeting spaces, ...) and communication tools and tons of other bullshit recursively spawning more and more waste -- all of these people require cars to go to work every day (even if some work from home, ultimately only a few can work from home 100% of the time and even so millions others need to physically go to factories to make all those computers, electricity, chair, food and other things those people need), they require keeping a high bandwidth 100% uptime global Internet network maintained, all of this requiring extra buildings, offices, factories, roads, buildings for governments overseeing the building of those buildings, maintenance of those roads etcetc. A newbie programmer (99.99999% programmers in the field nowadays) just don't see all this because they lack the big picture, a woman forced into programming has hard time comprehending an if statement, how do you expect her to see the deep interconnections between technology and society -- she may know that OpenGL is "something with graphics" and it's just there on every computer by default, she can't even picture the complexity that's behind what she sees on the screen. Hence the overall retardation. You just cannot have people living ecologically and at the same time have what we have now. So yes, by supporting and/or creating bloat you are killing the planet, whether you agree with it or not. No, you can't find excuses out of this, no, paper straws won't help, just admit you love point and click "programming without math" of your own shitty Minecraft clones in Godot even for the price of eliminating all life on Earth, that's fine (no it's not but it's better to just not bullshit oneself).

{ Fucking hell this shit has gone too far with the newest supershit gayme called Cities Skyline II, I literally can't anymore, apparently the game won't run smoothly even on Earth's most advanced supercomputer because, as someone analyzed, the retarddevs use billion poly models for pedestrians without any [LOD](lod.md), I bet they don't even know what it is, they probably don't even know what a computer is, these must be some extra retarded soy idiots making these games now. Though I knew it would come to this and that it will be getting yet much worse, I am always still surprised, my brain refuses to believe anyone would let such a piece or monstrous shit to happen. This just can't be real anymore. ~drummyfish }

## Typical Bloat

The following is a list of software usually considered a good, typical example of bloat. However keep in mind that bloat is a relative term, for example [vim](vim.md) can be seen as a minimalist suckless editor when compared to mainstream software ([IDEs](ide.md)), but at the same time it's pretty bloated when compared to strictly [suckless](suckless.md) programs.

- [Web](web.md) since the onset of "web 2.0" has been steadily becoming more and more bloated with things such as Adobe Flash and [JavaScript](javascript.md) (and billions of its web frameworks). By today the situation about web bloat is reaching almost unbearable levels, especially in [modern](modern.md) sites such as [YouTube](youtube.md). For a great read see [The Website Obesity Crisis](https://idlewords.com/talks/website_obesity.htm).
- Ads, [spyware](spyware.md), [DRM](drm.md), anti-cheats, anti-viruses, anti-sharing, anti-repair and other anti-user "features" are bloat.
- Desktop environments such as [KDE](kde.md) and [GNOME](gnome.md). The concept of a [desktop environment](de.md) itself is often considered bloat.
- [Windows](windows.md): one of the best examples of how software should NOT be done.
- [Blender](blender.md): quite useful [FOSS](foss.md) 3D editor which however integrates things like a whole video editor, game engine, several renderers, scripting language with text editor and so on.
- [CMake](cmake.md): gigantic [build system](build_system.md) that currently sits on top of a sky-high sandwich of other build systems, its number of dependencies is bigger than the number of retards in observable universe (known as *[drummyfish's](drummyfish.md) number*).
- [D-Bus](dbus.md)
- [Docker](docker.md)
- [Electron](electron.md): [GUI](gui.md) [framework](framework.md) infamous for its huge resource consumption.
- [flatpak](flatpak.md): Absolutely horrible "application distribution/execution platform???" with pakcage management, [sandboxes](sandbox.md) and all that kind of shit.
- [Systemd](systemd.md): Huge anti-[unix](unix_philosophy.md) do-it-all system taking over [GNU](gnu.md)/[Linux](linux.md).
- [Virtual machines](vm.md)/environments/sandboxes, big abstraction sandwiches (e.g. program running in an interpreter running in a sandbox inside web browser that's running in a virtual machine that's running on an operating system).
- [Firefox](firefox.md), [Chromium](chromium.md) and other mainstream web browsers.
- [Java](java.md), [Python](python.md) and similar languages.
- [IDEs](ide.md) such as [VSCode](vscode.md) or [NetBeans](netbeans.md).
- Big [game engines](game_engine.md) such as [Unreal](unreal_engine.md), [Unity](unity.md) or [Godot](godot.md).
- Practically all commercial [games](games.md) made in the [21st century](21st_century.md) such as [World of Warcraft](wow.md), Call of Duty etc.
- [pulse audio](pulse.md)
- office programs (e.g. M$ Office and [LibreOffice](libreoffice.md)) and a lot of [rich text](rich_text.md)
- [Neural networks](neural_network.md) aka "AI" that is forced into everything nowadays.
- ...

Some of these programs may be replaced with smaller bloat that basically does the same thing (e.g. produces the same output) just with less bullshit around, for example Libreoffice with [Ted](ted.md), [Godot](godot.md) with [Irrlicht](irrlicht.md), Firefox with [badwolf](badwolf.md) etc., however many times the spectacular pompous results these programs produce just cannot essentially be reproduced by anything minimal, wanting to achieve this is really a beginner mistake, the same as wanting to achieve the "Windows experience" on a GNU system. You will never be able to make an Unreal Engine style graphics with a minimalist game engine, just like you won't be able to shoot up your school with well written poetry (in both cases the former is something bad that however most Americans want to do, the latter is something truly good they should want instead). To truly get rid of bloat one has to become able to use truly minimal programs; this means unlearning the indoctrination that "bigger results are better", one has to understand that minimal results themselves are superior AND in addition allow using superior programs (i.e. minimal ones).

## Medium And Small Bloat

Besides the typical big programs that even normies admit are bloated there exists also a smaller bloat which many people don't see as such but which is nevertheless considered unnecessarily complex by some experts and/or idealists and/or hardcore minimalists, including [us](lrs.md).

Small bloat is a subject of popular [jokes](joke.md) such as "OMG he uses a unicode font -- BLOAT!!!". These are good jokes, it's nice to make fun out of one's own idealism. But watch out, this doesn't mean small bloat is only a joke concept at all, it plays an important role in designing good technology. When we identify something as *small bloat*, we don't necessarily have to completely avoid and reject that concept, we may just try to for example make it optional. In context of today's PCs using a Unicode font is not really an issue for performance, memory consumption or anything else, but we should keep in mind it may not be so on much weaker computers or for example post-[collapse](collapse.md) computers, so we should try to design systems that don't [depend](dependency.md) on Unicode.

Also remember that relatively small libraries for things that are easily done without a library, such as [fixed point](fixed_point.md) arithmetic, are also bloat.

Small/medium bloat includes for example:

- [floating point](float.md) (complex standard with design issues, requires special hardware for acceleration, [fixed point](fixed_point.md) is better)
- config files (and other unnecessary file I/O that requires a file I/O library, not all computers have file systems, configs should be part of source code)
- [directories](directory.md) (just have all files on the same level and prefix their file names to organize them)
- library [linking](linking.md) ([header only](header_only.md) libraries are better)
- any [GPU](gpu.md), [OpenGL](opengl.md) (complex hardware and specifications, not all computers have complex GPUs, [software rendering](software_rendering.md) is better)
- [Unicode](unicode.md) (big specification requiring special libraries and big fonts, [ASCII](ascii.md) is better)
- [antialiasing](antialiasing.md) (just ignore [aliasing](aliasing.md), use low resolution textures etc.)
- 64 bit architectures (they only exist to allow ungodly amounts of RAM, 32 bits completely suffice for any computation, many times even 16 or 8 bits are enough)
- [proportional font](proportional_font.md) (fixed width font is better)
- [linking](linking.md), build systems/scripts, [makefiles](make.md), directories and multiple source code files (just using a compiler or a few-line building shell script, single file source code, [header only](header_only.md) libraries and [single compilation unit](single_compilation_unit.md) programs are better)
- [infix notation](infix_notation.md) ([postfix notation](postfix_notation.md) is better)
- any [GUI](gui.md), [window managers](wm.md) (pure text mode is better)
- [operating system](os.md) ([bare metal](bare_metal.md) is better)
- [multithreading](multithreading.md), [parallelism](parallelism.md), [virtual memory](virtual_memory.md), ...
- [encryption](encryption.md), [security](security.md), [memory safety](memory_safety.md) (just don't care and/or don't handle sensitive data with computers connected to the internet, don't live in a shitty society)
- [X11](x11.md) (just pure screen drawing is better)
- [database](database.md) software (plain files are better, see [flatfile](flatfile.md))
- [C](c.md) (something in between C and [brainfuck](brainfuck.md) would is likely ideal, e.g. [comun](comun.md) or [Forth](forth.md))
- [glibc](glibc.md), [gcc](gcc.md), [clang](clang.md) etc. (better alternatives are [tcc](tcc.md), [musl](musl.md), [uclibc](uclibc.md) etc.)
- letter accents/diacritics (can normally be ignored in most languages that use them)
- [jpg](jpg.md), [png](png.md), [svg](svg.md) and similar formats (e.g. [ppm](ppm.md) or [farbfeld](farbfeld.md) is better)
- [syntax highlight](syntax_highlight.md), text formatting, [rich text](rich_text.md) and just [colors](color.md) anywhere they aren't absolutely necessary
- [html](html.md), [markdown](md.md) ([plain text](plaintext.md) is better)
- [x86](x86.md) instruction set (TODO: what's better? probably some [RISC](risc.md))
- any non-[public-domain](public_domain.md) license (any legal burden introduced by a license is unnecessary bloat)
- dynamic [linking](linking.md)/libraries (static linking is better, see [Stali](stali.md))
- [web](web.md) 1.0, [gemini](gemini.md) ([gopher](gopher.md) or [FTP](ftp.md) is better)
- [mouse](mouse.md) (keyboard is better)
- [TCP](tcp.md) ([UDP](udp.md) is probably better)
- [vim](vim.md) (things like [ed](ed.md) are probably better?)
- sound (picture is usually enough)
- high resolution (640x480 is probably the maximum you'll ever need, lower resolution takes less RAM and makes rendering faster)
- [true color](true_color.md) (256 colors, e.g. [332](332.md) palette, is better, even 1 bit displays suffice for most things), high [FPS](fps.md) (25 is more than enough), high resolution ([320 x 240](320_240.md) is more than enough) etc.
- [GNU](gnu.md) Unix utils (things like [busybox](busybox.md) or [sbase](sbase.md) are probably better)
- [data types](data_type.md) (untyped or single type is better, everything can be just a [number](number.md))
- [package managers](package_manager.md) (just don't use them, install just a few programs manually, or at least make package managers as simple as possible)
- [electricity](electricity.md) ([mechanical](mechanical.md) computers may be just fine)
- [computers](computer.md) (pen and paper or counting with [rocks](rock.md) is better)
- anything [wireless](wireless.md) ([wifi](wifi.md), mice, ...)
- ...

## Non-Computer Bloat

The concept of bloat can be applied even outside the computing world, e.g. to non-computer technology, [art](art.md), [culture](culture.md), [law](law.md) etc. Here it becomes kind of synonymous with [bullshit](bullshit.md), but using the word *bloat* says we're seeing the issue from the point of view of someone acquainted with computer bloat. Examples include:

- [clothes](clothes.md)
- decorations (body, house, ...)
- [cars](car.md)
- using languages other than [English](english.md) or [Esperanto](esperanto.md)
- luxury (big house, yacht with a swimming pool, ...)
- having electricity at home
- ...

## See Also

- [harmful](harmful.md)
- [maximalism](maximalism.md)
- [shitware](shitware.md)
- [obscurity](obscurity.md)
- [shit](shit.md)
- [cyclomatic complexity](cyclomatic_complexity.md)
- [freedom distance](freedom_ditance.md)