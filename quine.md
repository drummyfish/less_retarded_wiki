# Quine

Quine is a nonempty [program](program.md) that when executed prints its own [source code](source_code.md). It takes no input, just prints out the source code, without "[cheating](cheating.md)" such as reading the source code from a file. A [self-replicating](self_replication.md) program of a sort, quine performs a task similar to that of a robot capable of creating copies of itself (after all we humans are such robots). The name *quine* refers to the philosopher Willard Quine and his paradox that exhibits a structure similar to self-replicating programs. Quine belong to the class of standard/[fun](fun.md)/[interesting](interesting.md) programs with which we like to compare [programming languages](programming_language.md), it's a cousin of such superstars as [hello world](hello_world.md), [compiler bombs](compiler_bomb.md), [99 bottles of beer](99_bottles.md) or [fizzbuzz](fizzbuzz.md), but out of these quine is of the greatest interest to mathematicians.

From [mathematical](math.md) viewpoint quine is a **fixed point** of a [function](function.md) (not to be confused with [fixed_point arithmetic](fixed_point.md)) represented by the [programming language](programming_language.md). That is once we say the programming language is a function f(x), where *x* is source code and the function's output is the program's output, quine is such *x* that *f(x) = x*. **A quine can be written in any [Turing complete](turing_completeness.md) [language](programming_language.md)**, the proof comes from the *fixed point theorem* (which says functions satisfying certain conditions always have a fixed point).

Attempt mathematically similar to making quines include e.g. making self matching [regular expressions](regex.md) (for this task to be non-trivial the regex has to be for example enclosed between `/`s). Yet another similar challenge is a [polyglot](polyglot.md) program -- one that is a valid program in several languages -- some programs can be quines and polyglots at the same time, i.e. **polyglot quines**.

The challenging part of writing quines lies indeed in the [self reference](self_reference.md) -- normally we cannot just single-line print a string literal containing the source because that string literal would have to contain itself, making it [infinite](infinity.md) in length. The idea commonly used to solve the problem is following:

1. On first line start a definition of string *S*, later copy-paste to it the string on the second line.
2. On second line put a command that prints the first line, assigning to *S* the string in *S* itself, and then prints *S* (the second line itself).

Yet a stronger quine is so called **radiation hardened quine**, a quine that remains quine even after any one character from the program has been deleted (found here in [Ruby](ruby.md): https://github.com/mame/radiation-hardened-quine). Other plays on the theme of quine include e.g. a program that produces a bigger program which will again produce yet bigger program etc.

Another extension of a quine is **multiquine** -- this is NOT a polyglot quine! Multiquine is a quine written in some programming language *L0*; under normal circumstances this program behaves like a normal quine, but it has an extra feature: when passed a parameter *N* (e.g. through [CLI](cli.md) flag or through standard input), it will print a program in another language, *LN*, which itself is this multiquine (so it can again be used to get back the program in *L0* and so on). I.e. a multiquine is a quine which can switch between several languages.

In the [Text](plaintext.md) [esoteric programming language](esolang.md) every program is a quine (and so also a radiation hardened one).

## List Of Quines

**Brainfuck**: not short, has over 2100 characters.

**[C](c.md)**:

```
#include <stdio.h>
char s[] = "#include <stdio.h>%cchar s[] = %c%s%c;%cint main(void) { printf(s,10,34,s,34,10,10); return 0; }";
int main(void) { printf(s,10,34,s,34,10,10); return 0; }
```

**[comun](comun.md)**:

```
0 46 32 34 S 34 32 58 83 S --> S: "0 46 32 34 S 34 32 58 83 S --> " .
```

**[Python](python.md)**:

```
s="print(str().join([chr(115),chr(61),chr(34)]) + s + str().join([chr(34),chr(10)]) + s)"
print(str().join([chr(115),chr(61),chr(34)]) + s + str().join([chr(34),chr(10)]) + s)
```

**text**:

```
This is a quine in text.
```

TODO: more, make biquine of C and comun

## See Also

- [polyglot](polyglot.md)