# Shader

Shader is a program running on the [graphics processing unit](gpu.md) (GPU), typically in many parallel instances as to utilize the GPU's highly parallel nature. As such they are simple to mid complexity programs. The word *shader* is also used more loosely to stand for any specific effect, material or look in 3D graphics (e.g. [games](games.md)), as shaders are usually the means of achieving such effects.

Shaders are normally written in a special **shading language** such as [GLSL](glsl.md) in the [OpenGL](opengl.md) [API](api.md), [HLSL](hlsl.md) ([proprietary](proprietary.md)) in [Direct3D](d3d.md) API or the Metal shading language ([proprietary](proprietary.md)) in [Metal](metal.md) API. These languages are often similar to [C](c.md) with some additions (e.g. vector and matrix data types) and simplifications (e.g. no function [recursion](recursion.md)). High level [frameworks](framework.md) like [Blender](blender.md) many times offer [visual programming](visual_programming.md) (point-n-click) of shaders with graph/node editors.

Initially (basically early 2000s) shaders were used only for graphics, i.e. to transform 3D vertices, draw triangles and compute pixel colors. Later on as GPUs became more general purpose ([GPGPU](gpgpu.md)), flexibility was added to shaders that allowed to solve more problems with the GPU and eventually general *compute* shaders appeared (OpenGL added them in version  3.3 in 2010).

To put shaders in the context, the flow of data is this: a [CPU](cpu.md) uploads some data (3D models, textures, ...) to the GPU and then issues a draw command -- this makes the GPU start its **[pipeline](pipeline.md)** consisting of different **stages**, e.g. the vertices of 3D models are transformed to screens space (the vertex stage), then triangles are generated and rasterized (the shading stage) and the data is output (on screen, to a buffer etc.). Some of these stages are programmable and so they have their own type of a shader. The details of the pipeline differ from API to API, but in general, depending on the type of data the shader processes (the stage), we talk about:

- **vertex shaders**: Perform per-vertex computations on 3D models, typically their transformation from their world position to the position in the camera and screen space.
- **fragment/pixel shaders**: Compute the final color of each pixel (sometimes called more generally *fragments*), i.e. work per-pixel. A typical use is to perform [texturing](texture.md) and amount of reflected light (lighting model).
- **geometry shaders**: Advanced stage, serves to modify the geometry of 3D models (these shaders can, unlike vertex shaders, generate of discard vertices).
- **tesselation shaders**: Advanced stage, serves for subdividing primitives to create smoother geometry.
- **compute shaders**: Perform general computations, used e.g. for computing physics simulations, [AI](ai.md) etc.
- **[ray tracing](ray_tracing.md) shaders** and other specific types