# Idiot Fallacy

{ This may have an alternative, more politically correct name, but I think my name is more appropriate. ~drummyfish }

Idiot fallacy is an argument fallacy related to [shortcut thinking](shortcut_thinking.md), by which an idiot implies *B* from *A* on the basis that *A* is associated with *B* in any way, without *A* necessarily implying *B*; e.g. "[racial](race.md) realism always means hate of other races". However do not confuse idiot fallacy with statistical observations (e.g. observing that women are usually bad at [chess](chess.md) while acknowledging that on rare occasions it may not be so) -- idiot fallacy denotes a false belief in logical implication, i.e. that for example hatred of another group of people (e.g. racial) is an inevitable consequence of existence of such groups -- in a way this fallacy is a failure to separate two similar but essentially distinct things and making fatal simplifications such as "cars = driving", "guns = war", "[gay](gay.md) = [LGBT](lgbt.md)" and so on.

Some examples include:

- "He likes guns, therefore he also likes [war](war.md), violence, hunting etc."
- "He likes to watch gore videos, therefore he is a violent man and supports violence in real life."
- "He isn't politically correct, therefore he is a rightist."
- "He opposes bullying of [pedophiles](pedophilia.md), therefore he is a pedophile." -- This reasoning is guaranteed to be made by every single [American](USA.md) as Americans cannot comprehend any other motive than [self interest](self_interest.md), to them it's physically impossible to try to benefit a group one is not part of.
- "He opposes [bloat](bloat.md), therefore he supports the [privacy](privacy.md) and [productivity](productivity_cult.md) hysteria and his hobby must be ricing tiling window managers."
- "He acknowledges the existence and significance of [human races](race.md), therefore he is hostile to other races and supports their genocide."
- "He plays competitive games so he must think having society based on competition is a good idea."
- "He was born in country X so he likes and supports country X."
- "He dislikes/opposes a certain social group, therefore he wants to eliminate that group by any means necessary and he cannot love anyone in that group or anyone who supports that group."
- "He opposes [capitalism](capitalism.md), therefore he supports  [gay fascism](lgbt.md), [woman fascism](feminism.md) and [Marxism](marxism.md)." (this is also an [American](usa.md) [false dichotomy](false_dichotomy.md))
- "He got an erection from seeing a 16 year old girl, therefore he is a psychopath who wants to rape toddlers and is thinking about nothing else 24/7 and the only goal of his life is to rape children."
- ...