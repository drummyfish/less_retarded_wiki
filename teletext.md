# Teletext

Teletext is now pretty much obsolete technology that allowed broadcasting extremely simple read-only text/graphical pages along with TV signal so that people could browse them on their [TVs](tv.md). It was used mostly in the 70s, 80s and 90s but with [world wide web](www.md) teletext pretty much died.

{ Just checked on my TV and it still works in 2022 here. For me teletext was something I could pretend was "the internet" when I was little and when we didn't have internet at home yet, it was very cool. Back then it took a while to load any page but I could read some basic news or even browse graphical logos for cell phones. Nowadays TVs have buffers and have all the pages loaded at any time so the browsing is instantaneous. ~drummyfish }

The principal difference against the [Internet](internet.md) was that teletext was [broadcast](broadcast.md), i.e. it was a one-way communication. Users couldn't send back any data or even request any page, they could only wait and catch the pages that were broadcast by TV stations (this had advantages though, e.g. it couldn't be [DDOSed](ddos.md) and it couldn't spy on its users as they didn't send any information back). Each station would have its own teletext with roughly 1000 pages -- the user would write a three place number of the page he wanted to load ("catch") and the TV would wait until that page was broadcast (this might have been around 30 seconds at most), then it would be displayed. (More precisely teletext had 8 "magazines", each potentially having up to 256 pages, e.g. 3F7 would be magazine 3, hexadecimal page F7, but non decimal page numbers were rarely used because of the normies). The data of the pages were embedded into unused parts of the TV signal. This is actually very [interesting](interesting.md) because recorded [VHS](vhs.md) tapes still contain this signal and so it's possible to did old teletext pages out of the taps (see also [data archeology](data_archeology.md)). There is now a notable community doing just this, even creating something like the wayback machine for the teletext.

The pages allowed fixed width font text and some very blocky graphics, both could be colored with very few basic colors. It looked like something you render in a very primitive [terminal](terminal.md). The most important standard (that defined e.g. the format of the pages) seems to be the *World System Teletext* of 1986. This standard defined several "levels" of support, most common of which is 1.5 -- this level supports a 40x24 character grid (graphics was done using special text characters that practically further subdivided each character into 2x3 pixels) and 8 predefined colors. There were also some advanced features, some things could be blinking, some text could be hidden and revealed with a special button on the remote (this would be used for teletext quiz games), teletext could also transmit movie subtitles etc.

[Fun](fun.md) fact: **the video game Worms had a short lived teletext version**. It was surely very limited because the whole state had to be encoded in the page numbers and games took extremely long, so it was ultimately scratched. { Thanks to a friend for this cool piece of trivia <3 ~drummyfish }

## See Also

- [videotex](videotex.md)
- [world broadcast](world_broadcast.md)
- [BBS](bbs.md)
- [VHS](vhs.md)
- [ICQ](icq.md)