# USA

United States of America (also United Shitholes of America, burgerland, USA, US or just "murika") is a [dystopian](dystopia.md) imperialist country of fat, stupid idiots enslaved by [capitalism](capitalism.md), either rightist or [pseudoleftist](pseudoleft.md) [fascists](fascism.md) endlessly obsessed with [money](money.md), [wars](war.md), [fighting](fight_culture.md), shooting their presidents and shooting up their schools. Other things they like include guns, oil, throwing nuclear bombs on cities, detonating nuclear bombs in the sea and crashing planes into their own skyscrapers so that they can invade other countries. USA consists of 50 states located in North America, a continent that ancestors of Americans invaded and have stolen from Indians, the natives whom Americans mass murdered. Americans are stupid idiots with guns who above all value constant societal conflict and make the world so that all people are dragged into such conflict.

{ Sorry to some of my US frens :D I love you <3 ~drummyfish }

USA is very similar to [North Korea](north_korea.md): in both countries the people are successfully led to believe their country is the best and have strong propaganda based on [cults of personality](cult_of_personality.md), which to outsiders seem very ridiculous but which is nevertheless very effective: for example North Korea officially proclaims their supreme leader Kim Jong-il was born atop a sacred mountain and a new star came to existence on the day of his birth, while Americans on the other hand believe one of their retarded leaders named George Washington was a divine god who was PHYSICALLY UNABLE TO TELL A LIE, which was actually taught at their schools. North Korea is ruled by a single political party, US is ruled by two practically same militant capitalist imperialist parties (democrats and republicans), i.e. de-facto one party as well. Both countries are obsessed with weapons (especially nuclear ones) and their military, both are highly and openly [fascist](fascism.md) (nationalist). Both countries are full of extreme [propaganda](propaganda.md), [censorship](censorship.md) and [hero culture](hero_culture.md), people worship dictators such as Kim Jong-un or [Steve Jobs](steve_jobs.md). US is even worse than North Korea because it exports its toxic [culture](culture.md) all over the whole world and constantly invades other countries, it is destroying all other cultures and leads the whole world to doom and destruction of all life, while North Korea basically only destroys itself.

In US mainstream [politics](politics.md) there exists no true left, only [right](left_right.md) and [pseudoleft](pseudoleft.md). It is only in extreme underground, out of the sight of most people, where occasionally something good comes into existence as an exception to a general rule that nothing good comes from the US. One of these exceptions is [free software](free_software.md) (established by [Richard Stallman](rms.md)) which was however quickly smothered by the capitalist [open source](open_source.md) counter movement.

On 6th and 9th August 1945 **USA murdered about 200000 civilians**, most of whom were innocent men, women and children, by throwing atomic bombs on Japanese cities Hiroshima and Nagasaki. The men who threw the bombs and ordered the bombing were never put on trial, actually most Americans praise them as [heroes](hero_culture.md) and think it was a good thing to do.

**Americans are uber retarded** for example in trying to somehow pursue both [self interest](self_interest.md) and "social equality", it's extremely ridiculous, an american brain is literally incapable of imagining someone who doesn't at his core work on the basis of self interest, so the American that tries to identify with "wanting equality and human rights" just comes up with hugely fucked up arguments like ["SKIN COLOR IS JUST ILLUSION THEREFORE WE ARE ALL EQUAL"](political_correctness.md) -- because he inevitably sees differences implying oppression because self interest just cannot be not present (this idea won't even occur for a second to him during his whole lifetime, it's simply something he NEVER can physically think), his mind is hard wired to be unable of grasping the idea of accepting difference between people while giving up the self interest of falling to [fascism](fascism.md) as a consequence. Similar arguments are encountered e.g. regarding [vegetarianism](vegetarianism.md): an American supporting vegetarianism will resort to denying evolution, biology and anatomy and will argue something like "HUMANS ARE HERBIVORES BECAUSE THIS FEMINIST SCIENTIST SAYS IT AND MEAT KILLS US SO WE MUST NOT EAT IT", again because he just thinks that admitting meat is healthy to us automatically implies we have to eat it because self interest is just something that's an inherent part of laws of physics; a normal (non retarded) vegetarian will of course admit not eating meat at all is probably a bit unhealthy, but it's a voluntary choice made of altruistic love towards other living beings who now don't have to die for one's tastier food.

USA also has the worst justice system in the world, they literally let angry mob play judges in actual courts, they pick random trash from the streets (similarly to how they choose their presidents) and let them decide someone's guilt for giving them free lunch, which they call "jury duty". This is not a [joke](jokes.md), look it up, in USA you'll literally be judged by random amateurs who have no clue about law and will just judge you based on whether they like your face or not. You can't make this up.

{ "List of atrocities by the United States" is the longest page on leftypedia :-) https://wiki.leftypol.org/wiki/List_of_atrocities_committed_by_the_United_States. ~drummyfish }

Here is a comparison of average European country before and after infestation with American culture (judged by Czech Republic, the author's country of residence, but it's more or less the same in whole EU):

| what                          | before US culture (~1990s) | after US culture (~2020s) |
| ----------------------------- | -------------------------- | ------------------------- |
| mass shootings                | no                         | yes                       |
| [feminazism](feminism.md)     | no                         | yes                       |
| [gay fascism](lgbt.md)        | no                         | yes                       |
| [slavery](work.md)            | mild                       | extreme                   |
| public toilets                | yes                        | no                        |
| free healthcare               | yes                        | no                        |
| corruption                    | mild                       | extreme                   |
| youtubers                     | no                         | yes                       |
| Santa Claus                   | no                         | yes                       |
| ads                           | mild                       | unbearable, aggressive    |
| morality                      | sometimes                  | no                        |
| idiots                        | some                       | all                       |
| [free speech](free_speech.md) | mostly                     | no                        |
| [Apple](apple.md)             | no                         | yes                       |
|[privacy](privacy.md) hysteria | no                         | yes                       |
| social security               | yes                        | no                        |
| old age pension               | yes                        | no                        |
|[fear culture](fear_culture.md)| no                         | extreme                   |
| update culture, consumerism   | mostly not                 | extreme                   |
| powerty                       | none                       | extreme                   |
| financial crisis              | no                         | yes                       |
| society collapsing            | no                         | yes                       |
| [art](art.md) and culture     | good                       | none                      |
| [toxicity](toxic.md)          | rare                       | extreme                   |
| [technology](tech.md)         | fine                       | worst in history          |
| plastic surgery abominations  | no                         | yes                       |
| TV content to ad ratio        | > 10                       | < 1                       |
| wanted to commit suicide      | no                         | yes                       |
| society worked                | kinda                      | no                        |

**USA <3 [imperialism](imperialism.md).** In general it goes like this: there is some jungle tribe that has figured life out, they don't have to work much, lie down all day, have sex with anyone they like, have no money, walk around naked, are generally happy. USA sees their resources and they go: "Hey, you need some [DEMOCRACY](democracy.md). We'll take your fields but we'll build a factory there and GIVE YOU [JOBS](work.md). It's not slavery, you'll just be in the factory for 12 hours each day doing repetitive tasks, then you give half of the money you make there to us so that you can keep the land on which you have your hut -- no, it's not slavery, we don't use that word anymore, we call it [capitalism](capitalism.md). We'll teach you what [racism](racism.md) and [feminism](feminism.md) is. We'll build you MCdonalds in your village. We'll kill the jungle in 100km radius along with all life in it, but we'll give you a device to manage your finance and as a bonus it will be showing you ads all day so you don't even have to look around at all the mayhem, you'll just be watching the display, OK? You're welcome."

In [Europe](europe.md), or maybe just anywhere else in the world, you are afraid of getting hit by a car because you might die, in America you afraid of it because you couldn't afford the ambulance bill and would get into unpayable debt (yes, even if you pay "health insurance"). You can literally find footage of half dead people running away from ambulances so that they don't have to go to debt for being kept alive. In Europe you are afraid to hit someone with a car because you might kill him, in America you are afraid of it because he might sue you. This is not an exaggeration or [joke](jokes.md), it's literally how it is -- it's incredible how people can believe the country is somehow "more advanced", it is quite literally the least developed country in history.

In 1920 the "land of freedom" banned drinking alcohol so that capitalist slaves could spend less time on fun and more time on slavery, this was known as the prohibition and led to skyrocketing of organized crime. Another pinnacle of stupidity indeed. This lasted over a decade.

## See Also

- [elvis](elvis.md)
- [shithole](shithole.md)
- [imperialism](imperialism.md)