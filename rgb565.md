# RGB565

RGB565 is color format, or a way of representing [colors](color.md) with just 2 [bytes](byte.md) (unlike traditional 24 bit RGB formats that use 3 bytes, one for each component), that is 16 [bits](bit.md) (giving a total of 65536 distinct colors), by using 5 bits (highest) for red, 6 bits for green (to which human eye is most sensitive) and 5 bits for blue; it can also be seen as a color [palette](palette.md). It is similar to [rgb332](rgb332.md) -- it's basically a mid way between RGB332 and full 24bit RGB against which it saves one byte per pixel, but compared to RGB332 [byte sex](byte_sex.md) comes to play here. Practically speaking you will rarely need anything more than this, 65 thousand colors are absolutely sufficient for everything.

Yet another similar format to this one is [RGB555](rgb555.md) which sacrifices one useful bit for gaining the nice property of having the same size of each component. The one "wasted" bit may also be utilized, e.g. for marking transparency.

Here is a [C](c.md) code for the basic conversions to/from this format:

```
unsigned int rgbTo565(unsigned char red, unsigned char green,
  unsigned char blue)
{
  return (((unsigned int) (red / 8)) << 11) |
    (((unsigned int) (green / 4)) << 5) | (blue / 8);
}

void rgbFrom565(unsigned int colorIndex, unsigned char *red,
  unsigned char *green, unsigned char *blue)
{
  unsigned char value = colorIndex >> 11;
  *red = value != 31 ? value * 8 : 255;
  
  value = (colorIndex >> 5) & 0x3f;
  *green = value != 63 ? value * 4 : 255;
  
  value = colorIndex & 0x1f;
  *blue = value != 31 ? value * 8 : 255;
}
```

There exist nice tricks you can do with colors represented this way, like quickly divide all three R, G and B components by a power of two just by doing one bit shift and logical [AND](and.md) to zero the highest bits of the components, or approximating addition of colors with logical [OR](or.md) and so on -- for details see the article on [RGB332](rgb332.md).

