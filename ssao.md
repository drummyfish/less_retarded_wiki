# SSAO

Screen space ambient occlusion (SSAO) is a [screen space](screen_space.md) technique used in 3D [computer graphics](graphics.md) for **[approximating](approximation.md)** [ambient occlusion](ambient_occlusion.md) (basically "dim, very soft shadows in corners", which itself is an approximation of true [global illumination](global_illumination.md)) in a way that's easy and not so expensive to implement to run in [real time](real_time.md). The effect is overly used in video games but it very often looks extremely ugly and is often criticized, see e.g. an excellent article at https://nothings.org/gamedev/ssao/.

{ 2023 report: SSAO still sucks. ~drummyfish }

Exact ambient occlusions can be computed with algorithms such as RTAO (which uses [raytracing](raytracing.md)), but this requires complete information about the geometry and is too slow without special hardware. Therefore some game devs cheat and use a cheap approximation: SSAO is implemented as a [post-processing](post_processing.md) [shader](shader.md) and only uses the information available on the screen, specifically in the [depth buffer](z_buffer.md) -- this gives only partial information about the actual scene geometry, i.e. the algorithm doesn't know what the back facing, screen-perpendicular or off-screen geometry looks like and has to make guesses which sometimes result in quite visible inaccuracies.

This methods is notoriously ugly in certain conditions and many [modern](modern.md) [games](game.md) suffer from this, even the supposedly "photorealistic" engines like Unreal -- if someone is standing in front of a wall there is a shadow outline around him that looks so unbelievably ugly you literally want to puke. But normie eyes can't see this lol, they think that's how reality looks and they are okay with this shit, they allow this to happen. Normies literally destroy computer graphics by not being able to see correctly.

What to do then? The most [suckless](suckless.md) way is to simply do no ambient occlusion -- seriously test how it looks and if it's okay just save yourself the effort, performance and complexity. Back in the 90s we didn't have this shit and games unironically looked 100 times better. You can also just [bake](baking.md) the ambient occlusion in textures themselves, either directly in the color texture or use [light maps](light_map.md). Note that this makes the ambient occlusions static and with light maps you'll need more memory for textures. Finally, if you absolutely have to use SSAO, at least use it very lightly (there are parameters you can lower to make it less prominent).

## See Also

- [screen space reflections](screen_space_reflections.md)
- [vsync](vsync.md)