# Leading The Pig To The Slaughterhouse

*"Move forward, don't look back, forward is good!"* --slaughterer to the pig, also [capitalist](capitalism.md) to customer

The manipulative strategy of [slowly](slowly_boiling_the_frog.md) and calmly leading someone (oftentimes a whole crowd of someones) to a place of exploitation is called *leading the pig to the slaughterhouse*; it is widely made use of by [capitalists](capitalism.md) to rape people. The goal is to get someone to where he is defenseless so that he can start to be fully abused, but how to do this? When the victim sees the place, he won't want to move there. You do it by small steps:

1. Make the pig (customer) move a small step towards the slaughterhouse (technology dystopia), e.g. by offering it food (comfort, advanced features, more back cameras, discount price, faster porn download, ...).
2. As it moves, silently close the door (deprecate old technology) behind the pig so that it has no way to return. If the pig starts looking back just laugh at it: "Haha, why are you backwards? That place there is obsolete and out of fashion now. Forward lies a better place! [Update](update_culture.md)! Only move forward, never stop! Look, others [are doing the same](everyone_does_it.md)."
3. Repeat steps 1 and 2 until you get to the slaughterhouse (technology dystopia).
4. Congratulations, you are now in the slaughterhouse (technology dystopia), you can do whatever you want with the pig (customer) without it having any option to back up. Want to spy on the pig 24/7? No problem, the pig has a device it can't live without that's fully under your control. Want to take 90% of his month's pay? No problem, he is depending on software that's not even physically installed on his computer which you can shut down at any second. Maybe it even has an extremely [smart](smart.md) [proprietary](proprietary.d) pacemaker in its heart so you can just kill him if he doesn't pay. Good thing is also that the pig doesn't believe this can happen because it relies on someone "protecting" it (a government that just NEVER ever sleeps because it's so SO much concerned for the wellbeing of its people, or maybe a group of 10 14 year old girls that call themselves animal right activists that will at last second appear like Marvel superheroes and save all the pigs like in the Pixar movie yeah!).

## See Also

- [slowly boiling the frog](slowly_boiling_the_frog.md)