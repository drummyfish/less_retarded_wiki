# Frequently Asked Questions

*Not to be confused with [fuck](fuck.md) or [frequently questioned answers](fqa.md).*

Now degenerated into kind of AMA.

{ answers by ~[drummyfish](drummyfish.md) }

### Is this a joke? Are you [trolling](trolling.md)?

No. Jokes are [here](jokes.md). The tone of the wiki is informal, cynical, relaxed and full of jokes and irony, but the topics and the goal are completely serious.

### What the fuck?

See [WTF](wtf.md).

### Is there [RSS](rss.md) feed?

I dunno, I don't do anything like that now, remember this isn't a blog, tho you can probably get RSS by going to this wiki's git repo and getting the commit feed.

### How does LRS differ from [suckless](suckless.md), [KISS](kiss.md), [free software](free_software.md) and similar types of software?

Sometimes these sets may greatly overlap and LRS is at times just a slightly different angle of looking at the same things, but in short LRS cherry-picks the best of other things and is much greater in scope (it focuses on the big picture of whole society). I have invented LRS as my own take on suckless software and then modified it a bit and expanded its scope to encompass not just technology but the whole society -- as I cannot speak on behalf of the whole suckless community (and sometimes disagree with them a lot), I have created my own "fork" and simply set my own definitions without worrying about misinterpreting, misquoting or contradicting someone else. LRS advocates very similar technology to that advocated by suckless, but it furthermore has its specific ideas and areas of focus. The main point is that **LRS is derived from an unconditional love of all life** rather than some shallow idea such as "[productivity](productivity_cult.md)". In practice this leads to such things as a high stress put on [public domain](public_domain.md) and legal safety, [altruism](altruism.md), selflessness, anti-[capitalism](capitalism.md), accepting [games](game.md) as desirable type of software, **NOT subscribing to the [productivity cult](productivity_cult.md), rejecting [security](security.md), [privacy](privacy.md), [cryptocurrencies](crypto.md) etc.** While suckless is apolitical and its scope is mostly limited to software and its use for "getting job done", LRS speaks not just about technology but about the whole society -- there are two main parts of LRS: [less retarded software](lrs.md) and [less retarded society](less_retarded_society.md).

One way to see LRS is as a philosophy that takes only the [good](good.md) out of existing philosophies/movements/ideologies/etc. and adds them to a single unique [idealist](idealism.md) mix, without including [cancer](cancer.md), [bullshit](bullshit.md), errors, propaganda and other negative phenomena plaguing basically all existing philosophies/movements/ideologies/etc.

### Why this obsession with extreme [simplicity](minimalism.md)? Is it because you're too stupid to understand complex stuff?

On the contrary, LRS is not stupid enough to embrace complexity. I used to be the [mainstream](mainstream.md), complexity embracing programmer. I am in no way saying I'm a genius but I've put a lot of energy into studying computer science full time for many years so I believe I can say I have some understanding of the "complex" stuff. I speak from own experience and also on behalf of others who shared their experience with me that the appreciation of simplicity and realization of its necessity comes after many years of dealing with the complex and deep insight into the field and into the complex connections of that field to society.

You may ask: well then but why it's just you and a few weirdos who see this, why don't most good programmers share your opinions? Because they need to make living or because they simply WANT to make a lot of money and so they do what the system wants them to do. Education in technology (and generally just being exposed to corporate propaganda since birth) is kind of a trap: it teaches you to embrace complexity and when you realize it's not a good thing, it is too late, you already need to pay your student loan, your rent, your mortgage, and the only thing they want you to do is to keep this complexity cult rolling. So people just do what they need to do and many of them just psychologically make themselves believe something they subconsciously know isn't right because that makes their everyday life easier to live. "Everyone does it so it can't be bad, better not even bother thinking about it too much". It's difficult doing something every day that you think is wrong, so you make yourself believe it's right.

It's not that we can't understand the complex. It is that the simpler things we deal with, the more powerful things we can create out of them as the overhead of the accumulated complexity isn't burdening us so much.

Simplicity is crucial not only for the quality of technology, i.e. for example its safety and efficiency, but also for its freedom. The more complex technology becomes, the fewer people can control it. If technology is to serve all people, it has to be simple enough so that as many people as possible can understand it, maintain it, fix it, customize it, improve it. It's not just about being able to understand a complex program, it's also about how much time and energy it takes because time is a price not everyone can afford, even if they have the knowledge of programming. Even if you yourself cannot program, if you are using a simple program and it breaks, you can easily find someone with a basic knowledge of programming who can fix it, unlike with a very complex program whose fix will require a corporation.

Going for the simple technology doesn't necessarily have to mean we have to give up the "nice things" such as computer games or 3D graphics. Many things, such as responsiveness and customizability of programs, would improve. Even if the results won't be so shiny, we can recreate much of what we are used to in a much simpler way. You may now ask: why don't companies do things simply if they can? Because complexity benefits them in creating de facto monopolies, as mentioned above, by reducing the number of people who can tinker with their creations. And also because capitalism pushes towards making things quickly rather than well -- and yes, even non commercial "FOSS" programs are pushed towards this, they still compete and imitate the commercial programs. Already now you can see how technology and society are intertwined in complex ways that all need to be understood before one comes to realize the necessity of simplicity.

### How would your ideal society work? Isn't it utopia?

See the article on [less retarded society](less_retarded_society.md), it contains a detailed FAQ especially on that.

### So is there a whole community here or what? Do you have forums or anything?

Well, firstly LRS by [anarchist](anarchism.md) principles avoids establishing a centralized community with things such as platforms, senior members, moderators, bureaucracy, codified rules etc. Every nice thing that went this way turned to shit once it became more popular and grew, corruption always appears and prevails (see [Wikipedia](wikipedia.md), [GNU](gnu.md)/[FSF](fsf.md), [Linux](linux.md) etc.). So we rather aim for a community of decentralized, loosely associated individuals and small entities with greatly overlapping sets of values rather than any organizations etc. So rather think something like many people with their own websites referring and linking to each other.

At the moment this wiki/movement/ideology/etc. is basically me ([drummyfish](drummyfish.md)), and there are a few people around who agree more or less with me. There are quite a few people who like this to various degree and contact me from time to time, many just write me a one time email, suggest something, thank me, suggest I kill myself etc. -- currently I would count these in dozens, from which I estimate there is a non-negligible number of silent lurkers (given that only 1 in relatively many readers will actually write me an email).

I don't have any web analytics set up, I could probably dig up at least number of site downloads or something but I don't even look at that, I don't want to aim for popularity.

A forum, mailing list or something would possibly be nice, so far I've only experimented with more or less private text boards for closer friends and similar things, no one made anything bigger yet and I would be hesitant to call it an "official" LRS forum anyway, exactly to prevent growing into some kind of corrupt "democratic internet community" of which there are too many nowadays. I am also absolute shit at running any "community" (I tried several times, always failed) and also pretty anxious of collaborating with someone, making hard connections to other project etc. So if you'd like to make a LRS-focused forum (or anything similar), it would be best if you just make it your own thing -- of course I'll be very glad if you refer to my stuff etc., I just won't be able to keep up with another project, play an admin or something, I will only be able to be a regular user of that forum.

### Why the angry and aggressive tone, can't you write in a nicer way, especially when you advocate love etc.?

More than once I've been told that someone was initially afraid to talk to me because I write in this "aggressive/angry" way. I am sorry, this way of writing has its reasons that I established here and it's what works for me, I really don't intend to stress you out -- firstly this is how I internally think because yes, I am very frustrated, and **I want this wiki to capture my internal thoughts in a very unfiltered way** (note that having bad thoughts doesn't mean one has to act on them), also I find this way flows the best for me and allows me to communicate what I feel and think the best, and it also gives this wiki kind of its own "personality" and prevents it from taking on a super serious tone -- informality and fun are quite important for a healthy view of the world. I am really tired of all the overly correct and polite articles on the Internet (I have tried to write in different ways but it always stands in the way). Sometimes I get mood swings and regret writing something, other times I bash myself for being too soft -- but I don't want to delete stuff too much, this will all be reflected on the wiki. Underneath all this still lies the important message of **love and peace**. I guess I also want to show that to be truly loving you don't have to change your personality or censor your thoughts. In normal conversations I try as much as possible to be nice, I actually almost never get aggressive towards others, if I get very stressed I usually just leave or in more extreme cases target hate towards myself, but I really try to not hurt anyone (people also told me they were quite surprised that I was kind of "nice" when they actually talked to me). I actually have a lot of trouble in real life for not defending myself, people often abuse it and I let them, I don't fight back, I don't believe in revenge or violence and in addition I have social anxiety. Please don't be afraid to contact me <3

### Why the name "less retarded"? If you say you're serious about this, why not a more serious name?

I don't know, this is not so easy to answer because I came up with the name back when the project was smaller in scope and I didn't think about a name too hard: this name was playful, catchy, politically incorrect (keeping SJWs away) and had a kind of reference to *suckless*, potentially attracting attention of suckless fans. It also has the nice property of being unique, with low probability of name collision with some other existing project, as not many people will want to have the word "retarded" in the name. Overall the name captures the spirit of the philosophy and is very general, allowing it to be applied to new areas without being limited to certain means etc.

Now that the project has evolved a bit the name actually seems to have been a great choice and I'm pretty happy about it, not just for the above mentioned reasons but also because it is NOT some generic boring name that politicians, PR people and other tryhard populists would come up with. In a way it's trying to stimulate thought and make you think (if only by making you ask WHY anyone would choose such a name). Yes, in a way it's a small protest and showing we stay away from the rotten mainstream, but it's definitely NOT an attempt at catching attention at any cost or trying to look like cool rebels -- such mentality goes against our basic principles. Perhaps the greatest reason for the name is to serve as a test -- truth should prevail no matter what name it is given and we try to test and prove this, or rather maybe prevent succeeding for wrong reasons -- we are not interested in success (which is what mere politicians do); if our ideas are to become accepted, they have to be accepted for the right reasons. And if you refuse to accept truth because you don't like its name, you are retarded and by own ignorance doom yourself to live in a shit society with shit technology.

### Who writes this wiki? Can I contribute?

You can only contribute to this wiki if you're a straight [white](white.md) male. Just kidding, you can't contribute even if you're a straight white male :)

At the moment it's just me, [drummyfish](drummyfish.md). This started as a collaborative wiki name *based wiki* but after some disagreements I forked it (everything was practically written by me at that point) and made it my own wiki where I don't have to make any compromises or respect anyone else's opinions. I'm not opposed to the idea of collaboration in some situations but I bet we disagree on something in which case I probably don't want to search for a compromise. I also resist allowing contributions because with multiple authors the chance of legal complications grows, even if the work is under a free license or waiver (refer to e.g. the situation where some Linux developers were threatening to withdraw their code contribution license). But you can totally fork this wiki, it's [public domain](cc0.md).

If you want to contribute to the cause, just create your own website, spread the ideas you liked here -- you may or may not refer to LRS, everything's up to you. Start creating software with LRS philosophy if you can -- together we can help evolve and spread our ideas in a decentralized way, without me or anyone else being an authority, a potential censor. That's the best way forward I think.

### Why is it called a wiki when it's written just by one guy? Is it to deceive people into thinking there's a whole movement rather than just one weirdo?

Yes.

No, of course not you dumbo. There is no intention of deception, this project started as a collaborative wiki with multiple contributors, named *Based Wiki*, however I (drummyfish) forked my contributions (most of the original Wiki) into my own Wiki and renamed it to *Less Retarded Wiki* because I didn't like the direction of the original wiki. At that point I was still allowing and looking for more contributors, but somehow none of the original people came to contribute and meanwhile I've expanded my LRS Wiki to the point at which I decided it's simply a snapshot of my own views and so I decided to keep it my own project and kept the name that I established, the *LRS Wiki*. Even though at the moment it's missing the main feature of a wiki, i.e. collaboration of multiple people, it is still a project that most people would likely call a "wiki" naturally (even if only a personal one, also called a *monoproject*) due to having all the other features of wikis (separate, constantly changing articles linked via hypertext, non-linear structure etc.) and simply looking like a wiki -- nowadays there are many wikis that are mostly written by a single man (see e.g. small fandom wikis) and people still call them wikis because culturally the term has simply taken a wider meaning, people don't expect a wiki to absolutely necessarily be collaborative and so there is no deception. Additionally I am still open to the idea to possibly allowing contributions, so I'm simply keeping this a wiki, the wiki is in a sense waiting for a larger community to come. Finally the ideas I present here are not just mine but really do reflect existing movements/philosophies with significant numbers of supporters (suckless, free software, ...).

### Why did you make this wiki? What is its purpose?

There are many reasons, it serves multiple purposes which also change a bit over time -- the "net good" arising from the existence of this wiki seems to be quite higly positive, so it keeps existing and at least for now flourishes. Anyway here are some reasons for why this wiki exists:

- At the beginning this was mostly a silly fun and a way of communicating among a few people, though soon it became just my own project.
- It really tries to explore -- and also show -- how technology could be done well. By writing this wiki I invent new terms, categorize them, connect concepts together, find useful ideas buried in the history of technology. This wiki can become useful if there is a turnaround, e.g. after the collapse. It also tries to SHOW that many things can be done simply, many articles contain short code snippets that do very useful work in very few lines of code. Many project like libraries and games have been done just by myself and though they many not be perfect, just imagine if some real genius, of a small team of people, tried to do things the way I do them -- we would see miracles happen. Many articles link to cool minimalist programs and projects that demonstrate the principles.
- Hopefully it's going to be a useful resource for people who want to e.g. start programming in C, if only as a repository of code snippets for example. By this I hope to help bring more people to the world of good technology. It's also going to be a big book that's completely CC0, there is never enough of these.
- Similarly I hope to spark some critical thinking and non-mainstream midsets such as those of altruism, minimalism, nonviolence and selflessness. There are so few resources that do this, everything is overshadowed by the huge circlejerk avalanche of modern brainwashing of competitiveness, secrecy, self interest, basically just pure evil. I really feel that if I don't say certain things, no one else will.
- I hope to perhaps inspire others to make something similar, be it a wiki in similar style or any other kind of art, shared selflessly in the public domain, trying to lead some moral example etc.
- It's an experiment in creating what for now seems to be a unique kind of work, a highly uncensored "brain dump" of a social outcast which combines a serious encyclopedia, half serious shitposting, repository of various data, social comments and so on. It's also being published continuously, with TODOs and WIPs, errors and mistakes, there are no "stable versions", just a kind of "as is" stream of data, take whatever you will out of it.
- The wiki tries to document contemporary society, it may be useful for future historians.
- It helps me cope, vent my emotion.
- It immensely helps me explain my worldviews without wasting my time and having to talk to people too much. Before this wiki whenever someone asked me "why do you think X?" over email, I had to spend an hour formulating a reply, then I would send it, the guy would be like "hmm it's cool but I disagree" -- so that was an hour of my life lost. People kept asking me the same things over and over, I used to have the exact same hour long converstations with many people, so now I just write my reasonings here and point to my articles without having to suffer retarded arguments over and over. Really it made me much less suicidal.
- They said I can't write this stuff on their site and that I should go make my own site, so I did. They banned me so I made myself more free. Maybe also a nice example for others.
- I just love making it, it makes me relax like nothing else. I write this wiki in between making my software projects which I love too but which require a bit more effort. A few times in my life I wanted to perhaps be a teacher as I like education and explaining things but at the same time I CANNOT STAND FUCKING PEOPLE -- this wiki makes me able to do what I like without having to suffer what I hate.
- It satisfied my obsessive needs for making lists, cheatsheets and so on. Also helps me keep my notes, ideas etc.
- It makes me (and hopefully others) learn about and research quite obscure things, I have personally discovered so many new amazing things while writing this wiki. Browsing the Internet is sometimes not enough, writing about something is what makes you go deeper.
- I hope to contribute to the downfall of capitalism, though TBH I don't believe it can happen at this point.
- This also helped me find some cool online friends. If you go searching for people like you, you won't find them, but if you put your ideas publicly on the display, people with similar ideas will reach out to you themselves.
- It's a work I can read myself knowing I won't find things that make me more suicidal like the word "person" and stupid grammar mistakes such as using apostrophes for plurals, using "it's" instead of "its", sentences containing "the reason is because" or "just because ... doesn't mean" etc.
- I like wikis, I like editing the pages, but I hate wikis with other people on it.
- Probably other reasons I couldn't recall right now :-)
- ...

### Why is this rather not a blog?

Because blogs suck, they are based on the idea of content consumerism and subscribers following celebrities just like on youtube or facebook, blog posts are hasted, ugly and become obsolete in a week, this wiki is trying to create a reference work that can be polished and will last last some time.

### Since it is public domain, can I take this wiki and do anything with it? Even something you don't like, like sell it or rewrite it in a different way?

Yes, you can do anything... well, anything that's not otherwise illegal like falsely claiming authorship (copyright) of the original text. This is not because I care about being credited, I don't (you DON'T have to give me any credit), but because I care about this wiki not being owned by anyone. You can however claim copyright to anything you add to the wiki if you fork it, as that's your original creation.

### Why not keep politics out of this Wiki and make it purely about technology?

Firstly technological [progress](progress.md) is secondary to the primary type of progress in society: the social progress. The goal of our civilization is to provide good conditions for life -- this is social progress and mankind's main goal. Technological progress only serves to achieve this, so technological progress follows from the goals of social progress. So, to define technology we have to first know what it should help achieve in society. And for that we need to talk politics.

Secondly examining any existing subject in depth requires also understanding its context anyway. Politics and technology nowadays are very much intertwined and the politics of a society ultimately significantly affects what its technology looks like ([capitalist SW](capitalist_software.md), [censorship](censorship.md), [bloat](bloat.md), [spyware](spyware.md), [DRM](drm.md), ...), what goals it serves (consumerism, [productivity](productivity_cult.md), control, war, peace, ...) and how it is developed ([COCs](cos.md), [free software](free_software.md), ...), so studying technology ultimately requires understanding politics around it. I hate arguing about politics, sometimes it literally make me suicidal, but it is inevitable, we have to specify real-life goals clearly if we're to create good technology. Political goals guide us in making important design decisions about features, [tradeoffs](tradeoff.md) and other attributes of technology.

Thirdly society and computer programs are in many ways similar and we naturally see analogies between both these problems and the solutions.

Of course you can fork this wiki and try to remove politics from it, but I think it won't be possible to just keep the technology part alone so that it would still make sense, most things will be left without justification and explanation.

### What is the political direction of LRS then?

In three words basically [anarcho pacifist](anpac.md) [communism](communism.md), however the word [culture](culture.md) may be more appropriate than "politics" here as we aim for removing traditional systems of government based on power and enforcing complex laws, there shall be no politicians in today's sense in our society. For more details see the article about [LRS](lrs.md) itself.

### Why do you blame everything on capitalism when most of the issues you talk about, like propaganda, surveillance, exploitation of the poor and general abuse of power, appeared also under practically any other systems we've seen in history?

This is a good point, we talk about [capitalism](capitalism.md) simply because it is the system of today's world and an immediate threat that needs to be addressed, however we always try to stress that the root issue lies deeper: it is **[competition](competition.md)** that we see as causing all major evil. Competition between people is what always caused the main issues of a society, no matter whether the system at the time was called capitalism, feudalism or pseudosocialism. While historically competition and conflict between people was mostly forced by the nature, nowadays we've conquered technology to a degree at which we could practically eliminate competition, however we choose to artificially preserve it via capitalism, the glorification of competition, and we see this as an extremely wrong direction, hence we put stress on opposing capitalism, i.e. artificial prolonging of competition.

### Why are you calling everything "capitalism" when clearly you can't generalize so much, capitalism needs mass production, things like free trade, corporatism, libertarianism etc. are different things than capitalism, no?

Nah, it's all essentially the same, as long as it's based on [competition](competition.md) it's just a different stage of capitalism at best, all these things are based on the fundamentally flawed idea of letting humans compete -- this will always lead to things like trade, money, people forming bigger groups, companies, cartels and eventually corporations etcetc. Only those who believe capitalism can somehow be fixed or made manageable find it important to distinguish different types of it, we just oppose it all, so we don't bother to distinguish between different flavors of shit too much, that would be just unnecessary and distracting.

### How is this different from [Wikipedia](wikipedia.md)?

In many ways. Our wiki is better e.g. by being more [free](free_culture.md) (completely [public domain](public_domain.md), no [fair use](fair_use.md) [proprietary](proprietary.md) images etc.), less [bloated](bloat.md), better accessible, not infected by [pseudoleftist](psedoleft.md) [fascism](fascism.md) and censorship (we only censor absolutely necessary things, e.g. copyrighted things or things that would immediately put us in jail, though we still say many things that may get us in jail), we have articles that are better readable etc.

### WTF I am offended, is this a nazi site? Are you racist/Xphobic? Do you love [Hitler](hitler.md)?!?!

We're not fascists, we're in fact the exact opposite: our aim is to create technology that benefits everyone equally without any discrimination. I (drummyfish) am personally a pacifist anarchist, I love all living beings and believe in absolute social equality of all life forms. We invite and welcome everyone here, be it gays, communists, rightists, trannies, pedophiles or murderers, we love everyone equally, even you and Hitler.

Note that the fact that we love someone (e.g. Hitler) does NOT mean we embrace his ideas (e.g. Nazism) or even that we e.g. like the way he looks. You may hear us say someone is a stupid ugly fascist, but even such individuals are living beings we love.

What we do NOT engage in is [political correctness](political_correctness.md), censorship, offended culture, identity politics and pseudoleftism. We do NOT support fascist groups such as feminists and LGBT and we will NOT practice bullying and [codes of conducts](coc.md). We do not pretend there aren't any differences between people and we will make jokes that make you feel offended.

The fact that you're confused is caused by you assuming we are retards like everyone else, you think we will keep adjusting to any culture and fashion that develops around us but we don't do that because we think -- we stop adjusting to a culture once it becomes bad, so we have stopped behaving in accordance with [21st century](21st_century.md) culture; instead we keep developing our own [less retarded culture](less_retarded_culture.md) that's forked from the older, saner culture. So while to most people (retards) political incorrectness today implies hostility, violence and danger, to us it doesn't have the same meaning at all, we simply continue living in the past when political incorrectness was completely normal and non-harmful. So the error here is in you.

### But it really seems you hate gay people and trans people and even women, isn't it so?

No, we state it over and over: WE LOVE EVERYONE. If someone feels he's a woman despite having a dick, so be it, if someone's a man who loves to fuck other man or if someone likes to fuck cars or if someone likes to walk on his hands instead of his feet, it's all fine, even if we find some of it stupid, non-pleasant to look at, funny or if we don't care, people are simply how they are, why care about about how someone is? A different matter is forming groups practicing [fascism](fascism.md), i.e. if gays or transsexuals or women or Germans or whites or rightists or any other group gets together to [fight](fight_culture.md) against any other group, that's bad and it's what we oppose, i.e. we oppose the concept of this kind of war itself, it has nothing to do what's the physical or mental difference of individuals the grouping is based on.

### OK then why are there swastikas on your main page?

Swastika is an old religious symbol used e.g. in [Buddhism](buddhism.md), it is a sign of good fortune and protection from evil. Nazis later used swastika but it doesn't give them monopoly over it, and also they used the other version, with arms going clockwise (AND usually also turned additional 45 degrees), so you can only confuse it with Nazi swastika if you are retarded.

### Why do you use the [nigger](nigger.md)-w so much?

To counter its [censorship](censorship.md), we mustn't be afraid of words. The more they censor something, the more I am going to uncensor it. They have to learn that the only way to make me not say that word so often is to stop censoring it, so to their action of censorship I produce a reaction they dislike. That's basically how you train a dog. (Please don't ask who "they" are, it's pretty obvious).

It also has the nice side effect of making this less likely to be used by corporations and SJWs.

### How can you say you love all living beings and use offensive language at the same time?

Rather ask why you cannot do the same -- really, [think about it deeper](shortcut_thinking.md). Why are we forbidding "offensive" language? Maybe because in the spoiled mainstream culture offensive language implies aggression and threat, it may incite violence, encourage destruction and so on. In LRS culture this is in no way so, we practice [free speech](free_speech.md), i.e. we simply don't put on any masks or hide our thoughts from others, we are honest: if I think someone is stupid, why should I lie and say I think he is not stupid? (Keep in mind that staying silent may also be a form of lying and pretense.) This doesn't mean I can't love the individual -- if I couldn't love stupid beings, I couldn't love dogs and cats because they don't reach my level of intelligence, but I do love them. In LRS culture expressing disgust, frustration and strong disagreements doesn't at all mean hostility or an incentive to [fight](fight_culture.md) and destroy. Swearing and insults are an important way of expressing certain kinds of emotions and strong disagreement, we will not give up this tool, we have it and it's useful, so we will use it.

The culture of being offended is [bullshit](bullshit.md), it is a [pseudoleftist](pseudoleft.md) (fascist) invention that serves as a weapon to justify censorship, canceling and bullying of people. Since I love all people, I don't support any weapons against anyone (not even against people I dislike or disagree with). People are offended by language because they're taught to be offended by it by the propaganda, I am helping them unlearn it. [Political correctness](political_correctness.md) is one of the most retarded and toxic things to ever have been invented. Learn to separate being evil and being angry.

### But don't you think someone can misinterpret your politically incorrect speech for inciting violence, fascism etc.?

Yes, idiots can misinterpret anything, that's not my fault, truth has to be revealed -- if someone wrongly interprets a message or uses truth for doing harm, then he is to be blamed, not me. If you think I am to be blamed because I should take into account the possibility of my words leading to someone causing harm, then I am also not to blame because my politically incorrect speech is itself a reaction to the [pseudoleftist](pseudoleft.md) insanity, so ultimately the pseudoleft is to be blamed because they should take into account that by creating hell on Earth someone will start doing what I do which may lead to someone else doing something bad. Either way I, standing in the middle of the chain, am not responsible, logically you cannot put the blame on me, you have to choose whether the responsibility lies at the end of the chain (correct) or at its beginning (wrong but still doesn't serve you).

NOTE: blame here is used in moral sense, not in legal sense or in sense of implying punishment. I simply argue that a scientist shouldn't avoid creating inventions that may be abused. Would you blame Alan Turing for Apple's atrocities?

### But how can you so pretentiously preach "absolute love" and then say you hate capitalists, fascists, bloat etc.?

OK, firstly we do NOT love *everything*, we do NOT advocate against hate itself, only against hate of living beings (note we say we love *everyone*, not *everything*). Hating other things than living beings, such as some bad ideas or malicious objects, is totally acceptable, there's no problem with it. We in fact think hate of some concepts is necessary for finding better ways.

Now when it comes to *"hating"* people, there's an important distinction to be stressed: we never hate a living being as such, we may only hate their properties. So when we say we *hate* someone, it's merely a matter of language convenience -- saying we *hate* someone never means we hate a man as such, but only some thing about that man, for example his opinions, his work, actions, behavior or even appearance. I can hear you ask: what's the difference? The difference is we'll never try to eliminate a living being or cause it suffering because we love it, we may only try to change, in non-violent ways, their attributes we find wrong (which we *hate*): for example we may try to educate the person, point out errors in his arguments, give him advice, and if that doesn't work we may simply choose to avoid his presence. But we will never target hate against him. Hate the sin, not the sinner.

And yeah, of course sometimes we make [jokes](jokes.md) and sarcastic comments, it is relied on your ability to recognize those yourself. We see it as retarded and a great insult to intelligence to put disclaimers on jokes, that's really the worst thing you can do to a joke.

### B... b... but u cant write a big work it like this.

Bitch I can, I have no boss, no publisher, no sponsors, no collaborators, no paying customers, no TOS, no COC, I don't have to suck any dicks, I can write literally what I want here in any way I want -- this means the work is TRULY free, it has practically no barriers and censoring mechanisms. Why don't you do it too?

### Why are you insulting some people very much?

There is the following rule: if he's got [too much fame](hero_culture.md), he needs more shame.

Basically I help reduce their enormously inflated [ego](egoism.md) which is a very bad [disease](disease.md) (while also taking the opportunity to test [free speech](free_speech.md) etc.). If someone has very low self esteem, it's good to help him out by bringing up his positives -- NO, not in the "self improvement" way -- in normal, human way. Not by lying, but simply by focusing on the good. If someone feels down, it's good to give him hope, show him the good, give him a hug, because being depressed and feeling down is a state of illness we want to cure. Similarly the other extreme -- too much self esteem -- is an extremely harmful disease, the ego has to be reduced, just like you cut off [tumor](cancer.md) tissue despite normally cutting parts of human body is generally bad. Here it's good to focus on the negative, for example that his face looks like ass, that he's stupid and so on (but we have to be careful: even negative attention is still attention, many times it's better to just ignore the individual). Notice that greater insults are coming towards greater celebrities: society needs to learn that being a celebrity inevitably comes with the disease of inflated ego, it is practically impossible to be a celebrity and being humble at the same time -- if we learn to dislike celebrities, it will no longer be possible for celebrities to exist because becoming a celebrity will come with decreasing popularity (while today it comes with increasing popularity, which is a very unstable system), the system will be self sustaining, keeping good health of society and people alike, creating a society completely without any narcissists and celebrities. So basically we all have to learn to dislike celebrities and help them out by insulting (and also ignoring) them the more they are famous.

But it may also be the case that sometimes I just publish my uncensored thoughts of frustration in a bad mental state and just insult someone and later on I just don't want to censor myself -- for this I am sorry, I am a shitty imperfect retard as well, even though I try to minimize this. In any case it should still be the case that speech never hurts anyone, either what I say is true, in which case it's OK to say it, or it isn't and I am a liar, so I am at worst hurting myself.

### So you really "love" everyone, even dicks like Trump, school shooters, instagram manipulators etc.?

Yes, but it may need an elaboration. There are many different kinds of love: love of a sexual partner, love of a parent, love of a pet, love of a hobby, love of nature etc. Obviously we can't love everyone with the same kind of love we have e.g. for our life partner, that's impossible if we've actually never even seen most people who live on this planet. The love we are talking about -- our universal love of everyone -- is an unconditional love of life itself. Being alive is a miracle, it's beautiful, and as living beings we feel a sense of connection with all other living beings in this universe who were for some reason chosen to experience this rare miracle as well -- we know what it feels like to live and we know other living beings experience this special, mysterious privilege too, though for a limited time. This is the most basic kind of love, an empathy, the happiness of seeing someone else live. It is sacred, there's nothing more pure in this universe than feeling this empathy, it works without language, without science, without explanation. While not all living beings are capable of this love (a virus probably won't feel any empathy), we believe all humans have this love in them, even if it's being suppressed by their environment that often forces them compete, hate, even kill. Our goal is to awaken this love in everyone as we believe it's the only way to achieve a truly happy coexistence of us, living beings.

### Why do you say you don't fight when clearly you are fighting the whole world here.

That's what you would call it -- am I literally physically punching the world in the face or something? Rather ask yourself why you choose to compare things like education and advocating love to a war. I am just revealing truth, educating, sometimes expressing frustration, anger and emotion, sometimes [joking](jokes.md), without [political correctness](political_correctness.md). [Sometimes names greatly matter](name_is_important.md) and LRS voluntarily chooses to never view its endeavor as being comparable to a [fight](fight_culture.md), like capitalists like to do -- in many situations this literally IS only about using a different word -- seemingly something of small to no importance -- however the word sets a mood and establishes a mindset, when we go far enough, it will start to matter that we have chosen to not see ourselves as fighter, for example we **will NEVER advocate any violence or call for anyone's death**, unlike for example [LGBT](lgbt.md), [feminists](feminism.md) and [Antifa](antifa.md), the "fighters".

### I AM CRYING FROM FEAR THAT YOU COLLECT MY DATA, please please tell me what data you collect about me, I'm shaking right now.

Chill your balls motherfucker, I WOULD collect all your data because I absolutely shit on privacy, I would never respect any shitty privacy laws even if they gave a bullet for it, but you're lucky that I'm lazy to set anything up and that I'm not a capitalist, so I have literally no interest in collecting any data, I know exactly ZERO stuff about anything, I don't give a shit about popularity, appeal, SEO, I have no ads, I don't give a single shit about anything. Well, basically the one thing I can see is daily website bandwidth usage for the last week, so I just get a very rough idea of how many pages per day got downloaded (but I don't even know which they were), that's basically it. Unlearn [privacy](privacy.md) hysteria please (for your own good).

### OH MY GAAAAAAAWD, you are sooooo [productive](productivity_cult.md), you made so many projects all yourself, what magical rituals do you perform for such PRODUCTIVITY, do you listen to motivational speeches before sleep or drink newborn blood??? TELL ME, I HAVE TO KNOW, I wanna PRODUUUUUUCEEEE!

I am THE laziest bastard who EVER lived on this planet, if I seem """productive""" to you then only because I ditch all [bullshit](bullshit.md) and practice [minimalism](minimalism.md). Literally I sometimes do nothing for half a year, then I sometimes do thing here and there over weekends and in a few months I got a new thing made, it's simply the uneatable efficiency of minimalism. If I, the most anti-work, laziest, dumbest shit that ever walked this soil can make these things just by adopting minimalism, imagine what could be made by let's say three non-lazy, smart people. The world would be wonderful. Just ditch [capitalism](capitalism.md) and see it come true.

### I dislike this wiki, our teacher taught us that global variables are bad and that [OOP](oop.md) is good.

This is not a question you dummy. Have you even read the title of this page? Anyway, your teacher is stupid, he is, very likely unknowingly, just spreading the capitalist propaganda. He probably believes what he's saying but he's wrong.

### Lol you've got this fact wrong and you misunderstand this and this topic, you've got bugs in code, your writing sucks etc. How dare you write about things you have no clue about?

*see also [gatekeeping](gatekeeping.md)*

I want a public domain encyclopedia that also includes topics of new technology and correct views without censorship, and also one which doesn't literally make me want to kill myself due to inserted propaganda of evil. Since this supposedly [modern](modern.md) society failed to produce even a single such encyclopedia and since every idiot on this planet wants to keep his copyright on everything he writes and/or wants to censor what he creates, I am forced to write the encyclopedia myself from scratch, even for the price of making mistakes. No, US public domain doesn't count as world wide public domain. Even without copyright there are still so called [moral rights](moral_rights.md) etc. Blame this society for not allowing even a tiny bit of information to slip into public domain. Writing my own encyclopedia is literally the best I can do in the situation I am in. Nothing is perfect, I still believe this can be helpful to someone. You shouldn't take facts from a random website for granted. I have to do my own research on everything, even on thing I have basically no clue about -- I know there are people a million times more knowledgeable on many subjects I write about, but I simply cannot believe them, today's society forces experts to lie, so rather than taking a lie from an expert I am forced to take a honest view of a layman (me). This society is also quite fucked up in forcing the idea that you can't make basic observations unless you have 10 PhDs, you are only allowed to spread the gospel officially approved of by decorated [soyentists](soyence.md) -- I just ignore this. If you wanna help me correct errors, email me.

### Why is this shit so poorly written and inconsistent (typos, incorrect comma placement, inconsistent acronym case, weird grammar etc.)?

Mainly for these reasons:

- On purpose, this is kinda informal text and doesn't wanna get all serious, I want the text to flow in the same way in which informal Internet chat does (both during reading and writing). Though I personally hate such mistakes (and the laziness of authors to proofread) in nice/formal texts, I purposefully don't aim for creating such type of text here because that would firstly make it something else than I want, and secondly it would be extra work and effort that I think is better spent on actually communicating ideas (as opposed to to communicating them perfectly). Afterall a "nicer" version of the wiki can be made by anyone as it is completely public domain.
- This wiki is rather a dirty tool, a temporary vehicle on a way to a better technology and society, not a tidy encyclopedia. It prefers communicating ideas to achieving perfect form, focusing too much on nice form would result in having to leave out a lot of important information due to "quality assurance".
- I don't care sometimes.
- I'm lazy.
- I'm shit and just make mistakes.

### How can you use [CC0](cc0.md) if you, as anarchists, reject laws and intellectual property?

We use it to **remove** law from our project, it's kind of like using a weapon to destroy itself. Using a [license](license.md) such as [GFDL](gfdl.md) would mean we're keeping our copyright and are willing to execute enforcement of intellectual property laws, however using a CC0 [waiver](waiver.md) means we GIVE UP all lawful exclusive rights that have been forced on us. This has no negative effects: if law applies, then we use it to remove itself, and if it doesn't, then nothing happens. To those that acknowledge the reality of the fact that adapting proprietary information can lead to being bullied by the state we give a guarantee this won't happen, and others simply don't have to care.

A simple analogy is this: a law is so fucked up nowadays that it forces us to point a gun at anyone by default when we create something. It's as if they literally put a gun in our hand and force point it at someone. We decide to drop that weapon, not merely promise to not shoot.

However using legal tools will always be a question of considering the pros and cons -- at the time of writing this using CC0 is very easy and achieves great freedom with almost no negative effects, so we choose to do it. But if in the future situation changes so that achieving legal public domain would have more negative effects, then we might choose to ignore the law -- if for example in the future it is ruled that to release something into legal public domain one has to fill out many sheets of paper and pay some fees, we would probably reconsider accepting this as that burdens creators, discriminates against the poor, supports financial bullying and gives money to the overlords, so in such case we'd probably choose to rather ignore law and just use informal waivers. In the dystopia we are simply always looking for the least bad solution.

### What software does this wiki use?

[Git](git.md), the articles are written in [markdown](md.md) and converted to [HTML](html.md) with a simple script.

### I don't want my name associated with this, can you remove a reference to myself or my software from your wiki?

No.

### How can you think you are the smartest man in universe? How can you say your opinions are facts? Isn't it [egoistic](egoism.md)?

I don't think I am the smartest at all -- I never said that, except in the article about [IQ](iq.md). In fact I am highly dumb, I just have a gift of being completely immune to propaganda and seeing the world clearly, and I happen to be in circumstances under which I can do what others can't; for example as I have no friends and no one likes me, I can write and create freely, without self censorship due to fear of losing my job, offending my friends etc. I can write close to what is the absolute truth thanks to all this. I am also super autistic in that I enjoy just thinking 24/7 about programming and stuff instead of thinking about money and watching ads, which compensates for my dumbness a bit.

How do I know my opinions are facts? Experience. How do we discover facts? There is never a 100% certainty of anything, even of mathematical proofs, we may only ever have a great statistical confidence and beliefs so strong we call them facts. Just as by walking 1000 times against a wall you learn you won't walk through, I have over the decades learned I am correct in what I say and that everyone else is simply a monkey incapable of thinking. I used to be the kind of guy "open to discussion and opinions of others", I was giving this approach a chance over and over for about 30 years, I had more than enough patience, but it didn't work, the world has failed. People are absolutely stupid, you can physically show them something, give them tons of evidence and proofs, they won't believe what is literally in front of their eyes -- no, not even intellectuals, people in universities etc. Talking to others and listening to them is a complete waste of life, it's like trying to talk to potatoes or rocks, I might just as well be punching air all day or trying to eat dirt. The best I found I can do now is kind of talk to myself here, record my brain dump in hopes someone will once understand. I really don't know what else to do.

There is nothing egoistic about being special in something, everyone has a talent for something, egoism is about being fascist, preoccupied with oneself and focusing on self benefit, achieving fame, recognition etc., which I try my best to never do. Maybe I slip sometimes as an imperfect man I am, I make mistakes, I do stupid stuff, but I honestly just want the good of everyone without putting myself in the front.

### Don't you think people will leave your website when they read in the article they are being called retards?

*main article: [butthurt](butthurt.md)*

Only idiots will, those are people who are beyond saving anyway, so in reality I'm just saving them time. For me it works like this (it should also work like this for you, else you're faulty): if I read somewhere a thing *X* is retarded, along with a good explanation as to why, and I know I'm doing *X*, I really say to myself "hmm, I'd like to be less retarded, I should probably stop doing that", and then I try -- that's how I [unretard](unretard.md) myself more and more. That's how it works for people who have a good, essentially non-retarded mindset (even if they still do many retarded things; remember, only [God](god.md) can be perfect). Truth is a truth, if something is stupid it's stated to be stupid, being diplomatic is harmful and idiotic.

### Are you the only one in the world who is not affected by propaganda?

It definitely seems so.

### How does it feel to be the only one on this planet to see the undistorted truth of reality?

Pretty lonely and depressing.

### Are you a crank?

Depending on exact definition the answer is either "no" or "yes and it's a good thing".

### Are you retarded?

:( Maybe, but even stupid people can sometimes have smart ideas.
