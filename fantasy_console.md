# Fantasy Console

Fantasy console, also fantasy computer, is a software platform intended mainly for creating and playing simple [games](game.md), which imitates parameters, simplicity and [look and feel](look_and_feel.md) of classic retro consoles such as [GameBoy](gameboy.md). These consoles are called *fantasy* because they are not [emulators](emulator.md) of already existing hardware consoles but rather "dreamed up" platforms, [virtual machines](vm.md) made purely in software with artificially added restrictions that a real hardware console might have. These restrictions limit for example the resolution and color depth of the display, number of buttons and sometimes also computational resources.

The motivation behind creating fantasy consoles is normally twofold: firstly the enjoyment of [retro](retro.md) games and retro programming, and secondly the immense advantages of [simplicity](minimalism.md). It is much faster and easier to create a simple game than a full fledged PC game, this attracts many programmers, simple programming is also more enjoyable (fewer bugs and headaches) and simple games have many nice properties such as small size (playability over [web](web.md)), easy embedding or enabling emulator-like features.

Fantasy consoles usually include some kind of simple [IDE](ide.md); a typical mainstream fantasy console both runs and is programmed in a [web browser](browser.md) so as to be accessible to normies. They also use some kind of easy scripting language for game programming, e.g. [Lua](lua.md). Even though the games are simple, the code of such a mainstream console is normally [bloat](bloat.md), i.e. we are talking about **[pseudominimalism](pseudominimalism.md)**. Nevertheless some consoles, such as [SAF](saf.md), are truly [suckless](suckless.md), free and highly portable (it's not a coincidence that SAF is an official [LRS](lrs.md) project).

Some fantasy consoles may blend with [open consoles](open_console.md), e.g. by starting as a virtual console that's later implemented in real hardware.

## Notable Fantasy Consoles

The following are a few notable fantasy consoles.

| name                  | year | license                 | game lang.        | specs.              | comment                          |
| --------------------- | ---- | ----------------------- | ----------------- | ------------------- | -------------------------------- |
|[CToy](ctoy.md)        | 2016 | [zlib](zlib.md)         | [C](c.md)         |128x128              |[suckless](suckless.md)           |
|[IBNIZ](ibniz.md)      | 2011 | [zlib](zlib.md)         | own               |256x256 32b, 4M RAM  |for demos, by [Viznut](viznut.md) |
|[LIKO-12](liko12.md)   | 2016 | [MIT](mit.md)           | [Lua](lua.md)     |192x128              |                                  |
|[MEG4](meg4.md)        | 2023 | [GPL](gpl.md)           | C, Lua, ...       |320x200 8b, 576K RAM |                                  |
|[microw8](microw8.md)  | 2021 |[unlicense](unlicense.md)| webassembly       |320x240 8b, 256K RAM |                                  |
|[PICO-8](pico8.md)     | 2015 |[propr.](proprietary.md) | [Lua](lua.md)     |128x128 4b           |likely most famous                |
|PixelVision8           | 2020 |[MS-PL](ms_pl.md) (FOSS) | [Lua](lua.md)     |256x240              |written in C#                     |
|Pyxel                  | 2018 | [MIT](mit.md)           |[Python](python.md)|256x256 4b           |                                  |
|[SAF](saf.md)          | 2021 | [CC0](cc0.md)           | [C](c.md)         |64x64 8b             |[LRS](lrs.md), suckless           |
|[TIC-80](tic80.md)     | 2016 | [MIT](mit.md)           | Lua, JS, ...      |240x136 4b           |paid "pro" version                |
|[Vircon32](vircon32.md)|      | ???                     | C, assembly       |640x360, 16M RAM     |aims to be implementable in HW    |
|[uxn](uxn.md)          | 2021 | [MIT](mit.md)           | [Tal](tal.md)     |                     |very minimal                      |

Apart from these there are many more (MicroW8, PX8, WASM-4, ZZT, ...), you can find lists such as https://paladin-t.github.io/fantasy/index. There even exists a [Brainfuck](brainfuck.md) fantasy console, called BrainFuckConsole74.

## See Also

- [open console](open_console.md)
- [handheld](handheld.md)
- [virtual machine](vm.md)
- [IBNIZ](ibniz.md)
- [ISA](isa.md)
- [SAF](saf.md)
- [DOS](dos.md)
- [MIX](mix.md) ([Donald Knuth's](knuth.md) fantasy computer)
