# Backgammon

Backgammon is an old, very popular board [game](game.md) of both skill and [chance](randomness.md) (dice rolling) in which players race their stones from one side of the board to the other. It often involves betting (but can also be played without it) and is especially popular in countries of Near East such as Egypt, Syria etc. (where it is kind of what [chess](chess.md) is to our western world or what [shogi](shogi.md) and [go](go.md) are to Asia). It is a very old game whose predecessors were played by old Romans and can be traced even as far as 3000 BC. Similarly to [chess](chess.md), [go](go.md), [shogi](shogi.md) and other traditional board games backgammon is considered by [us](lrs.md) to be one of the best games as it is [owned by no one](public_domain.md), highly [free](free.md), cheap, simple yet deep and entertaining and can be played even without a [computer](computer.md), just with a bunch of [rocks](rock.md); compared to the other mentioned board games backgammon is unique by involving an element of chance and being only played on [1 dimensional](1d.md) board; it is also relatively simple and therefore noob-friendly and possibly more relaxed (if you lose you can just blame it on rolling bad numbers).

## Rules

Here we'll summarize the common rules, keep in mind there may be some variations, like extra rules on competitive level and so on. The rules seem quite complex and arbitrary at first, but by playing you'll see they're really pretty simple and sometimes quite intuitive (furthermore the game, at least on casual level, mostly doesn't require such hard thinking as e.g. chess, so it even feels more relaxed, you can focus on the rules well).

There are **two players**, black and white, each moving circular stone discs, or just **stones** of his color, here we'll use `{#` for black stones and `(O` for white ones. There are **two six sided dice** in the game. The board has **24 places** (vertical lines, traditionally drawn as long triangles) which stones can occupy. The following shows the board, the initial setup of stones, the directions in which players move and their goals.

```
   black's direction
 .------------ - -  -  -
 |   ___________________________
 |  |{# ; ; ;(O ; |(O ; ; ; ;{# | white's goal
 |  |{# : : :(O : |(O : : : :{# |
 |  |{# . . .(O . |(O . . . . . |
 V  |{# . . . . . |(O . . . . . |
    |{#           |(O           |
    |             |             |
    |(O           |{#           |
 ^  |(O . . . . . |{# . . . . . |
 |  |(O . . .{# . |{# . . . . . |
 |  |(O : : :{# : |{# : : : ;(O |
 |  |(O ; ; ;{# ; |{# ; ; ; ;(O | black's goal
 |   """""""""""""""""""""""""""
 '------------ - -  -  -
   white's direction
```

The **goal** of each player is to get all his stones to his goal -- the goal is one place beyond the last place on the board in the direction of his movement. Whoever does this the first wins.

The first six places on one's path are called the **home board**, the last six are called the **outer board**.

At start both players roll the dice (each one rolls one), whoever rolls the bigger number starts and has to use (details below) the numbers that were just rolled for his first turn (if the numbers were the same, they roll again). After the first player finishes his round, the other player rolls both dice, makes his turn, then the first player does the same again and so on, the players just take turns in rolling dice and playing.

A **turn** is played by rolling the two dice, resulting in numbers *X* (one die) and *Y* (the other one). The player then moves two stones (he can choose which), one by *X* places, the other by *Y* places. He can also move the same stone, but the move still counts as moving twice, i.e. first moving the stone by *X*, then moving it again by *Y*, or vice versa (this may be important in regards to rules explained later). If *X* and *Y* are the same, the numbers are doubled, so the player gets 4 numbers to play: *X*, *X*, *X*, *X* -- for example rolling 2 and 2, the player can move 4 stones, each by 2, or 1 stone by 8 (in separate steps) or 1 stone by 2 and other one by 6 and so on. Moves cannot be skipped by choice, the player has to move "as much as he can", i.e. if he can at least partially use the numbers he rolled, he has to (also if there is a choice between higher and lower number rolled, he has to use the higher number etc.).

**Movement**: players move their stones in opposite directions by the number of steps they roll, in a kind of horseshoe shaped path (as shown above -- topologically the board is just a 1D line, it's just curved to nicely fill the board) -- notice that on one end the stones jump from one side of the board to the other side. Stones can walk over stones of same color and can even stay on the same place -- if more than one stones occupy the same place, they are "stacked" and protected against being taken. A stone can move over enemy stones (even if multiple stacked enemy stones), but can end on such place only if there is exactly one enemy stone, in which case it is taken -- it is removed and placed in the middle of the board. Remember that a stone that is moving by a sum of rolled numbers counts as several discrete moves, so if a stone is moving e.g. by 3 + 3 steps, it's not the same as moving by 6 because after the first 3 steps taken it mustn't land on stacked enemy stones (but it can land on one enemy stone and take it).

A stone that's been taken (placed in the middle of the board) is seen as being one place before the player's starting place (the opposite of one's goal), and can be returned to the game (appearing in the enemy home board) -- in fact it HAS TO be returned to the game before any other move can be made by the player whose stone it is, i.e. if a player has any stones out of the game because the opponent has taken them, he cannot move any other stones until he returns all his stones back to the game.

Once the player has all his stones in the enemy home board, he can start **bearing off**, i.e. getting the stones to the goal (i.e. before this his stones aren't allowed to reach the goal). The goal is seen as a place one after the final board square in the direction of the player's movement -- if the stone gets to the goal, it is placed on the board border. Here there are a bit more complex rules: normally a stone may reach the goal only if it steps on it exactly, i.e. a stone on the very last place can only get to the goal by rolling 1, the stone before it by rolling 2 etc. However the stone furthest away from the goal may also use a value higher than this, i.e. if there is a stone 3 places before the goal AND it is the last one back, it may finish with 3, 4, 5 or 6. During bearing off the player may also use the lower rolled value first, even if it wouldn't fully utilize the higher value (exception to a rule mentioned above).

## Details

Despite chance playing some role, skill is highly important and there exist strategies and tactics that maximize once chance of winning -- for example a basic realization is that the different sums you may roll don't have the same probabilities, e.g. 8 can be achieved by 2 + 6 or 2 + 2 + 2 + 2, but 3 only as 2 + 1 -- one can account for this. The highest probability to take the enemy stone with one's own stone is when the stones are 6 places apart. Taking enemy stone while having own stones stacked in all places in enemy home board makes opponent unable to play (he is required to return the stone to play but there is no number that can do it for him). There is also some opening theory.

The game is internationally governed by WBGF (World Backgammon Federation), similarly to how chess is governed by FIDE.

Who was the **best player ever**? There doesn't seem to be a clear consensus, but Masayuki Mochizuki (Japan) seems to come up very often as an answer to the question, other names include Paul Magriel, Nack Ballard etc.

Backgammon was the first board game in which the world champion at the time (Luigi Villa) was defeated by [computer](computer.md) -- this happened in 1979. This was perhaps thanks to the element of chance.

As for backgammon **computer engines** the best [free as in freedom](free_software.md) one seems to be [GNU](gnu.md) backgammon, using [neural networks](neural_network.md), apparently beyond the strength of best human players. The Extreme Gammon engine is probably a bit stronger (currently said to be the strongest) but it is [proprietary](proprietary.md) and therefore unusable.

Some statistics about the game: there are 18528584051601162496 legal positions. Average branching factor (considering all possible dice rolls) is very high, somewhere around 400, which is likely why space search isn't as effective as in chess and why neural networks greatly prevail. Average number of moves in a game seem to be slightly above 20.

TODO