# <3 Love <3

*"The greatest crime is preaching love."* --Karel Kryl

Love is a deep feeling of liking and affection towards someone or something, usually accompanied by a strong emotion. There are many different kinds of love and love has always been one of the most important feelings that higher living being are capable of, it permeates human [art](art.md), culture and daily lives. Unconditional [selfless](selflessness.md) love towards all living beings is the basis of [less retarded society](less_retarded_society.md).

What is the opposite of love? Many say it is [hatred](hate.md), even though it may also very well be argued that it is rather indifference, i.e. just "not caring", because hate and love often come hand in hand and are sometimes actually very similar -- both hate and love arouse strong emotion, even obsession, and can be present at the same time (so called love-hate relationship). Love sometimes quickly changes to hate and vice versa.

As mentioned, **love is not a single feeling**, there are many types of it, for example parental love, love of a life partner, platonic love, self love, love for a friend, towards [God](god.md), of pet animal, love of [art](art.md), knowledge, [life](life.md), nature, as well as selfish obsessive love, [selfless](selflessness.md) love and many others. Some kinds of love may be so rare and complex that it's hard to describe them, for example it is possible to passionately love a complete stranger merely for his existence, without feeling a sexual desire towards him. One may love a [beautiful](beauty.md) mathematical formula and even people who hurt him. Love is a very complex thing.

Is there a **good real life example of unconditional selfless love**? Yes. When a fascist [Brenton Tarrant](brenton_tarrant.md) shot up the Christchurch mosques on 15 March 2019 and killed 51 people, there was a woman among them whose husband said after the incident he wanted to hug Tarrant. The husband was also present during the shooting. Not only has he forgiven the killer of his wife and someone who almost also murdered him alone, he showed him loved, something which must have been unimaginably difficult and something that proved him one of the most pure people on this planet. He said about it the following (paraphrased for copyright concerns): "There is no use in anger. Anger and fight will not fix it, only with love and caring can we warm hearts. [...] I love him because he is a human being, he is my brother. [...] I don't support his act. [...] But perhaps he was hurt in his life, perhaps something happened to him. [...] Everyone has two sides, a bad one and a good one; bring out the good in you.". (source: https://www.mirror.co.uk/news/world-news/husband-forgives-new-zealand-terrorist-14154882) { This moved me so much when I read it, I can't explain how much this affected my life. I have so much admiration for what this man said and I wish I could follow his message for my whole life. Only the words of the man alone have awoken so much of the purest love in me towards every living being on this planet, which I didn't even know existed. ~drummyfish }

In the past selfless love was often felt by mothers for their children (fathers not so much), although that's not the case anymore in [21st century](21st_century.md).

The only true love is unconditional love, love that doesn't punish. **Conditional love is not true love**, it is [evil](evil.md) trying to deceive by likening itself to something good. If love is conditioned, it is just a commodity offered for certain price, i.e. just another form of [business](business.md). Treating love as a form of [capital](capital.md) is the only way practiced in western world, especially the [USA](usa.md), an American is physically incapable of comprehending even the idea of unconditional love.

## The Way Of Love

[LRS](less_retarded_society.md) advocates living the way of love -- loving everyone and treating others with love, and making the whole world be so. If you have love in you, how can you press the trigger of a gun to kill? How can you kill an animal? How can you detonate a nuclear bomb over a city?

Our hope dwells in love being contagious; just like hate spawns hate, love gives growth to more love. Love is able to stop the self-sustaining circle of hate and revenge. If you show a true, unconditional love to someone who hates you, there is a great chance the hatred will be lost, that grievances will be forgiven and forgotten.

Today's society makes love kind of a commodity, as anything else; a subject of speculation, a tool of manipulation, sometimes even a weapon, a card to be kept hidden and played at the right time. People are taught to hide their feelings, that compassion is weakness and altruism stupidity, and so people are afraid to tell others they love them as it might make them look weak, vulnerable and social outcast, it might be socially unacceptable. We reject such toxic [bullshit](bullshit.md). If you love someone, whoever it is, tell him. You will soon be surrounded with loving people this way.

{ I know this from experience, once I truly started loving others unconditionally, I made many past enemies into great friends, and I saw many of them turn to being actually very nice people. It is just such a great feeling to let go of hate and so heartwarming to make peace with people <3 ~drummyfish }

## See Also

- [polyamory](polyamory.md)