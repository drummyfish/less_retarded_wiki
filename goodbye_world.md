# Goodbye World

Goodbye world is a [program](program.md) that is in some sense an opposite of the traditional [hello world](hello_world.md) program. What exactly this means is not strictly given, but some possibilities are:

- It just prints *goodbye world*, the programmer writes the program and never touches the language again.
- It is the last program a programmer writes before death, either unknowingly or possibly as a [suicide](suicide.md) note.
- Just as hello world shows the very basics of a language, a goodbye world may showcase the most advanced or masterful concepts of the language.
- It is a program that erases itself or possibly the whole [operating system](os.md) etc.
- TODO: more ideas?