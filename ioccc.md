# International Obfuscated C Code Contest

The International Obfuscated C Code Contest (IOCCC for short) is an annual online contest in making the most creatively [obfuscated](obfuscation.md) programs in [C](c.md). It's kind of a "just for [fun](fun.md)" thing but similarly to [esoteric languages](esolang.md) there's an element of [art](art.md) and clever [hacking](hacking.md) that carries a high value. While the [productivity freaks](productivity_cult.md) will undeniably argue this is nothing more than a waste of time, the true programmer appreciates the depth of knowledge and creative thinking required to develop a beautifully obfuscated program. The contest has been around since 1984 and was started by Landon Curt Noll and Larry Bassel.

Unfortunately some [shit](shit.md) is surrounding IOCCC too, for example confusing licensing -- having a [CC-BY-SA](cc_by_sa.md) license in website footer and explicitly prohibiting commercial use in the text, WTF? Also the team started to use [Microshit](microsoft.md)'s [GitHub](github.md). They also allow latest [capitalist](capitalist_software.md) C standards, but hey, this is a contest focused on ugly C, so perhaps that makes sense after all.

Hacking the rules of the contest is also encouraged and there is an extra award for "worst abuse of the rules".

To list a few common ideas employed in the programs let's mentioned these:

- formatting source code as [ASCII art](ascii_art.md)
- misleading identifiers and comments
- extreme [macro](macro.md)/[preprocessor](preprocessor.md) abuse
- abuse of compiler flags
- different behavior under different C standards
- doing simple things the hard way, e.g. by avoiding loops
- including weird files like `/dev/tty` or recursively including itself
- [code golfing](code_golf.md)
- weird stuff like the main function [recursion](recursion.md) or even using it as a signal handler :)
- ...

And let us also mention a few winning entries:

- program whose source code is taken from its file name (using `__FILE__`)
- [ray tracer](ray_tracing.md) in < 30 LOC formatted as ASCII art
- operating system with multi-tasking, GUI and filesystem support
- neural [machine learning](machine_learning.md) on text in < 4KB
- program printing "hello world" with error messages during compilation
- [X11](x11.md) Minecraft-like game
- [web browser](web_browser.md)
- self-replicating programs
- ...

## See Also

- [NaNoGenMo](nanogenmo.md)
- [SIGBOVIK](sigbovik.md)
- C [compiler bombs](compiler_bomb.md)