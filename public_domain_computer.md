# Public Domain Computer

Public domain computer is yet nonexistent but [planned](needed.md) and highly desired [simple](kiss.md) ethical [computer](computer.md) (in the common meaning of the word) whose specification is completely in the [public domain](public_domain.md) and which is made with completely [selfless](selflessness.md) [LRS](lrs.md)-aligned goal of being absolutely non-malicious and maximally helpful to everyone, with very small [freedom distance](freedom_distance.md). It should be the "people's computer", a simple, [suckless](suckless.md), user-respecting [hackable](hacking.md) computer offering maximum [freedom](free_software.md), a computer which anyone can study, improve, manufacture and repair without paying any "[intellectual property](intellectual_property.md)" fees, a computer which people can buy (well, while money still exist) for extremely low price and use for any purpose without being abused or oppressed.

"Public domain computer" is just a temporary placeholder/general term, the actual project would probably be called something different.

The project is basically about asking: what if computers were designed to serve us instead of corporations? Imagine a computer that wouldn't stand in your way in whatever you want to do.

In our [ideal society](less_retarded_society.md), one of the versions of the public domain computer could be the [less retarded watch](less_retarded_watch.md).

Note that **the computer has to be 100% from the ground up in the true, safe and worldwide [public domain](public_domain.md)**, i.e. not just "[FOSS](foss.md)"-licensed, partially open etc. It should be created from scratch, so as to have no external [dependencies](dependency.md) and released safely to the public domain e.g. with [CC0](cc0.md) + patent waivers. Why? In a [good society](less_retarded_society.md) there simply have to exist basic tools that aren't owned by anyone, tools simply available to everyone without any conditions, just as we have hammers, pencils, public domain mathematical formulas etc. -- computing has become an essential part of society and it certainly has to become a universal "human right", there HAS TO exist an ethical alternative to the oppressive [capitalist technology](capitalist_technology.md) so that people aren't forced to accepting oppression by their computers simply by lack of an alternative. Creating a public domain computer would have similarly positive effects to those of e.g. [universal basic income](ubi.md) -- with the simple presence of an ethical option the oppressive technology would have a competition and would have to start to behave a bit -- oppressive capitalist technology nowadays is possibly largely thanks to the conspiracy of big computer manufacturers that rely on people being de facto obliged to buy one of their expensive, [proprietary](proprietary.md), [spyware](spyware.md) littered non-repairable consumerist computer with secret internals.

**The computer can (and should) be very [simple](KISS.md)**. It doesn't -- and shouldn't -- try to be the way capitalist computers are, i.e. it would NOT be a typical computer "just in the public domain", **it would be different by basic design philosophy** because its goals would completely differ from those of capitalists. It would follow the [LRS](lrs.md) philosophy and be more similar to the very first personal computers rather than to the "[modern](modern.md)" HD/[bloated](bloat.md)/superfast/fashion computers. Let us realize that even a very simple computer can help tremendously as a great number of tasks people need can actually be handled by pretty primitive computers -- see what communities do e.g. with [open consoles](open_console.md).

Even a pretty simple computer without an [operating system](os.md) is able to:

- Browse much of the [Internet](internet.md), e.g. [smol web](smol_internet.md) (no [JavaScript](js.md) websites, [gopher](gopher.md), ...).
- Handle communication, e.g. [email](email.md), [IRC](irc.md), ...
- Allow reading, writing and storing [books](book.md), e.g. those from [Project Gutenberg](gutenberg.md) or offline [Wikipedia](wikipedia.md) -- this can tremendously help education e.g. in the third world.
- Run basic [software](software.md) such as calculator, stopwatch, calendar, note taking, alarm clock, memory-card reader, picture viewer, even simple [games](game.md) etc.
- Serve as an [embedded](embedded.md) computer, e.g. [DYI](dyi.md) people and small business may use the computer in similar ways [Raspberry pi](rpi.md) is used nowadays (auto switching lights, opening doors, recording data from sensors, tiny robots, ...).
- Be programmed and serve as an educational tool for programming.
- Do many scientific calculations.
- Control [peripherals](peripheral.md) through simple interfaces.
- Handle simple [multimedia](multimedia.md) such as low-res images and animations, 8bit sounds... 
- ...

## Details

The project wouldn't aim to create a specific single "model" of a computer but rather blueprints that would be easily adjusted and mapped to any specific existing technology -- the goal would be to create an abstract [hardware](hardware.md) specification as well as basic [software](software.md) for the computer.

Abstract hardware specification means e.g. description on the [logic gate](logic_gate.md) level so that the computer isn't dependent on any contemporary and potentially proprietary lower level technology such as [CMOS](cmos.md). The project would simply create a big [logic circuit](logic_circuit.md) of the computer and this description could be compiled/synthesized to a lower level circuit board description. The hardware description could also be parameterized so that certain features could be adjusted -- for example it might be possible to choose the amount of [RAM](ram.md) or disable specific CPU instructions to make a simpler, cheaper circuit board.

**The computer would have to be created from the ground up**, with every design aspect following the ultimate goal. The project roadmap could look similarly to this one:

1. Create a programming language that will be usable both as a scripting and compiled language for the computer. We already have one -- [comun](comun.md) -- though it is not fully finished yet. Now we can already start writing software for the computer. Optionally make other languages such as [C](c.md) compile to our ISA.
2. Design a simple [instruction set architecture](isa.md) (ISA). This will provide some challenge but will be doable.
3. Write basic [software](software.md) in our language, mainly:
  - Custom tools for designing, simulating and testing [logic circuits](logic_citcuit.md). Not extremely difficult if we [keep it simple](kiss.md).
  - [Emulator](emulator.md) of our custom ISA so that we can run and test it on our current computers. It will also be useful to make our computer possible to be run as a virtual hardware on other platforms.
  - [Shell](shell.md) that will serve to performing basic tasks with the computer, e.g. using it as a calculator or interactively programming it in simple ways. The shell will also serve as a kind of [operating system](operating_system.md), or rather a simple program loader. For now the shell can run on our current computers where we can test it and fine tune it. Implementation of this could be the [comun shell](comun_shell.md).
  - Compiler -- this basically just means self hosting our compiler.
  - Basic tools like a text editor, compression utility, image editor etc.
4. With the logic circuit tools design a simple [MCU](mcu.md) computer based on the above mentioned ISA. This is doable, there are hobbyists that have designed their own 8bit CPUs, a few collaborating people could definitely create a nice MCU if they keep it simple (no caching, no floating point, no GPUs, ...).
5. Compile the MCU logic-level description to an actual circuitboard, possibly even with [proprietary](proprietary.md) tools if other aren't available -- this may be fixed later.
6. Manufacture the first physical computer, test it, debug it, improve it, give it to people, ...
7. Now the main goal has been touched for the first time, however the real [fun](fun.md) only begins -- now it is needed to spread the project, keep improving it, write more software such as [games](game.md) etc. :)

## See Also

- [Project Oberon](oberon.md)
- [comun](comun.md)
- [comun shell](comun_shell.md)
- [uxn](uxn.md)
- [less retarded watch](less_retarded_watch.md)
- [PDOS](pdos.md)
