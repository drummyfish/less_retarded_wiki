# License

License is a legal text by which we grant some of our exclusive rights (e.g. [copyright](copyright.md) or [patents](patent.md)) over [intellectual](intellectual_property.md) works to others. To [us](lrs.md) a license is what enables us to legally implement [free (as in freedom) software](free_software.md) (as well as [free culture](free_culture.md)): we attach a license to our program (or other work) which says that we grant to everyone the basic freedom rights to our software/work with optional conditions (which must not be in conflict with free software definition, e.g. we may require [attribution](attribution.md) or [copyleft](copyleft.md), but we may NOT require e.g. [non-commercial](nc.md) use only). Licenses used to enable free software are called *free licenses* ([open source](open_source.md) licenses work the same way). Of course, there also exist [non-free](proprietary.md) licenses called [EULAs](eula.md), but we stay away from these -- from now on we will implicitly talk about free licenses only. Licenses are similar to [waivers](waiver.md).

**You shall always use a free license for your software.**

There exist fun/parody licenses like [WTFPL](wtfpl.md) (Do What the Fuck You Want to Public License) -- these are cool as a fun meme, though legally they may be invalid as they are too vague and the language could just make it look like a statement not meant seriously to the court, anything licensed this way should rather be seen as a licenseless work. It's better to not seriously use these, or if you do, dual license alongside with some "serious" license.

Free licenses are mainly divided into:

- **[copyleft](copyleft.md)**: Licenses that require that further modifications of the work will still remain free, i.e. "forcing freedom". Example of such licenses are [GPL](gpl.md) and [CC BY-SA](cc_by_sa.md). Copyleft licenses are a bit more associated with free software (as opposed to open source) as the main free software organization -- [GNU](gnu.md) -- advocates them because they disallow corporations to take free programs and make them into proprietary ones.
- **[permissive](permissive.md)**: Licenses that basically allow to "do anything you want" (though usually still requiring e.g. credit to the original author), even making modified non-free versions of the work. Most famous example is the [MIT](mit.md) license. Though not strictly so, permissive licenses are a bit more associated with open source than free software as they are friendlier to business (one can "unfree" new versions of a software at any time if it's desirable for money making; of course old versions of the program will still remain free), however some prefer them for other reasons, e.g. greater legal simplicity and not wanting to force a "correct" use of one's work.

At [LRS](lrs.md) we highly prefer [public domain](public_domain.md) [waivers](waiver.md) such as [CC0](cc0.md) instead of licenses, i.e. we release our works without any conditions/restrictions whatsoever (e.g. we don't require credit, [copyleft](copyleft.md) and similar conditions, even if by free software rules we could). This is because we oppose the very idea of being able to own information and ideas, which any license is inherently based on. Besides that, licenses are not as legally [suckless](suckless.md) as public domain and they come with their own issues, for example a license, even if free, may require that you promote some political ideology you disagree with (see e.g. the principle of [+NIGGER](plusnigger.md)).

Some most notable free licenses for software include (FSF: FSF approved, OSI: OSI approved, LRS: approved by us, CF: copyfree approved, short: is the license short?):

| license                 | type                                         | FSF | OSI | CF  | LRS |short|
| ----------------------- | -------------------------------------------- | --- | --- | --- | --- | --- |
| [Apache 2](apache.md)   | permissive, conditions                       |**+**|**+**|  -  |  -  |  -  |
| [AGPL](agpl.md)         | network copyleft                             |**+**|**+**|  -  |  -  |  -  |
| [BSD (0,1,2,3)](bsdl.md)| permissive                                   |**+**|**+**|some |  -  |**+**|
| [BOML](boml.md)         | permissive                                   |  -  |  -  |**+**|  -  |**+**|
| [CC0](cc0.md)           | [PD](public_domain.md) waiver, no conditions |**+**|  -  |**+**|**+**|  -  |
| [GPLv2, GPLv3](gpl.md)  | copyleft (strong)                            |**+**|**+**|  -  |  -  |  -  |
| [LGPL](lgpl.md)         | copyleft (weak)                              |**+**|**+**|  -  |  -  |  -  |
| [MIT](mitl.md)          | permissive, credit                           |**+**|**+**|**+**|**+**|**+**|
| [MIT-0](mitl.md)        | permissive, no conditions                    |  -  |**+**| +?  |**+**|**+**|
|[Unlicense](unlicense.md)| PD waiver, no conditions                     |**+**|**+**|**+**|**+**|**+**|
| [WTFPL](wtfpl.md)       | permissive, fun                              |**+**|  -  |  ?  |  -  |**+**|
| [zlib](zlib.md)         | permissive                                   |**+**|**+**|  -  |  -  |**+**|
| [0BSD](bsdl.md)         | permissive, no conditions                    |  -  |**+**| +?  |**+**|**+**|
| [WPPD](wppd.md)         | PD waiver, no conditions, in draft phase     |     |     |     |     |     |

Some most notable free licenses for general artworks and data (not just programs) include:

- Some [Creative Commons](creative_commons.md) licenses (but not ALL), most notably [CC BY](cc_by.md), [CC BY-SA](cc_by_sa.md) and [CC0](cc0.md).
- Some forms of [GFDL](gfdl.md) -- those the license is called "free", it may actually optionally include "invariant sections" that serve to insert unmodifiable propaganda; if such sections are present, the license is by definition not free.
- ...

## How To

If you're a noob or even an advanced noob and want to make sure you license correctly, consider the following advice:

- **Actually use a license or waiver**. Code without a license/waiver is proprietary. Statement like "do whatever you want" or "public domain" is legally absolutely insufficient and is worth nothing.
- **If you're collaborating with other people, put on a license ASAP.** Any change in legal conditions require an agreement of all authors so if you're developing code with dozen of people and then decide to add a license to it, you have to contact everyone and get a permission, and of course that can get difficult with more developers.
- **DO NOT fucking write "all rights reserved" if you're using a free license** since that literally means you're NOT reserving all the rights.
- Know that normally **you cannot take back your permissive license**, i.e. if you actually release something under permissive terms under a correct non-revokable waiver/license, you cannot introduce stricter conditions later on. What you CAN do is relax and drop conditions (e.g. [copyleft](copyleft.md)) of a license later on. I.e. you can make something strict less strict but not vice versa.
- **DO NOT use your own license**. Use an existing one. Firstly you're not a lawyer and secondly even if you are, your license will be non-standard, untested in practice, possibly buggy, untrusted and missing from the usual accepted license lists.
- **DO NOT modify existing licenses** (except for some special license modifiers, you should have experience to use these). You may add some conditions to the license if the license allows it and you should do it clearly, but do NOT change the text of the original license unless you change its name.
- **Put the license text into `LICENSE` or `COPYING` file in the root of your repository**. You can also put it as a comment in the header of your source code file and mention the license in `README`. Doing all of these is best. Be as clear and explicit as possible.
- **Read the license or at least its summary before you use it** so that you know what you can demand without violating it. If you use CC0 and then demand attribution, it's clear you don't know what you're doing and your work is seen as legally unsafe.
- **Be as clear as possible**, it's better to be extra clear and show your intent of using your license. Include a sentence such as "I release this code under XYZ (link)." Mention license version number and URL to its text.
- **Be extra clear and explicit about what your license covers, especially with non-software files**. E.g. when developing a game which has asset files such as 3D models, say if your license also applies to these files.
- **Have a list of authors and a reasonable evidence of their license acceptance.** This is in case an actual investigation takes place in legal case: authors need to be known (commit history, contributors.txt, ...) and it needs to be clear they knew a license was present and they agreed to it (e.g. the `LICENSE` file must have been present at the time of their contribution).
- **Think from the user's POV and consider worst case legal scenario**. Ask yourself: if I'm someone else and use this project commercially and for something controversial, am I well protected by the license? The answer has to be yes.
- **Include additional waivers if your license doesn't e.g. waive patents** (for example with [CC0](cco.md)).
