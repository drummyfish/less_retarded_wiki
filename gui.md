# Graphical User Interface

"*Always  add a commandline interface to your software. Graphical User interfaces are for sissies.*" --[bitreich](bitreich.md) manifesto

Graphical user interface (GUI) is a visual [user interface](ui.md) that uses graphical elements such as images, many distinct [colors](color.md) and fine geometrical shapes. This stands in contrast with [text user interface](tui.md) (TUI) which is also visual but only uses text for communication (TUI can further be pure line-based [command line interface](cli.md) or a graphical, grid-based text interface that mimics a true GUI, e.g. with [ASCII art](ascii_art.md)).

Expert computer users normally frown upon GUI because it is the "noobish", inefficient, limiting, cumbersome, hard to automate, [capitalist](capitalism.md) consumer-style way of interacting with computer. GUI brings [complexity](complexity.md) and [bloat](bloat.md), they are slow, inefficient, distracting, hard to automatize and test. GUI is like baby pictures or simple books with pictures for [illiterate people](plan9.md) and Chimpanzees, for people who need to click on boxes and have flashing strip club colors to keep them entertained, while command line is for normal adults who can read and want to do thing efficiently -- there is a reason why [programming languages](programming_language.md) are plain text (with exception of the ones literally aimed at babies in kindergarden). [Text is universal interface](unix_philosophy.md); text is a [formal language](formal_language.md) that can be handled mathematically well, it is a [stream](stream.md) of data easy to process by any languages, etc. So we try not to use GUIs and prefer the [command line](cli.md).

"[Modern](modern.md)" GUIs mostly use [callback](callback.md)-based programming, which again is more complicated than standard polling non-interactive I/O. If you need to do GUI, just use a normal infinite loop FFS.

## When And How To Do GUI

GUI is not forbidden, it has its place, but today it's way too overused -- it should be used only if completely necessary (e.g. in a painting program) or as a completely optional thing built upon a more [suckless](suckless.md) text interface or [API](api.md). So remember: first create a program and/or a [library](library.md) working without GUI and only then consider creating an optional GUI [frontend](frontend.md). GUI must never be tied to whatever functionality can be implemented without it.

Still, when making a GUI, you can make it [suckless](suckless.md) and lighthweight. Do your buttons need to have reflections, soft shadows and rounded anti-aliased borders? No. Do your windows need to be transparent with light-refraction simulation? No. Do you need to introduce many MB of dependencies and pain such as [QT](qt.md)? No.

The ergonomics and aesthetic design of GUIs has its own field and can't be covered here, but just keep in mind some basic things:

- Don't have too many elements (buttons etc.) at the screen at once, it's confusing as hell and drives noobs away.
- Things must be intuitive, i.e. behave in a way that they normally do (e.g. main menu should be at the top of the window, not the bottom etc.).
- Just use your brain. If a button is important and often used, it should probably be bigger than a button that's used almost never, etc.

The million dollar question is: **which GUI framework to use?** Ideally none. GUI is just pixels, buttons are just rectangles; make your GUI simple enough so that you don't need any shitty abstraction such as widget hierarchies etc. If you absolutely need some framework, look for a suckless one; e.g. [nuklear](nuklear.md) is worth checking out. The suckless community sometimes uses pure [X11](x11.md), however that's not ideal, X11 itself is kind of bloated and it's also getting obsoleted by  [Wayland](wayland.md). The ideal solution is to make your GUI **backend agnostic**, i.e. create your own very thin abstraction layer above the backend (e.g. X11) so that any other backend can be plugged in if needed just by rewriting a few simple functions of your abstraction layer (see how e.g. [Anarch](anarch.md) does rendering).

## State Of Mainstream GUI

Nowadays there are a great many GUI [libraries](library.md), [frameworks](framework.md), standards and [paradigms](paradigm.md), and it may be a bit hard to digest them at once.

TODO: some general shit bout graphical windows vs the "single window" mobile and web UI, analysis of the "GUI stack" (Linux framebuffer, X window, widget toolkits etc.), basic widgets etc.