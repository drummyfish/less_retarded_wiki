# Color

Color (also *colour*, from *celare*, "to cover") is the perceived visual quality of light that's associated with its [wavelength](wavelength.md)/[frequency](frequency.md) (or mixture of several); for example [red](red.md), [blue](blue.md) and [yellow](yellow.md) are colors. [Electromagnetic](electromagnetism.md) waves with wavelength from about 380 to 750 nm (about 400 to 790 THz) form the **visible spectrum**, i.e. waves our eyes can see -- combining such waves with different intensities and letting them fall on the retina of our eyes gives rise to the perception of color in our brain. There is a hugely deep *color theory* concerned with the concept of color (its definition, description, reproduction, psychological effect etc.). Needless to say colors are extremely important in anything related to visual [information](information.md) such as [art](art.md), [computer graphics](graphics.md), astrophysics, various visualizations or just everyday perception of our world. Color support is sometimes used as the opposite of systems that are extremely limited in the number of colors they can handle, which may be called [monochromatic](monochrome.md), 1bit (distinguishing only two colors), black&white or [grayscale](grayscale.md). Color can be seen to be in the same relation to sight as [pitch](pitch.md) is to hearing.

**How many colors are there?** The number of colors humans can distinguish is of course individual (color blindness makes people see fewer colors but there are also conditions that make one see more colors), then also we can ask what color really means (see below) but -- approximately speaking -- various sources state we are able to distinguish millions or even over 10 million different colors on average. In computer technology we talk about **color depth** which says the number of [bits](bit.md) we use to represent color -- the more bits, the more colors we can represent. 24 bits are nowadays mostly used to record color (8 bits for each red, green and blue component, so called *true color*), which allows for 16777216 distinct colors, though even something like [16 bits](rgb565.md) (65536 colors) is mostly enough for many use cases. Some advanced systems however support many more colors than true color, especially extremely bright and dim ones -- see [HDR](hdr.md).

**What gives physical objects their color?** Most everyday objects get its color from reflecting only specific parts of the white light (usually sunlight), while absorbing the opposite part of the spectrum, i.e. for example a white object reflects all incoming light, a black one absorbs all incoming light (that's why black things get hot in sunlight), a red one reflects the red light and absorbs the rest etc. This is determined by the qualities of the object's surface, such as the structure of its atoms or its microscopic geometry.

TODO

## What Is Color?

This is actually a non-trivial question, or rather there exist many varying definitions of it and furthermore it is a matter of subjective experience, perception of colors may differ between people. When asking what color really is, consider the following:

- Are non-primary colors true colors, or just mixtures of the primary colors? Red, green and blue are the three primary colors, the ones we can mix all other colors from. Many will say yes, non-primary colors are colors. But hold on.
- Are non-spectral colors colors or just mixtures of spectral colors? Spectral colors are the colors with a single wavelength (e.g. red, orange or violet), other colors (like pink) are just mixtures of these. Again, probably yes.
- Is [saturation](saturation.md) part of color, or a separate attribute? I.e. are e.g. green and greenish gray different colors, or same colors with different saturation? Now it depends.
- Is [black](black.md) a color, or rather a lack of a color? E.g. in computers it is usually treated just as another color, but [real world](irl.md) black is really the absence of any light.
- Is [white](white.md) a color? If we are using a subtractive color model, the argument is the same as for black (white paper is really just lack of any color on it).
- Is e.g. gold a color? Or just yellow with a lot of [specular reflection](specular.md)? In real world many things may be called to have a gold color, but in computer graphics we would likely separate the color from the light reflective attribute (such as metalicity).
- Is transparent a color?
- Is intensity part of color (especially in context of e.g. [HDR](hdr.md))? For example we might say both [Sun](sun.md) and paper are white, but still Sun's color is much "stronger" -- is it therefore a "whiter white" than that of a paper?
- Are colors not perceivable by average human colors? Many animals see colors we can't see (e.g. those in [infrared](infrared.md) spectrum), but there are also rare cases of humans (so called tetrachromats) who see many more colors than usual thanks to a mutation.
- Are [impossible colors](impossible_color.md) colors? Interestingly there exist colors perceivable by average humans which however cannot naturally be seen due to "physics" -- they can however be seen with "eye [hacks](hacking.md)". Do we count these too?
- ...

## Color In Computers/Programming

TODO: RGB, indexed color, depth, HDR, YUV, HSV etc.
