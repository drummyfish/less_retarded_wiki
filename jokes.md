# Jokes

Here you can shitpost your jokes that are somehow related to this wiki's topic. Just watch out for [copyright](copyright.md) (no copy-pasting jokes from other sites)!

Please do NOT post lame "big-bang-theory"/[9gag](9gag.md) jokes like *sudo make sandwich* or *there are 10 types of people*.

Also remember the worst thing you can do to a joke is put a [disclaimer](disclaimer.md) on it. Never fucking do that.

{ Many of the jokes are original, some are shamelessly pulled from other sites and reworded. I don't believe [copyright](copyright.md) can apply if the expression of a joke is different, ideas can't be copyrighted. Also the exact origins of jokes are difficult to track so it's probably a kind of folklore. ~drummyfish }

{ I would like to thank one anonymous friend who contributed to me many ideas for jokes here :D I usually modified them slightly. ~drummyfish }

- [C++](cpp.md)
- Why is the maximum speed called terminal velocity? Because [GUI](gui.md)s are slow.
- What's the worst kind of [lag](lag.md)? Gulag.
- Ingame chat: "What's the country in the middle of north Africa?" [{BANNED}](niger.md)
- What sound does an angry [C](c.md) programmer make? ARGCCCC ARGVVVVVVVV
- I have a mentally ill friend who tried to design the worst [operating system](os.md) on purpose. It boots for at least 10 minutes, then it changes the user's desktop background to random ads and it randomly crashes to make the user angry. He recently told me he is getting sued by [Microsoft](microsoft.md) for violating their look and feel.
- I am using a super minimal system, it only has one package installed on it. It is called [systemd](systemd.md) (alternatively [Emacs](emacs.md)).
- Any [Windows](windows.md) tutorial is really best called a "crash course".
- How do you know a project is truly [suckless](suckless.md)? The readme is longer than the program itself.
- How do you know a project is truly [LRS](lrs.md)? Its manifesto is longer than the program itself.
- How do you know a project is truly [bloated](bloat.md)? Instructions on how to build it are longer than whole specification of a [suckless programming language](comun.md). { Also [tranny software](tranny_software.md): [COC](coc.md) is longer than source code etcetc. ~drummyfish }
- Do you use [Emacs](emacs.md)? No, I already have an [operating system](os.md).
- Do you use [Emacs](emacs.md)? No, I already have a [waifu](waifu.md).
- Do you use [Emacs](emacs.md)? No, I already have carpal tunnel. Etc. :D
- `alias bitch=sudo`
- What's a trilobyte? 8 trilobits.
- "Never test for a bug that you don't know how to fix." --manager; "If we cannot fix it, it isn't broken." --also manager
- a joke for minimalists:
- When is [Micro$oft](microsoft.md) finally gonna make a product that doesn't suck???! Probably when they start manufacturing vacuum cleaners.
- Can [free software](free_software.md) lead to insanity? I don't know, but it can make you [GNU](gnu.md)ts.
- Man sits on a toilet, taking a [shit](shit.md), he takes a piece of toilet paper, wipes sweat off of his face, then wipes his ass with it and goes away. The next day he does the same, he sits, takes a shit, wipes his face, wipes his ass, goes away. The third day he goes to the toilet, takes a shit, wipes his ass, wipes his face... oh shit :D { I came up with this when I was taking a shit. ~drummyfish }
- Political activists walk into a bar. [Pseudoleftist](pseudoleft) tells his friends: "hey guys, how about we have oppressive rulers and call them a [government](government.md)?" Capitalist says: "well no, let's have oppressive rulers and call them [corporations](corporation.md)". [Liberal](liberal.md) replies: "Why not both?". Monarchist goes: "no, it's all wrong, let's have oppressive rulers and call them Kings." To this pseudo communist says: "that's just shit, let's have oppressive rulers and call them the [proletariat](proletariat.md)". Then [anarcho pacifist](anpac.md) turns to them and says: "Hmmm, how about we don't have any oppressive rulers?". They lynch him.
- There are a lot of jokes at https://jcdverha.home.xs4all.nl/scijokes/. Also http://textfiles.com/humor/JOKES/, http://textfiles.com/humor/TAGLINES/quotes-1.txt and so on. Also on [wikiwikiweb](wikiwikiweb.md) under *CategoryJoke*, *ProgrammerLightBulbJokes*, also https://www.gnu.org/fun/ etc.
- Hello, is this anonymous [pedophile](pedophilia.md) help hotline? Yes. My 8yo daughter begs for sex, can we do penetration right away or should we start with anal?
- What do you call a [woman](woman.md) that made a computer explode just by typing on it? Normal.
- Does the invisible hand exist in the [free market](free_market.md)? Maybe, but if so then all it's doing is masturbating (or giving us a middle finger).
- 90% of statistics are fake.
- When will they remove the *[touch](touch.md)* and *[kill](kill.md)* commands from [Unix](unix.md)? Probably when they rename *[man pages](man_page.md)* to *person pages*.
- If [law](law.md) was viewed as a programming code, it would be historically the worst case of [bloated](bloat.md) [spaghetti code](spaghetti_code.md) littered with [magic constants](magic_constant.md), undefined symbols and dead code, which is additionally deployed silently and without any [testing](testing.md). Yet it's the most important algorithm of our society.
- C++ is to C as brain cancer is to brain.
- [Entropy](entropy.md) is no longer what it used to be. Nostalgia too.
- At the beginning there was [machine code](machine_code.md). Then they added [assembly](assembly.md) on top of it to make it more comfortable. To make programs portable they created an [operating system](os.md) and a layer of [syscalls](syscall.md). Except it didn't work because other people made other operating systems with different syscalls. So to try to make it portable again they created a high-level language [compiler](compiler.md) on top of it. To make it yet more comfortable they created yet a higher level language and made a [transpiler](transpiler.md) to the lower level language. To make building more platform independent and comfortable they created [makefiles](makefile.md) on top of it. However, more jobs were needed so they created [CMake](cmake.md) on top of makefiles, just in case. It seems like CMake nowadays seems too low level so a new layer will be needed above all the meta-meta-meta build systems. I wonder how high of a tower we can make, maybe they're just trying to get a Guinness world record for the greatest bullshit sandwich in history.
- How to install a package on [Debian](debian.md)? I don't know, but on my [Arch](arch.md) it's done with `pacman`.
- [green](greenwashing.md) [capitalism](capitalism.md) :'D my sides
- Difference between a beginner and pro programmer? Pro programmer fails in a much more sophisticated manner.
- What is a [computer](computer.md)? A device that can make a hundred million very precise mistakes per second.
- How many [Python](python.md) programmers do you need to change a lightbulb? Only one -- he holds the bulb while the world revolves around him.
- After all it may not take so long to establish our [utopia](less_retarded_society.md). By the time [Windows](windows.md) has updated we will have already done it ten times over.
- One of the great milestones yet left to be achieved by science is to find intelligent life in our Solar System.
- An evil capitalist, good capitalist and [female](woman.md) genius walk in the park. A bee stings one of them. Who did it sting? The evil capitalists, the other two don't exist.
- Cool statistics: 9 out of 10 people enjoy a gang [rape](rape.md).
- Basement hackers never die, they just smell that way. Musicians never die, they just decompose (and musicians working part time are [semiconductors](semicoductor.md)).
- `int randomInt(void) { int x; return x; }`
- Boss: "We're going to need to store additional information about gender of all 1600 people in our database." Me: "OK that's only 200 extra bytes.". Diversity department: "You're fired."
- the [downto](downto.md) operator
- [Schizophrenia](schizo.md) beats being alone.
- Our new [app](app.md) partly adopts the [KISS](kiss.md) philosophy, specifically the "stupid" part.
- I just had sex with a German chick, for some reason she kept yelling her age. (Or maybe she just didn't consent.)
- I find it much more pleasant to browse the web on a 1 bit display, it can't display a [rainbow](lgbt.md).
- What's long and sticky? A stick.
- The term *military intelligence* is an oxymoron. The term *criminal lawyer* is a redundancy.
- Why are [noobs](noob.md) the most pacifist beings in existence? Because they never beat anyone.
- What does short circuited [capacitor](capacitor.md) and [gratis software](freeware.md) have in common? They are free of charge.
- You scratch my [tape](tape.md), I scratch yours.
- There's a new version of Debian Bull's Eye that's compiled exclusively with [Rust](rust.md). Its code name is [Bull's Shit](bullshit.md).
- [Manager](manager.md) is that who thinks 9 women can produce a child in 1 month.
- Have you heard the [atheists](atheism.md) are starting their own non-prophet?
- The latest [gender studies](gender_studies.md) paper concluded that if God exists, he is both man and woman, black and white, child and adult, straight and homosexual. Basically imagine Michael Jackson.
- Those who can, do. Those who cannot, teach. Those who cannot teach, [do business](entrepreneur.md).
- How do you accelerate a [Windows](windows.md) PC? By dropping it out of the window. (Hence the name?)
- Shakespeare for programmers: `0x2b || !0x2b`. { This one is a bit lame but at least it's not so common :D ~drummyfish }
- An [Apple](apple.md) a day keeps [sanity](lrs.md) away.
- The goal of [computer science](compsci.md) is to create things that will last at
- Vision is what capitalist claims to have had after making a correct guess.
least until we're finished building them.
- How many lesbians do you need to screw a lightbulb? Eleven: one to screw it and ten to talk about how great it was doing it without a man.
- Look at that obese singer typing something on her laptop. I think it's a Dell.
- A fine is tax for doing bad, a tax is fine for doing good.
- What do you like most in a [woman](woman.md)? My dick.
- [USA](usa.md) is the fastest progressing country in the world: it managed to jump from the uncivilized stage right to decadence without even going through the transitional stage of civilization.
- Autocorrect is my worst enema.
- Hey I won a box with lifetime supply of condoms in an [assembly](assembly.md) programming competition! Turns out the box was just empty.
- The new version of [Windows](windows.md) is going to be backdoor free! The backdoor will be free of charge.

## See Also

- [LMAO](lmao.md)
- [fun](fun.md)
