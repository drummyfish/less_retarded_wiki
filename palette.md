# Palette

In [computer graphics](graphics.md) palette is a set of possible [colors](color.md) that can be displayed, the term usually refers to a selected smaller subset of all colors that can in theory be displayed (large sets of colors tend to be called [color spaces](color_space.md) rather than palettes). Nowadays mainstream computers are powerful enough to work with over 6 million 24bit [RBG](rbg.md) colors (so called True Color) practically without limitations so the use of palettes is no longer such a huge thing, but with resource-limited machines, such as [embedded](embedded.md) devices and older computers, the use of palettes is sometimes necessary or at least offers many advantages (e.g. saving a lot of memory). Nevertheless palettes find uses even in "[modern](modern.md)" graphics, e.g. in the design of image formats that save space. Palettes are also greatly important in [pixel art](pixel_art.md) as an artistic choice.

Palettes usually contain a few to few thousand colors and the number is normally a power of 2, i.e. we see palettes with number of colors being 8, 16, 256, 2048, etc. -- this has advantages such as efficiency (fully utilizing color indices, keeping memory aligned etc.). Palettes can be general purpose or specialized (for example some image formats such as [GIF](gif.md) create a special palette for every individual image so as to best preserve its colors). Palettes can also be explicitly stored (the palette colors are stored somewhere in the memory) or implicit (the color can somehow be derived from its index, e.g. the [565](565.md) palette).

Palettes are related to **[screen modes](screen_mode.md)** -- systems that work with palettes will usually offer to set a specific screen mode that defines parameters such as screen [resolution](resolution.md) and number of colors we can use, i.e. the number of colors of our palette (we can normally set the colors in a palette). Modes that make use of palettes are called **indexed** because each pixel in memory is stored as an index to the palette (for example if we have a palette {red, yellow, white}, a pixel value 0 will stand for *red*, 1 for *yellow* and 2 for *white*) -- the palette serves as a color [look-up table](lut.md) (CLUT). Non-indexed modes on the other hand store the color directly (i.e. there will typically be a direct RGB value stored for each pixel). We can see that an indexed mode (i.e. choosing to use a palette) will save a lot of memory for the [framebuffer](framebuffer.md) ([VRAM](vram.md)) thanks to reducing the number of bits per [pixel](pixel.md): e.g. when using an 8 bit palette, storing each pixel (index) will take up 1 byte (8 bits, 256 colors) while in a non-indexed 24 bit RGB mode (over 6 million colors) each pixel will take 3 bytes (24 bits), i.e. three times as much. The same goes for using bigger palettes: e.g. using a 16 bit palette (65536 colors) will take four times as much memory for storing pixels than a 4 bit palette (16 colors). Note that even in indexed modes we may sometimes be able to draw pixels of arbitrary color with so called **direct** writes to the display, i.e. without the color being stored in framebuffer. With palettes we may see the use of [dithering](dithering.md) to achieve the illusion of mixing colors.

Using palettes has also more advantages, for example we can cycle the palette colors or quickly switch it for another palette and so e.g. increase contrast or apply some color effect (this trick was used e.g. in [Doom](doom.md)). Palettes can be constructed in clever ways (for example in [Anarch](anarch.md)) so that it is e.g. easy to make a color brighter or darker by simply incrementing or decrementing its index (while increasing brightness of a three-component RGB value is complex and slow) -- as we generally process big numbers of pixels this can lead to tremendous speed ups. Having fewer colors also makes them easier to compare and so easily implement things such as [pixel art upscaling](pixel_art_upscaling.md) (huge number of colors generally forces us to compare pixels with some amount of bias which is slower).

**Can palettes be [copyrighted](copyright.md)?** We hope not, that would indeed be pretty fucked up, however it's not that simple, for example those massive faggots at Pantone literally try to do just that and successfully removed their "proprietary colors" from [photoshop](photoshop.md). [Trademarks](trademark.md) and [trade dress](trade_dress.md) already allowed some kind of ownership of colors or at least their combinations (Milka even tried to trademark a single color), and some websites for sharing palettes claim that a picture of a palette can be copyrighted as some kind of "digital painting", even though they acknowledge a small set of colors as such probably can't be copyrighted. In general copyright MAY apply to selection (abstract set) of things: for example a mere selection of articles from Wikipedia may be considered a copyrightable work, though of course such a "work" (lol) still has to pass some threshold of originality etc. So for maximum safety try to create your own palette (and share it under [CC0](cc0.md) and other waivers just in case, to spare others the same pain) as a first option, as a second option use some common public domain mathematically generated palette (e.g. [332](332.md)) or a palette that's explicitly shared under free terms ([CC0](cc0.md) is probably best), and if you absolutely have to reuse someone else's palette (free or proprietary), at least try to make slight modifications to it by reordering the colors and possibly slightly changing the RGB values.

## Examples

Example of a basic 8 color palette may be (the color notation is in [hexadecimal](hex.md) `#rrggbb` format):

```
#000000 #808080 #ffffff #ff0000 #00ff00 #0000ff #ffff00 #00ffff
black   gray    white   red     green   blue    yellow  cyan
```

The following is a general purpose 256 color palette made by [drummyfish](drummyfish.md) and used in [Anarch](anarch.md). It is based on [HSV](hsv.md) model: it divides colors into 4 saturations, 10 or 11 hues and 8 levels of value ("brightness") which can easily be changed by incrementing/decrementing the color index (which in Anarch was exploited for ligtening up and darkening textures depending on distance).

```
#000000 #242424 #494949 #6d6d6d #929292 #b6b6b6 #dbdbdb #ffffff
#201515 #402a2a #604040 #805555 #a06a6a #c08080 #e09595 #ffaaaa
#201b15 #40372a #605240 #806e55 #a08a6a #c0a580 #e0c195 #ffdcaa
#1d2015 #3b402a #596040 #778055 #95a06a #b3c080 #d1e095 #edffaa
#172015 #2f402a #466040 #5e8055 #75a06a #8dc080 #a5e095 #bcffaa
#152019 #2a4033 #40604c #558066 #6aa080 #80c099 #95e0b3 #aaffcc
#15201f #2a403f #40605f #55807f #6aa09f #80c0bf #95e0df #aafffe
#151920 #2a3340 #404c60 #556680 #6a80a0 #8099c0 #95b3e0 #aaccff
#171520 #2e2a40 #464060 #5d5580 #746aa0 #8c80c0 #a395e0 #b9aaff
#1d1520 #3b2a40 #594060 #775580 #956aa0 #b380c0 #d195e0 #eeaaff
#20151b #402a37 #604053 #80556f #a06a8b #c080a7 #e095c3 #ffaadd
#200a0a #401515 #602020 #802a2a #a03535 #c04040 #e04a4a #ff5555
#20170a #402e15 #604520 #805c2a #a07435 #c08b40 #e0a24a #ffb955
#1b200a #374015 #536020 #6e802a #8aa035 #a6c040 #c2e04a #dcff55
#f200a0 #1e4015 #2d6020 #3c802a #4ba035 #5bc040 #6ae04a #79ff55
#a20130 #154026 #206039 #2a804c #35a060 #40c073 #4ae086 #55ff99
#a201f0 #15403f #20605f #2a807e #35a09e #40c0be #4ae0de #55fffd
#a13200 #152640 #203960 #2a4c80 #3560a0 #4073c0 #4a86e0 #5599ff
#e0a200 #1d1540 #2c2060 #3a2a80 #4935a0 #5840c0 #664ae0 #7455ff
#1b0a20 #371540 #532060 #6e2a80 #8a35a0 #a640c0 #c24ae0 #dd55ff
#200a17 #40152f #602047 #802a5e #a03576 #c0408e #e04aa6 #ff55bc
#200000 #400000 #600000 #800000 #a00000 #c00000 #e00000 #ff0000
#201100 #402200 #603300 #804500 #a05600 #c06700 #e07900 #ff8a00
#1d2000 #3a4000 #586000 #758000 #92a000 #b0c000 #cde000 #eaff00
#c20000 #184000 #246000 #308000 #3ca000 #48c000 #54e000 #60ff00
#200500 #400a00 #600f00 #801500 #a01a00 #c01f00 #e02400 #ff2900
#201600 #402d00 #604300 #805a00 #a07000 #c08700 #e09e00 #ffb400
#172000 #2e4000 #466000 #5d8000 #74a000 #8cc000 #a3e000 #baff00
#620000 #c40000 #126000 #188000 #1ea000 #24c000 #2ae000 #30ff00
#b00200 #160040 #210060 #2d0080 #3800a0 #4300c0 #4f00e0 #5900ff
#1c0020 #390040 #550060 #720080 #8f00a0 #ab00c0 #c800e0 #e400ff
#200012 #400024 #600036 #800048 #a0005a #c0006c #e0007e #ff008f
```

Other common palettes include [RGB332](rgb332.md) (256 colors, one byte represents RGB with 3, 3 and 2 bits for R, G and B) and [RGB565](rgb565.md) (65536 colors, two bytes represent RGB with 5, 6 and 5 bits for R, G and B).

## See Also

- [color ramp](color_ramp.md)