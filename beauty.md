# Beauty

`O-'"'-.__.-'"'-.__.-'"'-.__.-'"'-.__.-O`

Beauty is the quality of being extremely appealing and pleasing. Though the word will likely invoke association with traditional [art](art.md), in [technology](technology.md), [engineering](engineering.md), [mathematics](math.md) and other [science](science.md) beauty is, despite it's relative vagueness and subjectivity, an important aspect of design, and in fact this "mathematical beauty" has lots of times some clearly defined shapes -- for example [simplicity](kiss.md) is mostly considered beautiful. Beauty is similar to and many times synonymous with [elegance](elegance.md).

Beauty can perhaps be seen as a [heuristic](heuristic.md), a touch of intuition that guides the expert in exploration of previously unknown fields, as we have come to learn that the greatest discoveries tend to be very beautiful (however there is also an opposite side: some people, such as Sabine Hossenfelder, criticize e.g. the pursuit of beautiful theories in modern physics as this approach seems to be have led to stagnation). Indeed, beginners and [noobs](noob.md) are mostly concerned with learning hard facts, learning standards and getting familiar with already known ways of solving known problems, they often aren't able to recognize what's beautiful and what's ugly. But as one gets more and more experienced and finds himself near the borders of current knowledge, there is suddenly no guidance but intuition, beauty, to suggest ways forward, and here one starts to get the feel for beauty. At this point the field, even if highly exact and rigorous, has become an [art](art.md).

What is beautiful then? As stated, there is a lot of subjectivity, but generally the following attributes are correlated with beauty:

- **[simplicity](minimalism.md)/[minimalism](minimalism.md)**, typically finding simplicity in complexity, e.g. a very short formula or algorithm that describes an infinitely complex [fractal](fractal.md) shape, a simple but valuable equation in physics (*e = m * c^2*), a short computer program that yields rich results ([demoscene](demoscene.md), [code golfing](golf.md), [suckless](suckless.md), [minimal viable program](minimal_viable_program.md), ...).
- **deepness** -- if something very simple, let's say a single small equation, has consequences and implications that may be studied into great depth, for example [prime numbers](prime.md).
- **generality**, i.e. if a simple equation can describe many problems, not just a specific case.
- **lack of exceptions**, i.e. when our equation works without having to deal with special cases (in programming represented by `if-then` branches).
- **[symmetry](symmetry.md)**, i.e. when we can e.g. swap variables in the equation and get some kind of opposite result.
- **unification**, or when multiple other beautiful things meet, for example the [Euler's identity](eulers_identity.md) brings together into one equation the most important numbers in mathematics: *i*, *pi*, 1 and 0.
- **[self containment](self_hosting.md)**, describing itself, applying to itself, not depending on other things
- **aesthetics**, either of the equation itself (or for example the source code) or the generated object ([fractals](fractal.md), attractors, ...).
- TODO

Examples of beautiful things include:

- **Euler's identity**, an equation often cited as the most beautiful in mathematics: *e^{i*pi} + 1 = 0*. It is simple and contains many of the most important numbers: *e*, *pi*, *i* 1 and 0.
- **[minimalist software](suckless.md)**, **[Unix philosophy](unix_philosophy.md)**
- [fractals](fractal.md) TODO
- [bytebeat](bytebeat.md)