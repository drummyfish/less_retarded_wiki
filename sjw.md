# Social Justice Warrior

Social [justice](justice.md) [warrior](fight_culture.md) (SJW) is an especially active, [toxic](toxic.md) and aggressive kind of [pseudoleftist](pseudoleft.md) (a kind of [fascist](fascism.md)) that tries to [fight](fight_culture.md) (nowadays mostly on the Internet but eventually also as an a member of a physical execution squad) anyone opposing or even just slightly criticizing the mainstream pseudoleftist gospel such as the [feminism](feminism.md) and [LGBT](lgbt.md) propaganda. Their personality is practically always [narcissistic](egoism.md), they leech controversial topics (but not as controversial to actually be in significant minority) to get attention that they crave more than anything else, they spend almost all time [virtue signaling](virtue_signaling.md) on [social networks](social_network.md). SJWs divide people rather than unite them, they operate on the basis of hate, revenge and mass hysteria and as we know, hate spawns more hate and [fear](fear_culture.md), they fuel a war mentality in society. They support hard [censorship](censorship.md) (forced [political correctness](political_correctness.md)) and bullying of their opposition, so called [cancelling](cancel_culture.md), and also such retardism as [sanism](sanism.md) and whatnot. [Wokeism](woke.md) is yet more extreme form of SJWery that doesn't even anymore try to hide its militant ambitions.

SJWs say the term is pejorative. We say it's not pejorative enough xD

SJWs want to murder all straight white men, however they try to make it seem as though they tolerate all races and orientations by excluding from their death sentence (only for now) those straight white males that agree to help kill all straight white men than do not agree to do the same. Once they decimate the population of straight white men like this to a minimum, they will also kill off the rest. This works basically the same as how [Nazi](nazi.md) made some Jews collaborate on killing of other Jews by promising they would let them live; of course eventually Nazis aimed to exterminating all of them, but they figured they might just make it easier this way.

A sneaky tactic of an SJW is **masked hypocrisy**. As any good [marketing](marketing.md) guy he will proclaim some principle OUT LOUD IN BIG LETTERS, adding asterisks with exceptions that immediately break that principle. For example:

- "WE HAVE TO CREATE A CENSORSHIP RESISTANT INFORMATION NETWORK" (asterisk: "But we have to build in mechanisms to censor [pedophiles](pedophilia.md) and racists and other information we dislike.")
- "WE HAVE ZERO TOLERANCE OF CYBER BULLYING" (asterisk: "With the exception of bullying people we deem fair to be bullied.")
- "WE ARE PACIFIST REJECTING VIOLENCE" (asterisk: "With the exception of violence against people we deem good to use violence against.")
- "WE HAVE ZERO TOLERANCE OF [RACISM](racism.md)" (asterisk: "We don't count [reverse racism](reverse_racism.md) as racism.")
- "WE SUPPORT [FREE SPEECH](free_speech.md) AND FREE INFORMATION SHARING" (asterisk: "Speech we dislike doesn't fall under free speech.")
- "WE SUPPORT TECHNOLOGICAL [MINIMALISM](minimalism.md)" (asterisk: "As long as it's written in [Rust](rust.md), [Python](python.md) and [JavaScript](js.md) with [encryption](encryption.md) and [virtual machines](vm.md) for [security](security.md).")
- "WE PROMOTE SCIENCE" (asterisk: "Science being defined as trusting the word of authorities we approve without questioning them.")
- "THIS [ENCYCLOPEDIA](wikipedia.md) CAN BE EDITED BY EVERYONE" (asterisk: "Except for 90% of population who are blocked for not conforming to our political style of writing, also with the exception of articles that will probably be read by someone.")
- etc.

## See Also

- [soydev](soydev.md)
- [snowflake](snowflake.md)
- [idiot](idiot.md)
- [pseudoleftism](pseudoleft.md)