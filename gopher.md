# Gopher

Gopher (allegedly from "go for information") is a [network](network.md) [protocol](protocol.md) for publishing, browsing and downloading files and is known as a much simpler alternative to the [World Wide Web](www.md) (i.e. to [HTTP](http.md) and [HTML](html.md)). In fact it competed with the Web in its early days and even though the Web won in the mainstream, gopher still remains used by small communities (usually the more dedicated though, see e.g. [bitreich](bitreich.md)). Gopher is like the Web but well designed, it is the [suckless](suckless.md)/[KISS](kiss.md) way of doing what the Web does, it contains practically no [bloat](bloat.md) and so [we](lrs.md) highly advocate its use. Gopher inspired creation of [Gemini](gemini.md), a similar but bit more complex and "[modern](modern.md)" protocol, and the two together have recently become the main part of so called [Smol Internet](smol_internet.md). Gopher is much better than Gemini though. The set of all public gopher servers is called gopherspace. The Gopher protocol was defined in 1993 in [RFC](rfc.md) 1436.

Gopher **doesn't use any [encryption](encryption.md)** (though some servers allow access via [Tor](tor.md)... actually there's also some kinda encrypted "gophers" protocol too, but still it seems encrypting is thankfully not so popular at this point). **This is good, encryption is [bloat](bloat.md)**. Gopher also doesn't really know or care about [Unicode](unicode.md) and similar bloat (which mostly serves trannies to insert emojis of pregnant men into readmes anyway, we don't need that), it's basically just [ASCII](ascii.md) (of course you can employ Unicode as gopher just transfers files really, it's just that Unicode is not part of gopher's specification and most people prefer to keep it ASCII). Gopher's simple design is intentional, the authors deemed simplicity a [good](good.md) feature. Gopher is so simple that you may very well write your own client and server and comfortably use them -- **you can even browse gopher just by manually using [telnet](telnet.md)** to communicate with the server.

**How big is/was gopherspace?** In 1994 there were over 1300 gopher servers (source: 1994 book *Finding it on the Internet"*), around 1995 there were already more than 6000  (source: 1995 video *Searching the Internet - Gopher | The Internet Revealed*). Most of them are now gone, in 2005 there were only 145 servers reported by Veronica search engine (source: *2007 gopher archive*), though Gopher recently saw a new wave of popularity. As of 2023 the Veronica search engine reported 315 gopher servers in the world with 5+ million indexed selectors, which they estimated was 83% of the whole gopherspace (the peak server count was in 2020 at almost 400). Quarry search engine reports 369 servers and 1+ million indexed selectors. Contrition search engine reported even 495 servers and 7+ million selectors. The "grawler" crawler of gopherspace.de reported 192 active servers at the beginning of 2016, 182 in 2020, 413 in 2023 (bumped its search list) and 380 in 2024. Gopher LAWN directory (made by [bitreich](bitreich.md)) contains 281 selected quality gopher holes.

From the user's perspective **the most important distinction from the Web** is that gopher is based on **menus** instead of "webpages"; a menu is simply a column of items of different predefined types, most importantly e.g. a *text file* (which clients can directly display), *directory* (link to another menu), *text label* (just shows some text), *binary file* etc. A menu can't be formatted or visually changed, there are no colors, images, scripts or [hypertext](hypertext.md) -- a menu is not a presentation tool, it is simply a navigation node towards files users are searching for (but the mentioned ASCII art and label items allow for somewhat mimicking "websites" anyway). Gopher is also often **browsed from the [command line](cli.md)**, though graphical clients are a thing too. Addressing works with [URLs](url.md) just as the Web, the URLs just differ by the protocol part (`gopher://` instead of `http://`), e.g.: `gopher://gopher.floodgap.com:70/1/gstats`. What on Web is called a "website" on gopher we call a **gopherhole** or just *hole* (i.e. a collection of resources usually under a single [domain](domain.md)) and the whole gopher network is called a **gopherspace**. [Blogs](blog.md) are common on gopher and are called **phlogs** (collectively a *phlogosphere*). As menus can refer to one another, gopher creates something akin a **global [file system](file_system.md)**, so browsing gopher is like browsing folders and can comfortably be handled with just 4 arrow keys. Note that as menus can link to any other menu freely, the structure of the "file system" is not a [tree](tree.md) but rather a general [graph](graph.md). Another difference from the Web is gopher's great emphasis on **[plaintext](plaintext.md) and [ASCII art](ascii_art.md)** as it cannot embed images and other media in the menus (even though of course the menus can link to them). There is also a support for sending text to a server so it is possible to implement [search engines](search_engine.md), guest books, [games](game.md) etc.

Gopher is just an [application layer](l7.md) [protocol](protocol.md) (officially running on [port](port.md) 70 assigned by [IANA](iana.md)), i.e it sits above lower layer protocols like [TCP](tcp.md) and takes the same role as [HTTP](http.md) on the Web and so only defines how clients and servers talk to each other -- the gopher protocol doesn't say how menus are written or stored on servers. Nevertheless for the creation of menus so called **gophermaps** have been established, which is a simple format for writing menus and are the gopher equivalent of Web's [HTML](html.md) files (just much simpler, basically just menu items on separate lines, the exact syntax is ultimately defined by server implementation). A server doesn't have to use gophermaps, it may be e.g. configured to create menus automatically from directories and files stored on the server, however gophermaps allow users to write custom menus manually. Typically in someone's gopherhole you'll be served a welcoming intro menu similar to a personal webpage that's been written as a gophermap, which may then link to directories storing personal files or other hand written menus. Some gopher servers also allow creating dynamic content with scripts called **moles**.

**Gopher [software](software.md)**: sadly "[modern](modern.md)" browsers are so modern they have millions of lines of code but can't be bothered to support such a trivial protocol like gopher, however there are Web proxies you can use to explore gopherspace (look up e.g. floodgap). Better browsers such as [lynx](lynx.md) (terminal), [sacc](sacc.md), [clic](clic.md) or [forg](forg.md) ([GUI](gui.md)) can be used for browsing gopherspace natively (it's not hard, you don't need to learn any keybinds, using arrow keys usually just works). As a server you may use e.g. Gophernicus (used by [SDF](sdf.md)) or search for another one, there are dozens. { Personally I've used gophrier for server, it was the simplest one I found. ~drummyfish } For the creation of gophermaps you simply use a plaintext editor. **Where to host gopher?** [Pubnixes](pubnix.md) such as [SDF](sdf.md), [tilde.town](tilde_town.md) and [Circumlunar community](circumlunar.md) offer gopher hosting but many people simply [self-host](self_hosting.md) servers e.g. on [Raspberry Pis](rpi.md), it's pretty simple.

**A quick [tl;dr](tldr.md)/sumup of the gopher world/community** as of 2023: thankfully there doesn't seem to be much [censorship](censorship.md) and/or woke toxicity that's seen on the web, the community is still quite small, which is probably a [GOOD](good.md) thing, though something like doubling the gopherspace size would probably still be welcome; an issue/downside at this time seems to be a "self serving" nature of gopher ("come to gopher to learn about gopher"), i.e. instead of gopher being a "platform" for sharing all kinds of information, we mostly have a gopher community talking about gopher, so outsiders really have nothing to come in for. Of course there is interesting information of other kinds, but the overall impression is just this. Another issue is that current search engines like Veronica don't seem to be fulltext but rather search only document titles. Currently gopherspace seems to be mostly divided into following gopherholes:

- big center [hubs](hub.md) (floodgap, bitreich, ...): Stable holes providing info and tutorials for newcomers, links to important resources, manifestos, sometimes providing a search engine or directory of other holes.
- phlogs and personal holes: Small holes with "hello world messages" and personal rants, usually about gopher and related topics such as technology [minimalism](minimalism.md), independent living etc.
- socializing/roleplay [pubnices](pubnix.md) offering hosting (circumlunar, [SDF](sdf.md), tilde town, ...): Smaller noncommercial communities in the spirit of old [BBS](bbs.md)es, they offer user account, ssh access to their [Unix](unix.md) servers, email, web and gopher hosting space, chat, [games](game.md) etc. Sometimes they are roleplay focused, having some sci-fi backstory or something. They don't ask for any fees, however all seem to REQUIRE active participation in the community or else they'll delete your account -- this sucks big time for asocial introverts who just want a gopher hole without being forced to play with other kids.
- web proxies (gopherpedia, gophreddit): Mirrors/proxies to popular websites such as [Wikipedia](wikipedia.md), project gutenberg, [reddit](reddit.md) etc.

**Some basic/interesting gopher links**: gopher://bitreich.org/1/lawn (directory of gopher holes), gopher://gopher.floodgap.com/7/v2/vs (search engine), gopher://circumlunar.space:70/1 (circumlunar space), gopher://gopherpedia.com ([Wikipedia](wikipedia.md) on gopher), gopher://gopher.icu/7/quarry (search engine), ...

## How To

Here is a nice tutorial: `git clone git://bitreich.org/gopher-tutorials/`.

To quickly try browsing gopher either use a web proxy, e.g. at https://gopher.floodgap.com/gopher/gw.lite, or use some nice native browser, e.g. `lynx gopher://floodgap.com`.

More technical details: just as with the web, you have some gopher [server](server.md) running somewhere (some [IP address](ip_address.md)/[domain](domain.md), on [port](port.md) 70) which serves resources to [clients](client.md). A client connects to the server (via [TCP](tcp.md)) and simply sends the name of the resource (file or directory) it wants to retrieve as a [string](string.md) ending with a newline. If the string is empty, the server sends the default directory (the "main page"). You may try this manually in terminal using [telnet](telnet.md), [nc](nc.md) or a similar tool. For example:

```
echo "" | nc floodgap.com 70
```

The server just sends us back a list of available resources in the "main directory", one per line, each in format:

```
<TYPE><DISPLAY_STRING><TAB><PATH><TAB><SERVER><TAB><PORT>
```

For example one of the lines here looks like this:

```
1Search Gopherspace with Veronica-2 <TAB> /v2 <TAB> gopher.floodgap.com <TAB> 70
```

Here `1` says the resource is a directory, then we have a display string (which you'll see in the browser), then the path to the resource, i.e. `/v2`, and then the server and port. If we want to retrieve this directory, we send:

```
echo "/v2" | nc floodgap.com 70
```

And get a similar response. This is basically all a client needs to know.

As for running a server, details depend on each one, but generally they behave like this: you have a server running in some default directory, let's say `/home/me/my_gopherhole`. By default a server will just serve list of files present in this directory to clients who request the "main directory", treating directories as subdirectories and sending regular files back. However there is one important feature: you may create a **gophermap file** to create a custom menu, or something akin a "gopher website". Gophermap is something like gopher's [HTML](html.md), just much more simple. How to do this? You simply create a file name `gophermap` in the directory (the main one or any subdirectory) -- if the server sees such a file, it serves it instead of listing the directory file.

TODO: continue

## Example

TODO

## See Also

- [Gemini](gemini.md)
- [Fediverse](fediverse.md)
- [smol internet](smol_internet.md)
