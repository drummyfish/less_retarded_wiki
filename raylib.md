## Raylib

Raylib is a [free](free_software.md), relatively [KISS](kiss.md), [portable](portability.md) [C](c.md) (C99) [library](library.md) intended mainly for [game](game.md) development, offering [IO](io.md) handling, [2D](2d.md) and [3D](3d.md) graphics, audio, loading of different image and 3D formats etc., while restraining from a lot of [bullshit](bullshit.md) of "[modern](modern.md)" [bloated](bloat.md) engines/[frameworks](framework.md) such as having tons of [dependencies](dependency.md) and targeting only very fast computing platforms. Raylib is pretty cool and employs many of the [LRS](lrs.md)/[suckless](suckless.md) ideas, even though from our strict point of view it is still a bit more complex than it really needs to be, e.g. by using [floating point](float.md) and relying on [GPU](gpu.md) accelerated 3D graphics. In terms of [bloat](bloat.md) it can be seen as a mid way between the mainstream (e.g. [Godot](godot.md)) and [LRS](lrs.md)/[suckless](suckless.md) (e.g. [small3dlib](small3dlib.md)).

The following are some features of raylib as of writing this. The good and neutral features seem to be:

- written in [C99](c99.md)
- no external software [dependencies](dependency.md), all 3rd party libraries are packed with raylib (well, kind of, as specific platforms will require OpenGL implementation, windowing library etc.)
- no [COC](coc.md) detected
- supports many systems including [GNU](gnu.md)/[Linux](linux.md), [BSD](bsd.md), [Macshit](mac.md), [Winshit](windows.md), [Android](android.md), web browser, claims to support even [embedded](embedded.md) (but probably only the faster, not sure about [bare metal](bare_metal.md) due to OpenGL etc.)
- NOT a [framework](framework.md), very [bullshit](bullshit.md)-free
- permissive [zlib](zlib.md) [license](license.md)
- 2D and 3D support, [IO](io.md), audio, fonts, math, includes most things you need for games (seems to lack only a [physics engine](physics_engine.md))
- can load many file formats ([jpg](jpg.md), [png](png.md), [obj](obj.md), [m3d](m3d.md), [ttf](ttf.md), [wav](wav.md), [flac](flac.md), [mp3](mp3.md), [ogg](ogg.md), ...)
- bindings to 50+ languages (useless as all you need is [C](c.md) of course)
- nice documentation, many examples

And some of the bad features are:

- 150000+ [lines of code](loc.md)
- not a [header only](header_only) library, requires building ([makefile](make.md), optionally with [cmake](cmake.md))
- using [floating point](float.md) and [OpenGL](opengl.md) creates complex dependencies and sends a fuck you to small [embedded](embedded.md) computers
- even though abstracted, some software dependencies are still needed depending on the platform, e.g. on GNU/Linux you need to install [ALSA](alsa.md), [Mesa](mesa.md) and [X11](x11.md)