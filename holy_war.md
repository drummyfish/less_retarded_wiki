# Holy War

Holy war is a long passionate argument over a choice (many times between two options) that touches an issue deemed controversial and/or "religious" within given community; to an outsider this may seem like a childish, hard to understand rant about an insignificant thing. In [technology](tech.md) circles holy wars revolve e.g. around [operating systems](os.md), [text editors](text_editor.md), [programming languages](programming_language.md), [licenses](license.md), source code formatting etc. Such a war separates people into almost religious groups that sometimes argue to death about details such as what name something should be given, very much resembling traditional disagreements between religions and their churches -- you would think that people being on the same ship (e.g. all being promoters of [free software](free_software.md)) would at least get along better with each other than with outsiders but no, great many times these people hate each other over quite small details, though there is often a good reason too, just hard to spot to those without deeper insight (a choice regarding minor detail may tell a lot about that who makes it). In holy wars people tend to defend whichever side they stand on to the beyond grave and can get emotional when discussing the topic, leading to flame wars, [ragequits](ragequit.md), [Hitler](hitler.md) arguments etc. Some examples of holy wars are (in brackets indicated the side taken by [LRS](lrs.md)):

- **[tabs](tab.md) vs spaces** (spaces)
- **[vim](vim.md) vs [emacs](emacs.md) vs [acme](acme.md) and other text editors** (vim)
- **[free software](free_software.md) vs [open source](open_source.md) vs [proprietary](proprietary.md) vs whatever else** (free software)
- **[Chrome](chrome.md) vs [Firefox](firefox.md)**, and other browsers (none of this bloated shit)
- **[Java](java.md) vs [C++](cpp.md), [Lisp](lisp.md) vs [Forth](forth.md) and other [programming languages](programming_language.md)** ([C](c.md), [comun](comun.md))
- **[curly brackets on separate lines or not](programming_style.md)**, and other [style](programming_style.md) choices
- **[KDE](kde.md) vs [GNOME](gnome.md)** (neither, both are [bloat](bloat.md))
- **pronunciation of [gif](gif.md) as "gif" vs "jif"**
- **[Windows](windows.md) vs [Mac](mac.md)** (neither, this is a normie holy war)
- **"[GNU](gnu.md)/Linux" vs "[Linux](linux.md)"** (GNU/linux)
- **[copyleft](copyleft.md) vs [permissive](permissive.md)** (permissive, [public domain](public_domain.md))
- **[AMD](amd.md) vs [Intel](intel.md)**
- **[AMD](amd.md) vs [NVidia](nvidia.md)**
- **"Linux" [distros](distro.md)**
- **[window managers](wm.md)**
- **[Metric](metric_system.md) vs [Imperial](imperial_units.md) units** (metric)
- **Star Trek vs Star Wars**, and other franchise wars
- **[Quake](quake.md) vs Unreal Tournament**, and similar gaming shit
- **[gopher](gopher.md) vs [gemini](gemini.md)** (gopher)
- ...

Things like [cats](cat.md) vs dogs or sci-fi vs fantasy may or may not be a holy war, there is a bit of a doubt in the fact that one can easily like both and/or not be such a diehard fan of one or the other. A subject of holy war probably has to be something that doesn't allow too much of this. Maybe a controversy of its own could be the very topic of "what is a holy war" in itself.