# Watchdog

Watchdog is a special [timer](timer.md) that serves as a safety mechanism for detecting malfunction of [computer](computer.md) programs at [run time](run_time.md) by requiring programs to periodically reset the timer.

Basically watchdog keeps counting up and a correctly behaving program is supposed to periodically reset this count ("kick" or "feed the dog") -- if the reset doesn't happen for a longer period, the watchdog counts up to a high value and alerts that something's wrong ("the dog starts barking"), e.g. with an [interrupt](interrupt.md) or a [signal](signal.md). This can mean for example that the program has become stuck in an [infinite loop](infinite_loop.md) or that its instructions were corrupted and the program control jumped to some unintended area of RAM and is doing crazy [shit](shit.md). This is usually handled by resetting the system so as to prevent possible damage by the program gone insane, also [logs](log.md) can be made etc. Watchdogs are very often used in [embedded systems](embedded.md). [Operating systems](os.md) may also use them to detect nonresponsive [processes](process.md).

Watchdog is similar to the dead man's switch used e.g. in trains where the operator is required to periodically push a button otherwise the train will automatically activate brakes as the operator is probably sleeping or dead.

## See Also

- [dog](dog.md)