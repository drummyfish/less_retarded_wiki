# OpenArena

OpenArena (OA) is a first man pew pew arena shooter [game](game.md), a [free as in freedom](free_software.md) [clone](clone.md) of the famous game Quake 3. It runs on [GNU](gnu.md)/[Linux](linux.md), [Winshit](windows.md), [BSD](bsd.md) and other systems, it is quite light on system resources but does require a [GPU](gpu.md) acceleration (no [software rendering](sw_rendering.md)). Quake 3 engine ([Id tech 3](id_tech3.md)) has retroactively been made free software: OpenArena [forked](fork.md) it and additionally replaced the proprietary Quake assets, such as 3D models, sounds and maps, with community-created Japanese/nerd/[waifu](waifu.md)-themed [free culture](free_culture.md) assets, so as to create a completely free game. OpenArena plays almost exactly the same as Quake 3, it basically just looks different and has different maps. It has an official [wiki](wiki.md) at https://openarena.fandom.com and a forum at http://www.openarena.ws/board/. OpenArena has also been used as a research tool.

As of 2023 most players you encounter in OA are [cheating](cheating.md) (many [americans](usa.md) play it), but if you don't mind that, it's a pretty comfy game.

As of version 0.8.8 there are 45 maps and 12 game modes ([deathmatch](deathmatch.md), team deatchmatch, [capture the flag](ctf.md), last man standing, ...).

A bit of [fun](fun.md): you can redirect the chat to text to speech and let it be read aloud e.g. like this:

```
openarena 2>&1 | grep --line-buffered ".*^7: ^2.*" | sed -u "s/.*: ^2\(.*\)/\1/g" | sed -u "s/\^.//g" | espeak
```

Character art exhibits what [SJWs](sjw.md) would call high sexism. This is great, it's the good old [90s](90s.md) art style. The art is very nice and professional looking (no programmer art). Characters such as Angelyss are basically naked with just bikini strings covering her nipples. Other characters like Merman and Penguin (a typical "[Linux](linux.md) user") are pretty funny. Ratmod has very nice taunts that would definitely be labeled offensive to gays and other minorities nowadays. The community is also pretty nice, free speech is allowed in chat, no [codes of conduct](coc.md) anywhere, [boomers](boomer.md) thankfully don't buy into such bullshit. Very refreshing in the politically correct era.

OpenArena is similar to e.g. [Xonotic](xonotic.md) -- another free arena shooter -- but is a bit simpler and oldschool, both in graphics, features and gameplay. It has fewer weapons, game modes and options. However there exist additional modifications, most notably the Ratmod (or RatArena) which makes it a bit more "advanced" (adds game modes, projectiles go through portals, improved prediction code etc.). As of 2022 an asset reboot in a more Anime style, called OA3, is planned and it seems to be aiming in the right direction -- instead of making a "modern HD game" (and so basically just remake Xonotic) they specifically set to create a *2000 game* (i.e. keep the models low poly etc.). It could also help distinguish OpenArena from Quake more and so make it legally safer (e.g. in terms of [trade dress](trade_dress.md)).

{ I've been casually playing OA for a while alongside Xonotic. I love both games. OA is more oldschool, boomer, straightforward and KISS, feels slower in terms of movement and combat but DM matches are much quicker, fragging is very rapid. ~drummyfish }

The project was established on August 19 2005, one day after the Quake 3 engine was freed.

## See Also

- [Freedoom](freedoom.md)
- [Xonotic](xonotic.md)
- [clone](clone.md)