# Dodleston Mystery

The Dodleston mystery regards a teacher Ken Webster who in 1984 supposedly started exchanging messages with people from the past and future, most notably people from the 16th and 22nd century, via files on a [BBC micro](bbc_micro.md) computer. While probably a [hoax](hoax.md) and [creepypasta](creepypasta.md), there are some interesting unexplained details... and it's a fun story.

The guy has written a [proprietary](proprietary.md) book about it, called *The Vertical Plane*.

{ If the story is made up and maybe even if it isn't it may be a copyright violation to reproduce the story with all the details here so I don't know if I should, but reporting on a few facts probably can't hurt. Yes, this is how bad the copyrestriction laws have gotten. ~drummyfish }