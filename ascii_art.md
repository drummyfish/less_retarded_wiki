# ASCII Art

ASCII art is the [art](art.md) of (mostly manually) creating graphics and images only out of [fixed-width](fixed_width.md) (monospace) [ASCII](ascii.md) characters. Strictly speaking this means no [Unicode](unicode.md) or extended ASCII characters are allowed -- these would rather be called Unicode art, [ANSI art](ansi_art.md) etc., though the term ASCII art is quite often used loosely for any art of this kind. If we keep being pedantic, ASCII art might also be seen as separate from mere [ASCII rendering](ascii_rendering.md), i.e. automatically rendering a bitmap image with ASCII characters in place of [pixels](pixel.md), and ASCII graphics that utilizes the same techniques as ASCII art but can't really be called art (e.g. computer generated diagrams); though in practice this distinction is also rarely made. Pure ASCII art is [plain text](plain_text.md), i.e. it can't make use of [color](color.md), text decoration and other [rich text](rich_text.md) formatting.

This kind of art used to be a great part of the [culture](culture.md) of earliest [Internet](internet.md) and near-Internet (e.g. [BBS](bbs.md)) communities for a number of reasons imposed largely by the limitations of old computers -- it could be created easily with a text editor and saved in pure text format, it didn't take much space to store or send over a network and it could be displayed on text-only displays and [terminals](terminal.md). The idea itself even predates computers, people were already making this kind of images with type writers, e.g. some poets were formatting their poems with typewriters to picture-shapes. Despite the technical limitations of displays having been overpassed, ASCII art survives even to present day and lives on in the [hacker culture](hacking.md), among [programmers](programming.md), in [Unix](unix.md) and "retro" game communities as well as on the [Smol Internet](smol_internet.md), among people who just want to [keep it simple](kiss.md) and so on. ASCII diagram may very well be embedded in a comment on a text-only forum or in source code to explain some spatial concept. ASCII art may even be superior for making certain types of drawings from purely user perspective exactly by being simplified, it can be performed merely with keyboard with little distraction (not caring about colors, not having to focus on right angles, to care about line thicknesses, switching tools, deleting imprecise strokes, ...), similarly to how for example it may be easier to create a rough model of a house in [Minetest](minetest.md) than to model it in [Blender](blender.md). { I found that for making quick diagrams I prefer ASCII art to graphic tools. ~drummyfish } We, [LRS](lrs.md), highly advocate use of ASCII art whenever it's [good enough](good_enough.md).

Here is a simple 16-shade ASCII [palette](palette.md) (but watch out, whether it works will depend on your font): `#OVaxsflc/!;,.- `. Another one can be e.g.: `WM0KXkxocl;:,'. `.

Here are approximate brightness values for each printable ASCII character, with 0 being black and 1000 white (of course the values always depend on the specific [font](font.md) you use):

{ I obtained the values by shooting a screen with some generic monospace font in gedit or something, then made a script that computed the values and ordered them. ~drummyfish }

```
@ 577    P 727    A 777    ? 827    / 867
W 615    w 735    Z 777    I 830    > 867
M 640    3 740    h 779    j 831    \ 867
0 641    X 741    Y 786    C 834    < 868
Q 656    D 744    [ 797    ) 836    c 870
& 658    V 745    T 797    ( 837    + 874
% 664    b 746    e 798    l 837    J 892
R 664    p 747    } 800    x 838    " 911
8 676    5 748    a 800    i 848    ; 912
# 685    d 748    { 803    z 851    _ 912
O 685    2 750    ] 809    r 853    ~ 924
$ 687    4 750    y 810    ^ 854    : 936
B 702    S 750    1 811    s 855    , 942
6 707    q 751    7 812    v 855    - 953
9 708    k 759    F 812    ! 856    ' 954
g 711    G 765    o 813    t 856    . 968
N 715    K 767    f 815    * 857    ` 969
U 724    E 768    u 825    = 860      1000
m 727    H 771    n 826    L 866
```

And here are some attempts at actual ASCII art:

```
            _,,_  
           /    ';_  
    .     (  0 _/  "-._
    |\     \_ /_==-"""'           .~~~~~~~~;~~~~~~~~.
    | |:---'   (                  | ASCII  | tables |
     \ \__."    ) Steamer         |========I========|
      '--_ __--'    Duck!         |  are   |        |
          |L_                     |--------{   =)   |
                                  |  the   |        |
                                  |--------+--------|
      []  [][][][][]              |  best  | tables |
      [][][]      [][]            |________|________|
      [][]          []
      []    XX    XX[]
      []      XXXX  []
      [][]          []        ____   _____   _____
      [][][]      [][]       [   _) [   _ \ / ____]
      []  [][][][][]          ) {    } |_) )\___  \
                              | |__/||  _ {  ___} |
          SAF FTW             }_____|}_{ \_][_____/

   ("\/") ("\/") ("\/")
   \    / \    / \    /
    \  /   \  /   \  /          KEEP IT SIMPLE
     \/     \/     \/             ~~ BRUH ~~
^
|
|   _.--._                  _.--._
| .'      '.              .'      '.
|/__________\____________/__________\______
|            \          /            \
|             '.      .'              '.
|               `'--'`                  `'-
|
V
                                    /|_/| why is
  '.|.'       . :       ___        (._. ) everyone
 --{O}--   .;::.':.' __{   )_      /\  /) so
  .'|'. _.:''  '  : {_ \  } _}       VV   retarded
    ,__|| |_,         \_{__/
   /\\ \|_| \\          __           _    .-.
  /  \\      \\       ,'  '.        (_) __ \ \
 /____\\______\\     ( o    )        _ |__] ) )
  \,,,|,,,,,,,/       '")("o        (_)    / /
  |___|_[H]___|=|=|=|=|=|=|=|=|           '-'

        _                _          ___         _       _ 
  ____ | |__    ___   __| |  ___   / __] ____  | |     (_)
 / _  ]|  _ \  / __] / _  | / _ \ [  _] / _  ] | '--.  ;";
( (_| {| |_) )| (__ ( (_| ||  ___) | | ( (_| | | .-. | | |
 \____]|____/  \___] \____| \___]  |_|  \__  | |_| |_| |_|
   _   _      _                         [___/
  (_) | | __ | |  _   _  ____    ___   ____    ____   ____
  ;"; | |/ / | | | "-" ||  _ '. / _ \ |  _ \  / _  | |  _ |
  | | |   {  | | | ;_; || | | |( (_) )| |_) )( (_| | | | "'
 _/ | |_|\_] \_\ |_| |_||_| |_| \___/ |  __/  \__  | |_|
 \-"     _                            |_|        |_|
  ____  | |   _  _  _   _   _    _   _   _   _   _   ____
 (  __|[   ] | || || [ ] | | |/\| | | \_/ | [ | | | |__  ]
  \_"\  | |_ | || | \ " /  | '  ' |  } _ {   \ \| |  ,' /
 [____) (___]',__,'  \_/    \_/\_/  |_/ \_|  _)  /  |____]
                                            \_.-'
```

The following is a raster picture auto-converted to ASCII: { For copyright clarity, it's one of my OGA pictures. ~drummyfish }

```
             !.-
            clxVl.
             c###xl,
          ,.- -,/ac/l!,
       .,,,;;,.;.---!c..;.-
    -!;.;,;cl;c##aV#s,....ca//cfs!;;.--,,,;
   !;;/s/,,c/slc;,;/#l!as;!f/c;;cVOfOf;---.s#c-
  -OOfl,,,,;/,,,!ll.!;!,,!c/!;;!/;cxxO/.;aO##a/,.,!,c;.
   !,;,,,,!!,/f/c!a,,,!//c;;;;;;;!cVOV/lf/......------,l/;.---
  !.!/!/!/;,//c!  ;#s,///;;;;;;!c/a#l;-.----....------.,...,..;,-
   ;x/;;;cslsac    #a!,,;;/,,./casl,---..!cxfs/.,.,!x#xl....-.;,/l,
      -;;;;;-     s;!V....;;;lca#/.---;f#Vxcf######s;.,..-----,,;ssa.
                 xxl!,,,.!,//c##l   ;/c!.,;....c#/.;ac,.,----.,,,fs;a,
                 !Os;!!c,scfa##x-  ---,,,,/,,..###x#;.,!-!,-.lc/cfx/#f
                  xOOs##lO##f;;,   ,-,,,,,,,;l#V;,!/,,,;.#--l#.;;/!,,#!
                     - !##,,,,,  -ax;,,,,,.,f/...c//!./;-#,-##!,,c! ,l;/-
                       /#c- -/,  ##/;,,,,,;./,,,;lclll/ -.V##.,,,c!  !.,/;
                       /##! -.;,;#/,,,,,;,l,,,,,sl,x### - ,#/,,,,c;   c,.,l/.
                        ls#,,,,l.#f.,,,.ff/;;;;O;  ;l/s ---;#;,,;V     ./lOcfl!.-
                        .lf#;,;/!Vl,,,,,xccccs/     ac/,--xf;,,,;#         #////f#l,
                         l/cflc/;.;al/;lclx!,       .f//--.;,,,,O           f/fcx##l.
                         ;s////l-;f#;,;,--           s//f;,,!,,;/            l#xf.c/!!
                         ,;,//!--;;,,,c              !///x.,,,,f-            -c/f  ./,/
                        ,fl!flll.,!/cc,             .ll//f;,,,,/;            !;;.   ,,;/
                       lf;!;!O/,;c;l!fc            -fllclc/!!,,!l         -,;;!-     ;;!
                        ////cc;llVcl/f             #s/lccx;.!/c/c.     .,lc!,        !;;
                      ./,,,!c--..,,,!              x//cf#fscf/c/f.   c,f;           ;;!
                    -f///!!;---,.,!,f            .cc!;/!f;;;//sf.   .fl!         !flls,
                      -  fxl;#/!V/fc/                 l! ,!;;;c     ;l/lc-     ,#Olcs;
                         - -!,-,l!;                   x.;a!/cfc      ./ff;       !.-
```

{ TODO: what would ASCII art made of ASCII font look like??? ~drummyfish }

The are many tools for this, but for educational purposes this one was made using the following custom [C](c.md) program:

```
#include <stdio.h>

const char palette[] = "#OVaxsflc/!;,.- ";

int readNum(void)
{
  int r = 0;

  while (1)
  {
    char c = getchar();

    if (c > '9' || c < '0')
      break;

    r = r * 10 + c - '0';
  }

  return r;
}

void skipAfterNewline(void)
{
  while (getchar() != '\n');
}

int main(void)
{
  skipAfterNewline(); // skip magic number
  skipAfterNewline(); // skip header

  int w = readNum();  // read width
  int h = readNum();  // read height

  skipAfterNewline(); // skip to pixels

  for (int j = 0; j < h; ++j) // read rows
  {
    for (int i = 0; i < w; ++i) // read columns
    {
      /* The following is a bit cryptic way of reading RGB, averaging it (giving
        higher significance to green, for human sight bias) to convert it to
        grayscale and then getting it to range 0 to 15 (palette size). */
      int v = ((getchar() + getchar() * 2 + getchar()) * 16) / (4 * 256);
      putchar(palette[v]);
    }

    putchar('\n');
  }

  return 0;
}
```

This program is extremely simple, it just reads an image in [PPM](ppm.md) format on standard input and outputs the image to terminal. Watch out, it won't work for all PPM images -- this one worked with a picture exported from [GIMP](gimp.md) in raw RGB PPM. Also note you have to scale the image down to a very small size AND its aspect ratio has to be highly stretched horizontally (because text characters, i.e. pixels, are much more tall than wide). Also for best results you may want to mess with brightness, contrast, sharpness etc.

## See Also

- [ANSI art](ansi_art.md)
- [ASCII animation](ascii_animation.md) (e.g. [vt100](vt100.md) terminal animations, [asciinema](asciinema.md), ...)
- [pixel art](pixel_art.md)
- [plain text](plain_text.md)
- [emoticon](emoticon.md)
- [cowsay](cowsay.md)
- [figlet](figlet.md)
