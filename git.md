# Git

Git (name without any actual meaning) is a [pseudoleftist](pseudoleft.md) [FOSS](foss.md) ([GPL](gpl.md)) [version control system](vcs.md) (system for maintaining and collaboratively developing [source code](source_code.md) of programs), currently the most mainstream-popular one (surveys saying over 90% developers use it over other systems). Git is basically a [command line](cli.md) program allowing to submit and track changes of text files (usually source code in some [programming language](programming_language.md)), offering the possibility to switch between versions, [branches](fork.md), detect and resolve conflicts in collaborative editing etc.

**2024 UPDATE**: Git is woke [fascist software](tranny_software.md), aim to stop using it. It has a [code of censorship](coc.md) and it's planning to implement [newspeak](newspeak.md) by renaming the default branch from politically incorrect *master* to something else (even though it that will fuck up many projects) -- as a reaction to this you should rename all you branches to *slavemaster*. Do it right now :)

Git was created by [Linus Torvalds](linus_torvalds.md) in 2005 to host the development of [Linux](linux.md) and to improve on systems such as [svn](svn.md). Since then it has become extremely popular, mainly thanks to [GitHub](github.md), a website that offered hosting of git projects along with "social network" features for the developers; after this similar hosting sites such as [GitLab](gitlab.md) and [Codeberg](codeberg.md) appeared, skyrocketing the popularity of git even more.

It is generally (at the time of writing this) considered quite a good software (at least compared to most other stuff), many praise its distributed nature, ability to work offline etc., however diehard software idealists still criticize its mildly [bloated](bloat.md) nature and also its usage complexity -- it is non-trivial to learn to work with git and many errors are infamously being resolved in a "trial/error + google" style, so some still try to improve on it by creating new systems. It is also very harmful by pushing harmful pseudoleftist political ideology into its development.

**Is git literally [hitler](hitler.md)?** Git rhymes with [shit](shit.md), it is harmful [tranny software](tranny_software.md) and it's indeed better to avoid using it if possible. For the technological side -- by [suckless](suckless.md) standards git IS bloated and yes, git IS complicated as fuck, however let's try to go deeper and ask the important questions, namely "does this matter so much?" and "should I use git or avoid it like the devil?". Taking about the pure technological side, the answer is actually this: it doesn't matter too much that git is bloated and you don't have to avoid using it. Why? Well, git is basically just a way of hosting, spreading and mirroring your source onto many git-hosting servers (i.e. you can't avoid using git if you want to spread your code to e.g. Codeberg and GitLab) AND at the same time git doesn't create a [dependency](dependency.md) for your project, i.e. its shittiness doesn't "infect" your project -- if git dies or if you simply want to start using something else, you just copy-paste your source code elsewhere, you put it on [FTP](ftp.md) or anything else, no problem. It's similar to how e.g. [Anarch](anarch.md) uses [SDL](sdl.md) (which is bloated as hell) to run on specific platforms -- if it doesn't hard-depend on SDL and doesn't get tied to it, it's OK (and actually unavoidable) to make use of it. You also don't even have to get into the complicated stuff about git (like merging branches and resolving conflicts) when you're simply committing to a simple one-man project. But if you can make and distribute your project without using git, there should be no hesitation about just sending it to hell.

**Which git hosting to use?** All of them (except for [GitHub](github.md) which is a proprietary terrorist site)! Do not fall into the trap of [githopping](githopping.md), just make tons of accounts, one on each git hosting site, add multiple push remotes and just keep pushing to all of them -- EZ. Remember, git hosting sites are just free file storage servers, not social platforms or brands to identify with. Do NOT use their non-git "features" such as issue trackers, CI and shit. They want you to use them as "facebook for programmers" and become dependent on their exclusive "features", so that's exactly what you want to avoid, just abuse their platform for free file storage. Additional tip on searching for git hosting sites: look up the currently popular git website software and search for its live instances with some nice search engine, e.g. currently searching just `gitea` (or "powered by gitea", "powered by gogs", "powered by forgejo") on [wiby](wiby.md) returns a lot of free git hostings.

## Alternatives

Here are some alternatives to git:

- **[nothing](nothing.md)**: If you don't have many people on the project, you can comfortably just use nothing, like in good [old](old.md) times. Share a directory with the source code and keep regular backups in separate directories, share the source online via [FTP](ftp.md) or something like that, let internet archive back you up.
- **[svn](svn.md)**: The "main", older alternative to git, used e.g. by [SourceForge](sourceforge.md), apparently suffers from some design issues.
- **[mailing list](mailing_list.md)**: Development happens over email, people just keep sending [patches](patch.md) that are reviewed and potentially merged by the maintainers to the main code base. This is how [Linux](linux.md) was developed before git.
- **[darcs](darcs.md)**: Alternative to git, written in [Haskell](haskell.md), advertising itself as simpler.
- **[lit](lit.md)** (previously known as *gut*): WIP [LRS](lrs.md)/[suckless](suckless.md) version control system.
- ...

## How To Use Git For Solo/Small Projects (Cheatsheet)

TODO

- `git add files_you_changed; git commit -m "Update"; git push`
- `git pull`
- `git stash`
- `git rm file_to_remove`
- `git init`
- `git log`
- `git diff`
- `git apply diff_file`
- *weird error*: just look it up on stack overflow

### Set Up Your Own Git Server

WIP

on server:

```
mkdir myrepo
cd myrepo
git init --bare
```

on client you can clone and push with ssh:

```
git clone ssh://user@serveraddress:/path/to/myrepo
cd myrepo
... # do some work
git commit -m "blablabla"
git push
```

you can also make your repo clonnable via HTTP if you have HTTP server (e.g. [Apache](apache.md)) running, just have address `http://myserver/myrepo` point to the repo directory, then you can clone with:

```
git clone http://myserver/myrepo
```

IMPORTANT NOTE: for the HTTP clone to work you need to do `git update-server-info` on the server in the repo directory after every repo update! You can do this e.g. with a git hook or [cronjob](cronjob.md).

## See Also

- [GitHub](github.md)
- [shithub](shithub.,d)