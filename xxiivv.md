# XXIIVV

{ Still researching this shit etc. ~drummyfish }

XXIIVV is a [proprietary](proprietary.md) [soynet](soynet.md) snob website and personal [wiki](wiki.md) (in its concept similar to [our wiki](lrs_wiki.md)) of a Canadian [narcissist](egoism.md) [minimalist](minimalism.md)/esoteric programmer/"artist"/[generalist](generalism.md) David Mondou-Labbe who calls himself "Devine Lu Linvega" ([lmao](lmao.md)) who is a part of a highly cringe [SJW](sjw.md) [fascist](fascism.md) "artist"/programmer group called [Hundred Rabbits](100r.md) (100r) who live on a small boat or something. David seems to be a normie [feminist](feminism.md)/[gay](gay.md) [fascist](fascist.md) (see also [snowflake](snowflake.md)), proclaiming "aggressivity" on his web (under "/ethics.html" on his site). He's also a [plan9](plan9.md) fanboy (i.e. a [pseudominimalist](pseudominimalism.md)). The site is accessible at http://wiki.xxiivv.com/site/home.html. There are some quite good and pretty bad things about it.

{ Holy shit his webring is toxic AF, do not research it. Basically a one huge gay nazi wannabe "artist" circlejerk, it's like a small village worth of the kind of [furry](furry.md) psychopaths who like to draw cute cartoon animals while also advocating slow torture and castration of people who dislike them. ~drummyfish }

Firstly let's see about the letdowns: the site is [proprietary](proprietary.md) and **he licenses his "art" and some of his code under the proprietary [CC-BY-NC-SA](cc_by_nc_sa.md)**, big RETARD ALERT. This means he's a [capitalist](capitalist.md) [open soars](open_source.md) fanboy trying to monopolize art by keeping exclusive "commercial intellectual property rights" (as if his amateur stick figure level "art" had any commercial value lol). At least some of his code is [MIT](mit.md), but he also makes fucking **[PROPRIETARY](proprietary.md) PAID software** (e.g. Verreciel), then he somehow tries to brainwash the readers to believe he is "against capitalism" or what? :'D (Or is he not? I dunno. Definitely seems to be riding the [eco](eco.md) wave.) The guy also seems **[egoistic](egoism.md) as fuck**, invents weird hipster names and "personal pronouns", has some ugly "body modifications", wears cringe rabbit costumes and tries to write in a super cringe pompous/cryptic/poetic tryhard style probably in an attempt to appear smart while just making a fool of himself and, in addition, making it shithard to make any sense of his texts -- truly his tech writings are literal torture to read. The only thing he's missing is a fedora.

There are also nice things though, a few of them being:

- The guy is creating extremely minimalist, small tech from-scratch technology that's worthy of attention. Some of it includes:
  - [uxn](uxn.md): Simple (~100 [LOC](loc.md) of [C](c.md)) [virtual machine](virtual_machine.md), similar to a "[fantasy console](fantasy_console.md)" but intended more for [portability](portability.md). This also comes with an assembly language called [tal](tal.md).
  - [lietal](lietal.md): Simple [artificial language](conlang.md).
- The wiki writes on pretty [interesting](interesting.md) topics, many of which overlap with [our](lrs.md) topics of interest. For example [pen and paper computing](pen_and_paper.md) that includes [games](game.md).
- Some of the presented opinions and wisdoms are [based](based.md), e.g. "for writing fast programs use slow computers" etc.

## See Also

- [100r](100r.md)
- [uxn](uxn.md)
- [permacomputing wiki](permacomputing_wiki.md)