# Unretard

Unretarding means aligning oneself with the *less retarded [software](lrs.md) and [society](less_retarded_society.md)* after acquiring necessary education and mindset. This [wiki](lrs_wiki.md) should help achieve this goal.

Nowadays unretarding is almost synonymous to learning to live and think in exact opposite ways we have been taught to by [modern](modern.md) society.

## See Also

- [shortcut thinking](shortcut_thinking.md)
- [retard](retard.md)
- [how to](how_to.md)
- [nirvana](nirvana.md)
- [zen](zen.md)