# Pi

Pi (normally written with a Greek alphabet symbol with [Unicode](unicode.md) value U+03C0) is one of the most important and famous [numbers](number.md), equal to approximately 3.14, most popularly defined as the ratio of a circle's circumference to its diameter (but also definable in other ways). It is one of the most fundamental mathematical constants of our universe and appears extremely commonly in [mathematics](math.md), nature and, of course, [programming](programming.md). When written down in traditional decimal system, its digits go on and on without end and show no repetition or simple pattern, appearing "random" and [chaotic](chaos.md) -- as of 2021 pi has been evaluated by [computers](computer.md) to 62831853071796 digits, although approximate values have been known from very early times (e.g. the value (16/9)^2 ~= 3.16 has been known as early as around 1800 BC). In significance and properties pi is similar to another famous number: [e](e.md). Pi day is celebrated on March 14.

{ Very nice site about pi: http://www.pi314.net. ~drummyfish }

Pi is a [real](real_number.md) [transcendental](transcendental.md) number, i.e. simply put *it cannot be defined by a "simple" equation* (it is not a root of any [polynomial](polynomial.md) equation). As a transcendental number it is also an [irrational](irrational.md) number, i.e. it cannot be written as an integer [fraction](fraction.md). Mathematicians nowadays define pi via the period of the [exponential function](exp.md) rather than geometry of circles. If we stick to circles, it is [interesting](interesting.md) that in [non-Euclidean](non_euclidean.md) geometry the value of "pi" could be measured to different values (if we draw a circle on an equator of a ball, its circumference is just twice its diameter, i.e. "pi" would be measured to be just 2, reveling the curvature of space).

Pi to 100 decimal digits is:

3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679...

Pi to 100 binary fractional digits is:

11.001001000011111101101010100010001000010110100011000010001101001100010011000110011000101000101110000...

Among the first 50 billion digits the most common one is 8, then 4, 2, 7, 0, 5, 9, 1, 6 and 3.

Some people memorize digits of pi for [fun](fun.md) and competition, the official world record as of 2022 is 70030 memorized digits, however Akira Haraguchi allegedly holds an unofficial record of 100000 digits (made in 2006). Some people make [mnemonics](mnemonic.md) for remembering the digits of pi (this is known as *PiPhilology*), for example *"Now I fuck a pussy screaming in orgasm"* is a sentence that helps remember the first 8 digits (number of letters in each word encodes the digit).

**PI IS NOT INFINITE**. [Soyence](soyence.md) popularizators and nubs often say shit like "OH LOOK pi is so special because it infiniiiiiite". Pi is completely finite with an exact value that's not even greater than 4, what's infinite is just its expansion in [decimal](decimal.md) (or similar) numeral system, however this is nothing special, even numbers such as 1/3 have infinite decimal expansion -- yes, pi is more interesting because its decimal digits are non-repeating and appear [chaotic](chaos.md), but that's nothing special either, there are infinitely many numbers with the same properties and mysteries in this sense (most famously the number [e](e.md) but besides it an infinity of other no-name numbers). The fact we get an infinitely many digits in expansion of pi is given by the fact that we're simply using a system of writing numbers that is made to handle integers and simple fractions -- once we try to write an unusual number with our system, our [algorithm](algorithm.md) simply ends up stuck in an [infinite loop](infinite_loop.md). We can create systems of writing numbers in which pi has a finite expansion (e.g. base pi), in fact we can already write pi with a single symbol: *pi*. So yes, pi digits are interesting, but they are NOT what makes pi special among other numbers.

Additionally contrary to what's sometimes claimed **it is also unproven (though believed to be true), whether pi in its digits contains all possible finite strings** -- note that the fact that the series of digits is infinite doesn't alone guarantee this (as e.g. the infinite series 010011000111... doesn't contain any possible combinations of 1s and 0s either). This would hold if pi was [normal](normal_number.md) -- then pi's digits would contain e.g. every book that will ever be written (see also [Library Of Babel](library_of_babel.md)). But again, there are many other such numbers.

What makes pi special then? Well, mostly its significance as one of the most fundamental constants that seems to appear extremely commonly in math and nature, it seems to stand very close to the root of description of our universe -- not only does pi show that circles are embedded everywhere in nature, even in very abstract ways, but we find it in [Euler's identity](eulers_identity.md), one of the most important equations, it is related to [complex exponential](complex_exponential.md) and so to [Fourier transform](fourier_transform.md), waves, oscillation, trigonometry ([sin](sin.md), [cos](cos.md), ...) and angles ([radians](radian.md) use pi), it even starts appearing in [number theory](number_theory.md), e.g. the probability of two numbers being relative primes is 6/(pi^2), and so on.

## Approximations, Estimations, Measuring And Programming

Evaluating many digits of pi is mathematically [interesting](interesting.md), programs for computing pi are sometimes used as [CPU](cpu.md) [benchmarks](benchmark.md). There are programs that can search for a position of arbitrary string encoded in pi's digits. However in practical computations we can easily get away with pi approximated to just a few decimal digits, **you will NEVER need more than 20 decimal digits**, not even for space flights (NASA said they use 15 places).

One way to judge the quality of pi approximation can be to take the number of pi digits it accurately represents versus how many digits there are in the approximation formula -- this says kind of the approximation's [compression](compression.md) ratio. But other factors may be important too, e.g. simplicity of evaluation, functions used etc.

Also remember, **you can measure pi in real life** by many methods: you can draw a big circle, measure its radius and circumference and then make the division, you can also manually perform the Monte Carlo algorithm (see below) by drawing a circle and then throwing objects around, counting how many fall inside and outside (just watch out to do it correctly, for example you must have the fall spot probability as random as possible, not biased in any way), or you can similarly make a square from wood, then cut out its inscribed circle, weight both parts and compute pi (with the same formula as for Monte Carlo).

{ I tried this -- I took a pizza box, cut out four squares, then  used a pencil on string to draw quarter circles on each, cut them and weighted both groups. All the circle parts weighted 61 grams, the rest weighted 16 grams, this gives me a nice estimate value of pi of about 3.16. ~drummyfish }

An ugly engineering [approximation](approximation.md) that's actually usable sometimes (e.g. for fast rough estimates with integer-only hardware) is just (something like this was infamously almost made the legal value of pi by the so called Indiana bill in 1897)

pi ~= 3

A simple fractional approximation (correct to 6 decimal fractional digits, by Tsu Chung Chih) is

pi ~= 355/113

Such a fraction can again be used even without [floating point](float.md) -- let's say we want to multiply number 123 by pi, then we can use the above fraction and compute 355/113 * 123 = (355 * 123) / 113.

Srinivasa Ramanujan made a great number of pi approximations, e.g. an improvement of the previous to (14 correct digits):

pi ~= 355/113 * (1 - 0.0003/3533)

Similarly Plouffe, e.g. (30 correct digits):

pi ~= ln(262537412640768744)/sqrt(163)

Leibnitz formula for pi is an infinite series that converges to the value of pi, however it converges very slowly { Quickly checked, after adding million terms it was accurate to 5 decimal fractional places. ~drummyfish }. It goes as

pi = 4 - 4/3 + 4/5 - 4/7 + 4/9 - 4/11 + ...

Nilakantha series converges much more quickly { After adding only 1000 terms the result was correct to 9 decimal fractional places for me. ~drummyfish }. It goes as

pi = 3 + 4/(2 * 3 * 4) + 4/(4 * 5 * 6) + 4/(6 * 7 * 8) + ...

A simple **[algorithm](algorithm.md)** for computing approximate pi value can be based on approach used in further [history](history.md): approximating a circle with many-sided regular [polygon](polygon.md) and then computing the ratio of its circumference to diameter -- as a diameter here we can take the average of the "big" and "small" diameter of the polygon. For example if we use a simple square as the polygon, we get pi ~= 3.31 -- this is not very accurate but we'll get a much higher accuracy as we increase the number of sides of the polygon. In 15th century pi was computed to 16 decimal digits with this method. Using inscribed and circumscribed polygons we can use this to get lower and upper bounds on the value of pi.

Another simple approach is [monte carlo](monte_carlo.md) estimation of the area of a unit circle -- by generating random (or even regularly spaced) 2D points (samples) with coordinates in the range from -1 to 1 and seeing what portion of them falls inside the circle we can estimate the value of pi as *pi = 4 * x/N* where *x* is the number of points that fall in the circle and *N* the total number of generated points.

Digits of pi also emerge when trying to measure some distances inside [Mandelbrot set](mandelbrot_set.md) (see David Bolle, 1991) -- this can perhaps also be exploited.

[Spigot](spigot.md) algorithm can be used for computing digits of pi one by one, without [floating point](float.md). Bailey-Borwein-Plouffe formula (discovered in 1995) interestingly allows computing Nth hexadecimal (or binary) digit of pi, WITHOUT having to compute previous digits (and in a time faster than such computation would take). In 2022 Plouffe discovered a similar formula for computing Nth decimal digit.

The following is a [C](c.md) implementation of the Spigot algorithm for calculating digits of pi one by one that doesn't need [floating point](float.md) or special arbitrary length data types, adapted from the original 1995 paper. It works on the principle of converting pi to the decimal base from a special mixed radix base 1/3, 2/5, 3/7, 4/9, ... in which pi is expressed just as 2.22222... { For copyright clarity, this is NOT a web copy paste, it's been written by me according to the paper. ~drummyfish }

```
#include <stdio.h>

#define DIGITS 1000
#define ARRAY_LEN ((10 * DIGITS) / 3)

unsigned int pi[ARRAY_LEN];

void writeDigit(unsigned int digit)
{
  putchar('0' + digit);
}

int main(void)
{
  unsigned int carry, digit = 0, queue = 0;

  for (unsigned int i = 0; i < ARRAY_LEN; ++i)
    pi[i] = 2; // initially pi in this base is just 2s

  for (unsigned int i = 0; i < DIGITS; ++i)
  {
    carry = 0;

    for (int j = ARRAY_LEN - 1; j >= 0; --j)
    { // convert to base 10 and multiply by 10 (shift one to left)

      unsigned int divisor = (j + 1) * 2 - 1; // mixed radix denom.

      pi[j] = 10 * pi[j] + (j + 1) * carry;
      carry = pi[j] / divisor;
      pi[j] %= divisor;
    }

    pi[0] = carry % 10;
    carry /= 10;

    switch (carry)
    { // latter digits may influence earlier digits, hence these buffers
      case 9: // remember consecutive 9s
        queue++;
        break;

      case 10: // ..X 99999.. becomes ..X+1 00000...
        writeDigit(digit + 1);

        for (unsigned int k = 1; k <= queue; ++k)
          writeDigit(0);

        queue = 0;
        digit = 0;
        break;

      default: // normal digit, just print
        if (i != 0) // skip the first 0
          writeDigit(digit);

        if (i == 1) // write the decimal point after 1st digit
          putchar('.');

        digit = carry;

        for (unsigned int k = 1; k <= queue; ++k)
          writeDigit(9);

        queue = 0;
        break;
    }
  }

  writeDigit(digit); // write the last one

  return 0;
}
```

## See Also

- [e](e.md)
- [phi](phi.md), or golden ratio