# Furry

*"Human seriously believing to be a dog not considered mental illness anymore."* --[21st century](21st_century.md)

Furriness is a serious mental [disorder](disease.md) (dolphi will forgive :D) and fetish that makes people become stuck in the infantile stage of development and become extremely creepily obsessed and/or identify with anthropomorphic animals (usually those with fur) far beyond any line of acceptability as a healthy personality trait, they often identify e.g. with [cats](cat.md), foxes or even completely made up species. To a big degree it's a sexual identity but those people just try to pretend (and possibly even believe) they're animals everywhere; not only do they have furry conventions, you just see furry avatars all over the internet, on issue trackers on programming websites and forums, recently zoomer kids started to even e.g. meow in classes because they identify as cats (this caused some huge drama somewhere in the UK). You cannot NOT meet a furry on the [Internet](internet.md). They usually argue it's "cute" and try to make no big deal of it, however that's a mask beyond which something horribly rotten lies. There is something more to furrydom, it's basically a cult that has taken an idea too far, kind of like anorexia takes losing weight a bit too far -- cuteness is OK, however furries are not really cute, they are CREEPY, they take this beyond healthy passion, you see the psychopathic stares in their faces, they take child cartoon characters and fantasize about them being transsexual and gore raping them and having children with them, some even attempt suicides if you insult their favorite characters etc.

Also the furry community -- it's extremely [toxic](toxic.md), firstly as any big internet-centered group it's mostly completely infected with [wokeness](woke.md), [LGBT](lgbt.md)+[feminazism](feminism.md), which combined with the cult behavior may really lead to the community cyber pushing you to suicide if you e.g. question the gender of some child cartoon character (or even if you for example oppose the idea the character has to have some non-binary gender). A favorite hobby of furries is to destroy software project by pushing ugly woke furry mascots, threatening by suicide if the project doesn't accept them. Furries also seem to have a strong love of [copyright](copyright.md) so as to "protect" their shitty amateur art no one would want to copy anyway. Many create their own "fursonas" or "species" and then prohibit others from using them, they are so emotionally invested in this they may literally try to murder you if you do something to the drawing of their character. Stay away.

Furry [porn](porn.md) is called **[yiff](yiff.md)**.

In the 1990s we were wondering whether by 2020 we'd already have cured cancer, solved world hunger, if we'd have cloned the mammoth, whether we'd have cities on Mars and flying cars. Well no, but you can sexually identify as a fox now.

{ POST DISCLAIMER: Firstly I have to disclaim I hate disclaimers, this is more of a note. Some of my very best online friends are furries, I hope they'll forgive me for this article, hopefully it's clear I never hate any individual on a personal level. Yeah, I do hate furrydom, but if you don't mind being friends with me, I will love to be friends with you no matter what, I really don't give a damn about this shite. Peace and please be safe :) <3 ~drummyfish }

## See Also

- [uwu](uwu.md)
- [zoophilia](zoophilia.md)
- [retardedness](retard.md)
- [brony](brony.md)