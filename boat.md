# Boat Dock

*WELCOME :) You find yourself on a strange [island](island.md).*

```
           ,
           |\
           | \
           |  )
     ______|_/____
~-~-~\           /~-~-~
      "-._____.-"
```

What is this? Boat is a [LRS](lrs.md) spinoff of [Tour Bus](tourbus.md), a famous wiki [webring](webring.md) -- see http://meatballwiki.org/wiki/TourBus. Why not join Tour Bus? Because we are antisocial and don't wanna talk to anyone, so we just start our new thing (also they would prolly [censor](censorship.md) us). Also our island is isolated from the [normieland](normieland.md) and no buses go here :)

## On To The Island

You get greeted by a friendly [dog](dog.md) -- *WOOF* --playfully waggling his tail he leads you around, along the beach. The [island](island.md) seems a bit empty but a few people can be seen here and there [loosely associating](anarchism.md), looking very passionate about creating various things, some are writing, some constructing [weird machines](mechanical.md), some copulating. Everyone is naked -- "clothes are [bloat](bloat.md)" says a [weirdo of caveman appearance](drummyfish.md) sitting in front of what appears to be his hut. "We are trying to create stuff, mostly with [computers](computer.md)", he says, "also hiding here from the [hell](capitalism.md) out there, trying to live a [better life](less_retarded_society.md)". He scratches his butt and adds: "Seeing you are a living being like myself -- that means you are welcome, come join us if you want."

**sightseeing**:

- [less retarded software](lrs.md): what we create
- [less retarded society](less_retarded_society.md): what we strive for
- [capitalism](capitalism.md): what we oppose
- [jokes](jokes.md): we also try to have some fun [fun](fun.md)

## Continue Elsewhere

- **boat #1**: [Tour Bus Stop: meatballwiki](http://meatballwiki.org/wiki/TourBusStop), normieland (the main hub of Tour Bus)
- **boat #2**: [YOUR LINK HERE] :-) TODO, here will be some kinda site related to LRS

## How To Join Our Boat Tour

Just link to this site from your site. If you want your site added here as a new departure boat, send [me](drummyfish.md) an email -- it's ideal if it's a wiki or something that has something to do with [LRS](lrs.md) (even remotely, no need to mention LRS, can be just software minimalism or whatever, ...). **I don't promise to add everything**, but it's pretty likely I'll add you if it's not a complete [shit](shit.md) :D When (more like if) a few boats are here, other ones should be added further on to the chain, not here. Remember this isn't supposed to be a link dump but a selection of some kinda thematic quality links that form some nice webring.