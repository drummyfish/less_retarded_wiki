# Rock

Rocks and stones are natural formations of minerals that can be used to create the most primitive [technology](technology.md). Stone age was the first stage of our civilization; it was characterized by use of stone tools. Rock [nerds](nerd.md) are called geologists.

```
          ____
   __..--'    ''-.
  /  '        .-'"\
 {   .  \_.  ;     \
  ) /   , ':.      |
  (    :     .   '"}
   \_ (     :   __/
     ''.__---'''
```

*rock*

Rocks are pretty [suckless](suckless.md) and [LRS](lrs.md) because they are simple, they are everywhere, free and can be used in a number of ways such as:

- As a building material.
- For [fun](fun.md) and entertainment, you can play various games with rocks: rock skipping, petanque, long throw, even [chess](chess.md).
- As [weapons](weapon.md), even though we discourage this use.
- For making tools such as [knives](knife.md) or [hammers](hammer.md).
- To hold heat: you can e.g. heat stones in fire and let them heat you while you sleep or use them to cook something.
- For writing, some can be used as a chalk, some may be used to carve into, which makes rocks an extremely durable storage for both [digital](digital.md) and [analog](analog.md) [data](data.md) -- thanks to this  we have records of very old [history](history.md), see also [RCBD](rock_carved_binary_data.md). A dust from some rocks can also be used to make dye to paint or write with.
- As weights.
- For help with counting (e.g. [abacus](abacus.md)) -- this makes rocks a kind of [computer](computer.md)!
- To make [art](art.md), decorations, small statues etc.
- For breaking things, grinding, sharpening etc.
- With advanced technology we can get metals out of rocks, extract geological knowledge from them etc.
- Some rocks can be used to start [fire](fire.md).
- Some are pretty rare and can be used as a [currency](currency.md), even though we hate [money](money.md) and discourage this as well.
- You can collect them as a cool non-capitalist hobby.
- They can be shaped into many more things like bowls, plates or maybe marbles which in turn can be used e.g. in some kind of [mechanical computer](mechanical.md).
- As pets... apparently pet rocks are a thing :)
- ...

## See Also

- [sand](sand.md)
- [wood](wood.md)
- [fire](fire.md)
- [potato](potato.md)
- [metal](metal.md)
- [pop](pop.md)