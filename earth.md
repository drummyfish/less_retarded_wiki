# Earth

Well, Earth is the [planet](planet.md) we live on. It is the third planet from the [Sun](sun.md) of our Solar system which itself is part of the [Milky Way](milky_way.md) [galaxy](galaxy.md), [Universe](universe.md). So far it is the only known place to have [life](life.md).

Now behold the grand rendering of the Earth map in [ASCII](ascii_art.md) ([equirectangular](equirectangular.md) projection):

```
X      v      v      v      v      v      v      v      v      v      v      v      v      v      v      v      X
                        .-,./"">===-_.----..----..      :     -==- 
                     -=""-,><__-;;;<""._         /      :                     -===-
    ___          .=---""""\/ \/ ><."-, "\      /"       :      .--._     ____   __.-""""------""""---.....-----..
> -=_  """""---""           _.-"   \_/   |  .-" /"\     :  _.''     "..""    """                                <
"" _.'ALASKA               {_   ,".__     ""    '"'   _ : (    _/|                                         _  _.. 
  "-._.--"""-._    CANADA    "--"    "\              / \:  ""./ /                                     _--"","/
   ""          \                     _/_            ",_/:_./\_.'                     ASIA            "--.  \/
>               }                   /_\/               \:EUROPE      __  __                           /\|       <
                \            ""=- __.-"              /"":_-. -._ _, /__\ \ (                       .-" ) >-
                 \__   USA      _/                   """:___"   "  ",     ""                   ,-. \ __//
                    |\      __ /                     /"":   ""._..../                          \  "" \_/
>                    \\_  ."  \|      ATLANTIC      /   :          \\   <'\                     |               <
                        \ \_/| -=-      OCEAN       )   :AFRICA     \\_.-" """\                .'
       PACIFIC           "--._\                     \___:            "/        \ .""\_  <^,..-" __
        OCEAN                 \"""-""-.._               :""\         /          "     | _)      \_\INDONESIA
>.............................|..........",.............:...\......./................_\\_....__/\..,__..........<                
                              |   SOUTH    \            :   /      |                 "-._\_  \__/  \  ""-_
                               \ AMERICA   /            :  (       }                     """""===-  """""_  
                                \_        |             :   \      \                          __.-""._,"",
>                                 \      /              :   /      / |\                     ," AUSTRALIA  \     <
                                  |     |               :   \     /  \/      INDIAN         ";   __        )
                                  |     /               :    \___/            OCEAN           """  ""-._  / 
                                 /     /                :                                               ""   |\
>                                |    /                 :                                               {)   // <
                                 |   |                  :                                                   ""
                                 \_  \                  :
                                   """                  :
>                                     .,                :                                                       <
                       __....___  _/""  \               :          _____   ___.......___......-------...__
--....-----""""----""""         ""      "-..__    __......--"""""""     """                              .;_..... 
                                              """"      : ANTARCTICA
X      ^      ^      ^      ^      ^      ^      ^      ^      ^      ^      ^      ^      ^      ^      ^      X
```

Some numbers about the planet Earth:

- age: 4.54 billion years
- distance from the [Sun](sun.md) (nearest, furthest): 147098450 km, 152097597 km, 
- radius (equator, poles): 6378 km, 6356 km
- mass: 5.9 * 10^24 kg
- acceleration by gravity: 9.8 m/s^2
- axial tilt: 23.4 degrees
- length of year: 365.25 days
- land vs water area: 148940000 km^2, 361132000 km^2

```
     _---"""---_
   .':. ;::.: -./;.
  /;:;:: ':  .-':::\
.|';:'      ';':;:::|.
|::         .:'::;:::|
|::.         ':;:::::|
'|:;           ':;::|'
  \::          .:::/
   ':_        .:_.'
      """----"""
```

*Earth from space*