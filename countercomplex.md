# Countercomplex

*"True progress is about deepness and compression instead of maximization and accumulation."* -[Viznut](viznut.md)

Countercomplex is a [blog](blog.md) (running since 2008) of a Finnish [hacker](hacker_culture.md) and [demoscener](demoscene.md) [Viznut](viznut.md), criticizing technological complexity/[bloat](bloat.md) and promoting [minimalism](minimalism.md) as a basis of truly good technology. It is accessible at http://countercomplex.blogspot.com/.