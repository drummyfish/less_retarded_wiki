# Hard To Learn, Easy To Master

"Hard to learn, easy to master" is the opposite of "[easy to learn, hard to master](easy_to_lear_hard_to_master.md)".

Example: drinking coffee while flying a plane.