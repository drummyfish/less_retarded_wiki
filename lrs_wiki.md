# LRS Wiki

LRS wiki, also Less Retarded Wiki, is a [public domain](public_domain.md) ([CC0](cc0.md)) [encyclopedia](encyclopedia.md) focused on truly good, [minimalist](minimalism.md) [technology](tech.md), mainly [computer](computer.md) [software](sw.md) -- so called [less retarded software](lrs.md) (LRS) which should serve the people at large -- while also exploring related topics such as the relationship between technology and society, promoting so called [less retarded society](less_retarded_society.md). The basic motivation behind LRS and its wiki is unconditional [love](love.md) of all life, and the goal of LRS is to move towards creating a truly useful, [selfless](selflessness.md) technology that maximally helps all living beings as much as possible -- it does so while at the same time developing its own [culture](culture.md), one that's much more sane and compatible with the desired goal, unlike the [toxic](toxic.md) [modern](modern.md) [fascist](fascism.md) culture of the [21st century](21st_century.md). As such the wiki rejects for example [capitalist software](capitalist_software.md) (and [capitalism](capitalism.md) itself), [bloated](bloat.md) software, [intellectual property](intellectual_property.md) laws ([copyright](copyright.md), [patents](patent.md), ...) [censorship](censorship.md), [pseudoleftism](pseudoleft.md) ([political correctness](political_correctness.md), [cancel culture](cancel_culture.md), [COC](coc.md)s ...) etc. It embraces [free as in freedom](free_software.md), simple technology, i.e. [Unix philosophy](unix_philosophy.md), [suckless](suckless.md) software, [anarcho pacifism](anpac.md), [racial realism](racial_realism.md), [free speech](free_speech.md), [veganism](veganism.md) etc. As a work promoting pure [good](good.md) in a [time](21st_century.md) of universal rule of [evil](evil.md) it is greatly controversial, misunderstood and often met with hostility. However it already gained at least a few englightened followers.

The wiki is not funded by anyone, it doesn't have to be funded because it's not a capitalist project, it is also not affiliated with anyone, its author has no career for which he would have to fear, he has no friends or family, no material desires, no fear of demonetization/demonization, jail or death, so he can write truth without being limited by anything.

LRS wiki was started by [drummyfish](drummyfish.md) on November 3 2021 as a way of recording and sharing his views, experience and knowledge about technology, as well as for creating a completely public domain educational resource and account of current society for future generations. It was forked from so called "based wiki" at a point when all the content on it had been made by drummyfish, so at this point LRS wiki is 100% drummyfish's own work; over time it became kind of a snapshot of drummyfish's brain and so the wiki doesn't allow contributions (but allows and encourages [forks](fork.md)).

Some distinguishing features of LRS wiki that make it much better than other wikis include:

- Everything is absolutely and completely public domain under CC0 and there appears nothing under "fair use" so you can blindly copy-paste any piece of text or code and do absolutely whatever you want with it, without having to give credit or maintain some back link or any similar kind of [bullshit](bullshit.md) insanity.
- There are no images, only [ASCII art](ascii_art.md), making it very small in size, free and readable on devices that can only display text.
- Use of [Unicode](unicode.md) is minimized to absolutely bare minimum so you'll probably never encounter problems related to encoding -- again, anything that can display ASCII will be able to read the wiki.
- There is absolutely no moderation, no [code of censorship](coc.md) and the wiki doesn't adhere to cultural standards of [21st century](21st_century.md) -- for example it never uses "gender neutral" pronouns -- it rather tries to focus on real issues and reflect truth.
- There are no [SJW](sjw.md), [pseudoleftist](pseudoleft.md) or [rightist](right.md) editors.
- Articles try to be written somewhat well, they aim to actually help most people rather than be an ugly dump of every information ever discovered about the subject.
- Articles often contain simple runnable code examples, mostly in plain [C](c.md) code aligned with LRS principles, which can normally just be copy pasted into a file and compiled without additional libraries.
- There are no [ads](marketing.md) of course and there will never be a single one, it is impossible for LRS to contain any ad just as it is impossible for human to breathe water.
- It is not a website, websites is just one form of the wiki -- it exists in other formats such as plaintext on [gopher](gopher.md), one long pdf document, one long HTML document, collection of markdown files etc.
- plus much more :)

Over time, being written solely by drummyfish without much self censorship and "language filtering", the wiki also became something like drummyfish's raw brain dump with all the thoughts and moods averaged over the time span of writing the wiki -- reading through it makes you see relatively faithfully how drummyfish internally thinks (e.g. you see anticapitalist rants everywhere because these annoying thoughts are just constantly bothering drummyfish, whatever he's thinking about) -- this can make many people vomit but it's a kind of experiment and some even liked it, so it stays up. No one is forced to read it and CC0 ensures anyone can shape it into anything better hopefully.

The wiki can also additionally be seen as a dirty collection of drummyfish's cheatsheets, links, code snippets, [jokes](jokes.md), attempts at [ASCII art](ascii_art.md), vent rants etcetc. So the whole thing is like a digital swamp that one might see as a kind of retarded [art](art.md) that combines many things together: technical, cultural, personal, objective and subjective, beautiful and ugly. It might also be viewed as a shitpost or [meme](meme.md) taken too far. It's just its own thing.

The wiki is similar to and was inspired by other wikis and similar works, for example in its topics and technical aspects it is similar to the earliest (plain HTML) versions of [Wikipedia](wikipedia.md) and [wikiwikiweb](wikiwikiweb.md). In tone and political incorrectness it is similar to [Encyclopedia Dramatica](dramatica.md), but unlike Dramatica LRS is a "serious" project.

**Technology "powering" LRS wiki** is plain and simple, it uses no frameworks or static site generators, the wiki is currently written as a collection of [Markdown](markdown.md) files that use a few [shell scripts](shell_script.md) that convert the whole thing to HTML for the web (and can also produce txt and pdf version of it), i.e. it doesn't use any wiki engine or bloated static site generator. There is a plan to maybe rewrite the wiki in [comun](comun.md).

Is LRS is wiki as based as [encyclopedia dramatica](dramatica.md)? That's the big question. One thing is for sure though, you will probably find LRS wiki being actually online :)

## See Also

- [LRS wiki stats](wiki_stats.md)
- [LRS wiki style guide](wiki_style.md)
- [LRS wiki authors](wiki_authors.md)
- [LRS wiki "rights"](wiki_rights.md)
- [LRS wiki post mortem](wiki_post_mortem.md)
- [LRS](lrs.md)
- [less retarded society](less_retarded_society.md)
- [FAQ](faq.md)
- [encyclopedia dramatica](dramatica.md)