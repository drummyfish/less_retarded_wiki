# Finished

A finished [project](project.md) is completed, working and doesn't need regular [maintenance](maintenance.md), it serves its users and doesn't put any more significant burden of development cost on anyone. A finished project is not necessarily perfect and [bugless](bug.md), it is typically just working as intended, greatly [stable](stable.md), usable, well [optimized](optimization.md) and [good enough](good_enough.md). In a sane society (such as [lrs](less_retarded_society.md)) when we start a project, we are deciding to invest some effort into it with the promise of one day finishing it and then only benefiting from it for evermore; however under [capitalist's](capitalism.md) [update culture](update_culture.md) nothing really gets finished, projects are started with the goal of developing them forever so as to enslave more and more people (or, as capitalists put it, "create jobs" for them), which is extremely [harmful](harmful.md). Finished project often have the [version number](version_numbering.md) 1.0, however under capitalist update culture this just a checkpoint towards implementing basic features which doesn't really imply the project is finished (after 1.0 they simply aim for 2.0, 3.0 etc.). **Always aim for projects that will be finished** (even if potentially not by you); sure, even in a good society SOME projects may be "perpetual" in nature -- for example an [encyclopedia](encyclopedia.md) that's updated every 5 years with new knowledge and discoveries -- however this should only be the case where NECESSARY and the negative effects of this perpetual nature should be minimized (for example with the encyclopedia we should make the update span as large as possible, let's say 5 years as opposed to 1 year, and we should yield a nice and tidy release after every update).

Examples of projects that have been finished are:

- [Collapse OS](collapse_os.md)
- [Anarch](anarch.md)
- Old [video games](game.md), especially those for game consoles distributed on physical media such as cartridges or CDs. When for example a [GameBoy](gameboy.md) game was released back then, it had to work as the user would buy the cartridge whose content couldn't be updated over the [Internet](internet.md), if there was a bug, it was there forever.
- ...

**How to make greatly finishable projects?**

- Make a plan and stick to it, have a goal for what the project should look like when finished, have a [roadmap](roadmap.md) (even if only in your head). I.e. don't aim for "creating a good text editor", aim for "creating a text editor with features X, Y, Z that's written in under N lines of code".
- Only use time-tested [suckless](suckless.md)/[LRS](lrs.md) [future proof](future_proof.md) technology that's itself finished, for example the [C99](c.md) language.
- [Keep it simple](kiss.md), minimize [dependencies](dependency.md) (there are often the cause for the need of [maintenance](maintenance.md)), adhere to extreme [minimalism](minimalism.md). See also [portability](portability.md).
- TODO