# World Broadcast

{ I don't know too much about radio transmissions, please send me a mail if something here is a complete BS. ~drummyfish }

*WIP!*

World (or world-wide) [broadcast](broadcast.md) is a possible [technological](tech.md) service (possibly complementing the [Internet](internet.md)) which could be implemented in a [good society](less_retarded_society.md) and whose main idea is to broadcast generally useful [information](information.md) over the whole globe so that simple and/or energy saving [computers](computer.md) could get basic information without having to perform complex and costly two-way communication.

It would work on the same principle as e.g. [teletext](teletext.md): there would be many different [radio](radio.md) transmitters (e.g. towers, satellites or small radios) that would constantly be broadcasting generally useful information (e.g. time or news) in a very simple format (something akin text in [Morse code](morse_code.md)). Any device capable of receiving radio signal could wait for desired information (e.g. waiting for certain keyword such as `TIME:` or `NEWS:`) and then save it. The advantage would be [simplicity](kiss.md): unlike with [Internet](internet.md) (which would of course still exist) the device wouldn't have to communicate with anyone, there would be no servers communicating with the devices, there would be no communication protocols, no complex code, no [DDOS](ddos.md)-like overloading of servers, and the receiving devices wouldn't waste energy (as transmitting a signal requires significant energy compared to receiving it -- like shouting vs just listening). It would also be more widely available than Internet connection, e.g. in deserts.

## See Also

- [Ronja](ronja.md)
- [teletext](teletext.md)