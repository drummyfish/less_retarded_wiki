# Demo

Demo may stand for:

- A special kind of computer [program](program.md) that's made as a non-interactive audiovisual art. See [demoscene](demoscene.md).
- A non-video recording of game play (sometimes also called a *replay*), typically done by recording only user inputs in a [deterministic](determinism.md) game, or by only recording game state information (such as player positions at each frame etc.). Demos are used e.g. in [Doom](doom.md) or [Quake](quake.md). The advantage of a demo is its much smaller size compared to a video, as well as the possibility to replay it with different graphic settings, ability to detect cheating (i.e. verifying the game inputs are correct and humanly possible) etc.
- A significantly limited gratis trial version of an otherwise paid program. This term used to be used mainly in the past, when demo versions of programs (such as games) were distributed e.g. on CDs in magazines for user to try out and potentially buy the full version.
- ...