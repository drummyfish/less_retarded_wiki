# Global Discussion

This is a place for general discussion about anything related to our thing. To comment just edit-add your comment. I suggest we use a tree-like structure as shows this example:

- Hello, this is my comment. ~drummyfish
  - Hey, this is my response. ~drummyfish

If the tree gets too big we can create a new tree under a new heading.

## General Discussion

