# Copyfree

Copyfree Initiative is yet another [nonprofit](nonprofit.md) group trying to create and maintain its own definition and standardization of ["free as in freedom"](free_software.md). Its website is https://copyfree.org/, the symbol they use is F in a circle. Similarly to e.g. [FSF](fsf.md) copyfree maintains a list of approved and rejected [licenses](license.md); the main characteristics of the group are great opposition of [copyleft](copyleft.md) in favor of [permissive](permissive.md) licenses, which is good, however the group justifies its operation by "helping [business](business.md)", i.e. it's most likely just another unethical [capitalist](capitalism.md) organization that will ultimately stand against people and once/if becomes successful will sell its soul to the highest bidder. Copyfree was founded by Chad Perrin and has existed at least since 2007 (according to internet archive).

## See Also

- [FSF](fsf.md)
- [OSI](osi.md)
- [suckless](suckless.md)
- [GNG](gng.md)
- [Creative Commons](creative_commons.md)
- [Bitreich](bitreich.md)
- [LRS](lrs.md)