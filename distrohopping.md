# Distrohopping

Distrohopping is a serious mental [illness](disease.md) that makes people waste lives on constantly switching [GNU](gnu.md)/[linux](linux.md) [distributions](distro.md) ("distros"). This affects mostly those of the lowest skill in tech who feel the need to LARP as wannabe tech nerds; as it's been said, an amateur is obsessed with tools, an expert is obsessed with mastery ([Richard Stallman](rms.md) has for example famously never installed GNU/Linux himself as he has better things to do) -- a true programmer will just settle with a comfy [Unix](unix.md) environment that can run [vim](vim.md) and dedicate his time to creating a timeless source code while the hopper, like a mere animal, is just busy masturbating to a new bastard child of Ubuntu and Arch Linux that adds a new wallpaper and support for [vertical mice](vertical_mouse.md) -- **such an activity is basically as retarded as mainstream tech [consumerism](consumerism.md)** with the only difference being a hopper isn't limited by finance so he can just distrohop 24/7 and hop himself to death.

TODO: cure? take the bsd pill? :-)

## See Also

- [editorhopping](editorhopping.md)
- [genderhopping](genderhopping.md)
- [hopping](hopping.md) as a disease in general