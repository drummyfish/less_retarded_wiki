# Version Numbering

Version numbering is a [system](system.md) of assigning [numbers](number.md) (or even general text [strings](string.md)) to different versions of computer programs or similar projects. The basic purpose of this is to distinguish different versions from each other while also knowing which one is newer (more "[up to date](update_culture.md)"), however version identifiers often go further and provide more [information](information.md) such as the exact release date, whether the version is [stable](stability.md) or testing, with which other versions it is [compatible](compatibility.md) and so on.

TODO

## Traditional Version Numbering

TODO

## LRS Version Numbering

At [LRS](lrs.md) we suggest the following version numbering (invented/employed by [drummyfish](drummyfish.md)): { OFC I don't know if anyone else is already doing this, I'm not claiming any history firsts, just that this is what I independently started doing in my projects. ~drummyfish }

This is a simple system that tries to not be [dependent](dependency.md) on having a [version control system](vcs.md) such as [git](git.md), i.e. this system works without being able to make different development branches and can be comfortably used even if you're just developing a project in a local directory without any fancy tools. Of course you can use a VCS, this system will just make you depend less on it so that you can make easier transitions to a different VCS or for example drop a VCS altogether and just develop your projects in a directory with FTP downloads.

Version string is of format *major.minor* with optional suffix letter `d`, e.g. 0.35, 1.0 or 2.1d.

Major and minor numbers have the usual meaning. Major number signifies the great epochs of development and overall project state -- 0 means the project is in a state before the first stable, highly usable, [optimized](optimization.md), [tested](testing.md) and [refactored](refactoring.md) release that would have all initially set goals and features implemented. 1 means the project has already reached such state. Any further increment signifies a similarly significant change in the project ([API](api.md) overhaul, complete rewrite, extreme number of new features etc.) that has already been incorporated with all the testing, optimization, refactoring etc. At the start of each major version number the minor version number is set to 0 and within the same major version number a higher minor number signifies smaller performed changes (bug fixes, smaller optimizations, added/removed individual features etc.). Minor number doesn't have to be incremented by 1, the value may try to intuitively reflect the significance of the implemented changes (e.g. versions 0.2 and 0.201 differ only very slightly whereas versions 0.2 and 0.5 differ significantly).

From the user's perspective a greater *major.minor* number signifies a newer version of the project and the user can be sure that versions with the same *major.minor* number are the same.

The main difference against traditional version numberings is the optional `d` suffix. This suffix added to version number *X* signifies an in-development (non-release) branch based on version *X*. **If a version has the `d` suffix, it doesn't have to change major and minor numbers with implemented changes**, i.e. there may be several different versions of the project whose version number is for example 0.63d. I.e. with the `d` suffix it no longer holds that versions with the same number are necessarily the same. This allows developer to not bother about incrementing version number with every single change (commit). The developer simply takes the latest release version (the one without `d` suffix), adds the `d` suffix, then keeps modifying this version without caring about changing the version number with each change, and when the changes are ready for release, he removes the `d` suffix and increases the version number. The user may choose to only use the release (stable, tested, ...) version without the suffix or he can take the risk of using the latest development version (the one with the `d` suffix).

With this a project can be comfortably developed in s single git branch without managing separate stable and development branches. The head of the branch usually has the latest in-development version (with the `d` suffix) but you may just link to previous commits of release versions (without the `d` suffix) so that users can download the release versions. You can even not use any VCS and just develop your project in a directory and make hard backups of each release version.

Here is an example of version numbering a project with the LRS system:

| version | description                   | release?  |
| ------- | ----------------------------- | --------- |
| 0.0     | just started                  | YES       |
| 0.0d    | added license, Makefile, ...  | no        |
| 0.0d    | added some code               | no        |
| 0.0d    | added more code               | no        |
| 0.01    | it already does something!    | YES       |
| 0.01d   | added more features           | no        |
| 0.01d   | fixed some bugs               | no        |
| 0.01d   | refactored                    | no        |
| 0.2     | it is already a bit usable!   | YES       |
| 0.2d    | added more features           | no        |
| ...     | ...                           | ...       |
| 0.9d    | fixed bugs                    | no        |
| 0.9d    | refactored                    | no        |
| 1.0     | nice, stable, tested, usable! | YES       |
| 1.0d    | added a small optimization    | no        |
| ...     | ...                           | ...       |
| 2.0     | complete rewrite for perform. | YES       |
| 2.0d    | added a few features          | no        |
| 2.0d    | fixed a bug                   | no        |
| 2.01    | a few nice improvements       | YES       |
| ...     | ...                           | ...       |