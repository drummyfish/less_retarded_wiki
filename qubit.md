# Qubit

Qubit is a [quantum computing](quantum.md) equivalent of a [bit](bit.md). While bits in classical computers can have one of two state -- either 0 or 1 -- a qubit can additionally have infinitely many states "in between" 0 and 1 (so called [superposition](superposition.md)). Physically qubits can be realized thanks to quantum states of particles, e.g. the polarization of a photon or the spin of a photon. Qubits are processed with [quantum gates](quantum_gate.md).

**Whenever we measure a qubit, we get either 1 or 0**, just like with a normal bit. However during quantum computations the internal state of a qubit is more complex. This state determines the **probabilities** of measuring either 1 or 0. When the measurement is performed (which is basically any observation of its state), the qubit state collapses into one of those two states.

Now we will be dealing with so called **pure states** -- these are the states that can be expressed by the following representation. We will get to the more complex (mixed) states later.

The state of a qubit can be written as

*A * |0> + B * |1>*

where *A* and *B* are [complex numbers](complex_number.md) such that *A^2 + B^2 = 1*, *|0>* is a vector [0, 1] and *|1>* is a vector [1, 0]. *A^2* gives the probability of measuring the qubit in the state 0, *B^2* gives the probability of measuring 1.

The vectors *|0>* and *|1>* use so called bra-ket notation and represent a vector basis of a two dimensional state. So the qubit space is a point in a space with two axes, but since *A* and *B* are complex, the whole space is four dimensional (there are 4 variables: *A* real, *A* imaginary, *B* real and *B* imaginary). However, since *A + B* must be equal to 1 ([normalized](normalization.md)), the point cannot be anywhere in this space. Using logic^TM we can figure out that the final state of a qubit really IS a point in two dimensions: a point on a sphere (Bloch sphere). A point of the sphere can be specified with two coordinates: *phase* ([yaw](yaw.md), 0 to 2 [pi](pi.md), can be computed from many repeated measurements) and *p* ([pitch](pitch.md), says he probability of measuring 1). It holds that:

*A = sqrt(1 - p)*

*B = e^(i * phase) * sqrt(p)*

The sphere has the state |0> at the top (north pole) and |1> at the bottom (south pole); these are the only points a normal bit can occupy. The equator is an area of states where the probability of measuring 0 and 1 are equal (above the equator gives a higher probability to 0, below the equator to 1).

Now a qubit may actually be in a more complex state than the pure states we've been dealing with until now. Pure states can be expressed with the state vector described above. Such a state is achieved when we start with a qubit of known value, e.g. if we cool down the qubit, we know it has the value *|0>*, and transforming this state with quantum gates keep the state pure. However there are also so called **mixed states** which are more complex and appear e.g. when the qubit may have randomly been modified by an external event, or if we start with a qubit of unknown state. Imagine if we e.g. start with a qubit that we known is either *|0>* or *|1>*. In such case we have to consider all those states separately. A mixed state is composed of multiple pure states. Mixed states can be expressed with so called **density matrices**, an alternative state representation which is able to encode these states.