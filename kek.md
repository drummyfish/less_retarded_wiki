# Kek

Kek means [lol](lol.md). It comes from [World of Warcraft](wow.md) where the two opposing factions (Horde and Alliance) were made to speak mutually unintelligible languages as an attempt to prevent enemy players from communicating; when someone from Horde typed "lol", an Alliance player would see him say "kek". The other way around (Alliance speaking to Horde) would render "lol" as "bur", nevertheless kek became the popular one. On the Internet this has further mutated into advanced forms like *kik*, *kekw*, *topkek*, *giga kek* etc. Nowadays in some places such as [4chan](4chan.md) kek seems to be used even more than lol, it's the newer, "cooler" way of saying lol.

## See Also

- [lulz](lulz.md)
- [meme](meme.md)