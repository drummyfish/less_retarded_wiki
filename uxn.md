# Uxn

{ WIP, researching this etcetc. ~drummyfish }

Uxn is a [minimalist](minimalism.md) [self hosted](self_hosting.md) [stack](stack.md)-based [virtual machine](virtual_machine.md) 8bit/16bit [computer](computer.md) aiming for great [simplicity](simplicity.md) and [portability](portability.md). It is quite nice and impressive, having its own [instruction set](isa.md), [assembly](assembly.md) language, many implementations and many programs written for it already (e.g. [Left](left.md) text editor, Noodle drawing tool etc.); it was made by the author of [xxiivv](xxiivv.md) wiki (some weird [narcissist](egoism.md) self proclaimed artist that's sailing the seas or something). From the minimalist point of view uxn really seems to be going in the right direction, it is inspired by old computers such as [NES](nes.md) and [C64](c64.md), practicing real minimalism (not just [pseudominimalism](pseudominimalism.md) or just "lightweight minimalism") -- that's pretty awesome -- however its presentation is shit and while there are many [free as in freedom](free_software.md) implementations of uxn, official supplemental material to uxn (on the [xxiivv](xxiivv.md) wiki), such as its specification, is **[proprietary](proprietary.md)** ([NC](nc.md)) and therefore should be avoided and boycotted.

Uxn is similar to other projects such as [IBNIZ](ibniz.md), and can be compared to some of [our](lrs.md) projects as well, for example [SAF](saf.md), but mainly [comun](comun.md) -- the goals of uxn and comun may be seen as significantly overlapping, aiming to create a minimalist, completely independent from-ground-up computing "stack", an extremely portable platform for minimalist programs, and they do so in a similar way (both are e.g. stack based, inspired by [Forth](forth.md)). **To quickly compare uxn and comun**: comun is more of a pure programming [language](language.md) focusing only on expressing algorithms without talking about [I/O](io.md) or instruction sets, uxn on the other hand really is a [computer](computer.md) (even if initially only virtual), one that comes with its own instruction set, language and protocols for communication with peripheral devices, though the computer is purposefully made so that it can be implemented as a virtual machine running on other computers. Comun is a low level language but higher level than assembly (having e.g. control structures and a concept of "native integer" type), usually compiling to [bytecode](bytecode.md), while uxn is programmed directly in assembly and tied to its virtual machine's architecture and specifications. Comun is trying to stay more abstract, hardware independent and be more close to [math](math.md) notation, it doesn't assume any native integer size or working memory size, it doesn't use any English keywords, it assumes as little as possible about its platform -- it is trying to be a "better, simpler [C](c.md)". Uxn is more of a "new NES", a "practically useful [fantasy console](fantasy_console.md)", an idealization and improvement of old computers, it has a hardcoded amount of memory, specified integer size (8 or 16 bit), uses assembly with English mnemonics just like the old computers etc. As for complexity, uxn is probably a bit simpler, or rather allowing smaller implementations than those of full comun, though simplified versions of comun (such as minicomun) may possibly be as simple or simpler than uxn and specification of full comun (a possible measure of complexity) is extremely small and will probably compare to or beat uxn. Uxn only has 32 instructions and its self hosted implementation is around 2000 bytes big, while current comun's bytecode has around 80 instructions and self hosted compiler will probably have a few thousand lines of code (as it really is a library, compiler, interpreter and simple optimizer, as opposed to mere assembler). Comun is a completely [selfless](selfless.md), absolutely [public domain](public_domain.md) [free software](free_software.md), while uxn has a selfish proprietary ([NC](nc.md)) specification.

## Details

{ **Start rant**: the description of everything by 100rabbits is **so fucking hard and painful as fuck to understand** -- not because the described technology itself would be complicated but the writing is just really REALLY bad, it's hard to say exactly what it is but he just seems to be trying to write poetry in technical specifications, that's just extremely fucked up. There are tables where meaning of rows and columns is left to be guessed, hyperlinks of important terms lead to shitass long articles about something completely else, he invents 10 different fancy sounding words he uses interchangeably without ever explaining their meaning in a non-cryptic way or which simply have circular definitions. It's also fucking infuriating there are some **fucking shitty childish drawings randomly inserted into opcode specification** along with some random hand gesture signals for the opcodes and what the fuck. It's like he's abusing the specification to force feed you his ugly drawings which he probably thinks look good while also trying to teach you what sounds animals make as if it's fucking kindergarden. Fuck this shit, I now have to read through it and make sense of it so you don't have to. I'll prolly rather read some 3rd party tutorial lol. ~drummyfish }

Here is a kind of concise sum up of uxn "ecosystem"/terminology/specs/etc.:

- **uxn** (also *uxn CPU*): The [virtual machine](virtual_machine.md), "computing backend", something that runs uxn binary *rom*s.
  - so called *one page computer* (can implement itself in at most 500 lines of own assembly)
  - has **2 circular [stacks](stack.md)**: working stack and return stack, each one has size of 256 bytes.
  - **memory**: total size 64 kB, memory map is following:
    - 16 varvara devices, each one taking 16 bytes: the whole device section is called a **page**, the bytes inside it are called **ports**. A port that holds a [callback](callback.md) address is called **vector** (e.g. a mouse device will have a vector for *mouse click* callback).
    - address 256: start of program instructions.
    - extensible with so called *banks*.
  - **instructions**: Each one takes 8 bits. Every value is a valid instruction. Instruction format is following:
    - lowest 5 bits: instruction [opcode](opcode.md) (i.e. there are 32 instructions in total)
    - following bit (*2*): if set, instruction works with 16 bit shorts (otherwise it works with 8 bit bytes).
    - following bit (*k*): if set, instruction will not pop any operands.
    - following bit (*r*): if set, instruction operates on the return stack (otherwise it operates on working stack).
- **uxntal** (also just *tal*): [Assembly](assembly.md) language, i.e. human readable language that compiles to a binary *rom*, with more or less one-to-one mapping between machine code instructions and the language mnemonics. Tal is what uxn program are written in; the tal files have `.tal` extension. A quick sum up of the language follows:
  - Comments are in parentheses, as `( comment )`.
  - Mnemonics such as `ADD`, `EQU`, `POP` etc. are used.
  - [Postfix notation](postfix_notation.md).
  - "Runes" are used as directives and syntax sugar, for example `|` says where in memory to store the instruction, `@something` creates a jump label etc.
  - [Macros](macro.md) (inlined instructions) are supported.
- **[rom](rom.md)**: Compiled binary executable that can be directly run by an *uxn CPU*. The rom files have `.rom` extension.
- **varvara**: A device, "frontend", peripheral that's connected to *uxn CPU* to perform [input/output](io.md), for example a screen, keyboard or terminal.

TODO: subroutines, ...

## See Also

- [IBNIZ](ibniz.md)
- [xxiivv](xxiivv.md)
- [comun](comun.md)
- [SAF](saf.md)
- [MIX](mix.md)
- [fantasy console](fantasy_console.md)
