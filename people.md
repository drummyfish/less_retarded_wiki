# People

*"People are retarded."* --[Osho](osho.md)

All people are idiots, [love](love.md) all of them, never make anyone a [leader](hero_culture.md). So far people seem to be mentally the most inferior of all species on [Earth](earth.md), the only thing stupider than people are [feminists](feminism.md) and [capitalists](capitalism.md) (which is basically the same thing). Humans possess high level of intelligence which allows them to be extremely stupid, much more than other organisms can dream of -- just like playing the worst [chess](chess.md) moves requires deep understanding of chess, behaving in most stupid ways possible requires an intellect capable of deducing what is indeed the most stupid thing to do at any given time -- at this humans excel.

Remember: a cover mostly says just enough about the book.

Here is a list of people notable in technology or in other ways related to [LRS](lrs.md).

- **[Aaron Swartz](aaron_swartz.md)**: famous computer prodigy activist involved in creation of famous things like [Reddit](reddit.md), [RSS](rss.md) and [Creative Commons](creative_commons.md), [suicided](suicide.md) at 26
- **[Alan Cox](alan_cox.md)**: famous [Linux](linux.md) contributor, used to be considered second in command after Torvalds
- **[Alan Kay](alan_kay.md)**: oldfag hacker, inventor of [Smalltalk](smalltalk.md)
- **[Albert Einstein](einstein.md)**: 20th century physicist, author of [theory of relativity](relativity.md), [pacifist](pacifism.md) and [socialist](socialism.md), regarded as one of the most brilliant geniuses in history.
- **[Alan Turing](turing.md)**: 20th century mathematician, father of [computer science](compsci.md), [gay](gay.md)
- **[Alexandre Oliva](alexandre_oliva.md)**: [free software](free_software.md) advocate, founding member of [FSFLA](fsfla.md), maintainer of [Linux-libre](linux_libre.md)
- **[Ashley Jones](ashley_jones.md)**: Internet [girl](woman.md), one of very few specimens of a based woman
- **[Bill Gates](bill_gates.md)**: founder and CEO of [Micro$oft](microsoft.md), huge faggot
- **[Bobby Tables](bobby_tables.md)**: full name `Robert'); DROP TABLE Students;--`, appeared in [xkcd](xkcd.md) 327, see also [SQL](sql.md) [injection](injection.md)
- **[Buddha](buddha.md)** (Siddhartha Gautama): started [buddhism](buddhism.md), a religion seeking enlightenment attained by searching for the ultimate truth and so freeing oneself from all desire
- **[Charles Moore](charles_moore.md)**: inventor of [Forth](forth.md), advocate of software [minimalism](minimalism.md), oldschool hacker
- **David Mondou-Labbe** ("""Devine Lu Linvega"""): some weird narcissist soyboy making minimalist stuff, [100r](100r.md) member, cryptocapitalist, [pseudoleftist](pseudoleft.md) fascist, heavily utilizing [NC](nc.md) licenses
- **[Dennis Ritchie](dennis_ritchie)**: creator of [C](c.md) language and co-creator of [Unix](unix.md)
- **[Diogenes](diogenes.md)**: based Greek philosopher who opposed all authorities in very cool ways
- **Dirt Wizard** (Lafe Spietz, Trash Robot): author of [Trash Magic](trash_magic.md)
- **[Donald Knuth](knuth.md)**: computer scientist, Turing-award winner, author of the famous [Art of Computer Programming](taocp.md) books and the [TeX](tex.md) typesetting system
- **[drummyfish](drummyfish.md)** (Miloslav Číž): creator of [LRS](lrs.md), a few programs and this wiki, [anarcho-pacifist](anpac.md)
- **[Eric S. Raymond](esr.md)**: oldschool hacker turned capitalist, proponent of [open $ource](open_source.md), desperately trying to be popular, co-founder of [OSI](osi.md) and tech writer ([Jargon File](jargon_file.md), [CatB](catb.md), ...)
- **[Fabrice Bellard](fabrice_bellard.md)**: legendary programmer, made many famous programs such as [ffmpeg](ffmpeg.md), [tcc](tcc.md), [TinyGL](tinygl.md) etc.
- **[Geoffrey Knauth](geoffrey_knauth.md)**: very [shitty](shit.md) president of [Free Software Foundation](fsf.md) since 2020 who embraces [proprietary](proprietary.md) software lol
- **[Grigori Perelman](perelman.md)** based Russian mathematician who solved one of the biggest problems in math ([Poincare conjecture](poincare_conjecture.md)), then refused Fields medal and million dollar prize, refuses to talk to anyone and make [hero](hero_culture.md) of himself, just sent a huge fuck you to the system
- **[Jara Cimrman](jara_cimrman.md)**: a fictional [Czech](czechia.md) universal genius that basically secretly invented everything but was forgotten by [history](history.md), it's the whole country's inside joke
- **[Jason Scott](jason_scott.md)**: quite famous archivist and filmmaker (maintains e.g. textfiles.com), focused on old hacker/boomer tech like [BBSes](bbs.md)
- **[Jesus](jesus.md)**: probably the most famous guy in history, had a nice teaching of [nonviolence](nonviolence.md) and [love](love.md)
- **[Jimmy Wales](jimmy_wales.md)**: co-founder of [Wikipedia](wikipedia.md)
- **[John Carmack](john_carmack.md)**: legendary game ([Doom](doom.md), [Quake](quake.md), ...) and [graphics](graphics.md) developer, often called a programming god
- **[John Gilmore](john_gilmore.md)**: oldschool [hacker](hacking.md), founder of [Electronic Frontier Foundation](eff.md)
- **[John Romero](romero.md)**: legendary oldschool game dev, co-creator of [Doom](doom.md)
- **[John von Neumann](von_neumann.md)**: early 20th century multidisciplinary genius, one of the greatest [computer scientists](compsci.md) of all time, also famous for huge [IQ](iq.md) and being a human calculator
- **[Jonathan Blow](jonathan_blow.md)**: Mainstream proprietary indie game developer of puzzle games, kind of a celebrity of indie game dev, mostly retarded but sometimes says something based out of context, some people love him, some hate him.
- **[Karl Marx](karl_marx.md)**: jewish intellectual, philosopher, started [Marxism](marxism.md)
- **[Ken Silverman](key_silverman.md)**: famous oldschool 3D engine programmer ([Duke Nukem 3D](duke_3d.md)'s [BUILD engine](build_engine.md), ...), sadly proprietaryfag
- **[Ken Thompson](ken_thompson.md)**: co-creator of [Unix](unix.md), [C](c.md) and [Go](go.md)
- **[Kurt Godel](kurt_godel.md)**: mathematician famous for his groundbreaking incompleteness theorems proving that [logic](logic.md) itself has intrinsic limitations, was a tinfoil schizo and died of starvation believing his food to be poisoned
- **[Larry Sanger](larry_sanger.md)**: co-founder of [Wikipedia](wikipedia.md), also one of its biggest critics
- **[Larry Wall](larry_wall.md)**: creator of [Perl](perl.md) language, linguist
- **[Lawrence Lessig](lessig.md)**: lawyer, founder of [free culture](free_culture.md) movement and [Creative Commons](creative_commons.md), critic of [copyright](copyright.md)
- **[Lev Nikolayevich Tolstoy](leo_tolstoy.md)**: Russian writer -- regarded among the best of all times -- said to have been an [anarcho pacifist](anpac.md)
- **[Linus Torvalds](linus_torvalds.md)**: Finnish programmer who created [Linux](linux.md) and [git](git.md)
- **[Luke Smith](luke_smith.md)**: [suckless](suckless.md) vlogger/celebrity
- **[Mahatma Gandhi](gandhi.md)**: Indian man who greatly utilized and popularized [nonviolence](nonviolence.md)
- **[Melvin Kaye](mel.md) aka Mel**: genius old time programmer that appears in [hacker lore](hacker_culture.md) (*Story of Mel*)
- **[Mental Outlaw](mental_outlaw.md)**: [suckless](suckless.md) vlogger/celebrity
- **[Mother Teresa](mother_teresa.md)**
- **[Nina Paley](nina_paley.md)**: [female](woman.md) artist, one of the most famous proponents of [free culture](free_culture.md)
- **[Noam Chomsky](noam_chomsky.md)**: linguist notable in theoretical [compsci](computer_science.md), anarchist
- **[Oscar Toledo G.](toledo.md)**: programmer of tiny programs and [games](game.md) (e.g. the smallest [chess](chess.md) program), sadly [proprietary](proprietary.md) [winfag](windows.md)
- **[Petr Chelcicky](petr_chelcicky.md)**: old time [anarcho pacifist](anpac.md)
- **[Richard Stallman](rmd.md)**: inventor of [free software](free_software.md) and [copyleft](copyleft.md), founder of [GNU](gnu.md) and [FSF](fsf.md), hacker, also created [emacs](emacs.md)
- **[Rob Pike](rob_pike.md)**: oldschool hacker strayed from the path of good, involved in [Unix](unix.md), [Plan 9](plan9.md) and [go](golang.md)
- **[Roy Schestowitz](roy_schestowitz)**: [PhD](phd.md) journalist, running [Techrights](techrights.md), revealing corruption in technology
- **[Stephen Gough](gough.md)** (*naked rambler*): based guy who refuses to wear clothes, is bullied by society and kept in prison
- **[Steve Jobs](steve_jobs.md)**: founder and CEO of [Apple](apple.md), huge retard and dickhead
- **[Ted Kaczynski](ted_kaczynski.md)**: AKA the Unabomber, mathematician, prodigy, primitivist and murderer who pointed out the dangers of modern technology
- **[Terry Davis](terry_davis.md)**: deceased schizophrenic genius, creator of [Temple OS](temple_os.md), became a tech [meme](meme.md)
- **[Tom Murphy VII (Tom7)](tom7.md)**: researcher, famous [SIGBOVIK](sigbovik.md) contributor and YouTuber
- **[Uriel M. Pereira](uriel.md)**: deceased member of the [suckless](suckless.md)/[cat-v](cat_v.md) community, "philosopher"
- **[Virgil Dupras](dupras.md)**: creator of [Collapse OS](collapseos.md) and [Dusk OS](duskos.md)
- **[viznut](viznut.md)** (Ville-Matias Heikkilä): creator or [countercomplex](countercomplex.md), minimalist programmer, inventor of [bytebeat](bytebeat.md), hacker, [collapse](collapse.md) "[prepper](prepping.md)"
- ...
